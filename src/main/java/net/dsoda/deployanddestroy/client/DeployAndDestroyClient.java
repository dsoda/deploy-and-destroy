package net.dsoda.deployanddestroy.client;
import com.terraformersmc.terraform.boat.api.client.TerraformBoatClientHelper;
import net.dsoda.deployanddestroy.blocks.DDBlockEntities;
import net.dsoda.deployanddestroy.blocks.DDBlocks;
import net.dsoda.deployanddestroy.entity.DDBoats;
import net.dsoda.deployanddestroy.items.DDItems;
import net.dsoda.deployanddestroy.items.item.DyeBucketItem;
import net.dsoda.deployanddestroy.particle.DDParticles;
import net.dsoda.deployanddestroy.client.render.DDCampfireBlockEntityRenderer;
import net.dsoda.deployanddestroy.client.screens.DDScreenHandlers;
import net.dsoda.deployanddestroy.client.screens.screen.DD9x7Screen;
import net.dsoda.deployanddestroy.client.screens.screen.DD9x8Screen;
import net.dsoda.deployanddestroy.client.screens.screen.WoodcutterScreen;
import net.fabricmc.api.ClientModInitializer;
import net.fabricmc.fabric.api.blockrenderlayer.v1.BlockRenderLayerMap;
import net.fabricmc.fabric.api.client.particle.v1.ParticleFactoryRegistry;
import net.fabricmc.fabric.api.client.rendering.v1.ColorProviderRegistry;
import net.minecraft.client.color.world.BiomeColors;
import net.minecraft.client.color.world.FoliageColors;
import net.minecraft.client.gui.screen.ingame.HandledScreens;
import net.minecraft.client.item.ModelPredicateProviderRegistry;
import net.minecraft.client.particle.FlameParticle;
import net.minecraft.client.render.RenderLayer;
import net.minecraft.client.render.TexturedRenderLayers;
import net.minecraft.client.render.block.entity.BlockEntityRendererFactories;
import net.minecraft.client.render.block.entity.HangingSignBlockEntityRenderer;
import net.minecraft.client.render.block.entity.SignBlockEntityRenderer;
import net.minecraft.util.Identifier;

public class DeployAndDestroyClient implements ClientModInitializer{


    @Override
    public void onInitializeClient() {

        BlockEntityRendererFactories.register(DDBlockEntities.DD_CAMPFIRE, DDCampfireBlockEntityRenderer::new);

        ParticleFactoryRegistry.getInstance().register(DDParticles.COPPER_FLAME, FlameParticle.Factory::new);
        ParticleFactoryRegistry.getInstance().register(DDParticles.AMETHYST_FLAME, FlameParticle.Factory::new);

        BlockEntityRendererFactories.register(DDBlockEntities.DD_SIGN_BLOCK_ENTITY, SignBlockEntityRenderer::new);
        BlockEntityRendererFactories.register(DDBlockEntities.DD_HANGING_SIGN_BLOCK_ENTITY, HangingSignBlockEntityRenderer::new);

        TexturedRenderLayers.SIGN_TYPE_TEXTURES.put(DDBlocks.WoodTypes.AZALEA, TexturedRenderLayers.getSignTextureId(DDBlocks.WoodTypes.AZALEA));
        TexturedRenderLayers.SIGN_TYPE_TEXTURES.put(DDBlocks.WoodTypes.SWAMP, TexturedRenderLayers.getSignTextureId(DDBlocks.WoodTypes.SWAMP));

        TerraformBoatClientHelper.registerModelLayers(DDBoats.AZALEA_BOAT_ID, false);
        TerraformBoatClientHelper.registerModelLayers(DDBoats.SWAMP_BOAT_ID, false);

        HandledScreens.register(DDScreenHandlers.DD_9X7, DD9x7Screen::new);
        HandledScreens.register(DDScreenHandlers.DD_9X8, DD9x8Screen::new);
        HandledScreens.register(DDScreenHandlers.WOODCUTTER, WoodcutterScreen::new);

        BlockRenderLayerMap.INSTANCE.putBlock(DDBlocks.BONE_TORCH, RenderLayer.getCutout());
        BlockRenderLayerMap.INSTANCE.putBlock(DDBlocks.BONE_WALL_TORCH, RenderLayer.getCutout());
        BlockRenderLayerMap.INSTANCE.putBlock(DDBlocks.SOUL_BONE_TORCH, RenderLayer.getCutout());
        BlockRenderLayerMap.INSTANCE.putBlock(DDBlocks.SOUL_BONE_WALL_TORCH, RenderLayer.getCutout());
        BlockRenderLayerMap.INSTANCE.putBlock(DDBlocks.REDSTONE_BONE_TORCH, RenderLayer.getCutout());
        BlockRenderLayerMap.INSTANCE.putBlock(DDBlocks.REDSTONE_BONE_WALL_TORCH, RenderLayer.getCutout());
        BlockRenderLayerMap.INSTANCE.putBlock(DDBlocks.COPPER_TORCH, RenderLayer.getCutout());
        BlockRenderLayerMap.INSTANCE.putBlock(DDBlocks.COPPER_WALL_TORCH, RenderLayer.getCutout());
        BlockRenderLayerMap.INSTANCE.putBlock(DDBlocks.COPPER_BONE_TORCH, RenderLayer.getCutout());
        BlockRenderLayerMap.INSTANCE.putBlock(DDBlocks.COPPER_BONE_WALL_TORCH, RenderLayer.getCutout());
        BlockRenderLayerMap.INSTANCE.putBlock(DDBlocks.AMETHYST_TORCH, RenderLayer.getCutout());
        BlockRenderLayerMap.INSTANCE.putBlock(DDBlocks.AMETHYST_WALL_TORCH, RenderLayer.getCutout());
        BlockRenderLayerMap.INSTANCE.putBlock(DDBlocks.AMETHYST_BONE_TORCH, RenderLayer.getCutout());
        BlockRenderLayerMap.INSTANCE.putBlock(DDBlocks.AMETHYST_BONE_WALL_TORCH, RenderLayer.getCutout());
        BlockRenderLayerMap.INSTANCE.putBlock(DDBlocks.COPPER_LANTERN, RenderLayer.getCutout());
        BlockRenderLayerMap.INSTANCE.putBlock(DDBlocks.AMETHYST_LANTERN, RenderLayer.getCutout());
        BlockRenderLayerMap.INSTANCE.putBlock(DDBlocks.COPPER_FIRE, RenderLayer.getCutout());
        BlockRenderLayerMap.INSTANCE.putBlock(DDBlocks.COPPER_CAMPFIRE, RenderLayer.getCutout());
        BlockRenderLayerMap.INSTANCE.putBlock(DDBlocks.AMETHYST_FIRE, RenderLayer.getCutout());
        BlockRenderLayerMap.INSTANCE.putBlock(DDBlocks.AMETHYST_CAMPFIRE, RenderLayer.getCutout());
        BlockRenderLayerMap.INSTANCE.putBlock(DDBlocks.SWAMP_SAPLING, RenderLayer.getCutout());
        BlockRenderLayerMap.INSTANCE.putBlock(DDBlocks.POTTED_SWAMP_SAPLING, RenderLayer.getCutout());
        BlockRenderLayerMap.INSTANCE.putBlock(DDBlocks.SWAMP_DOOR, RenderLayer.getCutout());
        BlockRenderLayerMap.INSTANCE.putBlock(DDBlocks.SWAMP_TRAPDOOR, RenderLayer.getCutout());
        BlockRenderLayerMap.INSTANCE.putBlock(DDBlocks.WOODCUTTER, RenderLayer.getCutout());
        BlockRenderLayerMap.INSTANCE.putBlock(DDBlocks.SWAMP_LEAVES, RenderLayer.getCutoutMipped());

        ColorProviderRegistry.BLOCK.register((state, world, pos, tintIndex) -> {
            if (world == null || pos == null) return FoliageColors.getDefaultColor();
            return BiomeColors.getFoliageColor(world, pos);
        }, DDBlocks.SWAMP_LEAVES);
        ColorProviderRegistry.ITEM.register((stack, tintIndex) -> 0x14BA1F, DDBlocks.SWAMP_LEAVES);

        ModelPredicateProviderRegistry.register(DDItems.DYE_BUCKET, new Identifier("color"),
            (itemStack, clientWorld, livingEntity, i) -> itemStack.getNbt().getFloat(DyeBucketItem.COLOR_KEY) / 100);
    }
}
