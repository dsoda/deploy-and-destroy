package net.dsoda.deployanddestroy.client.screens.handler;

import net.dsoda.deployanddestroy.client.screens.DDScreenHandlers;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.inventory.Inventory;
import net.minecraft.inventory.SimpleInventory;
import net.minecraft.screen.GenericContainerScreenHandler;
import net.minecraft.screen.ScreenHandlerType;

public class DDGenericContainerScreenHandler extends GenericContainerScreenHandler {

    private DDGenericContainerScreenHandler(ScreenHandlerType<?> type, int syncId, PlayerInventory playerInventory, int rows) {
        this(type, syncId, playerInventory, new SimpleInventory(9 * rows), rows);
    }

    public DDGenericContainerScreenHandler(ScreenHandlerType<?> type, int syncId, PlayerInventory playerInventory, Inventory inventory, int rows) {
        super(type, syncId, playerInventory, inventory, rows);
    }

    // Thank you Mojang for mercifully providing 3 of my 5 screens

    public static GenericContainerScreenHandler createNew9x4(int syncId, PlayerInventory playerInventory, Inventory inventory) {
        return new GenericContainerScreenHandler(ScreenHandlerType.GENERIC_9X4, syncId, playerInventory, inventory, 4);
    }

    public static GenericContainerScreenHandler createNew9x5(int syncId, PlayerInventory playerInventory, Inventory inventory) {
        return new GenericContainerScreenHandler(ScreenHandlerType.GENERIC_9X5, syncId, playerInventory, inventory, 5);
    }

    public static GenericContainerScreenHandler createNew9x6(int syncId, PlayerInventory playerInventory, Inventory inventory) {
        return new GenericContainerScreenHandler(ScreenHandlerType.GENERIC_9X6, syncId, playerInventory, inventory, 6);
    }

    // Fuck you Mojang for only providing 3 of my 5 screens

    public static GenericContainerScreenHandler createNew9x7(int syncId, PlayerInventory playerInventory, Inventory inventory) {
        return new GenericContainerScreenHandler(DDScreenHandlers.DD_9X7, syncId, playerInventory, inventory, 7);
    }

    public static DDGenericContainerScreenHandler createGeneric9x7(int syncId, PlayerInventory playerInventory) {
        return new DDGenericContainerScreenHandler(DDScreenHandlers.DD_9X7, syncId, playerInventory, 7);
    }

    public static GenericContainerScreenHandler createNew9x8(int syncId, PlayerInventory playerInventory, Inventory inventory) {
        return new GenericContainerScreenHandler(DDScreenHandlers.DD_9X8, syncId, playerInventory, inventory, 8);
    }

    public static DDGenericContainerScreenHandler createGeneric9x8(int syncId, PlayerInventory playerInventory) {
        return new DDGenericContainerScreenHandler(DDScreenHandlers.DD_9X8, syncId, playerInventory, 8);
    }
}
