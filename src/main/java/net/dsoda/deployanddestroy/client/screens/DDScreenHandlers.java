package net.dsoda.deployanddestroy.client.screens;

import net.dsoda.deployanddestroy.DeployAndDestroy;
import net.dsoda.deployanddestroy.client.screens.handler.DDGenericContainerScreenHandler;
import net.dsoda.deployanddestroy.client.screens.handler.WoodcutterScreenHandler;
import net.minecraft.registry.Registries;
import net.minecraft.registry.Registry;
import net.minecraft.resource.featuretoggle.FeatureFlags;
import net.minecraft.screen.ScreenHandler;
import net.minecraft.screen.ScreenHandlerType;
import net.minecraft.util.Identifier;
import static net.dsoda.deployanddestroy.DeployAndDestroy.MOD_ID;

public class DDScreenHandlers {

    public static final ScreenHandlerType<DDGenericContainerScreenHandler> DD_9X7 = register("dd_generic_9x7", DDGenericContainerScreenHandler::createGeneric9x7);
    public static final ScreenHandlerType<DDGenericContainerScreenHandler> DD_9X8 = register("dd_generic_9x8", DDGenericContainerScreenHandler::createGeneric9x8);
    public static final ScreenHandlerType<WoodcutterScreenHandler> WOODCUTTER = register("woodcutter_screen", WoodcutterScreenHandler::new);

    public static void registerScreenHandlers() {
        DeployAndDestroy.LOGGER.info("Registering Screen Handlers for " + MOD_ID);
    }

    private static <T extends ScreenHandler> ScreenHandlerType<T> register(String id, ScreenHandlerType.Factory<T> factory) {
       return Registry.register(Registries.SCREEN_HANDLER, new Identifier(MOD_ID, id), new ScreenHandlerType<>(factory, FeatureFlags.VANILLA_FEATURES));
    }
}
