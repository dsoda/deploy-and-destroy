package net.dsoda.deployanddestroy.events;

import net.dsoda.deployanddestroy.items.item.DyeBucketItem;
import net.dsoda.deployanddestroy.items.item.behavior.*;
import net.fabricmc.fabric.api.event.player.UseBlockCallback;
import net.minecraft.block.*;
import net.minecraft.item.DyeItem;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Items;
import net.minecraft.util.ActionResult;

public class DDUseBlock {

    public static void registerUseBlockEvents() {
        UseBlockCallback.EVENT.register((player, world, hand, hitResult) -> {
            Block block = world.getBlockState(hitResult.getBlockPos()).getBlock();
            if (player.isSpectator()) return ActionResult.PASS;
            ItemStack stack = player.getStackInHand(hand);
            if (stack.isOf(Items.SLIME_BALL))
                return SlimeBallBehavior.useOnBlock(player, world, stack, hitResult);
            if (stack.isOf(Items.IRON_INGOT))
                return IronIngotBehavior.useOnBlock(player, world, stack, hitResult);
            if (stack.getItem() instanceof DyeItem)
                return DyeBehavior.useOnBlock(player, world, stack, hitResult);
            if (stack.getItem() instanceof DyeBucketItem && !player.isSneaking() && (block instanceof BedBlock || block instanceof ShulkerBoxBlock))
                return DyeBucketBehavior.useOnBlock(player, world, stack, hand, hitResult);
            return ActionResult.PASS;
        });
    }
}
