package net.dsoda.deployanddestroy;

import net.dsoda.deployanddestroy.datagen.*;
import net.dsoda.deployanddestroy.features.DDConfiguredFeatures;
import net.dsoda.deployanddestroy.features.DDPlacedFeatures;
import net.fabricmc.fabric.api.datagen.v1.DataGeneratorEntrypoint;
import net.fabricmc.fabric.api.datagen.v1.FabricDataGenerator;
import net.minecraft.registry.RegistryBuilder;
import net.minecraft.registry.RegistryKeys;

public class DeployAndDestroyDataGenerator implements DataGeneratorEntrypoint {

	@Override
	public void onInitializeDataGenerator(FabricDataGenerator fabricDataGenerator) {
		FabricDataGenerator.Pack pack = fabricDataGenerator.createPack();
		pack.addProvider(DDBlockTagProvider::new);
		pack.addProvider(DDItemTagProvider::new);
		pack.addProvider(DDLootTableProvider::new);
		pack.addProvider(DDModelProvider::new);
		pack.addProvider(DDEnglishLanguageProvider::new);
		pack.addProvider(DDRecipeProvider::new);
		pack.addProvider(DDWorldgenProvider::new);
		pack.addProvider(DDPoiTagProvider::new);
	}

	@Override
	public void buildRegistry(RegistryBuilder registryBuilder) {
		registryBuilder.addRegistry(RegistryKeys.CONFIGURED_FEATURE, DDConfiguredFeatures::bootstrap);
		registryBuilder.addRegistry(RegistryKeys.PLACED_FEATURE, DDPlacedFeatures::boostrap);
	}
}
