package net.dsoda.deployanddestroy.entity;

import com.terraformersmc.terraform.boat.api.TerraformBoatType;
import com.terraformersmc.terraform.boat.api.TerraformBoatTypeRegistry;
import net.dsoda.deployanddestroy.blocks.DDBlocks;
import net.dsoda.deployanddestroy.items.DDItems;
import net.minecraft.registry.Registry;
import net.minecraft.registry.RegistryKey;
import net.minecraft.util.Identifier;
import static net.dsoda.deployanddestroy.DeployAndDestroy.MOD_ID;

public class DDBoats {
    public static final Identifier AZALEA_BOAT_ID = new Identifier(MOD_ID, "azalea_boat");
    public static final Identifier AZALEA_CHEST_BOAT_ID = new Identifier(MOD_ID, "azalea_chest_boat");
    public static final Identifier SWAMP_BOAT_ID = new Identifier(MOD_ID, "swamp_boat");
    public static final Identifier SWAMP_CHEST_BOAT_ID = new Identifier(MOD_ID, "swamp_chest_boat");

    public static final RegistryKey<TerraformBoatType> AZALEA_BOAT_KEY = TerraformBoatTypeRegistry.createKey(AZALEA_BOAT_ID);
    public static final RegistryKey<TerraformBoatType> SWAMP_BOAT_KEY = TerraformBoatTypeRegistry.createKey(SWAMP_BOAT_ID);

    public static void registerBoats() {
        Registry.register(
            TerraformBoatTypeRegistry.INSTANCE,
            AZALEA_BOAT_KEY,
            new TerraformBoatType.Builder()
                .item(DDItems.AZALEA_BOAT)
                .chestItem(DDItems.AZALEA_CHEST_BOAT)
                .planks(DDBlocks.AZALEA_PLANKS.asItem())
                .build()
        );

        Registry.register(
            TerraformBoatTypeRegistry.INSTANCE,
            SWAMP_BOAT_KEY,
            new TerraformBoatType.Builder()
                .item(DDItems.SWAMP_BOAT)
                .chestItem(DDItems.SWAMP_CHEST_BOAT)
                .planks(DDBlocks.SWAMP_PLANKS.asItem())
                .build()
        );
    }
}
