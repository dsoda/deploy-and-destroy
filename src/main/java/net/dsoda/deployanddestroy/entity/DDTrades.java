package net.dsoda.deployanddestroy.entity;

import com.google.common.collect.ImmutableList;
import net.dsoda.deployanddestroy.blocks.DDBlocks;
import net.dsoda.deployanddestroy.entity.villager.DDVillagers;
import net.dsoda.deployanddestroy.items.DDItems;
import net.fabricmc.fabric.api.object.builder.v1.trade.TradeOfferHelper;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Items;
import net.minecraft.village.TradeOffer;

import java.util.List;

public class DDTrades {

    private static final List<Item> LOGS = ImmutableList.of(
        Items.OAK_LOG,
        Items.SPRUCE_LOG,
        Items.BIRCH_LOG,
        Items.JUNGLE_LOG,
        Items.ACACIA_LOG,
        Items.DARK_OAK_LOG,
        Items.MANGROVE_LOG,
        Items.CHERRY_LOG,
        DDBlocks.AZALEA_LOG.asItem(),
        DDBlocks.SWAMP_LOG.asItem(),
        Items.WARPED_STEM,
        Items.CRIMSON_STEM);

    private static final List<Item> STRIPPED_LOGS = ImmutableList.of(
        Items.STRIPPED_OAK_LOG,
        Items.STRIPPED_SPRUCE_LOG,
        Items.STRIPPED_BIRCH_LOG,
        Items.STRIPPED_JUNGLE_LOG,
        Items.STRIPPED_ACACIA_LOG,
        Items.STRIPPED_DARK_OAK_LOG,
        Items.STRIPPED_MANGROVE_LOG,
        Items.STRIPPED_CHERRY_LOG,
        DDBlocks.STRIPPED_AZALEA_LOG.asItem(),
        DDBlocks.STRIPPED_SWAMP_LOG.asItem(),
        Items.STRIPPED_WARPED_STEM,
        Items.STRIPPED_CRIMSON_STEM);

    private static final List<Item> SAPLINGS = ImmutableList.of(
        Items.OAK_SAPLING,
        Items.SPRUCE_SAPLING,
        Items.BIRCH_SAPLING,
        Items.JUNGLE_SAPLING,
        Items.ACACIA_SAPLING,
        Items.DARK_OAK_SAPLING,
        Items.MANGROVE_PROPAGULE,
        Items.CHERRY_SAPLING,
        DDBlocks.SWAMP_SAPLING.asItem(),
        Items.WARPED_FUNGUS,
        Items.CRIMSON_FUNGUS);

    private static final List<Item> BOATS = ImmutableList.of(
        Items.OAK_BOAT,
        Items.SPRUCE_BOAT,
        Items.BIRCH_BOAT,
        Items.JUNGLE_BOAT,
        Items.ACACIA_BOAT,
        Items.DARK_OAK_BOAT,
        Items.MANGROVE_BOAT,
        Items.BAMBOO_RAFT,
        Items.CHERRY_BOAT,
        DDItems.AZALEA_BOAT.asItem(),
        DDItems.SWAMP_BOAT.asItem());

    private static final List<Item> FENCES = ImmutableList.of(
        Items.OAK_FENCE,
        Items.SPRUCE_FENCE,
        Items.BIRCH_FENCE,
        Items.JUNGLE_FENCE,
        Items.ACACIA_FENCE,
        Items.DARK_OAK_FENCE,
        Items.MANGROVE_FENCE,
        Items.BAMBOO_RAFT,
        Items.CHERRY_FENCE,
        DDBlocks.AZALEA_FENCE.asItem(),
        DDBlocks.SWAMP_FENCE.asItem(),
        Items.WARPED_FENCE,
        Items.CRIMSON_FENCE,
        Items.BAMBOO_FENCE);

    private static final List<Item> FENCE_GATES = ImmutableList.of(
        Items.OAK_FENCE_GATE,
        Items.SPRUCE_FENCE_GATE,
        Items.BIRCH_FENCE_GATE,
        Items.JUNGLE_FENCE_GATE,
        Items.ACACIA_FENCE_GATE,
        Items.DARK_OAK_FENCE_GATE,
        Items.MANGROVE_FENCE_GATE,
        Items.BAMBOO_RAFT,
        Items.CHERRY_FENCE_GATE,
        DDBlocks.AZALEA_FENCE_GATE.asItem(),
        DDBlocks.SWAMP_FENCE_GATE.asItem(),
        Items.WARPED_FENCE_GATE,
        Items.CRIMSON_FENCE_GATE,
        Items.BAMBOO_FENCE_GATE);

    private static final List<Item> DOORS = ImmutableList.of(
        Items.OAK_DOOR,
        Items.SPRUCE_DOOR,
        Items.BIRCH_DOOR,
        Items.JUNGLE_DOOR,
        Items.ACACIA_DOOR,
        Items.DARK_OAK_DOOR,
        Items.MANGROVE_DOOR,
        Items.BAMBOO_RAFT,
        Items.CHERRY_DOOR,
        DDBlocks.AZALEA_DOOR.asItem(),
        DDBlocks.SWAMP_DOOR.asItem(),
        Items.WARPED_DOOR,
        Items.CRIMSON_DOOR,
        Items.BAMBOO_DOOR);

    public static void registerTrades() {
        TradeOfferHelper.registerWanderingTraderOffers(1,
            factories ->
                factories.add((entity, random) -> new TradeOffer(
                    new ItemStack(Items.EMERALD, 5),
                    new ItemStack(DDBlocks.SWAMP_SAPLING),
                    8, 5, 0.05f))
        );
        TradeOfferHelper.registerWanderingTraderOffers(2,
            factories -> {
                factories.add((entity, random) -> new TradeOffer(
                    new ItemStack(Items.EMERALD, 3),
                    new ItemStack(Items.SPONGE),
                    3, 5, 0.05f));
                factories.add((entity, random) -> new TradeOffer(
                    new ItemStack(Items.EMERALD, 5),
                    DDItems.WRENCH.getDefaultStack(),
                    1, 5, 0.05f));
                factories.add((entity, random) -> new TradeOffer(
                    new ItemStack(Items.EMERALD, 6),
                    DDItems.NETHERITE_NUGGET.getDefaultStack(),
                    4, 5, 0.05f));
                factories.add((entity, random) -> new TradeOffer(
                    new ItemStack(Items.EMERALD, 5),
                    Items.TADPOLE_BUCKET.getDefaultStack(),
                    4, 5, 0.05f));
                factories.add((entity, random) -> new TradeOffer(
                    new ItemStack(Items.EMERALD, 5),
                    DDItems.STRIDER_BUCKET.getDefaultStack(),
                    4, 5, 0.05f));
                factories.add((entity, random) -> new TradeOffer(
                    new ItemStack(Items.EMERALD, 10),
                    Items.SNIFFER_EGG.getDefaultStack(),
                    1, 5, 0.05f));
        });

        registerCarpenterTrades();
    }

    private static void registerCarpenterTrades() {
        TradeOfferHelper.registerVillagerOffers(DDVillagers.CARPENTER, 1,
            factories -> {
                factories.add((entity, random) -> new TradeOffer(
                    new ItemStack(LOGS.get(random.nextBetweenExclusive(0, LOGS.size() - 2)), random.nextBetween(5, 8)),
                    new ItemStack(Items.EMERALD, random.nextBetween(1, 3)),
                    random.nextBetween(8, 14), 2, 0.05f));
                factories.add((entity, random) -> new TradeOffer(
                    new ItemStack(Items.EMERALD, random.nextBetween(3, 7)),
                    new ItemStack(Items.IRON_AXE, 1),
                    random.nextBetween(2, 5), 3, 0.02f));
            });
        TradeOfferHelper.registerVillagerOffers(DDVillagers.CARPENTER, 2,
            factories -> {
                factories.add((entity, random) -> new TradeOffer(
                    new ItemStack(Items.EMERALD, 2),
                    new ItemStack(STRIPPED_LOGS.get(random.nextBetweenExclusive(0, STRIPPED_LOGS.size() - 2)), 4),
                    random.nextBetween(12, 19), 5, 0.05f));
                factories.add((entity, random) -> new TradeOffer(
                    new ItemStack(Items.EMERALD, 3),
                    new ItemStack(SAPLINGS.get(random.nextBetweenExclusive(0, SAPLINGS.size() - 2)), 2),
                    random.nextBetween(3, 6), 5, 0.05f));
            });
        TradeOfferHelper.registerVillagerOffers(DDVillagers.CARPENTER, 3,
            factories -> {
                factories.add((entity, random) -> new TradeOffer(
                    new ItemStack(Items.EMERALD, random.nextBetween(3, 7)),
                    new ItemStack(SAPLINGS.get(random.nextBetweenExclusive(SAPLINGS.size() - 2, SAPLINGS.size())), random.nextBetween(1, 2)),
                    random.nextBetween(2, 7), 10, 0.05f));
                factories.add((entity, random) -> new TradeOffer(
                    new ItemStack(LOGS.get(random.nextBetweenExclusive(LOGS.size() - 2, LOGS.size())), random.nextBetween(5, 8)),
                    new ItemStack(Items.EMERALD, random.nextBetween(3, 6)),
                    random.nextBetween(8, 13), 10, 0.05f));
            });
        TradeOfferHelper.registerVillagerOffers(DDVillagers.CARPENTER, 4,
            factories -> {
                factories.add((entity, random) -> new TradeOffer(
                    new ItemStack(Items.EMERALD, random.nextBetween(2, 4)),
                    new ItemStack(BOATS.get(random.nextBetweenExclusive(0, BOATS.size())), 1),
                    random.nextBetween(2, 6), 15, 0.05f));
                factories.add((entity, random) -> new TradeOffer(
                    new ItemStack(Items.EMERALD, random.nextBetween(7, 15)),
                    new ItemStack(Items.DIAMOND_AXE, 1),
                    random.nextBetween(1, 4), 20, 0.02f));
                factories.add((entity, random) -> new TradeOffer(
                    new ItemStack(Items.EMERALD, random.nextBetween(2, 5)),
                    new ItemStack(ImmutableList.of(Items.AZALEA, Items.FLOWERING_AZALEA).get(random.nextBetween(0, 1)), random.nextBetween(1, 3)),
                        random.nextBetween(4, 9), 15, 0.05f));
            });
        TradeOfferHelper.registerVillagerOffers(DDVillagers.CARPENTER, 5,
            factories -> {
                factories.add((entity, random) -> ImmutableList.of(
                    new TradeOffer(
                        new ItemStack(Items.BAMBOO, random.nextBetween(3, 6)),
                        new ItemStack(Items.EMERALD, random.nextBetween(1, 2)),
                        random.nextBetween(12, 20), 30, 0.05f),
                    new TradeOffer(
                        new ItemStack(Items.EMERALD, random.nextBetween(17, 32)),
                        new ItemStack(Items.NETHERITE_AXE, 1),
                        random.nextBetween(1, 2), 30, 0.01f)
                ).get(random.nextBetween(0, 1)));
                factories.add((entity, random) -> new TradeOffer(
                    new ItemStack(Items.EMERALD, random.nextBetween(1, 4)),
                    new ItemStack(FENCES.get(random.nextBetweenExclusive(0, FENCES.size())), 4),
                    random.nextBetween(4, 9), 30, 0.05f));
                factories.add((entity, random) -> new TradeOffer(
                    new ItemStack(Items.EMERALD, random.nextBetween(1, 4)),
                    new ItemStack(FENCE_GATES.get(random.nextBetweenExclusive(0, FENCE_GATES.size())), 4),
                    random.nextBetween(4, 9), 30, 0.05f));
                factories.add((entity, random) -> new TradeOffer(
                    new ItemStack(Items.EMERALD, random.nextBetween(1, 4)),
                    new ItemStack(DOORS.get(random.nextBetweenExclusive(0, DOORS.size())), 4),
                    random.nextBetween(4, 9), 30, 0.05f));
            });
    }
}