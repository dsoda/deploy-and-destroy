package net.dsoda.deployanddestroy.entity.villager;

import com.google.common.collect.ImmutableSet;
import net.dsoda.deployanddestroy.DeployAndDestroy;
import net.dsoda.deployanddestroy.blocks.DDBlocks;
import net.dsoda.deployanddestroy.sound.DDSounds;
import net.fabricmc.fabric.api.object.builder.v1.world.poi.PointOfInterestHelper;
import net.minecraft.block.Block;
import net.minecraft.registry.Registries;
import net.minecraft.registry.Registry;
import net.minecraft.registry.RegistryKey;
import net.minecraft.registry.RegistryKeys;
import net.minecraft.util.Identifier;
import net.minecraft.village.VillagerProfession;
import net.minecraft.world.poi.PointOfInterestType;

public class DDVillagers {

    public static final PointOfInterestType WOODCUTTER_POI = registerPoi("woodcutter_poi", DDBlocks.WOODCUTTER);
    public static final VillagerProfession CARPENTER = registerProfession("carpenter", poiKey("woodcutter_poi"));

    private static VillagerProfession registerProfession(String name, RegistryKey<PointOfInterestType> type) {
        return Registry.register(Registries.VILLAGER_PROFESSION, new Identifier(DeployAndDestroy.MOD_ID, name),
            new VillagerProfession(name, entry -> entry.matchesKey(type), entry -> entry.matchesKey(type),
                ImmutableSet.of(), ImmutableSet.of(), DDSounds.VILLAGER_USES_WOODCUTTER));
    }

    private static PointOfInterestType registerPoi(String name, Block block) {
        return PointOfInterestHelper.register(new Identifier(DeployAndDestroy.MOD_ID, name), 1, 1, block);
    }

    private static RegistryKey<PointOfInterestType> poiKey(String name) {
        return RegistryKey.of(RegistryKeys.POINT_OF_INTEREST_TYPE, new Identifier(DeployAndDestroy.MOD_ID, name));
    }

    public static void registerVillagers() {
        DeployAndDestroy.LOGGER.info("Registering Villagers For" + DeployAndDestroy.MOD_ID);
    }
}
