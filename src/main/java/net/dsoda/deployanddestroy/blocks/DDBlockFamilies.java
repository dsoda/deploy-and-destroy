package net.dsoda.deployanddestroy.blocks;

import net.dsoda.deployanddestroy.DeployAndDestroy;
import net.minecraft.block.Block;
import net.minecraft.block.Blocks;
import net.minecraft.data.family.BlockFamilies;
import net.minecraft.data.family.BlockFamily;

import static net.dsoda.deployanddestroy.DeployAndDestroy.MOD_ID;

public class DDBlockFamilies {

    public static final BlockFamily AZALEA_PLANKS = registerFamily(DDBlocks.AZALEA_PLANKS, DDBlocks.AZALEA_STAIRS, DDBlocks.AZALEA_SLAB, DDBlocks.AZALEA_FENCE, DDBlocks.AZALEA_FENCE_GATE, DDBlocks.AZALEA_DOOR, DDBlocks.AZALEA_TRAPDOOR, DDBlocks.AZALEA_BUTTON, DDBlocks.AZALEA_PRESSURE_PLATE, DDBlocks.AZALEA_SIGN, DDBlocks.AZALEA_WALL_SIGN, "wood", "has_planks");
    public static final BlockFamily SWAMP_PLANKS = registerFamily(DDBlocks.SWAMP_PLANKS, DDBlocks.SWAMP_STAIRS, DDBlocks.SWAMP_SLAB, DDBlocks.SWAMP_FENCE, DDBlocks.SWAMP_FENCE_GATE, DDBlocks.SWAMP_DOOR, DDBlocks.SWAMP_TRAPDOOR, DDBlocks.SWAMP_BUTTON, DDBlocks.SWAMP_PRESSURE_PLATE, DDBlocks.SWAMP_SIGN, DDBlocks.SWAMP_WALL_SIGN, "wood", "has_planks");
    public static final BlockFamily SANDSTONE_BRICKS = registerFamily(DDBlocks.SANDSTONE_BRICKS, DDBlocks.SANDSTONE_BRICK_STAIRS, DDBlocks.SANDSTONE_BRICK_SLAB, DDBlocks.SANDSTONE_BRICK_WALL, "bricks", "has_bricks");
    public static final BlockFamily CRACKED_SANDSTONE_BRICKS = registerFamily(DDBlocks.CRACKED_SANDSTONE_BRICKS, DDBlocks.CRACKED_SANDSTONE_BRICK_STAIRS, DDBlocks.CRACKED_SANDSTONE_BRICK_SLAB, DDBlocks.CRACKED_SANDSTONE_BRICK_WALL, "cracked_bricks", "has_cracked_bricks");
    public static final BlockFamily MOSSY_SANDSTONE_BRICKS = registerFamily(DDBlocks.MOSSY_SANDSTONE_BRICKS, DDBlocks.MOSSY_SANDSTONE_BRICK_STAIRS, DDBlocks.MOSSY_SANDSTONE_BRICK_SLAB, DDBlocks.MOSSY_SANDSTONE_BRICK_WALL, "mossy_bricks", "has_mossy_bricks");
    public static final BlockFamily POLISHED_SANDSTONE = registerFamily(DDBlocks.POLISHED_SANDSTONE, DDBlocks.POLISHED_SANDSTONE_STAIRS, DDBlocks.POLISHED_SANDSTONE_SLAB, DDBlocks.POLISHED_SANDSTONE_WALL, DDBlocks.CHISELED_POLISHED_SANDSTONE,  "polished_block", "has_polished_block");
    public static final BlockFamily RED_SANDSTONE_BRICKS = registerFamily(DDBlocks.RED_SANDSTONE_BRICKS, DDBlocks.RED_SANDSTONE_BRICK_STAIRS, DDBlocks.RED_SANDSTONE_BRICK_SLAB, DDBlocks.RED_SANDSTONE_BRICK_WALL, "bricks", "has_bricks");
    public static final BlockFamily CRACKED_RED_SANDSTONE_BRICKS = registerFamily(DDBlocks.CRACKED_RED_SANDSTONE_BRICKS, DDBlocks.CRACKED_RED_SANDSTONE_BRICK_STAIRS, DDBlocks.CRACKED_RED_SANDSTONE_BRICK_SLAB, DDBlocks.CRACKED_RED_SANDSTONE_BRICK_WALL, "cracked_bricks", "has_cracked_bricks");
    public static final BlockFamily MOSSY_RED_SANDSTONE_BRICKS = registerFamily(DDBlocks.MOSSY_RED_SANDSTONE_BRICKS, DDBlocks.MOSSY_RED_SANDSTONE_BRICK_STAIRS, DDBlocks.MOSSY_RED_SANDSTONE_BRICK_SLAB, DDBlocks.MOSSY_RED_SANDSTONE_BRICK_WALL, "mossy_bricks", "has_mossy_bricks");
    public static final BlockFamily POLISHED_RED_SANDSTONE = registerFamily(DDBlocks.POLISHED_RED_SANDSTONE, DDBlocks.POLISHED_RED_SANDSTONE_STAIRS, DDBlocks.POLISHED_RED_SANDSTONE_SLAB, DDBlocks.POLISHED_RED_SANDSTONE_WALL, DDBlocks.CHISELED_POLISHED_RED_SANDSTONE,  "polished_block", "has_polished_block");
    public static final BlockFamily CRACKED_STONE_BRICKS = registerFamily(Blocks.CRACKED_STONE_BRICKS, DDBlocks.CRACKED_STONE_BRICK_STAIRS, DDBlocks.CRACKED_STONE_BRICK_SLAB, DDBlocks.CRACKED_STONE_BRICK_WALL, "cracked_bricks", "has_cracked_bricks");
    public static final BlockFamily NETHERRACK = registerFamily(Blocks.NETHERRACK, DDBlocks.NETHERRACK_STAIRS, DDBlocks.NETHERRACK_SLAB, DDBlocks.NETHERRACK_WALL, "stone", "has_stone");
    public static final BlockFamily CRACKED_BRICKS = registerFamily(DDBlocks.CRACKED_BRICKS, DDBlocks.CRACKED_BRICK_STAIRS, DDBlocks.CRACKED_BRICK_SLAB, DDBlocks.CRACKED_BRICK_WALL, "cracked_bricks", "has_cracked_bricks");
    public static final BlockFamily MOSSY_BRICKS = registerFamily(DDBlocks.MOSSY_BRICKS, DDBlocks.MOSSY_BRICK_STAIRS, DDBlocks.MOSSY_BRICK_SLAB, DDBlocks.MOSSY_BRICK_WALL, "mossy_bricks", "has_mossy_bricks");
    public static final BlockFamily QUARTZ_BRICKS = registerFamily(Blocks.QUARTZ_BRICKS, DDBlocks.QUARTZ_BRICK_STAIRS, DDBlocks.QUARTZ_BRICK_SLAB, DDBlocks.QUARTZ_BRICK_WALL, "bricks", "has_bricks");
    public static final BlockFamily SMOOTH_CHARRED_QUARTZ = registerFamily(DDBlocks.SMOOTH_CHARRED_QUARTZ_BLOCK, DDBlocks.SMOOTH_CHARRED_QUARTZ_STAIRS, DDBlocks.SMOOTH_CHARRED_QUARTZ_SLAB, DDBlocks.SMOOTH_CHARRED_QUARTZ_WALL, "smooth_block", "has_smooth_block");
    public static final BlockFamily CHARRED_QUARTZ_BRICKS = registerFamily(DDBlocks.CHARRED_QUARTZ_BRICKS, DDBlocks.CHARRED_QUARTZ_BRICK_STAIRS, DDBlocks.CHARRED_QUARTZ_BRICK_SLAB, DDBlocks.CHARRED_QUARTZ_BRICK_WALL, "bricks", "has_bricks");
    public static final BlockFamily ANDESITE_BRICKS = registerFamily(DDBlocks.ANDESITE_BRICKS, DDBlocks.ANDESITE_BRICK_STAIRS, DDBlocks.ANDESITE_BRICK_SLAB, DDBlocks.ANDESITE_BRICK_WALL, "bricks", "has_bricks");
    public static final BlockFamily DIORITE_BRICKS = registerFamily(DDBlocks.DIORITE_BRICKS, DDBlocks.DIORITE_BRICK_STAIRS, DDBlocks.DIORITE_BRICK_SLAB, DDBlocks.DIORITE_BRICK_WALL, "bricks", "has_bricks");
    public static final BlockFamily GRANITE_BRICKS = registerFamily(DDBlocks.GRANITE_BRICKS, DDBlocks.GRANITE_BRICK_STAIRS, DDBlocks.GRANITE_BRICK_SLAB, DDBlocks.GRANITE_BRICK_WALL, "bricks", "has_bricks");
    public static final BlockFamily CRACKED_ANDESITE_BRICKS = registerFamily(DDBlocks.CRACKED_ANDESITE_BRICKS, DDBlocks.CRACKED_ANDESITE_BRICK_STAIRS, DDBlocks.CRACKED_ANDESITE_BRICK_SLAB, DDBlocks.CRACKED_ANDESITE_BRICK_WALL, "cracked_bricks", "has_cracked_bricks");
    public static final BlockFamily CRACKED_DIORITE_BRICKS = registerFamily(DDBlocks.CRACKED_DIORITE_BRICKS, DDBlocks.CRACKED_DIORITE_BRICK_STAIRS, DDBlocks.CRACKED_DIORITE_BRICK_SLAB, DDBlocks.CRACKED_DIORITE_BRICK_WALL, "cracked_bricks", "has_cracked_bricks");
    public static final BlockFamily CRACKED_GRANITE_BRICKS = registerFamily(DDBlocks.CRACKED_GRANITE_BRICKS, DDBlocks.CRACKED_GRANITE_BRICK_STAIRS, DDBlocks.CRACKED_GRANITE_BRICK_SLAB, DDBlocks.CRACKED_GRANITE_BRICK_WALL, "cracked_bricks", "has_cracked_bricks");
    public static final BlockFamily MOSSY_ANDESITE_BRICKS = registerFamily(DDBlocks.MOSSY_ANDESITE_BRICKS, DDBlocks.MOSSY_ANDESITE_BRICK_STAIRS, DDBlocks.MOSSY_ANDESITE_BRICK_SLAB, DDBlocks.MOSSY_ANDESITE_BRICK_WALL, "mossy_bricks", "has_mossy_bricks");
    public static final BlockFamily MOSSY_DIORITE_BRICKS = registerFamily(DDBlocks.MOSSY_DIORITE_BRICKS, DDBlocks.MOSSY_DIORITE_BRICK_STAIRS, DDBlocks.MOSSY_DIORITE_BRICK_SLAB, DDBlocks.MOSSY_DIORITE_BRICK_WALL, "mossy_bricks", "has_mossy_bricks");
    public static final BlockFamily MOSSY_GRANITE_BRICKS = registerFamily(DDBlocks.MOSSY_GRANITE_BRICKS, DDBlocks.MOSSY_GRANITE_BRICK_STAIRS, DDBlocks.MOSSY_GRANITE_BRICK_SLAB, DDBlocks.MOSSY_GRANITE_BRICK_WALL, "mossy_bricks", "has_mossy_bricks");
    public static final BlockFamily CALCITE = registerFamily(Blocks.CALCITE, DDBlocks.CALCITE_STAIRS, DDBlocks.CALCITE_SLAB, DDBlocks.CALCITE_WALL,  "stone", "has_stone");
    public static final BlockFamily CUT_IRON = registerFamily(DDBlocks.CUT_IRON_BLOCK, DDBlocks.CUT_IRON_STAIRS, DDBlocks.CUT_IRON_SLAB, DDBlocks.CUT_IRON_WALL, "cut_block", "has_cut_block");
    public static final BlockFamily CUT_GOLD = registerFamily(DDBlocks.CUT_GOLD_BLOCK, DDBlocks.CUT_GOLD_STAIRS, DDBlocks.CUT_GOLD_SLAB, DDBlocks.CUT_GOLD_WALL, "cut_block", "has_cut_block");
    public static final BlockFamily CUT_DIAMOND = registerFamily(DDBlocks.CUT_DIAMOND_BLOCK, DDBlocks.CUT_DIAMOND_STAIRS, DDBlocks.CUT_DIAMOND_SLAB, DDBlocks.CUT_DIAMOND_WALL, "cut_block", "has_cut_block");
    public static final BlockFamily CUT_EMERALD = registerFamily(DDBlocks.CUT_EMERALD_BLOCK, DDBlocks.CUT_EMERALD_STAIRS, DDBlocks.CUT_EMERALD_SLAB, DDBlocks.CUT_EMERALD_WALL, "cut_block", "has_cut_block");
    public static final BlockFamily CUT_NETHERITE = registerFamily(DDBlocks.CUT_NETHERITE_BLOCK, DDBlocks.CUT_NETHERITE_STAIRS, DDBlocks.CUT_NETHERITE_SLAB, DDBlocks.CUT_NETHERITE_WALL, "cut_block", "has_cut_block");
    public static final BlockFamily CUT_LAPIS = registerFamily(DDBlocks.CUT_LAPIS_BLOCK, DDBlocks.CUT_LAPIS_STAIRS, DDBlocks.CUT_LAPIS_SLAB, DDBlocks.CUT_LAPIS_WALL, "cut_block", "has_cut_block");
    public static final BlockFamily POLISHED_AMETHYST = registerFamily(DDBlocks.POLISHED_AMETHYST_BLOCK, DDBlocks.POLISHED_AMETHYST_STAIRS, DDBlocks.POLISHED_AMETHYST_SLAB, DDBlocks.POLISHED_AMETHYST_WALL, "polished_block", "has_polished_block");
    public static final BlockFamily POLISHED_AMETHYST_BRICKS = registerFamily(DDBlocks.POLISHED_AMETHYST_BRICKS, DDBlocks.POLISHED_AMETHYST_BRICK_STAIRS, DDBlocks.POLISHED_AMETHYST_BRICK_SLAB, DDBlocks.POLISHED_AMETHYST_BRICK_WALL, "bricks", "has_bricks");
    public static final BlockFamily CUT_POLISHED_AMETHYST = registerFamily(DDBlocks.CUT_POLISHED_AMETHYST, DDBlocks.CUT_POLISHED_AMETHYST_STAIRS, DDBlocks.CUT_POLISHED_AMETHYST_SLAB, DDBlocks.CUT_POLISHED_AMETHYST_WALL, "cut_block", "has_cut_block");
    public static final BlockFamily PACKED_ICE_BRICKS = registerFamily(DDBlocks.PACKED_ICE_BRICKS, DDBlocks.PACKED_ICE_BRICK_STAIRS, DDBlocks.PACKED_ICE_BRICK_SLAB, DDBlocks.PACKED_ICE_BRICK_WALL, DDBlocks.CHISELED_PACKED_ICE_BRICKS, "bricks", "has_bricks");
    public static final BlockFamily LARGE_PACKED_ICE_BRICKS = registerFamily(DDBlocks.LARGE_PACKED_ICE_BRICKS, DDBlocks.LARGE_PACKED_ICE_BRICK_STAIRS, DDBlocks.LARGE_PACKED_ICE_BRICK_SLAB, DDBlocks.LARGE_PACKED_ICE_BRICK_WALL, "bricks", "has_bricks");
    public static final BlockFamily CRACKED_PACKED_ICE_BRICKS = registerFamily(DDBlocks.CRACKED_PACKED_ICE_BRICKS, DDBlocks.CRACKED_PACKED_ICE_BRICK_STAIRS, DDBlocks.CRACKED_PACKED_ICE_BRICK_SLAB, DDBlocks.CRACKED_PACKED_ICE_BRICK_WALL, "cracked_bricks", "has_cracked_bricks");
    public static final BlockFamily LARGE_CRACKED_PACKED_ICE_BRICKS = registerFamily(DDBlocks.LARGE_CRACKED_PACKED_ICE_BRICKS, DDBlocks.LARGE_CRACKED_PACKED_ICE_BRICK_STAIRS, DDBlocks.LARGE_CRACKED_PACKED_ICE_BRICK_SLAB, DDBlocks.LARGE_CRACKED_PACKED_ICE_BRICK_WALL, "cracked_bricks", "has_cracked_bricks");
    public static final BlockFamily POLISHED_PACKED_ICE = registerFamily(DDBlocks.POLISHED_PACKED_ICE, DDBlocks.POLISHED_PACKED_ICE_STAIRS, DDBlocks.POLISHED_PACKED_ICE_SLAB, DDBlocks.POLISHED_PACKED_ICE_WALL,  "polished_block", "has_polished_block");
    public static final BlockFamily BLUE_ICE_BRICKS = registerFamily(DDBlocks.BLUE_ICE_BRICKS, DDBlocks.BLUE_ICE_BRICK_STAIRS, DDBlocks.BLUE_ICE_BRICK_SLAB, DDBlocks.BLUE_ICE_BRICK_WALL, DDBlocks.CHISELED_BLUE_ICE_BRICKS, "bricks", "has_bricks");
    public static final BlockFamily LARGE_BLUE_ICE_BRICKS = registerFamily(DDBlocks.LARGE_BLUE_ICE_BRICKS, DDBlocks.LARGE_BLUE_ICE_BRICK_STAIRS, DDBlocks.LARGE_BLUE_ICE_BRICK_SLAB, DDBlocks.LARGE_BLUE_ICE_BRICK_WALL, "bricks", "has_bricks");
    public static final BlockFamily CRACKED_BLUE_ICE_BRICKS = registerFamily(DDBlocks.CRACKED_BLUE_ICE_BRICKS, DDBlocks.CRACKED_BLUE_ICE_BRICK_STAIRS, DDBlocks.CRACKED_BLUE_ICE_BRICK_SLAB, DDBlocks.CRACKED_BLUE_ICE_BRICK_WALL, "cracked_bricks", "has_cracked_bricks");
    public static final BlockFamily LARGE_CRACKED_BLUE_ICE_BRICKS = registerFamily(DDBlocks.LARGE_CRACKED_BLUE_ICE_BRICKS, DDBlocks.LARGE_CRACKED_BLUE_ICE_BRICK_STAIRS, DDBlocks.LARGE_CRACKED_BLUE_ICE_BRICK_SLAB, DDBlocks.LARGE_CRACKED_BLUE_ICE_BRICK_WALL, "cracked_bricks", "has_cracked_bricks");
    public static final BlockFamily POLISHED_BLUE_ICE = registerFamily(DDBlocks.POLISHED_BLUE_ICE, DDBlocks.POLISHED_BLUE_ICE_STAIRS, DDBlocks.POLISHED_BLUE_ICE_SLAB, DDBlocks.POLISHED_BLUE_ICE_WALL,  "polished_block", "has_polished_block");
    public static final BlockFamily CRACKED_DEEPSLATE_BRICKS = registerFamily(Blocks.CRACKED_DEEPSLATE_BRICKS, DDBlocks.CRACKED_DEEPSLATE_BRICK_STAIRS, DDBlocks.CRACKED_DEEPSLATE_BRICK_SLAB, DDBlocks.CRACKED_DEEPSLATE_BRICK_WALL, "cracked_bricks", "has_cracked_bricks");
    public static final BlockFamily CRACKED_DEEPSLATE_TILES = registerFamily(Blocks.CRACKED_DEEPSLATE_TILES, DDBlocks.CRACKED_DEEPSLATE_TILE_STAIRS, DDBlocks.CRACKED_DEEPSLATE_TILE_SLAB, DDBlocks.CRACKED_DEEPSLATE_TILE_WALL, "cracked_bricks", "has_cracked_bricks");
    public static final BlockFamily CRACKED_NETHER_BRICKS = registerFamily(Blocks.CRACKED_NETHER_BRICKS, DDBlocks.CRACKED_NETHER_BRICK_STAIRS, DDBlocks.CRACKED_NETHER_BRICK_SLAB, DDBlocks.CRACKED_NETHER_BRICK_WALL, "cracked_bricks", "has_cracked_bricks");
    public static final BlockFamily CRACKED_POLISHED_BLACKSTONE_BRICKS = registerFamily(Blocks.CRACKED_POLISHED_BLACKSTONE_BRICKS, DDBlocks.CRACKED_POLISHED_BLACKSTONE_BRICK_STAIRS, DDBlocks.CRACKED_POLISHED_BLACKSTONE_BRICK_SLAB, DDBlocks.CRACKED_POLISHED_BLACKSTONE_BRICK_WALL, "cracked_bricks", "has_cracked_bricks");
    public static final BlockFamily SMOOTH_BASALT = registerFamily(Blocks.SMOOTH_BASALT, DDBlocks.SMOOTH_BASALT_STAIRS, DDBlocks.SMOOTH_BASALT_SLAB, DDBlocks.SMOOTH_BASALT_WALL, "stone", "has_stone");
    public static final BlockFamily POLISHED_CALCITE = registerFamily(DDBlocks.POLISHED_CALCITE, DDBlocks.POLISHED_CALCITE_STAIRS, DDBlocks.POLISHED_CALCITE_SLAB, DDBlocks.POLISHED_CALCITE_WALL, "polished_block", "has_polished_block");
    public static final BlockFamily CALCITE_BRICKS = registerFamily(DDBlocks.CALCITE_BRICKS, DDBlocks.CALCITE_BRICK_STAIRS, DDBlocks.CALCITE_BRICK_SLAB, DDBlocks.CALCITE_BRICK_WALL, "bricks", "has_bricks");
    public static final BlockFamily CRACKED_CALCITE_BRICKS = registerFamily(DDBlocks.CRACKED_CALCITE_BRICKS, DDBlocks.CRACKED_CALCITE_BRICK_STAIRS, DDBlocks.CRACKED_CALCITE_BRICK_SLAB, DDBlocks.CRACKED_CALCITE_BRICK_WALL, "cracked_bricks", "has_cracked_bricks");
    public static final BlockFamily MOSSY_CALCITE_BRICKS = registerFamily(DDBlocks.MOSSY_CALCITE_BRICKS, DDBlocks.MOSSY_CALCITE_BRICK_STAIRS, DDBlocks.MOSSY_CALCITE_BRICK_SLAB, DDBlocks.MOSSY_CALCITE_BRICK_WALL, "mossy_bricks", "has_mossy_bricks");
    public static final BlockFamily DRIPSTONE = registerFamily(Blocks.DRIPSTONE_BLOCK, DDBlocks.DRIPSTONE_STAIRS, DDBlocks.DRIPSTONE_SLAB, DDBlocks.DRIPSTONE_WALL, "stone", "has_stone");
    public static final BlockFamily POLISHED_DRIPSTONE = registerFamily(DDBlocks.POLISHED_DRIPSTONE, DDBlocks.POLISHED_DRIPSTONE_STAIRS, DDBlocks.POLISHED_DRIPSTONE_SLAB, DDBlocks.POLISHED_DRIPSTONE_WALL, "polished_block", "has_polished_block");
    public static final BlockFamily DRIPSTONE_BRICKS = registerFamily(DDBlocks.DRIPSTONE_BRICKS, DDBlocks.DRIPSTONE_BRICK_STAIRS, DDBlocks.DRIPSTONE_BRICK_SLAB, DDBlocks.DRIPSTONE_BRICK_WALL, "bricks", "has_bricks");
    public static final BlockFamily CRACKED_DRIPSTONE_BRICKS = registerFamily(DDBlocks.CRACKED_DRIPSTONE_BRICKS, DDBlocks.CRACKED_DRIPSTONE_BRICK_STAIRS, DDBlocks.CRACKED_DRIPSTONE_BRICK_SLAB, DDBlocks.CRACKED_DRIPSTONE_BRICK_WALL, "cracked_bricks", "has_cracked_bricks");
    public static final BlockFamily MOSSY_DRIPSTONE_BRICKS = registerFamily(DDBlocks.MOSSY_DRIPSTONE_BRICKS, DDBlocks.MOSSY_DRIPSTONE_BRICK_STAIRS, DDBlocks.MOSSY_DRIPSTONE_BRICK_SLAB, DDBlocks.MOSSY_DRIPSTONE_BRICK_WALL, "mossy_bricks", "has_mossy_bricks");
    public static final BlockFamily MOSSY_COBBLED_DEEPSLATE = registerFamily(DDBlocks.MOSSY_COBBLED_DEEPSLATE, DDBlocks.MOSSY_COBBLED_DEEPSLATE_STAIRS, DDBlocks.MOSSY_COBBLED_DEEPSLATE_SLAB, DDBlocks.MOSSY_COBBLED_DEEPSLATE_WALL, "mossy_bricks", "has_mossy_bricks");
    public static final BlockFamily MOSSY_DEEPSLATE_BRICKS = registerFamily(DDBlocks.MOSSY_DEEPSLATE_BRICKS, DDBlocks.MOSSY_DEEPSLATE_BRICK_STAIRS, DDBlocks.MOSSY_DEEPSLATE_BRICK_SLAB, DDBlocks.MOSSY_DEEPSLATE_BRICK_WALL, "mossy_bricks", "has_mossy_bricks");
    public static final BlockFamily CRACKED_TUFF_BRICKS = registerFamily(DDBlocks.CRACKED_TUFF_BRICKS, DDBlocks.CRACKED_TUFF_BRICK_STAIRS, DDBlocks.CRACKED_TUFF_BRICK_SLAB, DDBlocks.CRACKED_TUFF_BRICK_WALL, "cracked_bricks", "has_cracked_bricks");
    public static final BlockFamily MOSSY_TUFF_BRICKS = registerFamily(DDBlocks.MOSSY_TUFF_BRICKS, DDBlocks.MOSSY_TUFF_BRICK_STAIRS, DDBlocks.MOSSY_TUFF_BRICK_SLAB, DDBlocks.MOSSY_TUFF_BRICK_WALL, "mossy_bricks", "has_mossy_bricks");


    public static void registerBlockFamilies() {
        DeployAndDestroy.LOGGER.info("Registering Block Families For " + MOD_ID);
    }

    private static BlockFamily registerFamily(Block base, Block stairs, Block slab, Block fence, Block fenceGate, Block door, Block trapdoor, Block button, Block pressurePlate, Block sign, Block wallSign, String group, String unlockCriteria) {
        return BlockFamilies.register(base).stairs(stairs).slab(slab).fence(fence).fenceGate(fenceGate).door(door).trapdoor(trapdoor).button(button).pressurePlate(pressurePlate).sign(sign, wallSign).group(group).unlockCriterionName(unlockCriteria).build();
    }

    private static BlockFamily registerFamily(Block base, Block stairs, Block slab, Block wall, String group, String unlockCriteria) {
        return BlockFamilies.register(base).stairs(stairs).slab(slab).wall(wall).group(group).unlockCriterionName(unlockCriteria).build();
    }

    private static BlockFamily registerFamily(Block base, Block stairs, Block slab, Block wall, Block chiseled, String group, String unlockCriteria) {
        return BlockFamilies.register(base).stairs(stairs).slab(slab).wall(wall).chiseled(chiseled).group(group).unlockCriterionName(unlockCriteria).build();
    }
}
