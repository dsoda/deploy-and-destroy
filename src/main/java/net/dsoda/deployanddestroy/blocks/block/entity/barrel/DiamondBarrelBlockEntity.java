package net.dsoda.deployanddestroy.blocks.block.entity.barrel;

import net.dsoda.deployanddestroy.blocks.DDBlockEntities;
import net.dsoda.deployanddestroy.blocks.block.MaterialBarrelBlock;
import net.dsoda.deployanddestroy.blocks.block.entity.MaterialBarrelBlockEntity;
import net.minecraft.block.BlockState;
import net.minecraft.util.math.BlockPos;

public class DiamondBarrelBlockEntity extends MaterialBarrelBlockEntity {
    public DiamondBarrelBlockEntity(BlockPos pos, BlockState state) {
        super(DDBlockEntities.DIAMOND_BARREL_BLOCK_ENTITY, pos, state, MaterialBarrelBlock.BarrelMaterial.DIAMOND);
    }
}
