package net.dsoda.deployanddestroy.blocks.block;

import com.mojang.logging.LogUtils;
import net.dsoda.deployanddestroy.blocks.DDBlockEntities;
import net.dsoda.deployanddestroy.blocks.block.entity.DeployerBlockEntity;
import net.dsoda.deployanddestroy.stats.DDStats;
import net.minecraft.block.*;
import net.minecraft.block.entity.*;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.AutomaticItemPlacementContext;
import net.minecraft.item.BlockItem;
import net.minecraft.item.ItemStack;
import net.minecraft.server.world.ServerWorld;
import net.minecraft.util.ActionResult;
import net.minecraft.util.Hand;
import net.minecraft.util.ItemScatterer;
import net.minecraft.util.hit.BlockHitResult;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Direction;
import net.minecraft.world.World;
import net.minecraft.world.WorldEvents;
import net.minecraft.world.event.GameEvent;
import org.slf4j.Logger;

public class DeployerBlock extends DispenserBlock {

    private static final Logger LOGGER = LogUtils.getLogger();

    public DeployerBlock(AbstractBlock.Settings settings) {
        super(settings);
    }

    @Override
    public BlockEntity createBlockEntity(BlockPos pos, BlockState state) {
        return new DeployerBlockEntity(pos, state);
    }

    @Override
    protected void dispense(ServerWorld world, BlockState state, BlockPos pos) {
        ItemStack stack;
        DeployerBlockEntity deployerBlockEntity = world.getBlockEntity(pos, DDBlockEntities.DEPLOYER_BLOCK_ENTITY).orElse(null);
        if (deployerBlockEntity == null) {
            LOGGER.warn("Ignoring dispensing attempt for Dropper without matching block entity at {}", pos);
            return;
        }
        int slot = deployerBlockEntity.chooseNonEmptySlot(world.random);
        if (slot < 0) {
            world.syncWorldEvent(WorldEvents.DISPENSER_FAILS, pos, 0);
            world.emitGameEvent(GameEvent.BLOCK_ACTIVATE, pos, GameEvent.Emitter.of(deployerBlockEntity.getCachedState()));
            return;
        }
        stack = deployerBlockEntity.getStack(slot);
        if (stack.isEmpty() || !(stack.getItem() instanceof BlockItem)) return;
        Direction direction = world.getBlockState(pos).get(FACING);
        placeBlock(world, pos.offset(direction), direction, stack);
    }

    @Override
    public ActionResult onUse(BlockState state, World world, BlockPos pos, PlayerEntity player, Hand hand, BlockHitResult hit) {
        if (world.isClient) {
            return ActionResult.SUCCESS;
        }
        BlockEntity blockEntity = world.getBlockEntity(pos);
        if (blockEntity instanceof DeployerBlockEntity) {
            player.openHandledScreen((DeployerBlockEntity)blockEntity);
            player.incrementStat(DDStats.INTERACT_WITH_DEPLOYER);
        }
        return ActionResult.CONSUME;
    }

    @Override
    public void onStateReplaced(BlockState state, World world, BlockPos pos, BlockState newState, boolean moved) {
        ItemScatterer.onStateReplaced(state, newState, world, pos);
        super.onStateReplaced(state, world, pos, newState, moved);
    }

    @Override
    public void onPlaced(World world, BlockPos pos, BlockState state, LivingEntity placer, ItemStack itemStack) {
        BlockEntity blockEntity;
        if (itemStack.hasCustomName() && (blockEntity = world.getBlockEntity(pos)) instanceof DeployerBlockEntity) {
            ((DeployerBlockEntity)blockEntity).setCustomName(itemStack.getName());
        }
    }

    private void placeBlock(ServerWorld world, BlockPos pos, Direction direction, ItemStack stack) {
        BlockItem blockItem = (BlockItem)(stack.getItem());
        Direction direction2 = world.isAir(pos.down()) ? direction : Direction.UP;
        ActionResult actionResult = blockItem.place(new AutomaticItemPlacementContext(world, pos, direction, stack, direction2));
        if (actionResult == ActionResult.SUCCESS) stack.decrement(1);
    }
}
