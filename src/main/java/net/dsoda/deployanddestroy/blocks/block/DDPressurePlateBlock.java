package net.dsoda.deployanddestroy.blocks.block;

import com.mojang.serialization.MapCodec;
import net.minecraft.block.AbstractPressurePlateBlock;
import net.minecraft.block.Block;
import net.minecraft.block.BlockSetType;
import net.minecraft.block.BlockState;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.state.StateManager;
import net.minecraft.state.property.BooleanProperty;
import net.minecraft.state.property.Properties;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

public class DDPressurePlateBlock extends AbstractPressurePlateBlock {

    public static final MapCodec<DDPressurePlateBlock> CODEC = DDPressurePlateBlock.createCodec(DDPressurePlateBlock::new);

    public static final BooleanProperty POWERED = Properties.POWERED;

    public DDPressurePlateBlock(Settings settings) {
        super(settings, BlockSetType.IRON);
        this.setDefaultState((this.stateManager.getDefaultState()).with(POWERED, false));
    }

    @Override
    protected MapCodec<? extends AbstractPressurePlateBlock> getCodec() {
        return CODEC;
    }

    @Override
    protected int getRedstoneOutput(World world, BlockPos pos) {
        return getEntityCount(world, BOX.offset(pos), PlayerEntity.class) > 0 ? 15 : 0;
    }

    @Override
    protected int getRedstoneOutput(BlockState state) {
        return state.get(POWERED) ? 15 : 0;
    }

    @Override
    protected BlockState setRedstoneOutput(BlockState state, int rsOut) {
        return state.with(POWERED, rsOut > 0);
    }

    @Override
    protected void appendProperties(StateManager.Builder<Block, BlockState> builder) {
        builder.add(POWERED);
    }
}
