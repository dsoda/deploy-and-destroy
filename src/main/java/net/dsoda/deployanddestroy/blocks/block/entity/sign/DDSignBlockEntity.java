package net.dsoda.deployanddestroy.blocks.block.entity.sign;

import net.dsoda.deployanddestroy.blocks.DDBlockEntities;
import net.minecraft.block.BlockState;
import net.minecraft.block.entity.BlockEntityType;
import net.minecraft.block.entity.SignBlockEntity;
import net.minecraft.util.math.BlockPos;

public class DDSignBlockEntity extends SignBlockEntity {

    public DDSignBlockEntity(BlockPos pos, BlockState state) {
        super(pos, state);
    }

    @Override
    public BlockEntityType<?> getType() {
        return DDBlockEntities.DD_SIGN_BLOCK_ENTITY;
    }
}
