package net.dsoda.deployanddestroy.blocks.block.entity.barrel;

import net.dsoda.deployanddestroy.blocks.DDBlockEntities;
import net.dsoda.deployanddestroy.blocks.block.MaterialBarrelBlock;
import net.dsoda.deployanddestroy.blocks.block.entity.MaterialBarrelBlockEntity;
import net.minecraft.block.BlockState;
import net.minecraft.util.math.BlockPos;

public class NetheriteBarrelBlockEntity extends MaterialBarrelBlockEntity {
    public NetheriteBarrelBlockEntity(BlockPos pos, BlockState state) {
        super(DDBlockEntities.NETHERITE_BARREL_BLOCK_ENTITY, pos, state, MaterialBarrelBlock.BarrelMaterial.NETHERITE);
    }
}
