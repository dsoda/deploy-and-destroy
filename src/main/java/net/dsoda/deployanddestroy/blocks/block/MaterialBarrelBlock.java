package net.dsoda.deployanddestroy.blocks.block;

import com.google.common.collect.ImmutableMap;
import com.mojang.serialization.MapCodec;
import net.dsoda.deployanddestroy.blocks.DDBlocks;
import net.dsoda.deployanddestroy.blocks.block.entity.MaterialBarrelBlockEntity;
import net.dsoda.deployanddestroy.blocks.block.entity.barrel.*;
import net.dsoda.deployanddestroy.stats.DDStats;
import net.minecraft.block.*;
import net.minecraft.block.entity.BlockEntity;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.mob.PiglinBrain;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.ItemPlacementContext;
import net.minecraft.item.ItemStack;
import net.minecraft.screen.ScreenHandler;
import net.minecraft.server.world.ServerWorld;
import net.minecraft.state.StateManager;
import net.minecraft.state.property.BooleanProperty;
import net.minecraft.state.property.DirectionProperty;
import net.minecraft.state.property.Properties;
import net.minecraft.util.*;
import net.minecraft.util.hit.BlockHitResult;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Direction;
import net.minecraft.util.math.random.Random;
import net.minecraft.world.World;
import org.jetbrains.annotations.Nullable;

import java.util.Map;
import java.util.Optional;

public class MaterialBarrelBlock extends BlockWithEntity {

    public static MapCodec<MaterialBarrelBlock> CODEC = MaterialBarrelBlock.createCodec(MaterialBarrelBlock::new);

    private static final Map<BarrelMaterial, Identifier> STAT_FOR_MATERIAL = new ImmutableMap.Builder<BarrelMaterial, Identifier>()
        .put(BarrelMaterial.COPPER, DDStats.OPEN_COPPER_BARREL)
        .put(BarrelMaterial.IRON, DDStats.OPEN_IRON_BARREL)
        .put(BarrelMaterial.GOLD, DDStats.OPEN_GOLD_BARREL)
        .put(BarrelMaterial.DIAMOND, DDStats.OPEN_DIAMOND_BARREL)
        .put(BarrelMaterial.NETHERITE, DDStats.OPEN_NETHERITE_BARREL)
        .build();

    public static final DirectionProperty FACING = Properties.FACING;
    public static final BooleanProperty OPEN = Properties.OPEN;
    private final BarrelMaterial material;

    public MaterialBarrelBlock(Settings settings, BarrelMaterial material) {
        super(settings);
        this.material = material;
        this.setDefaultState(((this.stateManager.getDefaultState()).with(FACING, Direction.NORTH)).with(OPEN, false));
    }

    private MaterialBarrelBlock(Settings settings) {
        super(settings);
        this.material = BarrelMaterial.COPPER;
    }

    @Override
    public ActionResult onUse(BlockState state, World world, BlockPos pos, PlayerEntity player, Hand hand, BlockHitResult hit) {
        if (world.isClient)
            return ActionResult.SUCCESS;
        BlockEntity blockEntity = world.getBlockEntity(pos);
        if (blockEntity instanceof MaterialBarrelBlockEntity) {
            player.openHandledScreen((MaterialBarrelBlockEntity)blockEntity);
            player.incrementStat(STAT_FOR_MATERIAL.get(this.material));
            PiglinBrain.onGuardedBlockInteracted(player, true);
        }
        return ActionResult.CONSUME;
    }

    @Override
    public void onStateReplaced(BlockState state, World world, BlockPos pos, BlockState newState, boolean moved) {
        ItemScatterer.onStateReplaced(state, newState, world, pos);
        super.onStateReplaced(state, world, pos, newState, moved);
    }

    @Override
    public void scheduledTick(BlockState state, ServerWorld world, BlockPos pos, Random random) {
        BlockEntity blockEntity = world.getBlockEntity(pos);
        if (blockEntity instanceof MaterialBarrelBlockEntity) {
            ((MaterialBarrelBlockEntity)blockEntity).tick();
        }
    }

    @Override
    @Nullable
    public BlockEntity createBlockEntity(BlockPos pos, BlockState state) {
        return switch (this.material) {
            case COPPER -> new CopperBarrelBlockEntity(pos, state);
            case IRON -> new IronBarrelBlockEntity(pos, state);
            case GOLD -> new GoldBarrelBlockEntity(pos, state);
            case DIAMOND -> new DiamondBarrelBlockEntity(pos, state);
            case NETHERITE -> new NetheriteBarrelBlockEntity(pos, state);
        };
    }

    @Override
    protected MapCodec<? extends BlockWithEntity> getCodec() {
        return CODEC;
    }

    @Override
    public BlockRenderType getRenderType(BlockState state) {
        return BlockRenderType.MODEL;
    }

    @Override
    public void onPlaced(World world, BlockPos pos, BlockState state, @Nullable LivingEntity placer, ItemStack itemStack) {
        BlockEntity blockEntity;
        if (itemStack.hasCustomName() && (blockEntity = world.getBlockEntity(pos)) instanceof MaterialBarrelBlockEntity) {
            ((MaterialBarrelBlockEntity)blockEntity).setCustomName(itemStack.getName());
        }
    }

    @Override
    public boolean hasComparatorOutput(BlockState state) {
        return true;
    }

    @Override
    public int getComparatorOutput(BlockState state, World world, BlockPos pos) {
        return ScreenHandler.calculateComparatorOutput(world.getBlockEntity(pos));
    }

    @Override
    public BlockState rotate(BlockState state, BlockRotation rotation) {
        return state.with(FACING, rotation.rotate(state.get(FACING)));
    }

    @Override
    public BlockState mirror(BlockState state, BlockMirror mirror) {
        return state.rotate(mirror.getRotation(state.get(FACING)));
    }

    @Override
    protected void appendProperties(StateManager.Builder<Block, BlockState> builder) {
        builder.add(FACING, OPEN);
    }

    @Override
    public BlockState getPlacementState(ItemPlacementContext ctx) {
        return this.getDefaultState().with(FACING, ctx.getPlayerLookDirection().getOpposite());
    }

    public static Optional<Block> getUpgradedBarrel(Block block, BarrelMaterial material) {
        return switch(material) {
            case COPPER -> block.equals(Blocks.BARREL) ? Optional.ofNullable(DDBlocks.COPPER_BARREL) : Optional.empty();
            case IRON -> block.equals(DDBlocks.COPPER_BARREL) ? Optional.ofNullable(DDBlocks.IRON_BARREL) : Optional.empty();
            case GOLD -> block.equals(DDBlocks.IRON_BARREL) ? Optional.ofNullable(DDBlocks.GOLD_BARREL) : Optional.empty();
            case DIAMOND -> block.equals(DDBlocks.GOLD_BARREL) ? Optional.ofNullable(DDBlocks.DIAMOND_BARREL) : Optional.empty();
            case NETHERITE -> block.equals(DDBlocks.DIAMOND_BARREL) ? Optional.ofNullable(DDBlocks.NETHERITE_BARREL) : Optional.empty();
        };
    }

    public enum BarrelMaterial {
        COPPER,
        IRON,
        GOLD,
        DIAMOND,
        NETHERITE
    }
}
