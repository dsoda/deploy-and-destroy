package net.dsoda.deployanddestroy.blocks.block;

import net.minecraft.block.RedstoneLampBlock;
import net.minecraft.util.DyeColor;

public class ColoredLampBlock extends RedstoneLampBlock {

    private final DyeColor color;

    public ColoredLampBlock(Settings settings, DyeColor color) {
        super(settings);
        this.color = color;
    }

    public DyeColor getColor() {
        return this.color;
    }
}
