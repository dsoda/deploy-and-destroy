package net.dsoda.deployanddestroy.blocks.block;

import com.mojang.logging.LogUtils;
import net.dsoda.deployanddestroy.blocks.DDBlockEntities;
import net.dsoda.deployanddestroy.blocks.block.entity.DestroyerBlockEntity;
import net.dsoda.deployanddestroy.stats.DDStats;
import net.dsoda.deployanddestroy.util.tag.DDBlockTags;
import net.fabricmc.fabric.impl.mininglevel.MiningLevelManagerImpl;
import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.block.DispenserBlock;
import net.minecraft.block.entity.BlockEntity;
import net.minecraft.entity.Entity;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Items;
import net.minecraft.item.MiningToolItem;
import net.minecraft.registry.tag.BlockTags;
import net.minecraft.registry.tag.ItemTags;
import net.minecraft.registry.tag.TagKey;
import net.minecraft.server.world.ServerWorld;
import net.minecraft.util.ActionResult;
import net.minecraft.util.Hand;
import net.minecraft.util.ItemScatterer;
import net.minecraft.util.collection.DefaultedList;
import net.minecraft.util.hit.BlockHitResult;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraft.world.WorldEvents;
import net.minecraft.world.event.GameEvent;
import org.jetbrains.annotations.Nullable;
import org.slf4j.Logger;

public class DestroyerBlock extends DispenserBlock {

    private static final Logger LOGGER = LogUtils.getLogger();

    public DestroyerBlock(Settings settings) {
        super(settings);
    }

    @Override
    public BlockEntity createBlockEntity(BlockPos pos, BlockState state) {
        return new DestroyerBlockEntity(pos, state);
    }

    @Override
    protected void dispense(ServerWorld world, BlockState state, BlockPos pos) {
        DestroyerBlockEntity destroyerBlockEntity = world.getBlockEntity(pos, DDBlockEntities.DESTROYER_BLOCK_ENTITY).orElse(null);
        BlockPos facing = pos.offset(state.get(FACING));
        if (destroyerBlockEntity == null) {
            LOGGER.warn("Ignoring dispensing attempt for Dropper without matching block entity at {}", pos);
            return;
        }
        int slot = destroyerBlockEntity.chooseNonEmptySlot(world.getRandom());
        if (slot < 0) {
            world.syncWorldEvent(WorldEvents.DISPENSER_FAILS, pos, 0);
            world.emitGameEvent(GameEvent.BLOCK_ACTIVATE, pos, GameEvent.Emitter.of(destroyerBlockEntity.getCachedState()));
            return;
        }
        this.tryBreakBlockAtPos(world, facing, destroyerBlockEntity);
    }

    @Override
    public ActionResult onUse(BlockState state, World world, BlockPos pos, PlayerEntity player, Hand hand, BlockHitResult hit) {
        if (world.isClient) {
            return ActionResult.SUCCESS;
        }
        BlockEntity blockEntity = world.getBlockEntity(pos);
        if (blockEntity instanceof DestroyerBlockEntity) {
            player.openHandledScreen((DestroyerBlockEntity)blockEntity);
            player.incrementStat(DDStats.INTERACT_WITH_DESTROYER);
        }
        return ActionResult.CONSUME;
    }

    @Override
    public void onStateReplaced(BlockState state, World world, BlockPos pos, BlockState newState, boolean moved) {
        ItemScatterer.onStateReplaced(state, newState, world, pos);
        super.onStateReplaced(state, world, pos, newState, moved);
    }

    @Override
    public void onPlaced(World world, BlockPos pos, BlockState state, LivingEntity placer, ItemStack itemStack) {
        BlockEntity blockEntity;
        if (itemStack.hasCustomName() && (blockEntity = world.getBlockEntity(pos)) instanceof DestroyerBlockEntity) {
            ((DestroyerBlockEntity)blockEntity).setCustomName(itemStack.getName());
        }
    }

    private int getDesiredToolSlot(DefaultedList<ItemStack> inventory, TagKey<Item> tool) {
        for (int i = 0; i < inventory.size() - 1; i++)
            if (inventory.get(i).isIn(tool)) return i;
        return -1;
    }

    private int getDesiredToolSlot(DefaultedList<ItemStack> inventory, Item item) {
        for (int i = 0; i < inventory.size() - 1; i++)
            if (inventory.get(i).isOf(item)) return i;
        return -1;
    }

    private void tryBreakBlockAtPos(World world, BlockPos pos, DestroyerBlockEntity blockEntity) {
        boolean success = true;
        DefaultedList<ItemStack> inventory = blockEntity.method_11282();
        BlockState state = world.getBlockState(pos);
        ItemStack stack;
        int slot = -1;
        if (state.isIn(BlockTags.PICKAXE_MINEABLE))
            slot = this.getDesiredToolSlot(inventory, ItemTags.PICKAXES);
        else if (state.isIn(BlockTags.AXE_MINEABLE))
            slot = this.getDesiredToolSlot(inventory, ItemTags.AXES);
        else if (state.isIn(BlockTags.HOE_MINEABLE))
            slot = this.getDesiredToolSlot(inventory, ItemTags.HOES);
        else if (state.isIn(BlockTags.SHOVEL_MINEABLE))
            slot = this.getDesiredToolSlot(inventory, ItemTags.SHOVELS);
        else if (state.isIn(DDBlockTags.SHEARS_MINEABLE))
            slot = this.getDesiredToolSlot(inventory, Items.SHEARS);
        else
            success = false;

        if (success && slot > -1) {
            stack = blockEntity.getStack(slot);
            if (DestroyerBlock.canMine(state, stack)) {
                world.breakBlock(pos, false);
                DestroyerBlock.dropStacks(state, world, pos, null, null, stack);
                if (stack.damage(1, world.getRandom(), null)) stack.setCount(0);
            }
        }
    }

    public static void dropStacks(BlockState state, World world, BlockPos pos, @Nullable BlockEntity blockEntity, @Nullable Entity entity, ItemStack tool) {
        if (world instanceof ServerWorld) {
            Block.getDroppedStacks(state, (ServerWorld)world, pos, blockEntity, entity, tool).forEach(stack -> Block.dropStack(world, pos, stack));
            state.onStacksDropped((ServerWorld)world, pos, tool, false);
        }
    }

    public static boolean canMine(BlockState state, ItemStack tool) {
        if (tool.getItem() instanceof MiningToolItem)
            return ((MiningToolItem) tool.getItem()).getMaterial().getMiningLevel() >= MiningLevelManagerImpl.getRequiredMiningLevel(state);
        return true;
    }
}
