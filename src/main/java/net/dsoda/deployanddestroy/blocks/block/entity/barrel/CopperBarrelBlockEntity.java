package net.dsoda.deployanddestroy.blocks.block.entity.barrel;

import net.dsoda.deployanddestroy.blocks.DDBlockEntities;
import net.dsoda.deployanddestroy.blocks.block.MaterialBarrelBlock;
import net.dsoda.deployanddestroy.blocks.block.entity.MaterialBarrelBlockEntity;
import net.minecraft.block.BlockState;
import net.minecraft.util.math.BlockPos;

public class CopperBarrelBlockEntity extends MaterialBarrelBlockEntity {
    public CopperBarrelBlockEntity(BlockPos pos, BlockState state) {
        super(DDBlockEntities.COPPER_BARREL_BLOCK_ENTITY, pos, state, MaterialBarrelBlock.BarrelMaterial.COPPER);
    }
}
