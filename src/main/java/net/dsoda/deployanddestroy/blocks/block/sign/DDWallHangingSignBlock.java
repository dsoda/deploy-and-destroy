package net.dsoda.deployanddestroy.blocks.block.sign;

import net.dsoda.deployanddestroy.blocks.block.entity.sign.DDHangingSignBlockEntity;
import net.minecraft.block.BlockState;
import net.minecraft.block.WallHangingSignBlock;
import net.minecraft.block.WoodType;
import net.minecraft.block.entity.BlockEntity;
import net.minecraft.util.math.BlockPos;

public class DDWallHangingSignBlock extends WallHangingSignBlock {

    public DDWallHangingSignBlock(WoodType woodType, Settings settings) {
        super(woodType, settings);
    }

    @Override
    public BlockEntity createBlockEntity(BlockPos pos, BlockState state) {
        return new DDHangingSignBlockEntity(pos, state);
    }
}
