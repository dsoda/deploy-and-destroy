package net.dsoda.deployanddestroy.blocks.block.entity;

import net.dsoda.deployanddestroy.blocks.block.MaterialBarrelBlock;
import net.dsoda.deployanddestroy.client.screens.handler.DDGenericContainerScreenHandler;
import net.minecraft.block.BarrelBlock;
import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.block.entity.BlockEntityType;
import net.minecraft.block.entity.LootableContainerBlockEntity;
import net.minecraft.block.entity.ViewerCountManager;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.inventory.Inventories;
import net.minecraft.inventory.Inventory;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NbtCompound;
import net.minecraft.screen.GenericContainerScreenHandler;
import net.minecraft.screen.ScreenHandler;
import net.minecraft.sound.SoundCategory;
import net.minecraft.sound.SoundEvent;
import net.minecraft.sound.SoundEvents;
import net.minecraft.text.Text;
import net.minecraft.util.collection.DefaultedList;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Vec3i;
import net.minecraft.world.World;

public class MaterialBarrelBlockEntity extends LootableContainerBlockEntity {

    private DefaultedList<ItemStack> inventory;
    private final MaterialBarrelBlock.BarrelMaterial material;
    private int size;
    private Text customName;

    public MaterialBarrelBlockEntity(BlockEntityType<?> type, BlockPos pos, BlockState state, MaterialBarrelBlock.BarrelMaterial material) {
        super(type, pos, state);
        this.material = material;
        this.inventory = getInventorySize();
    }

    private final ViewerCountManager stateManager = new ViewerCountManager(){

        @Override
        protected void onContainerOpen(World world, BlockPos pos, BlockState state) {
            MaterialBarrelBlockEntity.this.playSound(state, SoundEvents.BLOCK_BARREL_OPEN);
            MaterialBarrelBlockEntity.this.setOpen(state, true);
        }

        @Override
        protected void onContainerClose(World world, BlockPos pos, BlockState state) {
            MaterialBarrelBlockEntity.this.playSound(state, SoundEvents.BLOCK_BARREL_CLOSE);
            MaterialBarrelBlockEntity.this.setOpen(state, false);
        }

        @Override
        protected void onViewerCountUpdate(World world, BlockPos pos, BlockState state, int oldViewerCount, int newViewerCount) {
        }

        @Override
        protected boolean isPlayerViewing(PlayerEntity player) {
            if (player.currentScreenHandler instanceof GenericContainerScreenHandler) {
                Inventory inventory = ((GenericContainerScreenHandler)player.currentScreenHandler).getInventory();
                return inventory == MaterialBarrelBlockEntity.this;
            }
            return false;
        }
    };

    @Override
    protected void writeNbt(NbtCompound nbt) {
        super.writeNbt(nbt);
        if (!this.writeLootTable(nbt)) {
            Inventories.writeNbt(nbt, this.inventory);
        }
    }

    @Override
    public void readNbt(NbtCompound nbt) {
        super.readNbt(nbt);
        this.inventory = DefaultedList.ofSize(this.size(), ItemStack.EMPTY);
        if (!this.readLootTable(nbt)) {
            Inventories.readNbt(nbt, this.inventory);
        }
    }

    @Override
    public int size() {
        return this.size;
    }

    @Override
    protected DefaultedList<ItemStack> method_11282() {
        return this.inventory;
    }

    @Override
    protected void setInvStackList(DefaultedList<ItemStack> list) {
        this.inventory = list;
    }

    @Override
    protected Text getContainerName() {
        return switch (this.material) {
            case COPPER -> Text.translatable("block.deployanddestroy.copper_barrel");
            case IRON -> Text.translatable("block.deployanddestroy.iron_barrel");
            case GOLD -> Text.translatable("block.deployanddestroy.gold_barrel");
            case DIAMOND -> Text.translatable("block.deployanddestroy.diamond_barrel");
            case NETHERITE -> Text.translatable("block.deployanddestroy.netherite_barrel");
        };
    }

    @Override
    protected ScreenHandler createScreenHandler(int syncId, PlayerInventory playerInventory) {
        return getScreenHandlerForMaterialType(syncId, playerInventory);
    }

    @Override
    public void onOpen(PlayerEntity player) {
        if (!this.removed && !player.isSpectator()) {
            this.stateManager.openContainer(player, this.getWorld(), this.getPos(), this.getCachedState());
        }
    }

    @Override
    public void onClose(PlayerEntity player) {
        if (!this.removed && !player.isSpectator()) {
            this.stateManager.closeContainer(player, this.getWorld(), this.getPos(), this.getCachedState());
        }
    }

    public void tick() {
        if (!this.removed) {
            this.stateManager.updateViewerCount(this.getWorld(), this.getPos(), this.getCachedState());
        }
    }

    void setOpen(BlockState state, boolean open) {
        this.world.setBlockState(this.getPos(), state.with(BarrelBlock.OPEN, open), Block.NOTIFY_ALL);
    }

    void playSound(BlockState state, SoundEvent soundEvent) {
        Vec3i vec3i = state.get(BarrelBlock.FACING).getVector();
        double d = (double)this.pos.getX() + 0.5 + (double)vec3i.getX() / 2.0;
        double e = (double)this.pos.getY() + 0.5 + (double)vec3i.getY() / 2.0;
        double f = (double)this.pos.getZ() + 0.5 + (double)vec3i.getZ() / 2.0;
        this.world.playSound(null, d, e, f, soundEvent, SoundCategory.BLOCKS, 0.5f, this.world.random.nextFloat() * 0.1f + 0.9f);
    }

    private DefaultedList<ItemStack> getInventorySize() {
        return switch (this.material) {
            case COPPER -> setSizeAndReturnList(36);
            case IRON -> setSizeAndReturnList(45);
            case GOLD -> setSizeAndReturnList(54);
            case DIAMOND -> setSizeAndReturnList(63);
            case NETHERITE -> setSizeAndReturnList(72);
        };
    }

    private DefaultedList<ItemStack> setSizeAndReturnList(int size) {
        this.size = size;
        return DefaultedList.ofSize(size, ItemStack.EMPTY);
    }

    private ScreenHandler getScreenHandlerForMaterialType(int syncId, PlayerInventory playerInventory) {
        return switch (this.material) {
            case COPPER -> DDGenericContainerScreenHandler.createNew9x4(syncId, playerInventory, this);
            case IRON -> DDGenericContainerScreenHandler.createNew9x5(syncId, playerInventory, this);
            case GOLD -> DDGenericContainerScreenHandler.createNew9x6(syncId, playerInventory, this);
            case DIAMOND -> DDGenericContainerScreenHandler.createNew9x7(syncId, playerInventory, this);
            case NETHERITE -> DDGenericContainerScreenHandler.createNew9x8(syncId, playerInventory, this);
        };
    }
}
