package net.dsoda.deployanddestroy.blocks.block;

import com.mojang.serialization.MapCodec;
import net.dsoda.deployanddestroy.util.tag.DDBlockTags;
import net.minecraft.block.*;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Direction;
import net.minecraft.world.WorldAccess;
import net.minecraft.world.WorldView;

public class CopperFireBlock extends AbstractFireBlock {

    public static final MapCodec<CopperFireBlock> CODEC = CopperFireBlock.createCodec(CopperFireBlock::new);

    public CopperFireBlock(Settings settings) {
        super(settings, 1.0f);
    }

    @Override
    public BlockState getStateForNeighborUpdate(BlockState state, Direction direction, BlockState neighborState, WorldAccess world, BlockPos pos, BlockPos neighborPos) {
        if (this.canPlaceAt(state, world, pos)) {
            return this.getDefaultState();
        }
        return Blocks.AIR.getDefaultState();
    }

    @Override
    public boolean canPlaceAt(BlockState state, WorldView world, BlockPos pos) {
        return isCopperBase(world.getBlockState(pos.down()));
    }

    public static boolean isCopperBase(BlockState state) {
        return state.isIn(DDBlockTags.COPPER_FIRE_BASE_BLOCKS);
    }

    @Override
    protected MapCodec<? extends AbstractFireBlock> getCodec() {
        return CODEC;
    }

    @Override
    protected boolean isFlammable(BlockState state) {
        return true;
    }
}
