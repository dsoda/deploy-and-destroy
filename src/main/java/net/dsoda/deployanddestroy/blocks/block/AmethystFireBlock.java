package net.dsoda.deployanddestroy.blocks.block;

import com.mojang.serialization.MapCodec;
import net.dsoda.deployanddestroy.util.tag.DDBlockTags;
import net.minecraft.block.AbstractFireBlock;
import net.minecraft.block.BlockState;
import net.minecraft.block.Blocks;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Direction;
import net.minecraft.world.WorldAccess;
import net.minecraft.world.WorldView;

public class AmethystFireBlock extends AbstractFireBlock {

    public static final MapCodec<AmethystFireBlock> CODEC = AmethystFireBlock.createCodec(AmethystFireBlock::new);

    public AmethystFireBlock(Settings settings) {
        super(settings, 1.0f);
    }

    @Override
    public BlockState getStateForNeighborUpdate(BlockState state, Direction direction, BlockState neighborState, WorldAccess world, BlockPos pos, BlockPos neighborPos) {
        if (this.canPlaceAt(state, world, pos)) {
            return this.getDefaultState();
        }
        return Blocks.AIR.getDefaultState();
    }

    @Override
    public boolean canPlaceAt(BlockState state, WorldView world, BlockPos pos) {
        return isAmethystBase(world.getBlockState(pos.down()));
    }

    public static boolean isAmethystBase(BlockState state) {
        return state.isIn(DDBlockTags.AMETHYST_FIRE_BASE_BLOCKS);
    }

    @Override
    protected MapCodec<? extends AbstractFireBlock> getCodec() {
        return CODEC;
    }

    @Override
    protected boolean isFlammable(BlockState state) {
        return true;
    }
}
