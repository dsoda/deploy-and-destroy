package net.dsoda.deployanddestroy.blocks.block;

import net.dsoda.deployanddestroy.util.tag.DDBlockTags;
import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.block.ObserverBlock;
import net.minecraft.server.world.ServerWorld;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.random.Random;
import net.minecraft.world.World;

public class DetectorBlock extends ObserverBlock {

    public DetectorBlock(Settings settings) {
        super(settings);
    }

    @Override
    public void scheduledTick(BlockState state, ServerWorld world, BlockPos pos, Random random) {
        BlockState facing = world.getBlockState(pos.offset(state.get(FACING)));
        if (facing.isOpaque() && !facing.isIn(DDBlockTags.DETECTOR_EXCLUDE)) {
            world.setBlockState(pos, state.with(POWERED, true), Block.NOTIFY_LISTENERS);
            world.scheduleBlockTick(pos, this, 2);
        } else if (state.get(POWERED)) {
            world.setBlockState(pos, state.with(POWERED, false), Block.NOTIFY_LISTENERS);
        }
        this.updateNeighbors(world, pos, state);
    }

    @Override
    public void onBlockAdded(BlockState state, World world, BlockPos pos, BlockState oldState, boolean notify) {
        if (state.isOf(oldState.getBlock())) return;
        if (!world.isClient()) {
            this.scheduledTick(state, (ServerWorld) world, pos, world.random);
        }
    }
}
