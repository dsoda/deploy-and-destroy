package net.dsoda.deployanddestroy.blocks.block.entity;

import net.dsoda.deployanddestroy.blocks.DDBlockEntities;
import net.dsoda.deployanddestroy.client.screens.handler.DestroyerScreenHandler;
import net.minecraft.block.BlockState;
import net.minecraft.block.entity.LootableContainerBlockEntity;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.inventory.Inventories;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Items;
import net.minecraft.nbt.NbtCompound;
import net.minecraft.registry.tag.ItemTags;
import net.minecraft.screen.ScreenHandler;
import net.minecraft.text.Text;
import net.minecraft.util.collection.DefaultedList;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.random.Random;

public class DestroyerBlockEntity extends LootableContainerBlockEntity {

    private DefaultedList<ItemStack> inventory = DefaultedList.ofSize(9, ItemStack.EMPTY);

    public DestroyerBlockEntity(BlockPos blockPos, BlockState blockState) {
        super(DDBlockEntities.DESTROYER_BLOCK_ENTITY, blockPos, blockState);
    }

    @Override
    public DefaultedList<ItemStack> method_11282() {
        return this.inventory;
    }

    @Override
    protected void setInvStackList(DefaultedList<ItemStack> list) {
        this.inventory = list;
    }

    public int chooseNonEmptySlot(Random random) {
        this.generateLoot(null);
        int i = -1;
        int j = 1;
        for (int k = 0; k < this.inventory.size(); ++k) {
            if (this.inventory.get(k).isEmpty() || random.nextInt(j++) != 0) continue;
            i = k;
        }
        return i;
    }

    @Override
    protected Text getContainerName() {
        return Text.translatable("block.deployanddestroy.destroyer");
    }

    @Override
    protected void writeNbt(NbtCompound nbt) {
        super.writeNbt(nbt);
        Inventories.writeNbt(nbt, this.inventory);
    }

    @Override
    public void readNbt(NbtCompound nbt) {
        super.readNbt(nbt);
        Inventories.readNbt(nbt, this.inventory);
    }

    @Override
    protected ScreenHandler createScreenHandler(int syncId, PlayerInventory playerInventory) {
        return new DestroyerScreenHandler(syncId, playerInventory, this);
    }

    @Override
    public boolean isValid(int slot, ItemStack stack) {
        if (slot < 9) {
            return stack.isIn(ItemTags.PICKAXES)
                || stack.isIn(ItemTags.AXES)
                || stack.isIn(ItemTags.SHOVELS)
                || stack.isIn(ItemTags.HOES)
                || stack.isOf(Items.SHEARS);
        }
        return false;
    }

    @Override
    public int size() {
        return 9;
    }
}
