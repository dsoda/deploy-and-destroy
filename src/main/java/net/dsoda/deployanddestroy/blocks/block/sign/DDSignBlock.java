package net.dsoda.deployanddestroy.blocks.block.sign;

import net.dsoda.deployanddestroy.blocks.block.entity.sign.DDSignBlockEntity;
import net.minecraft.block.BlockState;
import net.minecraft.block.SignBlock;
import net.minecraft.block.WoodType;
import net.minecraft.block.entity.BlockEntity;
import net.minecraft.util.math.BlockPos;

public class DDSignBlock extends SignBlock {

    public DDSignBlock(WoodType woodType, Settings settings) {
        super(woodType, settings);
    }

    @Override
    public BlockEntity createBlockEntity(BlockPos pos, BlockState state) {
        return new DDSignBlockEntity(pos, state);
    }
}
