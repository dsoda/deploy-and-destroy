package net.dsoda.deployanddestroy.blocks;

import net.dsoda.deployanddestroy.DeployAndDestroy;
import net.dsoda.deployanddestroy.blocks.block.*;
import net.dsoda.deployanddestroy.worldgen.DDSaplingGenerators;
import net.dsoda.deployanddestroy.blocks.block.sign.DDHangingSignBlock;
import net.dsoda.deployanddestroy.blocks.block.sign.DDSignBlock;
import net.dsoda.deployanddestroy.blocks.block.sign.DDWallHangingSignBlock;
import net.dsoda.deployanddestroy.blocks.block.sign.DDWallSignBlock;
import net.dsoda.deployanddestroy.particle.DDParticles;
import net.fabricmc.fabric.api.item.v1.FabricItemSettings;
import net.fabricmc.fabric.api.object.builder.v1.block.FabricBlockSettings;
import net.fabricmc.fabric.api.object.builder.v1.block.type.WoodTypeBuilder;
import net.fabricmc.fabric.api.registry.FlammableBlockRegistry;
import net.fabricmc.fabric.api.registry.StrippableBlockRegistry;
import net.minecraft.block.*;
import net.minecraft.block.enums.Instrument;
import net.minecraft.item.BlockItem;
import net.minecraft.item.Item;
import net.minecraft.particle.ParticleTypes;
import net.minecraft.registry.Registries;
import net.minecraft.registry.Registry;
import net.minecraft.sound.BlockSoundGroup;
import net.minecraft.util.DyeColor;
import net.minecraft.util.Identifier;
import net.minecraft.util.math.Direction;

import static net.dsoda.deployanddestroy.DeployAndDestroy.MOD_ID;

public class DDBlocks {
    // Azalea Wood
    public static final Block AZALEA_LOG = registerBlock("azalea_log", new PillarBlock(createLogSettings(MapColor.OFF_WHITE, MapColor.TERRACOTTA_BROWN)));
    public static final Block AZALEA_WOOD = registerBlock("azalea_wood", new PillarBlock(createLogSettings(MapColor.TERRACOTTA_BROWN, MapColor.TERRACOTTA_BROWN)));
    public static final Block STRIPPED_AZALEA_LOG = registerBlock("stripped_azalea_log", new PillarBlock(createLogSettings(MapColor.OFF_WHITE, MapColor.OFF_WHITE)));
    public static final Block STRIPPED_AZALEA_WOOD = registerBlock("stripped_azalea_wood", new PillarBlock(createLogSettings(MapColor.OFF_WHITE, MapColor.OFF_WHITE)));
    public static final Block AZALEA_PLANKS = registerBlock("azalea_planks", new Block(FabricBlockSettings.copyOf(Blocks.OAK_PLANKS).mapColor(MapColor.OFF_WHITE)));
    public static final Block AZALEA_SLAB = registerBlock("azalea_slab", new SlabBlock(FabricBlockSettings.copyOf(Blocks.OAK_SLAB).mapColor(MapColor.OFF_WHITE)));
    public static final Block AZALEA_STAIRS = registerBlock("azalea_stairs", new StairsBlock(DDBlocks.AZALEA_PLANKS.getDefaultState(), FabricBlockSettings.copyOf(Blocks.OAK_PLANKS).mapColor(MapColor.OFF_WHITE)));
    public static final Block AZALEA_FENCE = registerBlock("azalea_fence", new FenceBlock(FabricBlockSettings.copyOf(Blocks.OAK_FENCE).mapColor(MapColor.OFF_WHITE)));
    public static final Block AZALEA_FENCE_GATE = registerBlock("azalea_fence_gate", new FenceGateBlock(WoodTypes.AZALEA, FabricBlockSettings.copyOf(Blocks.OAK_FENCE).mapColor(MapColor.OFF_WHITE)));
    public static final Block AZALEA_PRESSURE_PLATE = registerBlock("azalea_pressure_plate", new PressurePlateBlock(BlockSetTypes.AZALEA, FabricBlockSettings.copyOf(Blocks.OAK_PRESSURE_PLATE).mapColor(MapColor.OFF_WHITE)));
    public static final Block AZALEA_BUTTON = registerBlock("azalea_button", new ButtonBlock(BlockSetTypes.AZALEA, 30, FabricBlockSettings.copyOf(Blocks.OAK_BUTTON)));
    public static final Block AZALEA_DOOR = registerBlock("azalea_door", new DoorBlock(BlockSetTypes.AZALEA, FabricBlockSettings.copyOf(Blocks.OAK_DOOR).mapColor(MapColor.OFF_WHITE)));
    public static final Block AZALEA_TRAPDOOR = registerBlock("azalea_trapdoor", new TrapdoorBlock(BlockSetTypes.AZALEA, FabricBlockSettings.copyOf(Blocks.OAK_TRAPDOOR).mapColor(MapColor.OFF_WHITE)));
    public static final Block AZALEA_SIGN = Registry.register(Registries.BLOCK, new Identifier(MOD_ID, "azalea_sign"), new DDSignBlock(WoodTypes.AZALEA, FabricBlockSettings.copyOf(Blocks.OAK_SIGN).mapColor(MapColor.OFF_WHITE)));
    public static final Block AZALEA_WALL_SIGN = Registry.register(Registries.BLOCK, new Identifier(MOD_ID, "azalea_wall_sign"), new DDWallSignBlock(WoodTypes.AZALEA, FabricBlockSettings.copyOf(Blocks.OAK_WALL_SIGN).mapColor(MapColor.OFF_WHITE).dropsLike(AZALEA_SIGN)));
    public static final Block AZALEA_HANGING_SIGN = Registry.register(Registries.BLOCK, new Identifier(MOD_ID, "azalea_hanging_sign"), new DDHangingSignBlock(WoodTypes.AZALEA, FabricBlockSettings.copyOf(Blocks.OAK_HANGING_SIGN).mapColor(MapColor.OFF_WHITE)));
    public static final Block AZALEA_WALL_HANGING_SIGN = Registry.register(Registries.BLOCK, new Identifier(MOD_ID, "azalea_wall_hanging_sign"), new DDWallHangingSignBlock(WoodTypes.AZALEA, FabricBlockSettings.copyOf(Blocks.OAK_WALL_HANGING_SIGN).mapColor(MapColor.OFF_WHITE).dropsLike(AZALEA_HANGING_SIGN)));

    // Swamp Wood
    public static final Block SWAMP_SAPLING = registerBlock("swamp_sapling", new SaplingBlock(DDSaplingGenerators.SWAMP, FabricBlockSettings.copyOf(Blocks.OAK_SAPLING)));
    public static final Block POTTED_SWAMP_SAPLING = Registry.register(Registries.BLOCK, new Identifier(MOD_ID, "potted_swamp_sapling"), new FlowerPotBlock(SWAMP_SAPLING, FabricBlockSettings.copyOf(Blocks.POTTED_OAK_SAPLING).nonOpaque()));
    public static final Block SWAMP_LEAVES = registerBlock("swamp_leaves", Blocks.createLeavesBlock(BlockSoundGroup.GRASS));
    public static final Block SWAMP_LOG = registerBlock("swamp_log", new PillarBlock(createLogSettings(MapColor.PALE_GREEN, MapColor.BROWN)));
    public static final Block SWAMP_WOOD = registerBlock("swamp_wood", new PillarBlock(createLogSettings(MapColor.BROWN, MapColor.BROWN)));
    public static final Block STRIPPED_SWAMP_LOG = registerBlock("stripped_swamp_log", new PillarBlock(createLogSettings(MapColor.PALE_GREEN, MapColor.PALE_GREEN)));
    public static final Block STRIPPED_SWAMP_WOOD = registerBlock("stripped_swamp_wood", new PillarBlock(createLogSettings(MapColor.PALE_GREEN, MapColor.PALE_GREEN)));
    public static final Block SWAMP_PLANKS = registerBlock("swamp_planks", new Block(FabricBlockSettings.copyOf(Blocks.OAK_PLANKS).mapColor(MapColor.PALE_GREEN)));
    public static final Block SWAMP_SLAB = registerBlock("swamp_slab", new SlabBlock(FabricBlockSettings.copyOf(Blocks.OAK_SLAB).mapColor(MapColor.PALE_GREEN)));
    public static final Block SWAMP_STAIRS = registerBlock("swamp_stairs", new StairsBlock(DDBlocks.AZALEA_PLANKS.getDefaultState(), FabricBlockSettings.copyOf(Blocks.OAK_PLANKS).mapColor(MapColor.PALE_GREEN)));
    public static final Block SWAMP_FENCE = registerBlock("swamp_fence", new FenceBlock(FabricBlockSettings.copyOf(Blocks.OAK_FENCE).mapColor(MapColor.PALE_GREEN)));
    public static final Block SWAMP_FENCE_GATE = registerBlock("swamp_fence_gate", new FenceGateBlock(WoodTypes.SWAMP, FabricBlockSettings.copyOf(Blocks.OAK_FENCE).mapColor(MapColor.PALE_GREEN)));
    public static final Block SWAMP_PRESSURE_PLATE = registerBlock("swamp_pressure_plate", new PressurePlateBlock(BlockSetTypes.SWAMP, FabricBlockSettings.copyOf(Blocks.OAK_PRESSURE_PLATE).mapColor(MapColor.PALE_GREEN)));
    public static final Block SWAMP_BUTTON = registerBlock("swamp_button", new ButtonBlock(BlockSetTypes.SWAMP, 30, FabricBlockSettings.copyOf(Blocks.OAK_BUTTON).mapColor(MapColor.PALE_GREEN)));
    public static final Block SWAMP_DOOR = registerBlock("swamp_door", new DoorBlock(BlockSetTypes.SWAMP, FabricBlockSettings.copyOf(Blocks.OAK_DOOR).mapColor(MapColor.PALE_GREEN)));
    public static final Block SWAMP_TRAPDOOR = registerBlock("swamp_trapdoor", new TrapdoorBlock(BlockSetTypes.SWAMP, FabricBlockSettings.copyOf(Blocks.OAK_TRAPDOOR).mapColor(MapColor.PALE_GREEN)));
    public static final Block SWAMP_SIGN = Registry.register(Registries.BLOCK, new Identifier(MOD_ID, "swamp_sign"), new DDSignBlock(WoodTypes.SWAMP, FabricBlockSettings.copyOf(Blocks.OAK_SIGN).mapColor(MapColor.OFF_WHITE)));
    public static final Block SWAMP_WALL_SIGN = Registry.register(Registries.BLOCK, new Identifier(MOD_ID, "swamp_wall_sign"), new DDWallSignBlock(WoodTypes.SWAMP, FabricBlockSettings.copyOf(Blocks.OAK_WALL_SIGN).mapColor(MapColor.OFF_WHITE).dropsLike(SWAMP_SIGN)));
    public static final Block SWAMP_HANGING_SIGN = Registry.register(Registries.BLOCK, new Identifier(MOD_ID, "swamp_hanging_sign"), new DDHangingSignBlock(WoodTypes.SWAMP, FabricBlockSettings.copyOf(Blocks.OAK_HANGING_SIGN).mapColor(MapColor.OFF_WHITE)));
    public static final Block SWAMP_WALL_HANGING_SIGN = Registry.register(Registries.BLOCK, new Identifier(MOD_ID, "swamp_wall_hanging_sign"), new DDWallHangingSignBlock(WoodTypes.SWAMP, FabricBlockSettings.copyOf(Blocks.OAK_WALL_HANGING_SIGN).mapColor(MapColor.OFF_WHITE).dropsLike(SWAMP_HANGING_SIGN)));

    // Colored Redstone Lamps
    public static final Block WHITE_LAMP = registerBlock("white_lamp", new ColoredLampBlock(FabricBlockSettings.copyOf(Blocks.REDSTONE_LAMP).mapColor(DyeColor.WHITE), DyeColor.WHITE));
    public static final Block ORANGE_LAMP = registerBlock("orange_lamp", new ColoredLampBlock(FabricBlockSettings.copyOf(Blocks.REDSTONE_LAMP).mapColor(DyeColor.ORANGE), DyeColor.ORANGE));
    public static final Block MAGENTA_LAMP = registerBlock("magenta_lamp", new ColoredLampBlock(FabricBlockSettings.copyOf(Blocks.REDSTONE_LAMP).mapColor(DyeColor.MAGENTA), DyeColor.MAGENTA));
    public static final Block LIGHT_BLUE_LAMP = registerBlock("light_blue_lamp", new ColoredLampBlock(FabricBlockSettings.copyOf(Blocks.REDSTONE_LAMP).mapColor(DyeColor.LIGHT_BLUE), DyeColor.LIGHT_BLUE));
    public static final Block YELLOW_LAMP = registerBlock("yellow_lamp", new ColoredLampBlock(FabricBlockSettings.copyOf(Blocks.REDSTONE_LAMP).mapColor(DyeColor.YELLOW), DyeColor.YELLOW));
    public static final Block LIME_LAMP = registerBlock("lime_lamp", new ColoredLampBlock(FabricBlockSettings.copyOf(Blocks.REDSTONE_LAMP).mapColor(DyeColor.LIME), DyeColor.LIME));
    public static final Block PINK_LAMP = registerBlock("pink_lamp", new ColoredLampBlock(FabricBlockSettings.copyOf(Blocks.REDSTONE_LAMP).mapColor(DyeColor.PINK), DyeColor.PINK));
    public static final Block GRAY_LAMP = registerBlock("gray_lamp", new ColoredLampBlock(FabricBlockSettings.copyOf(Blocks.REDSTONE_LAMP).mapColor(DyeColor.GRAY), DyeColor.GRAY));
    public static final Block LIGHT_GRAY_LAMP = registerBlock("light_gray_lamp", new ColoredLampBlock(FabricBlockSettings.copyOf(Blocks.REDSTONE_LAMP).mapColor(DyeColor.LIGHT_GRAY), DyeColor.LIGHT_GRAY));
    public static final Block CYAN_LAMP = registerBlock("cyan_lamp", new ColoredLampBlock(FabricBlockSettings.copyOf(Blocks.REDSTONE_LAMP).mapColor(DyeColor.CYAN), DyeColor.CYAN));
    public static final Block PURPLE_LAMP = registerBlock("purple_lamp", new ColoredLampBlock(FabricBlockSettings.copyOf(Blocks.REDSTONE_LAMP).mapColor(DyeColor.PURPLE), DyeColor.PURPLE));
    public static final Block BLUE_LAMP = registerBlock("blue_lamp", new ColoredLampBlock(FabricBlockSettings.copyOf(Blocks.REDSTONE_LAMP).mapColor(DyeColor.BLUE), DyeColor.BLUE));
    public static final Block BROWN_LAMP = registerBlock("brown_lamp", new ColoredLampBlock(FabricBlockSettings.copyOf(Blocks.REDSTONE_LAMP).mapColor(DyeColor.BROWN), DyeColor.BROWN));
    public static final Block GREEN_LAMP = registerBlock("green_lamp", new ColoredLampBlock(FabricBlockSettings.copyOf(Blocks.REDSTONE_LAMP).mapColor(DyeColor.GREEN), DyeColor.GREEN));
    public static final Block RED_LAMP = registerBlock("red_lamp", new ColoredLampBlock(FabricBlockSettings.copyOf(Blocks.REDSTONE_LAMP).mapColor(DyeColor.RED), DyeColor.RED));
    public static final Block BLACK_LAMP = registerBlock("black_lamp", new ColoredLampBlock(FabricBlockSettings.copyOf(Blocks.REDSTONE_LAMP).mapColor(DyeColor.BLACK), DyeColor.BLACK));

    // Ore
    public static final Block END_REDSTONE_ORE = registerBlock("end_redstone_ore", new RedstoneOreBlock(FabricBlockSettings.copyOf(Blocks.END_STONE).strength(4.5f, 7.5f)));

    // Utility Blocks
    public static final Block DEPLOYER = registerBlock("deployer", new DeployerBlock(FabricBlockSettings.copyOf(Blocks.DISPENSER)));
    public static final Block DESTROYER = registerBlock("destroyer", new DestroyerBlock(FabricBlockSettings.copyOf(Blocks.DISPENSER)));
    public static final Block DETECTOR = registerBlock("detector", new DetectorBlock(FabricBlockSettings.copyOf(Blocks.OBSERVER)));
    public static final Block COPPER_BARREL = registerBlock("copper_barrel", new MaterialBarrelBlock(FabricBlockSettings.copyOf(Blocks.COPPER_BLOCK), MaterialBarrelBlock.BarrelMaterial.COPPER));
    public static final Block IRON_BARREL = registerBlock("iron_barrel", new MaterialBarrelBlock(FabricBlockSettings.copyOf(Blocks.IRON_BLOCK), MaterialBarrelBlock.BarrelMaterial.IRON));
    public static final Block GOLD_BARREL = registerBlock("gold_barrel", new MaterialBarrelBlock(FabricBlockSettings.copyOf(Blocks.GOLD_BLOCK), MaterialBarrelBlock.BarrelMaterial.GOLD));
    public static final Block DIAMOND_BARREL = registerBlock("diamond_barrel", new MaterialBarrelBlock(FabricBlockSettings.copyOf(Blocks.DIAMOND_BLOCK), MaterialBarrelBlock.BarrelMaterial.DIAMOND));
    public static final Block NETHERITE_BARREL = registerBlock("netherite_barrel", new MaterialBarrelBlock(FabricBlockSettings.copyOf(Blocks.NETHERITE_BLOCK), MaterialBarrelBlock.BarrelMaterial.NETHERITE), true);
    public static final Block WOODCUTTER = registerBlock("woodcutter", new WoodcutterBlock(FabricBlockSettings.copyOf(Blocks.STONECUTTER)));
    public static final Block FAST_HOPPER = registerBlock("fast_hopper", new FastHopperBlock(FabricBlockSettings.copyOf(Blocks.HOPPER).mapColor(MapColor.GOLD)));

    public static final Block COPPER_PRESSURE_PLATE = registerBlock("copper_pressure_plate", new OxidizablePressurePlateBlock(FabricBlockSettings.copyOf(Blocks.HEAVY_WEIGHTED_PRESSURE_PLATE).mapColor(MapColor.ORANGE), Oxidizable.OxidationLevel.UNAFFECTED));
    public static final Block EXPOSED_COPPER_PRESSURE_PLATE = registerBlock("exposed_copper_pressure_plate", new OxidizablePressurePlateBlock(FabricBlockSettings.copyOf(Blocks.HEAVY_WEIGHTED_PRESSURE_PLATE).mapColor(MapColor.TERRACOTTA_LIGHT_GRAY), Oxidizable.OxidationLevel.EXPOSED));
    public static final Block WEATHERED_COPPER_PRESSURE_PLATE = registerBlock("weathered_copper_pressure_plate", new OxidizablePressurePlateBlock(FabricBlockSettings.copyOf(Blocks.HEAVY_WEIGHTED_PRESSURE_PLATE).mapColor(MapColor.DARK_AQUA), Oxidizable.OxidationLevel.WEATHERED));
    public static final Block OXIDIZED_COPPER_PRESSURE_PLATE = registerBlock("oxidized_copper_pressure_plate", new OxidizablePressurePlateBlock(FabricBlockSettings.copyOf(Blocks.HEAVY_WEIGHTED_PRESSURE_PLATE).mapColor(MapColor.TEAL), Oxidizable.OxidationLevel.OXIDIZED));
    public static final Block WAXED_COPPER_PRESSURE_PLATE = registerBlock("waxed_copper_pressure_plate", new DDPressurePlateBlock(FabricBlockSettings.copyOf(Blocks.HEAVY_WEIGHTED_PRESSURE_PLATE).mapColor(MapColor.ORANGE)));
    public static final Block WAXED_EXPOSED_COPPER_PRESSURE_PLATE = registerBlock("waxed_exposed_copper_pressure_plate", new DDPressurePlateBlock(FabricBlockSettings.copyOf(Blocks.HEAVY_WEIGHTED_PRESSURE_PLATE).mapColor(MapColor.TERRACOTTA_LIGHT_GRAY)));
    public static final Block WAXED_WEATHERED_COPPER_PRESSURE_PLATE = registerBlock("waxed_weathered_copper_pressure_plate", new DDPressurePlateBlock(FabricBlockSettings.copyOf(Blocks.HEAVY_WEIGHTED_PRESSURE_PLATE).mapColor(MapColor.DARK_AQUA)));
    public static final Block WAXED_OXIDIZED_COPPER_PRESSURE_PLATE = registerBlock("waxed_oxidized_copper_pressure_plate", new DDPressurePlateBlock(FabricBlockSettings.copyOf(Blocks.HEAVY_WEIGHTED_PRESSURE_PLATE).mapColor(MapColor.TEAL)));

    // Building Blocks
    public static final Block SANDSTONE_BRICKS = registerBlock("sandstone_bricks", new Block(FabricBlockSettings.copyOf(Blocks.SMOOTH_SANDSTONE)));
    public static final Block SANDSTONE_BRICK_STAIRS = registerBlock("sandstone_brick_stairs", new StairsBlock(DDBlocks.SANDSTONE_BRICKS.getDefaultState(), FabricBlockSettings.copyOf(Blocks.SMOOTH_SANDSTONE_STAIRS)));
    public static final Block SANDSTONE_BRICK_SLAB = registerBlock("sandstone_brick_slab", new SlabBlock(FabricBlockSettings.copyOf(Blocks.SMOOTH_SANDSTONE_SLAB)));
    public static final Block SANDSTONE_BRICK_WALL = registerBlock("sandstone_brick_wall", new WallBlock(FabricBlockSettings.copyOf(Blocks.SMOOTH_SANDSTONE)));
    public static final Block CRACKED_SANDSTONE_BRICKS = registerBlock("cracked_sandstone_bricks", new Block(FabricBlockSettings.copyOf(Blocks.SMOOTH_SANDSTONE)));
    public static final Block CRACKED_SANDSTONE_BRICK_STAIRS = registerBlock("cracked_sandstone_brick_stairs", new StairsBlock(DDBlocks.CRACKED_SANDSTONE_BRICKS.getDefaultState(), FabricBlockSettings.copyOf(Blocks.SMOOTH_SANDSTONE_STAIRS)));
    public static final Block CRACKED_SANDSTONE_BRICK_SLAB = registerBlock("cracked_sandstone_brick_slab", new SlabBlock(FabricBlockSettings.copyOf(Blocks.SMOOTH_SANDSTONE_SLAB)));
    public static final Block CRACKED_SANDSTONE_BRICK_WALL = registerBlock("cracked_sandstone_brick_wall", new WallBlock(FabricBlockSettings.copyOf(Blocks.SMOOTH_SANDSTONE)));
    public static final Block MOSSY_SANDSTONE_BRICKS = registerBlock("mossy_sandstone_bricks", new Block(FabricBlockSettings.copyOf(Blocks.SMOOTH_SANDSTONE)));
    public static final Block MOSSY_SANDSTONE_BRICK_STAIRS = registerBlock("mossy_sandstone_brick_stairs", new StairsBlock(DDBlocks.MOSSY_SANDSTONE_BRICKS.getDefaultState(), FabricBlockSettings.copyOf(Blocks.SMOOTH_SANDSTONE_STAIRS)));
    public static final Block MOSSY_SANDSTONE_BRICK_SLAB = registerBlock("mossy_sandstone_brick_slab", new SlabBlock(FabricBlockSettings.copyOf(Blocks.SMOOTH_SANDSTONE_SLAB)));
    public static final Block MOSSY_SANDSTONE_BRICK_WALL = registerBlock("mossy_sandstone_brick_wall", new WallBlock(FabricBlockSettings.copyOf(Blocks.SMOOTH_SANDSTONE)));
    public static final Block CHISELED_POLISHED_SANDSTONE = registerBlock("chiseled_polished_sandstone", new Block(FabricBlockSettings.copyOf(Blocks.SMOOTH_SANDSTONE)));
    public static final Block POLISHED_SANDSTONE = registerBlock("polished_sandstone", new Block(FabricBlockSettings.copyOf(Blocks.SMOOTH_SANDSTONE)));
    public static final Block POLISHED_SANDSTONE_STAIRS = registerBlock("polished_sandstone_stairs", new StairsBlock(DDBlocks.POLISHED_SANDSTONE.getDefaultState(), FabricBlockSettings.copyOf(Blocks.SMOOTH_SANDSTONE_STAIRS)));
    public static final Block POLISHED_SANDSTONE_SLAB = registerBlock("polished_sandstone_slab", new SlabBlock(FabricBlockSettings.copyOf(Blocks.SMOOTH_SANDSTONE_SLAB)));
    public static final Block POLISHED_SANDSTONE_WALL = registerBlock("polished_sandstone_wall", new WallBlock(FabricBlockSettings.copyOf(Blocks.SMOOTH_SANDSTONE)));
    public static final Block RED_SANDSTONE_BRICKS = registerBlock("red_sandstone_bricks", new Block(FabricBlockSettings.copyOf(Blocks.SMOOTH_RED_SANDSTONE)));
    public static final Block RED_SANDSTONE_BRICK_STAIRS = registerBlock("red_sandstone_brick_stairs", new StairsBlock(DDBlocks.RED_SANDSTONE_BRICKS.getDefaultState(), FabricBlockSettings.copyOf(Blocks.SMOOTH_RED_SANDSTONE_STAIRS)));
    public static final Block RED_SANDSTONE_BRICK_SLAB = registerBlock("red_sandstone_brick_slab", new SlabBlock(FabricBlockSettings.copyOf(Blocks.SMOOTH_RED_SANDSTONE_SLAB)));
    public static final Block RED_SANDSTONE_BRICK_WALL = registerBlock("red_sandstone_brick_wall", new WallBlock(FabricBlockSettings.copyOf(Blocks.SMOOTH_RED_SANDSTONE)));
    public static final Block CRACKED_RED_SANDSTONE_BRICKS = registerBlock("cracked_red_sandstone_bricks", new Block(FabricBlockSettings.copyOf(Blocks.SMOOTH_RED_SANDSTONE)));
    public static final Block CRACKED_RED_SANDSTONE_BRICK_STAIRS = registerBlock("cracked_red_sandstone_brick_stairs", new StairsBlock(DDBlocks.CRACKED_RED_SANDSTONE_BRICKS.getDefaultState(), FabricBlockSettings.copyOf(Blocks.SMOOTH_RED_SANDSTONE_STAIRS)));
    public static final Block CRACKED_RED_SANDSTONE_BRICK_SLAB = registerBlock("cracked_red_sandstone_brick_slab", new SlabBlock(FabricBlockSettings.copyOf(Blocks.SMOOTH_RED_SANDSTONE_SLAB)));
    public static final Block CRACKED_RED_SANDSTONE_BRICK_WALL = registerBlock("cracked_red_sandstone_brick_wall", new WallBlock(FabricBlockSettings.copyOf(Blocks.SMOOTH_RED_SANDSTONE)));
    public static final Block MOSSY_RED_SANDSTONE_BRICKS = registerBlock("mossy_red_sandstone_bricks", new Block(FabricBlockSettings.copyOf(Blocks.SMOOTH_RED_SANDSTONE)));
    public static final Block MOSSY_RED_SANDSTONE_BRICK_STAIRS = registerBlock("mossy_red_sandstone_brick_stairs", new StairsBlock(DDBlocks.MOSSY_RED_SANDSTONE_BRICKS.getDefaultState(), FabricBlockSettings.copyOf(Blocks.SMOOTH_RED_SANDSTONE_STAIRS)));
    public static final Block MOSSY_RED_SANDSTONE_BRICK_SLAB = registerBlock("mossy_red_sandstone_brick_slab", new SlabBlock(FabricBlockSettings.copyOf(Blocks.SMOOTH_RED_SANDSTONE_SLAB)));
    public static final Block MOSSY_RED_SANDSTONE_BRICK_WALL = registerBlock("mossy_red_sandstone_brick_wall", new WallBlock(FabricBlockSettings.copyOf(Blocks.SMOOTH_RED_SANDSTONE)));
    public static final Block CHISELED_POLISHED_RED_SANDSTONE = registerBlock("chiseled_polished_red_sandstone", new Block(FabricBlockSettings.copyOf(Blocks.SMOOTH_RED_SANDSTONE)));
    public static final Block POLISHED_RED_SANDSTONE = registerBlock("polished_red_sandstone", new Block(FabricBlockSettings.copyOf(Blocks.SMOOTH_RED_SANDSTONE)));
    public static final Block POLISHED_RED_SANDSTONE_STAIRS = registerBlock("polished_red_sandstone_stairs", new StairsBlock(DDBlocks.POLISHED_RED_SANDSTONE.getDefaultState(), FabricBlockSettings.copyOf(Blocks.SMOOTH_RED_SANDSTONE_STAIRS)));
    public static final Block POLISHED_RED_SANDSTONE_SLAB = registerBlock("polished_red_sandstone_slab", new SlabBlock(FabricBlockSettings.copyOf(Blocks.SMOOTH_RED_SANDSTONE_SLAB)));
    public static final Block POLISHED_RED_SANDSTONE_WALL = registerBlock("polished_red_sandstone_wall", new WallBlock(FabricBlockSettings.copyOf(Blocks.SMOOTH_RED_SANDSTONE)));
    public static final Block POLISHED_OBSIDIAN = registerBlock("polished_obsidian", new Block(FabricBlockSettings.copyOf(Blocks.OBSIDIAN)));
    public static final Block WAXED_POLISHED_OBSIDIAN = registerBlock("waxed_polished_obsidian", new Block(FabricBlockSettings.copyOf(Blocks.OBSIDIAN).slipperiness(0.98f)));
    public static final Block GRASS_BUNDLE = registerBlock("grass_bundle", new HayBlock(FabricBlockSettings.copyOf(Blocks.HAY_BLOCK).mapColor(MapColor.GREEN)));
    public static final Block NETHERRACK_STAIRS = registerBlock("netherrack_stairs", new StairsBlock(Blocks.NETHERRACK.getDefaultState(), FabricBlockSettings.copyOf(Blocks.NETHERRACK)));
    public static final Block NETHERRACK_SLAB = registerBlock("netherrack_slab", new SlabBlock(FabricBlockSettings.copyOf(Blocks.NETHERRACK)));
    public static final Block NETHERRACK_WALL = registerBlock("netherrack_wall", new WallBlock(FabricBlockSettings.copyOf(Blocks.NETHERRACK)));
    public static final Block CHARCOAL_BLOCK = registerBlock("charcoal_block", new Block(FabricBlockSettings.copyOf(Blocks.COAL_BLOCK).mapColor(MapColor.DEEPSLATE_GRAY)));
    public static final Block BLAZING_COAL_BLOCK = registerBlock("blazing_coal_block", new Block(FabricBlockSettings.copyOf(Blocks.COAL_BLOCK).mapColor(MapColor.ORANGE)));
    public static final Block SMOOTH_STONE_STAIRS = registerBlock("smooth_stone_stairs", new StairsBlock(Blocks.SMOOTH_STONE.getDefaultState(), FabricBlockSettings.copyOf(Blocks.SMOOTH_STONE)));
    public static final Block SMOOTH_STONE_WALL = registerBlock("smooth_stone_wall", new WallBlock(FabricBlockSettings.copyOf(Blocks.SMOOTH_STONE)));
    public static final Block STONE_WALL = registerBlock("stone_wall", new WallBlock(FabricBlockSettings.copyOf(Blocks.STONE)));
    public static final Block POLISHED_ANDESITE_WALL = registerBlock("polished_andesite_wall", new WallBlock(FabricBlockSettings.copyOf(Blocks.POLISHED_ANDESITE)));
    public static final Block POLISHED_DIORITE_WALL = registerBlock("polished_diorite_wall", new WallBlock(FabricBlockSettings.copyOf(Blocks.POLISHED_DIORITE)));
    public static final Block POLISHED_GRANITE_WALL = registerBlock("polished_granite_wall", new WallBlock(FabricBlockSettings.copyOf(Blocks.POLISHED_GRANITE)));
    public static final Block SMOOTH_SANDSTONE_WALL = registerBlock("smooth_sandstone_wall", new WallBlock(FabricBlockSettings.copyOf(Blocks.SMOOTH_SANDSTONE)));
    public static final Block SMOOTH_RED_SANDSTONE_WALL = registerBlock("smooth_red_sandstone_wall", new WallBlock(FabricBlockSettings.copyOf(Blocks.SMOOTH_RED_SANDSTONE)));
    public static final Block PRISMARINE_BRICK_WALL = registerBlock("prismarine_brick_wall", new WallBlock(FabricBlockSettings.copyOf(Blocks.PRISMARINE_BRICKS)));
    public static final Block DARK_PRISMARINE_WALL = registerBlock("dark_prismarine_wall", new WallBlock(FabricBlockSettings.copyOf(Blocks.DARK_PRISMARINE)));
    public static final Block PURPUR_WALL = registerBlock("purpur_wall", new WallBlock(FabricBlockSettings.copyOf(Blocks.PURPUR_BLOCK)));
    public static final Block CRACKED_STONE_BRICK_STAIRS = registerBlock("cracked_stone_brick_stairs", new StairsBlock(Blocks.CRACKED_STONE_BRICKS.getDefaultState(), FabricBlockSettings.copyOf(Blocks.CRACKED_STONE_BRICKS)));
    public static final Block CRACKED_STONE_BRICK_SLAB = registerBlock("cracked_stone_brick_slab", new SlabBlock(FabricBlockSettings.copyOf(Blocks.CRACKED_STONE_BRICKS)));
    public static final Block CRACKED_STONE_BRICK_WALL = registerBlock("cracked_stone_brick_wall", new WallBlock(FabricBlockSettings.copyOf(Blocks.CRACKED_STONE_BRICKS)));
    public static final Block CRACKED_BRICKS = registerBlock("cracked_bricks", new Block(FabricBlockSettings.copyOf(Blocks.BRICKS)));
    public static final Block CRACKED_BRICK_STAIRS = registerBlock("cracked_brick_stairs", new StairsBlock(Blocks.BRICKS.getDefaultState(), FabricBlockSettings.copyOf(Blocks.BRICKS)));
    public static final Block CRACKED_BRICK_SLAB = registerBlock("cracked_brick_slab", new SlabBlock(FabricBlockSettings.copyOf(Blocks.BRICKS)));
    public static final Block CRACKED_BRICK_WALL = registerBlock("cracked_brick_wall", new WallBlock(FabricBlockSettings.copyOf(Blocks.BRICKS)));
    public static final Block MOSSY_BRICKS = registerBlock("mossy_bricks", new Block(FabricBlockSettings.copyOf(Blocks.BRICKS)));
    public static final Block MOSSY_BRICK_STAIRS = registerBlock("mossy_brick_stairs", new StairsBlock(Blocks.BRICKS.getDefaultState(), FabricBlockSettings.copyOf(Blocks.BRICKS)));
    public static final Block MOSSY_BRICK_SLAB = registerBlock("mossy_brick_slab", new SlabBlock(FabricBlockSettings.copyOf(Blocks.BRICKS)));
    public static final Block MOSSY_BRICK_WALL = registerBlock("mossy_brick_wall", new WallBlock(FabricBlockSettings.copyOf(Blocks.BRICKS)));
    public static final Block QUARTZ_WALL = registerBlock("quartz_wall", new WallBlock(FabricBlockSettings.copyOf(Blocks.QUARTZ_BLOCK)));
    public static final Block SMOOTH_QUARTZ_WALL = registerBlock("smooth_quartz_wall", new WallBlock(FabricBlockSettings.copyOf(Blocks.SMOOTH_QUARTZ)));
    public static final Block QUARTZ_BRICK_WALL = registerBlock("quartz_brick_wall", new WallBlock(FabricBlockSettings.copyOf(Blocks.QUARTZ_BRICKS)));
    public static final Block QUARTZ_BRICK_STAIRS = registerBlock("quartz_brick_stairs", new StairsBlock(Blocks.QUARTZ_BRICKS.getDefaultState(), FabricBlockSettings.copyOf(Blocks.QUARTZ_BRICKS)));
    public static final Block QUARTZ_BRICK_SLAB = registerBlock("quartz_brick_slab", new SlabBlock(FabricBlockSettings.copyOf(Blocks.QUARTZ_BRICKS)));
    public static final Block CHARRED_QUARTZ_BLOCK = registerBlock("charred_quartz_block", new Block(FabricBlockSettings.copyOf(Blocks.QUARTZ_BLOCK).mapColor(DyeColor.BLACK)));
    public static final Block CHARRED_QUARTZ_STAIRS = registerBlock("charred_quartz_stairs", new StairsBlock(CHARRED_QUARTZ_BLOCK.getDefaultState(), FabricBlockSettings.copyOf(Blocks.QUARTZ_STAIRS).mapColor(DyeColor.BLACK)));
    public static final Block CHARRED_QUARTZ_SLAB = registerBlock("charred_quartz_slab", new SlabBlock(FabricBlockSettings.copyOf(Blocks.QUARTZ_SLAB).mapColor(DyeColor.BLACK)));
    public static final Block CHARRED_QUARTZ_WALL = registerBlock("charred_quartz_wall", new WallBlock(FabricBlockSettings.copyOf(DDBlocks.QUARTZ_WALL).mapColor(DyeColor.BLACK)));
    public static final Block SMOOTH_CHARRED_QUARTZ_BLOCK = registerBlock("smooth_charred_quartz_block", new Block(FabricBlockSettings.copyOf(Blocks.SMOOTH_QUARTZ).mapColor(DyeColor.BLACK)));
    public static final Block SMOOTH_CHARRED_QUARTZ_STAIRS = registerBlock("smooth_charred_quartz_stairs", new StairsBlock(SMOOTH_CHARRED_QUARTZ_BLOCK.getDefaultState(), FabricBlockSettings.copyOf(Blocks.SMOOTH_QUARTZ_STAIRS).mapColor(DyeColor.BLACK)));
    public static final Block SMOOTH_CHARRED_QUARTZ_SLAB = registerBlock("smooth_charred_quartz_slab", new SlabBlock(FabricBlockSettings.copyOf(Blocks.SMOOTH_QUARTZ_SLAB).mapColor(DyeColor.BLACK)));
    public static final Block SMOOTH_CHARRED_QUARTZ_WALL = registerBlock("smooth_charred_quartz_wall", new WallBlock(FabricBlockSettings.copyOf(DDBlocks.SMOOTH_QUARTZ_WALL).mapColor(DyeColor.BLACK)));
    public static final Block CHARRED_QUARTZ_BRICKS = registerBlock("charred_quartz_bricks", new Block(FabricBlockSettings.copyOf(Blocks.QUARTZ_BRICKS).mapColor(DyeColor.BLACK)));
    public static final Block CHARRED_QUARTZ_BRICK_STAIRS = registerBlock("charred_quartz_brick_stairs", new StairsBlock(CHARRED_QUARTZ_BRICKS.getDefaultState(), FabricBlockSettings.copyOf(QUARTZ_BRICK_STAIRS).mapColor(DyeColor.BLACK)));
    public static final Block CHARRED_QUARTZ_BRICK_SLAB = registerBlock("charred_quartz_brick_slab", new SlabBlock(FabricBlockSettings.copyOf(QUARTZ_BRICK_SLAB).mapColor(DyeColor.BLACK)));
    public static final Block CHARRED_QUARTZ_BRICK_WALL = registerBlock("charred_quartz_brick_wall", new WallBlock(FabricBlockSettings.copyOf(QUARTZ_BRICK_WALL).mapColor(DyeColor.BLACK)));
    public static final Block CHISELED_CHARRED_QUARTZ_BLOCK = registerBlock("chiseled_charred_quartz_block", new Block(FabricBlockSettings.copyOf(Blocks.CHISELED_QUARTZ_BLOCK).mapColor(DyeColor.BLACK)));
    public static final Block CHARRED_QUARTZ_PILLAR = registerBlock("charred_quartz_pillar", new PillarBlock(FabricBlockSettings.copyOf(Blocks.QUARTZ_PILLAR).mapColor(DyeColor.BLACK)));
    public static final Block CUT_IRON_BLOCK = registerBlock("cut_iron", new Block(FabricBlockSettings.copyOf(Blocks.IRON_BLOCK)));
    public static final Block CUT_IRON_STAIRS = registerBlock("cut_iron_stairs", new StairsBlock(CUT_IRON_BLOCK.getDefaultState(), FabricBlockSettings.copyOf(Blocks.IRON_BLOCK)));
    public static final Block CUT_IRON_SLAB = registerBlock("cut_iron_slab", new SlabBlock(FabricBlockSettings.copyOf(Blocks.IRON_BLOCK)));
    public static final Block CUT_IRON_WALL = registerBlock("cut_iron_wall", new WallBlock(FabricBlockSettings.copyOf(Blocks.IRON_BLOCK)));
    public static final Block CUT_GOLD_BLOCK = registerBlock("cut_gold", new Block(FabricBlockSettings.copyOf(Blocks.GOLD_BLOCK)));
    public static final Block CUT_GOLD_STAIRS = registerBlock("cut_gold_stairs", new StairsBlock(CUT_GOLD_BLOCK.getDefaultState(), FabricBlockSettings.copyOf(Blocks.GOLD_BLOCK)));
    public static final Block CUT_GOLD_SLAB = registerBlock("cut_gold_slab", new SlabBlock(FabricBlockSettings.copyOf(Blocks.GOLD_BLOCK)));
    public static final Block CUT_GOLD_WALL = registerBlock("cut_gold_wall", new WallBlock(FabricBlockSettings.copyOf(Blocks.GOLD_BLOCK)));
    public static final Block CUT_DIAMOND_BLOCK = registerBlock("cut_diamond", new Block(FabricBlockSettings.copyOf(Blocks.DIAMOND_BLOCK)));
    public static final Block CUT_DIAMOND_STAIRS = registerBlock("cut_diamond_stairs", new StairsBlock(CUT_DIAMOND_BLOCK.getDefaultState(), FabricBlockSettings.copyOf(Blocks.DIAMOND_BLOCK)));
    public static final Block CUT_DIAMOND_SLAB = registerBlock("cut_diamond_slab", new SlabBlock(FabricBlockSettings.copyOf(Blocks.DIAMOND_BLOCK)));
    public static final Block CUT_DIAMOND_WALL = registerBlock("cut_diamond_wall", new WallBlock(FabricBlockSettings.copyOf(Blocks.DIAMOND_BLOCK)));
    public static final Block CUT_EMERALD_BLOCK = registerBlock("cut_emerald", new Block(FabricBlockSettings.copyOf(Blocks.EMERALD_BLOCK)));
    public static final Block CUT_EMERALD_STAIRS = registerBlock("cut_emerald_stairs", new StairsBlock(CUT_EMERALD_BLOCK.getDefaultState(), FabricBlockSettings.copyOf(Blocks.EMERALD_BLOCK)));
    public static final Block CUT_EMERALD_SLAB = registerBlock("cut_emerald_slab", new SlabBlock(FabricBlockSettings.copyOf(Blocks.EMERALD_BLOCK)));
    public static final Block CUT_EMERALD_WALL = registerBlock("cut_emerald_wall", new WallBlock(FabricBlockSettings.copyOf(Blocks.EMERALD_BLOCK)));
    public static final Block CUT_NETHERITE_BLOCK = registerBlock("cut_netherite", new Block(FabricBlockSettings.copyOf(Blocks.NETHERITE_BLOCK)), true);
    public static final Block CUT_NETHERITE_STAIRS = registerBlock("cut_netherite_stairs", new StairsBlock(CUT_NETHERITE_BLOCK.getDefaultState(), FabricBlockSettings.copyOf(Blocks.NETHERITE_BLOCK)), true);
    public static final Block CUT_NETHERITE_SLAB = registerBlock("cut_netherite_slab", new SlabBlock(FabricBlockSettings.copyOf(Blocks.NETHERITE_BLOCK)), true);
    public static final Block CUT_NETHERITE_WALL = registerBlock("cut_netherite_wall", new WallBlock(FabricBlockSettings.copyOf(Blocks.NETHERITE_BLOCK)), true);
    public static final Block CUT_LAPIS_BLOCK = registerBlock("cut_lapis", new Block(FabricBlockSettings.copyOf(Blocks.LAPIS_BLOCK)));
    public static final Block CUT_LAPIS_STAIRS = registerBlock("cut_lapis_stairs", new StairsBlock(CUT_LAPIS_BLOCK.getDefaultState(), FabricBlockSettings.copyOf(Blocks.LAPIS_BLOCK)));
    public static final Block CUT_LAPIS_SLAB = registerBlock("cut_lapis_slab", new SlabBlock(FabricBlockSettings.copyOf(Blocks.LAPIS_BLOCK)));
    public static final Block CUT_LAPIS_WALL = registerBlock("cut_lapis_wall", new WallBlock(FabricBlockSettings.copyOf(Blocks.LAPIS_BLOCK)));
    public static final Block POLISHED_AMETHYST_BLOCK = registerBlock("polished_amethyst", new Block(FabricBlockSettings.copyOf(Blocks.AMETHYST_BLOCK)));
    public static final Block POLISHED_AMETHYST_STAIRS = registerBlock("polished_amethyst_stairs", new StairsBlock(POLISHED_AMETHYST_BLOCK.getDefaultState(), FabricBlockSettings.copyOf(Blocks.AMETHYST_BLOCK)));
    public static final Block POLISHED_AMETHYST_SLAB = registerBlock("polished_amethyst_slab", new SlabBlock(FabricBlockSettings.copyOf(Blocks.AMETHYST_BLOCK)));
    public static final Block POLISHED_AMETHYST_WALL = registerBlock("polished_amethyst_wall", new WallBlock(FabricBlockSettings.copyOf(Blocks.AMETHYST_BLOCK)));
    public static final Block POLISHED_AMETHYST_BRICKS = registerBlock("polished_amethyst_bricks", new Block(FabricBlockSettings.copyOf(Blocks.AMETHYST_BLOCK)));
    public static final Block POLISHED_AMETHYST_BRICK_STAIRS = registerBlock("polished_amethyst_brick_stairs", new StairsBlock(POLISHED_AMETHYST_BRICKS.getDefaultState(), FabricBlockSettings.copyOf(Blocks.AMETHYST_BLOCK)));
    public static final Block POLISHED_AMETHYST_BRICK_SLAB = registerBlock("polished_amethyst_brick_slab", new SlabBlock(FabricBlockSettings.copyOf(Blocks.AMETHYST_BLOCK)));
    public static final Block POLISHED_AMETHYST_BRICK_WALL = registerBlock("polished_amethyst_brick_wall", new WallBlock(FabricBlockSettings.copyOf(Blocks.AMETHYST_BLOCK)));
    public static final Block CUT_POLISHED_AMETHYST = registerBlock("cut_polished_amethyst", new Block(FabricBlockSettings.copyOf(Blocks.AMETHYST_BLOCK)));
    public static final Block CUT_POLISHED_AMETHYST_STAIRS = registerBlock("cut_polished_amethyst_stairs", new StairsBlock(CUT_POLISHED_AMETHYST.getDefaultState(), FabricBlockSettings.copyOf(Blocks.AMETHYST_BLOCK)));
    public static final Block CUT_POLISHED_AMETHYST_SLAB = registerBlock("cut_polished_amethyst_slab", new SlabBlock(FabricBlockSettings.copyOf(Blocks.AMETHYST_BLOCK)));
    public static final Block CUT_POLISHED_AMETHYST_WALL = registerBlock("cut_polished_amethyst_wall", new WallBlock(FabricBlockSettings.copyOf(Blocks.AMETHYST_BLOCK)));

    public static final Block CRACKED_DEEPSLATE_BRICK_STAIRS = registerBlock("cracked_deepslate_brick_stairs", new StairsBlock(Blocks.CRACKED_DEEPSLATE_BRICKS.getDefaultState(), FabricBlockSettings.copyOf(Blocks.CRACKED_DEEPSLATE_BRICKS)));
    public static final Block CRACKED_DEEPSLATE_BRICK_SLAB = registerBlock("cracked_deepslate_brick_slab", new SlabBlock(FabricBlockSettings.copyOf(Blocks.CRACKED_DEEPSLATE_BRICKS)));
    public static final Block CRACKED_DEEPSLATE_BRICK_WALL = registerBlock("cracked_deepslate_brick_wall", new WallBlock(FabricBlockSettings.copyOf(Blocks.CRACKED_DEEPSLATE_BRICKS)));
    public static final Block CRACKED_DEEPSLATE_TILE_STAIRS = registerBlock("cracked_deepslate_tile_stairs", new StairsBlock(Blocks.CRACKED_DEEPSLATE_TILES.getDefaultState(), FabricBlockSettings.copyOf(Blocks.DEEPSLATE_TILE_STAIRS)));
    public static final Block CRACKED_DEEPSLATE_TILE_SLAB = registerBlock("cracked_deepslate_tile_slab", new SlabBlock(FabricBlockSettings.copyOf(Blocks.DEEPSLATE_TILE_SLAB)));
    public static final Block CRACKED_DEEPSLATE_TILE_WALL = registerBlock("cracked_deepslate_tile_wall", new WallBlock(FabricBlockSettings.copyOf(Blocks.DEEPSLATE_TILE_WALL)));
    public static final Block CRACKED_NETHER_BRICK_STAIRS = registerBlock("cracked_nether_brick_stairs", new StairsBlock(Blocks.CRACKED_NETHER_BRICKS.getDefaultState(), FabricBlockSettings.copyOf(Blocks.NETHER_BRICK_STAIRS)));
    public static final Block CRACKED_NETHER_BRICK_SLAB = registerBlock("cracked_nether_brick_slab", new SlabBlock(FabricBlockSettings.copyOf(Blocks.NETHER_BRICK_SLAB)));
    public static final Block CRACKED_NETHER_BRICK_WALL = registerBlock("cracked_nether_brick_wall", new WallBlock(FabricBlockSettings.copyOf(Blocks.NETHER_BRICK_WALL)));
    public static final Block CRACKED_NETHER_BRICK_FENCE = registerBlock("cracked_nether_brick_fence", new FenceBlock(FabricBlockSettings.copyOf(Blocks.NETHER_BRICK_FENCE)));
    public static final Block CRACKED_POLISHED_BLACKSTONE_BRICK_STAIRS = registerBlock("cracked_polished_blackstone_brick_stairs", new StairsBlock(Blocks.CRACKED_POLISHED_BLACKSTONE_BRICKS.getDefaultState(), FabricBlockSettings.copyOf(Blocks.POLISHED_BLACKSTONE_BRICK_STAIRS)));
    public static final Block CRACKED_POLISHED_BLACKSTONE_BRICK_SLAB = registerBlock("cracked_polished_blackstone_brick_slab", new SlabBlock(FabricBlockSettings.copyOf(Blocks.POLISHED_BLACKSTONE_BRICK_SLAB)));
    public static final Block CRACKED_POLISHED_BLACKSTONE_BRICK_WALL = registerBlock("cracked_polished_blackstone_brick_wall", new WallBlock(FabricBlockSettings.copyOf(Blocks.POLISHED_BLACKSTONE_BRICK_WALL)));

    public static final Block PACKED_ICE_BRICKS = registerBlock("packed_ice_bricks", new Block(FabricBlockSettings.copyOf(Blocks.PACKED_ICE)));
    public static final Block PACKED_ICE_BRICK_STAIRS = registerBlock("packed_ice_brick_stairs", new StairsBlock(PACKED_ICE_BRICKS.getDefaultState(), FabricBlockSettings.copyOf(Blocks.PACKED_ICE)));
    public static final Block PACKED_ICE_BRICK_SLAB = registerBlock("packed_ice_brick_slab", new SlabBlock(FabricBlockSettings.copyOf(Blocks.PACKED_ICE)));
    public static final Block PACKED_ICE_BRICK_WALL = registerBlock("packed_ice_brick_wall", new WallBlock(FabricBlockSettings.copyOf(Blocks.PACKED_ICE)));
    public static final Block CRACKED_PACKED_ICE_BRICKS = registerBlock("cracked_packed_ice_bricks", new Block(FabricBlockSettings.copyOf(Blocks.PACKED_ICE)));
    public static final Block CRACKED_PACKED_ICE_BRICK_STAIRS = registerBlock("cracked_packed_ice_brick_stairs", new StairsBlock(CRACKED_PACKED_ICE_BRICKS.getDefaultState(), FabricBlockSettings.copyOf(Blocks.PACKED_ICE)));
    public static final Block CRACKED_PACKED_ICE_BRICK_SLAB = registerBlock("cracked_packed_ice_brick_slab", new SlabBlock(FabricBlockSettings.copyOf(Blocks.PACKED_ICE)));
    public static final Block CRACKED_PACKED_ICE_BRICK_WALL = registerBlock("cracked_packed_ice_brick_wall", new WallBlock(FabricBlockSettings.copyOf(Blocks.PACKED_ICE)));
    public static final Block LARGE_PACKED_ICE_BRICKS = registerBlock("large_packed_ice_bricks", new Block(FabricBlockSettings.copyOf(Blocks.PACKED_ICE)));
    public static final Block LARGE_PACKED_ICE_BRICK_STAIRS = registerBlock("large_packed_ice_brick_stairs", new StairsBlock(LARGE_PACKED_ICE_BRICKS.getDefaultState(), FabricBlockSettings.copyOf(Blocks.PACKED_ICE)));
    public static final Block LARGE_PACKED_ICE_BRICK_SLAB = registerBlock("large_packed_ice_brick_slab", new SlabBlock(FabricBlockSettings.copyOf(Blocks.PACKED_ICE)));
    public static final Block LARGE_PACKED_ICE_BRICK_WALL = registerBlock("large_packed_ice_brick_wall", new WallBlock(FabricBlockSettings.copyOf(Blocks.PACKED_ICE)));
    public static final Block LARGE_CRACKED_PACKED_ICE_BRICKS = registerBlock("large_cracked_packed_ice_bricks", new Block(FabricBlockSettings.copyOf(Blocks.PACKED_ICE)));
    public static final Block LARGE_CRACKED_PACKED_ICE_BRICK_STAIRS = registerBlock("large_cracked_packed_ice_brick_stairs", new StairsBlock(LARGE_CRACKED_PACKED_ICE_BRICKS.getDefaultState(), FabricBlockSettings.copyOf(Blocks.PACKED_ICE)));
    public static final Block LARGE_CRACKED_PACKED_ICE_BRICK_SLAB = registerBlock("large_cracked_packed_ice_brick_slab", new SlabBlock(FabricBlockSettings.copyOf(Blocks.PACKED_ICE)));
    public static final Block LARGE_CRACKED_PACKED_ICE_BRICK_WALL = registerBlock("large_cracked_packed_ice_brick_wall", new WallBlock(FabricBlockSettings.copyOf(Blocks.PACKED_ICE)));
    public static final Block POLISHED_PACKED_ICE = registerBlock("polished_packed_ice", new Block(FabricBlockSettings.copyOf(Blocks.PACKED_ICE)));
    public static final Block POLISHED_PACKED_ICE_STAIRS = registerBlock("polished_packed_ice_stairs", new StairsBlock(POLISHED_PACKED_ICE.getDefaultState(), FabricBlockSettings.copyOf(Blocks.PACKED_ICE)));
    public static final Block POLISHED_PACKED_ICE_SLAB = registerBlock("polished_packed_ice_slab", new SlabBlock(FabricBlockSettings.copyOf(Blocks.PACKED_ICE)));
    public static final Block POLISHED_PACKED_ICE_WALL = registerBlock("polished_packed_ice_wall", new WallBlock(FabricBlockSettings.copyOf(Blocks.PACKED_ICE)));
    public static final Block CHISELED_PACKED_ICE_BRICKS = registerBlock("chiseled_packed_ice_bricks", new Block(FabricBlockSettings.copyOf(Blocks.PACKED_ICE)));
    public static final Block BLUE_ICE_BRICKS = registerBlock("blue_ice_bricks", new Block(FabricBlockSettings.copyOf(Blocks.BLUE_ICE)));
    public static final Block BLUE_ICE_BRICK_STAIRS = registerBlock("blue_ice_brick_stairs", new StairsBlock(BLUE_ICE_BRICKS.getDefaultState(), FabricBlockSettings.copyOf(Blocks.BLUE_ICE)));
    public static final Block BLUE_ICE_BRICK_SLAB = registerBlock("blue_ice_brick_slab", new SlabBlock(FabricBlockSettings.copyOf(Blocks.BLUE_ICE)));
    public static final Block BLUE_ICE_BRICK_WALL = registerBlock("blue_ice_brick_wall", new WallBlock(FabricBlockSettings.copyOf(Blocks.BLUE_ICE)));
    public static final Block CRACKED_BLUE_ICE_BRICKS = registerBlock("cracked_blue_ice_bricks", new Block(FabricBlockSettings.copyOf(Blocks.BLUE_ICE)));
    public static final Block CRACKED_BLUE_ICE_BRICK_STAIRS = registerBlock("cracked_blue_ice_brick_stairs", new StairsBlock(CRACKED_BLUE_ICE_BRICKS.getDefaultState(), FabricBlockSettings.copyOf(Blocks.BLUE_ICE)));
    public static final Block CRACKED_BLUE_ICE_BRICK_SLAB = registerBlock("cracked_blue_ice_brick_slab", new SlabBlock(FabricBlockSettings.copyOf(Blocks.BLUE_ICE)));
    public static final Block CRACKED_BLUE_ICE_BRICK_WALL = registerBlock("cracked_blue_ice_brick_wall", new WallBlock(FabricBlockSettings.copyOf(Blocks.BLUE_ICE)));
    public static final Block LARGE_BLUE_ICE_BRICKS = registerBlock("large_blue_ice_bricks", new Block(FabricBlockSettings.copyOf(Blocks.BLUE_ICE)));
    public static final Block LARGE_BLUE_ICE_BRICK_STAIRS = registerBlock("large_blue_ice_brick_stairs", new StairsBlock(LARGE_BLUE_ICE_BRICKS.getDefaultState(), FabricBlockSettings.copyOf(Blocks.BLUE_ICE)));
    public static final Block LARGE_BLUE_ICE_BRICK_SLAB = registerBlock("large_blue_ice_brick_slab", new SlabBlock(FabricBlockSettings.copyOf(Blocks.BLUE_ICE)));
    public static final Block LARGE_BLUE_ICE_BRICK_WALL = registerBlock("large_blue_ice_brick_wall", new WallBlock(FabricBlockSettings.copyOf(Blocks.BLUE_ICE)));
    public static final Block LARGE_CRACKED_BLUE_ICE_BRICKS = registerBlock("large_cracked_blue_ice_bricks", new Block(FabricBlockSettings.copyOf(Blocks.BLUE_ICE)));
    public static final Block LARGE_CRACKED_BLUE_ICE_BRICK_STAIRS = registerBlock("large_cracked_blue_ice_brick_stairs", new StairsBlock(LARGE_CRACKED_BLUE_ICE_BRICKS.getDefaultState(), FabricBlockSettings.copyOf(Blocks.BLUE_ICE)));
    public static final Block LARGE_CRACKED_BLUE_ICE_BRICK_SLAB = registerBlock("large_cracked_blue_ice_brick_slab", new SlabBlock(FabricBlockSettings.copyOf(Blocks.BLUE_ICE)));
    public static final Block LARGE_CRACKED_BLUE_ICE_BRICK_WALL = registerBlock("large_cracked_blue_ice_brick_wall", new WallBlock(FabricBlockSettings.copyOf(Blocks.BLUE_ICE)));
    public static final Block POLISHED_BLUE_ICE = registerBlock("polished_blue_ice", new Block(FabricBlockSettings.copyOf(Blocks.BLUE_ICE)));
    public static final Block POLISHED_BLUE_ICE_STAIRS = registerBlock("polished_blue_ice_stairs", new StairsBlock(POLISHED_BLUE_ICE.getDefaultState(), FabricBlockSettings.copyOf(Blocks.BLUE_ICE)));
    public static final Block POLISHED_BLUE_ICE_SLAB = registerBlock("polished_blue_ice_slab", new SlabBlock(FabricBlockSettings.copyOf(Blocks.BLUE_ICE)));
    public static final Block POLISHED_BLUE_ICE_WALL = registerBlock("polished_blue_ice_wall", new WallBlock(FabricBlockSettings.copyOf(Blocks.BLUE_ICE)));
    public static final Block CHISELED_BLUE_ICE_BRICKS = registerBlock("chiseled_blue_ice_bricks", new Block(FabricBlockSettings.copyOf(Blocks.BLUE_ICE)));

    public static final Block ANDESITE_BRICKS = registerBlock("andesite_bricks", new Block(FabricBlockSettings.copyOf(Blocks.POLISHED_ANDESITE)));
    public static final Block ANDESITE_BRICK_STAIRS = registerBlock("andesite_brick_stairs", new StairsBlock(ANDESITE_BRICKS.getDefaultState(), FabricBlockSettings.copyOf(Blocks.POLISHED_ANDESITE_STAIRS)));
    public static final Block ANDESITE_BRICK_SLAB = registerBlock("andesite_brick_slab", new SlabBlock(FabricBlockSettings.copyOf(Blocks.POLISHED_ANDESITE_SLAB)));
    public static final Block ANDESITE_BRICK_WALL = registerBlock("andesite_brick_wall", new WallBlock(FabricBlockSettings.copyOf(POLISHED_ANDESITE_WALL)));
    public static final Block CRACKED_ANDESITE_BRICKS = registerBlock("cracked_andesite_bricks", new Block(FabricBlockSettings.copyOf(Blocks.POLISHED_ANDESITE)));
    public static final Block CRACKED_ANDESITE_BRICK_STAIRS = registerBlock("cracked_andesite_brick_stairs", new StairsBlock(CRACKED_ANDESITE_BRICKS.getDefaultState(), FabricBlockSettings.copyOf(Blocks.POLISHED_ANDESITE_STAIRS)));
    public static final Block CRACKED_ANDESITE_BRICK_SLAB = registerBlock("cracked_andesite_brick_slab", new SlabBlock(FabricBlockSettings.copyOf(Blocks.POLISHED_ANDESITE_SLAB)));
    public static final Block CRACKED_ANDESITE_BRICK_WALL = registerBlock("cracked_andesite_brick_wall", new WallBlock(FabricBlockSettings.copyOf(POLISHED_ANDESITE_WALL)));
    public static final Block MOSSY_ANDESITE_BRICKS = registerBlock("mossy_andesite_bricks", new Block(FabricBlockSettings.copyOf(Blocks.POLISHED_ANDESITE)));
    public static final Block MOSSY_ANDESITE_BRICK_STAIRS = registerBlock("mossy_andesite_brick_stairs", new StairsBlock(MOSSY_ANDESITE_BRICKS.getDefaultState(), FabricBlockSettings.copyOf(Blocks.POLISHED_ANDESITE_STAIRS)));
    public static final Block MOSSY_ANDESITE_BRICK_SLAB = registerBlock("mossy_andesite_brick_slab", new SlabBlock(FabricBlockSettings.copyOf(Blocks.POLISHED_ANDESITE_SLAB)));
    public static final Block MOSSY_ANDESITE_BRICK_WALL = registerBlock("mossy_andesite_brick_wall", new WallBlock(FabricBlockSettings.copyOf(POLISHED_ANDESITE_WALL)));
    public static final Block DIORITE_BRICKS = registerBlock("diorite_bricks", new Block(FabricBlockSettings.copyOf(Blocks.POLISHED_DIORITE)));
    public static final Block DIORITE_BRICK_STAIRS = registerBlock("diorite_brick_stairs", new StairsBlock(DIORITE_BRICKS.getDefaultState(), FabricBlockSettings.copyOf(Blocks.POLISHED_DIORITE_STAIRS)));
    public static final Block DIORITE_BRICK_SLAB = registerBlock("diorite_brick_slab", new SlabBlock(FabricBlockSettings.copyOf(Blocks.POLISHED_DIORITE_SLAB)));
    public static final Block DIORITE_BRICK_WALL = registerBlock("diorite_brick_wall", new WallBlock(FabricBlockSettings.copyOf(POLISHED_DIORITE_WALL)));
    public static final Block CRACKED_DIORITE_BRICKS = registerBlock("cracked_diorite_bricks", new Block(FabricBlockSettings.copyOf(Blocks.POLISHED_DIORITE)));
    public static final Block CRACKED_DIORITE_BRICK_STAIRS = registerBlock("cracked_diorite_brick_stairs", new StairsBlock(CRACKED_DIORITE_BRICKS.getDefaultState(), FabricBlockSettings.copyOf(Blocks.POLISHED_DIORITE_STAIRS)));
    public static final Block CRACKED_DIORITE_BRICK_SLAB = registerBlock("cracked_diorite_brick_slab", new SlabBlock(FabricBlockSettings.copyOf(Blocks.POLISHED_DIORITE_SLAB)));
    public static final Block CRACKED_DIORITE_BRICK_WALL = registerBlock("cracked_diorite_brick_wall", new WallBlock(FabricBlockSettings.copyOf(POLISHED_DIORITE_WALL)));
    public static final Block MOSSY_DIORITE_BRICKS = registerBlock("mossy_diorite_bricks", new Block(FabricBlockSettings.copyOf(Blocks.POLISHED_DIORITE)));
    public static final Block MOSSY_DIORITE_BRICK_STAIRS = registerBlock("mossy_diorite_brick_stairs", new StairsBlock(MOSSY_DIORITE_BRICKS.getDefaultState(), FabricBlockSettings.copyOf(Blocks.POLISHED_DIORITE_STAIRS)));
    public static final Block MOSSY_DIORITE_BRICK_SLAB = registerBlock("mossy_diorite_brick_slab", new SlabBlock(FabricBlockSettings.copyOf(Blocks.POLISHED_DIORITE_SLAB)));
    public static final Block MOSSY_DIORITE_BRICK_WALL = registerBlock("mossy_diorite_brick_wall", new WallBlock(FabricBlockSettings.copyOf(POLISHED_DIORITE_WALL)));
    public static final Block GRANITE_BRICKS = registerBlock("granite_bricks", new Block(FabricBlockSettings.copyOf(Blocks.POLISHED_GRANITE)));
    public static final Block GRANITE_BRICK_STAIRS = registerBlock("granite_brick_stairs", new StairsBlock(GRANITE_BRICKS.getDefaultState(), FabricBlockSettings.copyOf(Blocks.POLISHED_GRANITE_STAIRS)));
    public static final Block GRANITE_BRICK_SLAB = registerBlock("granite_brick_slab", new SlabBlock(FabricBlockSettings.copyOf(Blocks.POLISHED_GRANITE_SLAB)));
    public static final Block GRANITE_BRICK_WALL = registerBlock("granite_brick_wall", new WallBlock(FabricBlockSettings.copyOf(POLISHED_GRANITE_WALL)));
    public static final Block CRACKED_GRANITE_BRICKS = registerBlock("cracked_granite_bricks", new Block(FabricBlockSettings.copyOf(Blocks.POLISHED_GRANITE)));
    public static final Block CRACKED_GRANITE_BRICK_STAIRS = registerBlock("cracked_granite_brick_stairs", new StairsBlock(CRACKED_GRANITE_BRICKS.getDefaultState(), FabricBlockSettings.copyOf(Blocks.POLISHED_GRANITE_STAIRS)));
    public static final Block CRACKED_GRANITE_BRICK_SLAB = registerBlock("cracked_granite_brick_slab", new SlabBlock(FabricBlockSettings.copyOf(Blocks.POLISHED_GRANITE_SLAB)));
    public static final Block CRACKED_GRANITE_BRICK_WALL = registerBlock("cracked_granite_brick_wall", new WallBlock(FabricBlockSettings.copyOf(POLISHED_GRANITE_WALL)));
    public static final Block MOSSY_GRANITE_BRICKS = registerBlock("mossy_granite_bricks", new Block(FabricBlockSettings.copyOf(Blocks.POLISHED_GRANITE)));
    public static final Block MOSSY_GRANITE_BRICK_STAIRS = registerBlock("mossy_granite_brick_stairs", new StairsBlock(MOSSY_GRANITE_BRICKS.getDefaultState(), FabricBlockSettings.copyOf(Blocks.POLISHED_GRANITE_STAIRS)));
    public static final Block MOSSY_GRANITE_BRICK_SLAB = registerBlock("mossy_granite_brick_slab", new SlabBlock(FabricBlockSettings.copyOf(Blocks.POLISHED_GRANITE_SLAB)));
    public static final Block MOSSY_GRANITE_BRICK_WALL = registerBlock("mossy_granite_brick_wall", new WallBlock(FabricBlockSettings.copyOf(POLISHED_GRANITE_WALL)));
    public static final Block CALCITE_STAIRS = registerBlock("calcite_stairs", new StairsBlock(Blocks.CALCITE.getDefaultState(), FabricBlockSettings.copyOf(Blocks.CALCITE)));
    public static final Block CALCITE_SLAB = registerBlock("calcite_slab", new SlabBlock(FabricBlockSettings.copyOf(Blocks.CALCITE)));
    public static final Block CALCITE_WALL = registerBlock("calcite_wall", new WallBlock(FabricBlockSettings.copyOf(Blocks.CALCITE)));
    public static final Block POLISHED_CALCITE = registerBlock("polished_calcite", new Block(FabricBlockSettings.copyOf(Blocks.CALCITE)));
    public static final Block POLISHED_CALCITE_STAIRS = registerBlock("polished_calcite_stairs", new StairsBlock(POLISHED_CALCITE.getDefaultState(), FabricBlockSettings.copyOf(CALCITE_STAIRS)));
    public static final Block POLISHED_CALCITE_SLAB = registerBlock("polished_calcite_slab", new SlabBlock(FabricBlockSettings.copyOf(CALCITE_SLAB)));
    public static final Block POLISHED_CALCITE_WALL = registerBlock("polished_calcite_wall", new WallBlock(FabricBlockSettings.copyOf(CALCITE_WALL)));
    public static final Block CALCITE_BRICKS = registerBlock("calcite_bricks", new Block(FabricBlockSettings.copyOf(POLISHED_CALCITE)));
    public static final Block CALCITE_BRICK_STAIRS = registerBlock("calcite_brick_stairs", new StairsBlock(CALCITE_BRICKS.getDefaultState(), FabricBlockSettings.copyOf(POLISHED_CALCITE_STAIRS)));
    public static final Block CALCITE_BRICK_SLAB = registerBlock("calcite_brick_slab", new SlabBlock(FabricBlockSettings.copyOf(POLISHED_CALCITE_SLAB)));
    public static final Block CALCITE_BRICK_WALL = registerBlock("calcite_brick_wall", new WallBlock(FabricBlockSettings.copyOf(POLISHED_CALCITE_WALL)));
    public static final Block CRACKED_CALCITE_BRICKS = registerBlock("cracked_calcite_bricks", new Block(FabricBlockSettings.copyOf(POLISHED_CALCITE)));
    public static final Block CRACKED_CALCITE_BRICK_STAIRS = registerBlock("cracked_calcite_brick_stairs", new StairsBlock(CRACKED_CALCITE_BRICKS.getDefaultState(), FabricBlockSettings.copyOf(POLISHED_CALCITE_STAIRS)));
    public static final Block CRACKED_CALCITE_BRICK_SLAB = registerBlock("cracked_calcite_brick_slab", new SlabBlock(FabricBlockSettings.copyOf(POLISHED_CALCITE_SLAB)));
    public static final Block CRACKED_CALCITE_BRICK_WALL = registerBlock("cracked_calcite_brick_wall", new WallBlock(FabricBlockSettings.copyOf(POLISHED_CALCITE_WALL)));
    public static final Block MOSSY_CALCITE_BRICKS = registerBlock("mossy_calcite_bricks", new Block(FabricBlockSettings.copyOf(POLISHED_CALCITE)));
    public static final Block MOSSY_CALCITE_BRICK_STAIRS = registerBlock("mossy_calcite_brick_stairs", new StairsBlock(MOSSY_CALCITE_BRICKS.getDefaultState(), FabricBlockSettings.copyOf(POLISHED_CALCITE_STAIRS)));
    public static final Block MOSSY_CALCITE_BRICK_SLAB = registerBlock("mossy_calcite_brick_slab", new SlabBlock(FabricBlockSettings.copyOf(POLISHED_CALCITE_SLAB)));
    public static final Block MOSSY_CALCITE_BRICK_WALL = registerBlock("mossy_calcite_brick_wall", new WallBlock(FabricBlockSettings.copyOf(POLISHED_CALCITE_WALL)));
    public static final Block SMOOTH_BASALT_STAIRS = registerBlock("smooth_basalt_stairs", new StairsBlock(Blocks.SMOOTH_BASALT.getDefaultState(), FabricBlockSettings.copyOf(Blocks.SMOOTH_BASALT)));
    public static final Block SMOOTH_BASALT_SLAB = registerBlock("smooth_basalt_slab", new SlabBlock(FabricBlockSettings.copyOf(Blocks.SMOOTH_BASALT)));
    public static final Block SMOOTH_BASALT_WALL = registerBlock("smooth_basalt_wall", new WallBlock(FabricBlockSettings.copyOf(Blocks.SMOOTH_BASALT)));

    public static final Block DRIPSTONE_STAIRS = registerBlock("dripstone_stairs", new StairsBlock(Blocks.DRIPSTONE_BLOCK.getDefaultState(), FabricBlockSettings.copyOf(Blocks.DRIPSTONE_BLOCK)));
    public static final Block DRIPSTONE_SLAB = registerBlock("dripstone_slab", new SlabBlock(FabricBlockSettings.copyOf(Blocks.DRIPSTONE_BLOCK)));
    public static final Block DRIPSTONE_WALL = registerBlock("dripstone_wall", new WallBlock(FabricBlockSettings.copyOf(Blocks.DRIPSTONE_BLOCK)));
    public static final Block POLISHED_DRIPSTONE = registerBlock("polished_dripstone", new Block(FabricBlockSettings.copyOf(Blocks.DRIPSTONE_BLOCK)));
    public static final Block POLISHED_DRIPSTONE_STAIRS = registerBlock("polished_dripstone_stairs", new StairsBlock(POLISHED_DRIPSTONE.getDefaultState(), FabricBlockSettings.copyOf(POLISHED_DRIPSTONE)));
    public static final Block POLISHED_DRIPSTONE_SLAB = registerBlock("polished_dripstone_slab", new SlabBlock(FabricBlockSettings.copyOf(POLISHED_DRIPSTONE)));
    public static final Block POLISHED_DRIPSTONE_WALL = registerBlock("polished_dripstone_wall", new WallBlock(FabricBlockSettings.copyOf(POLISHED_DRIPSTONE)));
    public static final Block DRIPSTONE_BRICKS = registerBlock("dripstone_bricks", new Block(FabricBlockSettings.copyOf(Blocks.DRIPSTONE_BLOCK)));
    public static final Block DRIPSTONE_BRICK_STAIRS = registerBlock("dripstone_brick_stairs", new StairsBlock(DRIPSTONE_BRICKS.getDefaultState(), FabricBlockSettings.copyOf(DRIPSTONE_BRICKS)));
    public static final Block DRIPSTONE_BRICK_SLAB = registerBlock("dripstone_brick_slab", new SlabBlock(FabricBlockSettings.copyOf(DRIPSTONE_BRICKS)));
    public static final Block DRIPSTONE_BRICK_WALL = registerBlock("dripstone_brick_wall", new WallBlock(FabricBlockSettings.copyOf(DRIPSTONE_BRICKS)));
    public static final Block CRACKED_DRIPSTONE_BRICKS = registerBlock("cracked_dripstone_bricks", new Block(FabricBlockSettings.copyOf(Blocks.DRIPSTONE_BLOCK)));
    public static final Block CRACKED_DRIPSTONE_BRICK_STAIRS = registerBlock("cracked_dripstone_brick_stairs", new StairsBlock(CRACKED_DRIPSTONE_BRICKS.getDefaultState(), FabricBlockSettings.copyOf(CRACKED_DRIPSTONE_BRICKS)));
    public static final Block CRACKED_DRIPSTONE_BRICK_SLAB = registerBlock("cracked_dripstone_brick_slab", new SlabBlock(FabricBlockSettings.copyOf(CRACKED_DRIPSTONE_BRICKS)));
    public static final Block CRACKED_DRIPSTONE_BRICK_WALL = registerBlock("cracked_dripstone_brick_wall", new WallBlock(FabricBlockSettings.copyOf(CRACKED_DRIPSTONE_BRICKS)));
    public static final Block MOSSY_DRIPSTONE_BRICKS = registerBlock("mossy_dripstone_bricks", new Block(FabricBlockSettings.copyOf(Blocks.DRIPSTONE_BLOCK)));
    public static final Block MOSSY_DRIPSTONE_BRICK_STAIRS = registerBlock("mossy_dripstone_brick_stairs", new StairsBlock(MOSSY_DRIPSTONE_BRICKS.getDefaultState(), FabricBlockSettings.copyOf(MOSSY_DRIPSTONE_BRICKS)));
    public static final Block MOSSY_DRIPSTONE_BRICK_SLAB = registerBlock("mossy_dripstone_brick_slab", new SlabBlock(FabricBlockSettings.copyOf(MOSSY_DRIPSTONE_BRICKS)));
    public static final Block MOSSY_DRIPSTONE_BRICK_WALL = registerBlock("mossy_dripstone_brick_wall", new WallBlock(FabricBlockSettings.copyOf(MOSSY_DRIPSTONE_BRICKS)));

    public static final Block CUT_COPPER_WALL = registerBlock("cut_copper_wall", new OxidizableWallBlock(FabricBlockSettings.copyOf(Blocks.CUT_COPPER), Oxidizable.OxidationLevel.UNAFFECTED));
    public static final Block WAXED_CUT_COPPER_WALL = registerBlock("waxed_cut_copper_wall", new WallBlock(FabricBlockSettings.copyOf(Blocks.WAXED_CUT_COPPER)));
    public static final Block EXPOSED_CUT_COPPER_WALL = registerBlock("exposed_cut_copper_wall", new OxidizableWallBlock(FabricBlockSettings.copyOf(Blocks.EXPOSED_CUT_COPPER), Oxidizable.OxidationLevel.EXPOSED));
    public static final Block WAXED_EXPOSED_CUT_COPPER_WALL = registerBlock("waxed_exposed_cut_copper_wall", new WallBlock(FabricBlockSettings.copyOf(Blocks.WAXED_EXPOSED_CUT_COPPER)));
    public static final Block WEATHERED_CUT_COPPER_WALL = registerBlock("weathered_cut_copper_wall", new OxidizableWallBlock(FabricBlockSettings.copyOf(Blocks.WEATHERED_CUT_COPPER), Oxidizable.OxidationLevel.WEATHERED));
    public static final Block WAXED_WEATHERED_CUT_COPPER_WALL = registerBlock("waxed_weathered_cut_copper_wall", new WallBlock(FabricBlockSettings.copyOf(Blocks.WAXED_WEATHERED_CUT_COPPER)));
    public static final Block OXIDIZED_CUT_COPPER_WALL = registerBlock("oxidized_cut_copper_wall", new OxidizableWallBlock(FabricBlockSettings.copyOf(Blocks.OXIDIZED_CUT_COPPER), Oxidizable.OxidationLevel.OXIDIZED));
    public static final Block WAXED_OXIDIZED_CUT_COPPER_WALL = registerBlock("waxed_oxidized_cut_copper_wall", new WallBlock(FabricBlockSettings.copyOf(Blocks.WAXED_OXIDIZED_CUT_COPPER)));

    public static final Block MOSSY_COBBLED_DEEPSLATE = registerBlock("mossy_cobbled_deepslate", new Block(FabricBlockSettings.copyOf(Blocks.COBBLED_DEEPSLATE)));
    public static final Block MOSSY_COBBLED_DEEPSLATE_STAIRS = registerBlock("mossy_cobbled_deepslate_stairs", new StairsBlock(MOSSY_COBBLED_DEEPSLATE.getDefaultState(), FabricBlockSettings.copyOf(MOSSY_COBBLED_DEEPSLATE)));
    public static final Block MOSSY_COBBLED_DEEPSLATE_SLAB = registerBlock("mossy_cobbled_deepslate_slab", new SlabBlock(FabricBlockSettings.copyOf(MOSSY_COBBLED_DEEPSLATE)));
    public static final Block MOSSY_COBBLED_DEEPSLATE_WALL = registerBlock("mossy_cobbled_deepslate_wall", new WallBlock(FabricBlockSettings.copyOf(MOSSY_COBBLED_DEEPSLATE)));
    public static final Block MOSSY_DEEPSLATE_BRICKS = registerBlock("mossy_deepslate_bricks", new Block(FabricBlockSettings.copyOf(Blocks.DEEPSLATE_BRICKS)));
    public static final Block MOSSY_DEEPSLATE_BRICK_STAIRS = registerBlock("mossy_deepslate_brick_stairs", new StairsBlock(MOSSY_DEEPSLATE_BRICKS.getDefaultState(), FabricBlockSettings.copyOf(MOSSY_DEEPSLATE_BRICKS)));
    public static final Block MOSSY_DEEPSLATE_BRICK_SLAB = registerBlock("mossy_deepslate_brick_slab", new SlabBlock(FabricBlockSettings.copyOf(MOSSY_DEEPSLATE_BRICKS)));
    public static final Block MOSSY_DEEPSLATE_BRICK_WALL = registerBlock("mossy_deepslate_brick_wall", new WallBlock(FabricBlockSettings.copyOf(MOSSY_DEEPSLATE_BRICKS)));

    public static final Block CHISELED_GRANITE = registerBlock("chiseled_granite", new Block(FabricBlockSettings.copyOf(Blocks.GRANITE)));
    public static final Block CHISELED_DIORITE = registerBlock("chiseled_diorite", new Block(FabricBlockSettings.copyOf(Blocks.DIORITE)));
    public static final Block CHISELED_ANDESITE = registerBlock("chiseled_andesite", new Block(FabricBlockSettings.copyOf(Blocks.ANDESITE)));
    public static final Block CHISELED_CALCITE = registerBlock("chiseled_calcite", new Block(FabricBlockSettings.copyOf(Blocks.CALCITE)));
    public static final Block CHISELED_DRIPSTONE = registerBlock("chiseled_dripstone", new Block(FabricBlockSettings.copyOf(Blocks.DRIPSTONE_BLOCK)));
    public static final Block CHISELED_GRANITE_SLAB = registerBlock("chiseled_granite_slab", new SlabBlock(FabricBlockSettings.copyOf(CHISELED_GRANITE)));
    public static final Block CHISELED_DIORITE_SLAB = registerBlock("chiseled_diorite_slab", new SlabBlock(FabricBlockSettings.copyOf(CHISELED_DIORITE)));
    public static final Block CHISELED_ANDESITE_SLAB = registerBlock("chiseled_andesite_slab", new SlabBlock(FabricBlockSettings.copyOf(CHISELED_ANDESITE)));
    public static final Block CHISELED_CALCITE_SLAB = registerBlock("chiseled_calcite_slab", new SlabBlock(FabricBlockSettings.copyOf(CHISELED_CALCITE)));
    public static final Block CHISELED_DRIPSTONE_SLAB = registerBlock("chiseled_dripstone_slab", new SlabBlock(FabricBlockSettings.copyOf(CHISELED_DRIPSTONE)));

    public static final Block CRACKED_TUFF_BRICKS = registerBlock("cracked_tuff_bricks", new Block(FabricBlockSettings.copyOf(Blocks.TUFF_BRICKS)));
    public static final Block CRACKED_TUFF_BRICK_STAIRS = registerBlock("cracked_tuff_brick_stairs", new StairsBlock(CRACKED_TUFF_BRICKS.getDefaultState(), FabricBlockSettings.copyOf(Blocks.TUFF_BRICK_STAIRS)));
    public static final Block CRACKED_TUFF_BRICK_SLAB = registerBlock("cracked_tuff_brick_slab", new SlabBlock(FabricBlockSettings.copyOf(Blocks.TUFF_BRICK_SLAB)));
    public static final Block CRACKED_TUFF_BRICK_WALL = registerBlock("cracked_tuff_brick_wall", new WallBlock(FabricBlockSettings.copyOf(Blocks.TUFF_BRICK_WALL)));


    public static final Block MOSSY_TUFF_BRICKS = registerBlock("mossy_tuff_bricks", new Block(FabricBlockSettings.copyOf(Blocks.TUFF_BRICKS)));
    public static final Block MOSSY_TUFF_BRICK_STAIRS = registerBlock("mossy_tuff_brick_stairs", new StairsBlock(MOSSY_TUFF_BRICKS.getDefaultState(), FabricBlockSettings.copyOf(Blocks.TUFF_BRICK_STAIRS)));
    public static final Block MOSSY_TUFF_BRICK_SLAB = registerBlock("mossy_tuff_brick_slab", new SlabBlock(FabricBlockSettings.copyOf(Blocks.TUFF_BRICK_SLAB)));
    public static final Block MOSSY_TUFF_BRICK_WALL = registerBlock("mossy_tuff_brick_wall", new WallBlock(FabricBlockSettings.copyOf(Blocks.TUFF_BRICK_WALL)));



    // Torches
    public static final Block BONE_TORCH = Registry.register(Registries.BLOCK, new Identifier(MOD_ID, "bone_torch"), new TorchBlock(ParticleTypes.FLAME, FabricBlockSettings.copyOf(Blocks.TORCH)));
    public static final Block BONE_WALL_TORCH = registerBlock("wall_bone_torch", new WallTorchBlock(ParticleTypes.FLAME, FabricBlockSettings.copyOf(Blocks.WALL_TORCH).dropsLike(DDBlocks.BONE_TORCH)));
    public static final Block SOUL_BONE_TORCH = Registry.register(Registries.BLOCK, new Identifier(MOD_ID, "soul_bone_torch"), new TorchBlock(ParticleTypes.SOUL_FIRE_FLAME, FabricBlockSettings.copyOf(Blocks.SOUL_TORCH)));
    public static final Block SOUL_BONE_WALL_TORCH = registerBlock("wall_soul_bone_torch", new WallTorchBlock(ParticleTypes.SOUL_FIRE_FLAME, FabricBlockSettings.copyOf(Blocks.SOUL_WALL_TORCH).dropsLike(DDBlocks.SOUL_BONE_TORCH)));
    public static final Block REDSTONE_BONE_TORCH = Registry.register(Registries.BLOCK, new Identifier(MOD_ID, "redstone_bone_torch"), new RedstoneTorchBlock(FabricBlockSettings.copyOf(Blocks.REDSTONE_TORCH)));
    public static final Block REDSTONE_BONE_WALL_TORCH = registerBlock("wall_redstone_bone_torch", new WallRedstoneTorchBlock(FabricBlockSettings.copyOf(Blocks.REDSTONE_WALL_TORCH).dropsLike(DDBlocks.REDSTONE_BONE_TORCH)));
    public static final Block COPPER_TORCH = Registry.register(Registries.BLOCK, new Identifier(MOD_ID, "copper_torch"), new TorchBlock(DDParticles.COPPER_FLAME, FabricBlockSettings.copyOf(Blocks.TORCH)));
    public static final Block COPPER_WALL_TORCH = registerBlock("wall_copper_torch", new WallTorchBlock(DDParticles.COPPER_FLAME, FabricBlockSettings.copyOf(Blocks.WALL_TORCH).dropsLike(DDBlocks.COPPER_TORCH)));
    public static final Block COPPER_BONE_TORCH = Registry.register(Registries.BLOCK, new Identifier(MOD_ID, "copper_bone_torch"), new TorchBlock(DDParticles.COPPER_FLAME, FabricBlockSettings.copyOf(Blocks.TORCH)));
    public static final Block COPPER_BONE_WALL_TORCH = registerBlock("wall_copper_bone_torch", new WallTorchBlock(DDParticles.COPPER_FLAME, FabricBlockSettings.copyOf(Blocks.WALL_TORCH).dropsLike(DDBlocks.COPPER_BONE_TORCH)));
    public static final Block AMETHYST_TORCH = Registry.register(Registries.BLOCK, new Identifier(MOD_ID, "amethyst_torch"), new TorchBlock(DDParticles.AMETHYST_FLAME, FabricBlockSettings.copyOf(Blocks.TORCH)));
    public static final Block AMETHYST_WALL_TORCH = registerBlock("wall_amethyst_torch", new WallTorchBlock(DDParticles.AMETHYST_FLAME, FabricBlockSettings.copyOf(Blocks.WALL_TORCH).dropsLike(DDBlocks.AMETHYST_TORCH)));
    public static final Block AMETHYST_BONE_TORCH = Registry.register(Registries.BLOCK, new Identifier(MOD_ID, "amethyst_bone_torch"), new TorchBlock(DDParticles.AMETHYST_FLAME, FabricBlockSettings.copyOf(Blocks.TORCH)));
    public static final Block AMETHYST_BONE_WALL_TORCH = registerBlock("wall_amethyst_bone_torch", new WallTorchBlock(DDParticles.AMETHYST_FLAME, FabricBlockSettings.copyOf(Blocks.WALL_TORCH).dropsLike(DDBlocks.AMETHYST_BONE_TORCH)));

    // Lanterns
    public static final Block COPPER_LANTERN = registerBlock("copper_lantern", new LanternBlock(FabricBlockSettings.copyOf(Blocks.LANTERN)));
    public static final Block AMETHYST_LANTERN = registerBlock("amethyst_lantern", new LanternBlock(FabricBlockSettings.copyOf(Blocks.LANTERN)));

    // Jack O' Lanterns
    public static final Block SOUL_JACK_O_LANTERN = registerBlock("soul_jack_o_lantern", new CarvedPumpkinBlock(FabricBlockSettings.copyOf(Blocks.JACK_O_LANTERN)));
    public static final Block COPPER_JACK_O_LANTERN = registerBlock("copper_jack_o_lantern", new CarvedPumpkinBlock(FabricBlockSettings.copyOf(Blocks.JACK_O_LANTERN)));
    public static final Block AMETHYST_JACK_O_LANTERN = registerBlock("amethyst_jack_o_lantern", new CarvedPumpkinBlock(FabricBlockSettings.copyOf(Blocks.JACK_O_LANTERN)));

    // Campfires
    public static final Block COPPER_CAMPFIRE = registerBlock("copper_campfire", new DDCampfireBlock(true, 1, FabricBlockSettings.copyOf(Blocks.CAMPFIRE)));
    public static final Block AMETHYST_CAMPFIRE = registerBlock("amethyst_campfire", new DDCampfireBlock(true, 1, FabricBlockSettings.copyOf(Blocks.CAMPFIRE)));

    // Fire Blocks
    public static final Block COPPER_FIRE = Registry.register(Registries.BLOCK, new Identifier(MOD_ID, "copper_fire"), new CopperFireBlock(FabricBlockSettings.copyOf(Blocks.FIRE)));
    public static final Block AMETHYST_FIRE = Registry.register(Registries.BLOCK, new Identifier(MOD_ID, "amethyst_fire"), new AmethystFireBlock(FabricBlockSettings.copyOf(Blocks.FIRE)));

    // Columns
    public static final Block OAK_LOG_COLUMN = registerBlock("oak_log_column", new ColumnBlock(logSettings(MapColor.OAK_TAN)));
    public static final Block OAK_WOOD_COLUMN = registerBlock("oak_wood_column", new ColumnBlock(logSettings(MapColor.OAK_TAN)));
    public static final Block STRIPPED_OAK_LOG_COLUMN = registerBlock("stripped_oak_log_column", new ColumnBlock(logSettings(MapColor.OAK_TAN)));
    public static final Block STRIPPED_OAK_WOOD_COLUMN = registerBlock("stripped_oak_wood_column", new ColumnBlock(logSettings(MapColor.OAK_TAN)));
    public static final Block OAK_PLANKS_COLUMN = registerBlock("oak_planks_column", new ColumnBlock(FabricBlockSettings.copyOf(Blocks.OAK_PLANKS)));
    public static final Block SPRUCE_LOG_COLUMN = registerBlock("spruce_log_column", new ColumnBlock(logSettings(MapColor.SPRUCE_BROWN)));
    public static final Block SPRUCE_WOOD_COLUMN = registerBlock("spruce_wood_column", new ColumnBlock(logSettings(MapColor.SPRUCE_BROWN)));
    public static final Block STRIPPED_SPRUCE_LOG_COLUMN = registerBlock("stripped_spruce_log_column", new ColumnBlock(logSettings(MapColor.SPRUCE_BROWN)));
    public static final Block STRIPPED_SPRUCE_WOOD_COLUMN = registerBlock("stripped_spruce_wood_column", new ColumnBlock(logSettings(MapColor.SPRUCE_BROWN)));
    public static final Block SPRUCE_PLANKS_COLUMN = registerBlock("spruce_planks_column", new ColumnBlock(FabricBlockSettings.copyOf(Blocks.SPRUCE_PLANKS)));
    public static final Block BIRCH_LOG_COLUMN = registerBlock("birch_log_column", new ColumnBlock(logSettings(MapColor.PALE_YELLOW)));
    public static final Block BIRCH_WOOD_COLUMN = registerBlock("birch_wood_column", new ColumnBlock(logSettings(MapColor.PALE_YELLOW)));
    public static final Block STRIPPED_BIRCH_LOG_COLUMN = registerBlock("stripped_birch_log_column", new ColumnBlock(logSettings(MapColor.PALE_YELLOW)));
    public static final Block STRIPPED_BIRCH_WOOD_COLUMN = registerBlock("stripped_birch_wood_column", new ColumnBlock(logSettings(MapColor.PALE_YELLOW)));
    public static final Block BIRCH_PLANKS_COLUMN = registerBlock("birch_planks_column", new ColumnBlock(FabricBlockSettings.copyOf(Blocks.BIRCH_PLANKS)));
    public static final Block JUNGLE_LOG_COLUMN = registerBlock("jungle_log_column", new ColumnBlock(logSettings(MapColor.DIRT_BROWN)));
    public static final Block JUNGLE_WOOD_COLUMN = registerBlock("jungle_wood_column", new ColumnBlock(logSettings(MapColor.DIRT_BROWN)));
    public static final Block STRIPPED_JUNGLE_LOG_COLUMN = registerBlock("stripped_jungle_log_column", new ColumnBlock(logSettings(MapColor.DIRT_BROWN)));
    public static final Block STRIPPED_JUNGLE_WOOD_COLUMN = registerBlock("stripped_jungle_wood_column", new ColumnBlock(logSettings(MapColor.DIRT_BROWN)));
    public static final Block JUNGLE_PLANKS_COLUMN = registerBlock("jungle_planks_column", new ColumnBlock(FabricBlockSettings.copyOf(Blocks.JUNGLE_PLANKS)));
    public static final Block ACACIA_LOG_COLUMN = registerBlock("acacia_log_column", new ColumnBlock(logSettings(MapColor.ORANGE)));
    public static final Block ACACIA_WOOD_COLUMN = registerBlock("acacia_wood_column", new ColumnBlock(logSettings(MapColor.ORANGE)));
    public static final Block STRIPPED_ACACIA_LOG_COLUMN = registerBlock("stripped_acacia_log_column", new ColumnBlock(logSettings(MapColor.ORANGE)));
    public static final Block STRIPPED_ACACIA_WOOD_COLUMN = registerBlock("stripped_acacia_wood_column", new ColumnBlock(logSettings(MapColor.ORANGE)));
    public static final Block ACACIA_PLANKS_COLUMN = registerBlock("acacia_planks_column", new ColumnBlock(FabricBlockSettings.copyOf(Blocks.ACACIA_PLANKS)));
    public static final Block DARK_OAK_LOG_COLUMN = registerBlock("dark_oak_log_column", new ColumnBlock(logSettings(MapColor.BROWN)));
    public static final Block DARK_OAK_WOOD_COLUMN = registerBlock("dark_oak_wood_column", new ColumnBlock(logSettings(MapColor.BROWN)));
    public static final Block STRIPPED_DARK_OAK_LOG_COLUMN = registerBlock("stripped_dark_oak_log_column", new ColumnBlock(logSettings(MapColor.BROWN)));
    public static final Block STRIPPED_DARK_OAK_WOOD_COLUMN = registerBlock("stripped_dark_oak_wood_column", new ColumnBlock(logSettings(MapColor.BROWN)));
    public static final Block DARK_OAK_PLANKS_COLUMN = registerBlock("dark_oak_planks_column", new ColumnBlock(FabricBlockSettings.copyOf(Blocks.DARK_OAK_PLANKS)));
    public static final Block MANGROVE_LOG_COLUMN = registerBlock("mangrove_log_column", new ColumnBlock(logSettings(MapColor.RED)));
    public static final Block MANGROVE_WOOD_COLUMN = registerBlock("mangrove_wood_column", new ColumnBlock(logSettings(MapColor.RED)));
    public static final Block STRIPPED_MANGROVE_LOG_COLUMN = registerBlock("stripped_mangrove_log_column", new ColumnBlock(logSettings(MapColor.RED)));
    public static final Block STRIPPED_MANGROVE_WOOD_COLUMN = registerBlock("stripped_mangrove_wood_column", new ColumnBlock(logSettings(MapColor.RED)));
    public static final Block MANGROVE_PLANKS_COLUMN = registerBlock("mangrove_planks_column", new ColumnBlock(FabricBlockSettings.copyOf(Blocks.MANGROVE_PLANKS)));
    public static final Block CHERRY_LOG_COLUMN = registerBlock("cherry_log_column", new ColumnBlock(logSettings(MapColor.TERRACOTTA_WHITE, BlockSoundGroup.CHERRY_WOOD)));
    public static final Block CHERRY_WOOD_COLUMN = registerBlock("cherry_wood_column", new ColumnBlock(logSettings(MapColor.TERRACOTTA_WHITE, BlockSoundGroup.CHERRY_WOOD)));
    public static final Block STRIPPED_CHERRY_LOG_COLUMN = registerBlock("stripped_cherry_log_column", new ColumnBlock(logSettings(MapColor.TERRACOTTA_WHITE, BlockSoundGroup.CHERRY_WOOD)));
    public static final Block STRIPPED_CHERRY_WOOD_COLUMN = registerBlock("stripped_cherry_wood_column", new ColumnBlock(logSettings(MapColor.TERRACOTTA_WHITE, BlockSoundGroup.CHERRY_WOOD)));
    public static final Block CHERRY_PLANKS_COLUMN = registerBlock("cherry_planks_column", new ColumnBlock(FabricBlockSettings.copyOf(Blocks.CHERRY_PLANKS)));
    public static final Block BAMBOO_BLOCK_COLUMN = registerBlock("bamboo_block_column", new ColumnBlock(logSettings(MapColor.YELLOW, BlockSoundGroup.BAMBOO_WOOD)));
    public static final Block STRIPPED_BAMBOO_BLOCK_COLUMN = registerBlock("stripped_bamboo_block_column", new ColumnBlock(logSettings(MapColor.YELLOW, BlockSoundGroup.BAMBOO_WOOD)));
    public static final Block BAMBOO_PLANKS_COLUMN = registerBlock("bamboo_planks_column", new ColumnBlock(FabricBlockSettings.copyOf(Blocks.BAMBOO_PLANKS)));
    public static final Block BAMBOO_MOSAIC_COLUMN = registerBlock("bamboo_mosaic_column", new ColumnBlock(FabricBlockSettings.copyOf(Blocks.BAMBOO_MOSAIC)));
    public static final Block AZALEA_LOG_COLUMN = registerBlock("azalea_log_column", new ColumnBlock(logSettings(MapColor.OFF_WHITE)));
    public static final Block AZALEA_WOOD_COLUMN = registerBlock("azalea_wood_column", new ColumnBlock(logSettings(MapColor.OFF_WHITE)));
    public static final Block STRIPPED_AZALEA_LOG_COLUMN = registerBlock("stripped_azalea_log_column", new ColumnBlock(logSettings(MapColor.OFF_WHITE)));
    public static final Block STRIPPED_AZALEA_WOOD_COLUMN = registerBlock("stripped_azalea_wood_column", new ColumnBlock(logSettings(MapColor.OFF_WHITE)));
    public static final Block AZALEA_PLANKS_COLUMN = registerBlock("azalea_planks_column", new ColumnBlock(FabricBlockSettings.copyOf(AZALEA_PLANKS)));
    public static final Block SWAMP_LOG_COLUMN = registerBlock("swamp_log_column", new ColumnBlock(logSettings(MapColor.PALE_GREEN)));
    public static final Block SWAMP_WOOD_COLUMN = registerBlock("swamp_wood_column", new ColumnBlock(logSettings(MapColor.PALE_GREEN)));
    public static final Block STRIPPED_SWAMP_LOG_COLUMN = registerBlock("stripped_swamp_log_column", new ColumnBlock(logSettings(MapColor.PALE_GREEN)));
    public static final Block STRIPPED_SWAMP_WOOD_COLUMN = registerBlock("stripped_swamp_wood_column", new ColumnBlock(logSettings(MapColor.PALE_GREEN)));
    public static final Block SWAMP_PLANKS_COLUMN = registerBlock("swamp_planks_column", new ColumnBlock(FabricBlockSettings.copyOf(SWAMP_PLANKS)));
    public static final Block CRIMSON_STEM_COLUMN = registerBlock("crimson_stem_column", new ColumnBlock(netherLogSettings(MapColor.DULL_PINK)));
    public static final Block CRIMSON_HYPHAE_COLUMN = registerBlock("crimson_hyphae_column", new ColumnBlock(netherLogSettings(MapColor.DULL_PINK)));
    public static final Block STRIPPED_CRIMSON_STEM_COLUMN = registerBlock("stripped_crimson_stem_column", new ColumnBlock(netherLogSettings(MapColor.DULL_PINK)));
    public static final Block STRIPPED_CRIMSON_HYPHAE_COLUMN = registerBlock("stripped_crimson_hyphae_column", new ColumnBlock(netherLogSettings(MapColor.DULL_PINK)));
    public static final Block CRIMSON_PLANKS_COLUMN = registerBlock("crimson_planks_column", new ColumnBlock(FabricBlockSettings.copyOf(Blocks.CRIMSON_PLANKS)));
    public static final Block WARPED_STEM_COLUMN = registerBlock("warped_stem_column", new ColumnBlock(netherLogSettings(MapColor.DARK_DULL_PINK)));
    public static final Block WARPED_HYPHAE_COLUMN = registerBlock("warped_hyphae_column", new ColumnBlock(netherLogSettings(MapColor.DARK_DULL_PINK)));
    public static final Block STRIPPED_WARPED_STEM_COLUMN = registerBlock("stripped_warped_stem_column", new ColumnBlock(netherLogSettings(MapColor.DARK_DULL_PINK)));
    public static final Block STRIPPED_WARPED_HYPHAE_COLUMN = registerBlock("stripped_warped_hyphae_column", new ColumnBlock(netherLogSettings(MapColor.DARK_DULL_PINK)));
    public static final Block WARPED_PLANKS_COLUMN = registerBlock("warped_planks_column", new ColumnBlock(FabricBlockSettings.copyOf(Blocks.WARPED_PLANKS)));
    public static final Block STONE_COLUMN = registerBlock("stone_column", new ColumnBlock(FabricBlockSettings.copyOf(Blocks.STONE)));
    public static final Block COBBLESTONE_COLUMN = registerBlock("cobblestone_column", new ColumnBlock(FabricBlockSettings.copyOf(Blocks.COBBLESTONE)));
    public static final Block MOSSY_COBBLESTONE_COLUMN = registerBlock("mossy_cobblestone_column", new ColumnBlock(FabricBlockSettings.copyOf(Blocks.MOSSY_COBBLESTONE)));
    public static final Block SMOOTH_STONE_COLUMN = registerBlock("smooth_stone_column", new ColumnBlock(FabricBlockSettings.copyOf(Blocks.SMOOTH_STONE)));
    public static final Block STONE_BRICKS_COLUMN = registerBlock("stone_bricks_column", new ColumnBlock(FabricBlockSettings.copyOf(Blocks.STONE_BRICKS)));
    public static final Block CRACKED_STONE_BRICKS_COLUMN = registerBlock("cracked_stone_bricks_column", new ColumnBlock(FabricBlockSettings.copyOf(Blocks.CRACKED_STONE_BRICKS)));
    public static final Block MOSSY_STONE_BRICKS_COLUMN = registerBlock("mossy_stone_bricks_column", new ColumnBlock(FabricBlockSettings.copyOf(Blocks.MOSSY_STONE_BRICKS)));
    public static final Block GRANITE_COLUMN = registerBlock("granite_column", new ColumnBlock(FabricBlockSettings.copyOf(Blocks.GRANITE)));
    public static final Block POLISHED_GRANITE_COLUMN = registerBlock("polished_granite_column", new ColumnBlock(FabricBlockSettings.copyOf(Blocks.POLISHED_GRANITE)));
    public static final Block GRANITE_BRICKS_COLUMN = registerBlock("granite_bricks_column", new ColumnBlock(FabricBlockSettings.copyOf(GRANITE_BRICKS)));
    public static final Block CRACKED_GRANITE_BRICKS_COLUMN = registerBlock("cracked_granite_bricks_column", new ColumnBlock(FabricBlockSettings.copyOf(CRACKED_GRANITE_BRICKS)));
    public static final Block MOSSY_GRANITE_BRICKS_COLUMN = registerBlock("mossy_granite_bricks_column", new ColumnBlock(FabricBlockSettings.copyOf(MOSSY_GRANITE_BRICKS)));
    public static final Block DIORITE_COLUMN = registerBlock("diorite_column", new ColumnBlock(FabricBlockSettings.copyOf(Blocks.DIORITE)));
    public static final Block POLISHED_DIORITE_COLUMN = registerBlock("polished_diorite_column", new ColumnBlock(FabricBlockSettings.copyOf(Blocks.POLISHED_DIORITE)));
    public static final Block DIORITE_BRICKS_COLUMN = registerBlock("diorite_bricks_column", new ColumnBlock(FabricBlockSettings.copyOf(DIORITE_BRICKS)));
    public static final Block CRACKED_DIORITE_BRICKS_COLUMN = registerBlock("cracked_diorite_bricks_column", new ColumnBlock(FabricBlockSettings.copyOf(CRACKED_DIORITE_BRICKS)));
    public static final Block MOSSY_DIORITE_BRICKS_COLUMN = registerBlock("mossy_diorite_bricks_column", new ColumnBlock(FabricBlockSettings.copyOf(MOSSY_DIORITE_BRICKS)));
    public static final Block ANDESITE_COLUMN = registerBlock("andesite_column", new ColumnBlock(FabricBlockSettings.copyOf(Blocks.ANDESITE)));
    public static final Block POLISHED_ANDESITE_COLUMN = registerBlock("polished_andesite_column", new ColumnBlock(FabricBlockSettings.copyOf(Blocks.POLISHED_ANDESITE)));
    public static final Block ANDESITE_BRICKS_COLUMN = registerBlock("andesite_bricks_column", new ColumnBlock(FabricBlockSettings.copyOf(ANDESITE_BRICKS)));
    public static final Block CRACKED_ANDESITE_BRICKS_COLUMN = registerBlock("cracked_andesite_bricks_column", new ColumnBlock(FabricBlockSettings.copyOf(CRACKED_ANDESITE_BRICKS)));
    public static final Block MOSSY_ANDESITE_BRICKS_COLUMN = registerBlock("mossy_andesite_bricks_column", new ColumnBlock(FabricBlockSettings.copyOf(MOSSY_ANDESITE_BRICKS)));
    public static final Block CALCITE_COLUMN = registerBlock("calcite_column", new ColumnBlock(FabricBlockSettings.copyOf(Blocks.CALCITE)));
    public static final Block POLISHED_CALCITE_COLUMN = registerBlock("polished_calcite_column", new ColumnBlock(FabricBlockSettings.copyOf(POLISHED_CALCITE)));
    public static final Block CALCITE_BRICKS_COLUMN = registerBlock("calcite_bricks_column", new ColumnBlock(FabricBlockSettings.copyOf(CALCITE_BRICKS)));
    public static final Block CRACKED_CALCITE_BRICKS_COLUMN = registerBlock("cracked_calcite_bricks_column", new ColumnBlock(FabricBlockSettings.copyOf(CRACKED_CALCITE_BRICKS)));
    public static final Block MOSSY_CALCITE_BRICKS_COLUMN = registerBlock("mossy_calcite_bricks_column", new ColumnBlock(FabricBlockSettings.copyOf(MOSSY_CALCITE_BRICKS)));
    public static final Block COBBLED_DEEPSLATE_COLUMN = registerBlock("cobbled_deepslate_column", new ColumnBlock(FabricBlockSettings.copyOf(Blocks.COBBLED_DEEPSLATE)));
    public static final Block POLISHED_DEEPSLATE_COLUMN = registerBlock("polished_deepslate_column", new ColumnBlock(FabricBlockSettings.copyOf(Blocks.POLISHED_DEEPSLATE)));
    public static final Block DEEPSLATE_BRICKS_COLUMN = registerBlock("deepslate_bricks_column", new ColumnBlock(FabricBlockSettings.copyOf(Blocks.DEEPSLATE_BRICKS)));
    public static final Block CRACKED_DEEPSLATE_BRICKS_COLUMN = registerBlock("cracked_deepslate_bricks_column", new ColumnBlock(FabricBlockSettings.copyOf(Blocks.CRACKED_DEEPSLATE_BRICKS)));
    public static final Block DEEPSLATE_TILES_COLUMN = registerBlock("deepslate_tiles_column", new ColumnBlock(FabricBlockSettings.copyOf(Blocks.DEEPSLATE_TILES)));
    public static final Block CRACKED_DEEPSLATE_TILES_COLUMN = registerBlock("cracked_deepslate_tiles_column", new ColumnBlock(FabricBlockSettings.copyOf(Blocks.CRACKED_DEEPSLATE_TILES)));
    public static final Block BRICKS_COLUMN = registerBlock("bricks_column", new ColumnBlock(FabricBlockSettings.copyOf(Blocks.BRICKS)));
    public static final Block CRACKED_BRICKS_COLUMN = registerBlock("cracked_bricks_column", new ColumnBlock(FabricBlockSettings.copyOf(CRACKED_BRICKS)));
    public static final Block MOSSY_BRICKS_COLUMN = registerBlock("mossy_bricks_column", new ColumnBlock(FabricBlockSettings.copyOf(MOSSY_BRICKS)));
    public static final Block MUD_BRICKS_COLUMN = registerBlock("mud_bricks_column", new ColumnBlock(FabricBlockSettings.copyOf(Blocks.MUD_BRICKS)));
    public static final Block SANDSTONE_COLUMN = registerBlock("sandstone_column", new ColumnBlock(FabricBlockSettings.copyOf(Blocks.SANDSTONE)));
    public static final Block SMOOTH_SANDSTONE_COLUMN = registerBlock("smooth_sandstone_column", new ColumnBlock(FabricBlockSettings.copyOf(Blocks.SMOOTH_SANDSTONE)));
    public static final Block POLISHED_SANDSTONE_COLUMN = registerBlock("polished_sandstone_column", new ColumnBlock(FabricBlockSettings.copyOf(DDBlocks.POLISHED_SANDSTONE)));
    public static final Block SANDSTONE_BRICKS_COLUMN = registerBlock("sandstone_bricks_column", new ColumnBlock(FabricBlockSettings.copyOf(SANDSTONE_BRICKS)));
    public static final Block CRACKED_SANDSTONE_BRICKS_COLUMN = registerBlock("cracked_sandstone_bricks_column", new ColumnBlock(FabricBlockSettings.copyOf(CRACKED_SANDSTONE_BRICKS)));
    public static final Block MOSSY_SANDSTONE_BRICKS_COLUMN = registerBlock("mossy_sandstone_bricks_column", new ColumnBlock(FabricBlockSettings.copyOf(MOSSY_SANDSTONE_BRICKS)));
    public static final Block RED_SANDSTONE_COLUMN = registerBlock("red_sandstone_column", new ColumnBlock(FabricBlockSettings.copyOf(Blocks.RED_SANDSTONE)));
    public static final Block SMOOTH_RED_SANDSTONE_COLUMN = registerBlock("smooth_red_sandstone_column", new ColumnBlock(FabricBlockSettings.copyOf(Blocks.SMOOTH_RED_SANDSTONE)));
    public static final Block POLISHED_RED_SANDSTONE_COLUMN = registerBlock("polished_red_sandstone_column", new ColumnBlock(FabricBlockSettings.copyOf(DDBlocks.POLISHED_RED_SANDSTONE)));
    public static final Block RED_SANDSTONE_BRICKS_COLUMN = registerBlock("red_sandstone_bricks_column", new ColumnBlock(FabricBlockSettings.copyOf(RED_SANDSTONE_BRICKS)));
    public static final Block CRACKED_RED_SANDSTONE_BRICKS_COLUMN = registerBlock("cracked_red_sandstone_bricks_column", new ColumnBlock(FabricBlockSettings.copyOf(CRACKED_RED_SANDSTONE_BRICKS)));
    public static final Block MOSSY_RED_SANDSTONE_BRICKS_COLUMN = registerBlock("mossy_red_sandstone_bricks_column", new ColumnBlock(FabricBlockSettings.copyOf(MOSSY_RED_SANDSTONE_BRICKS)));
    public static final Block PRISMARINE_COLUMN = registerBlock("prismarine_column", new ColumnBlock(FabricBlockSettings.copyOf(Blocks.PRISMARINE)));
    public static final Block PRISMARINE_BRICKS_COLUMN = registerBlock("prismarine_bricks_column", new ColumnBlock(FabricBlockSettings.copyOf(Blocks.PRISMARINE_BRICKS)));
    public static final Block DARK_PRISMARINE_COLUMN = registerBlock("dark_prismarine_column", new ColumnBlock(FabricBlockSettings.copyOf(Blocks.DARK_PRISMARINE)));
    public static final Block NETHERRACK_COLUMN = registerBlock("netherrack_column", new ColumnBlock(FabricBlockSettings.copyOf(Blocks.NETHERRACK)));
    public static final Block NETHER_BRICKS_COLUMN = registerBlock("nether_bricks_column", new ColumnBlock(FabricBlockSettings.copyOf(Blocks.NETHER_BRICKS)));
    public static final Block CRACKED_NETHER_BRICKS_COLUMN = registerBlock("cracked_nether_bricks_column", new ColumnBlock(FabricBlockSettings.copyOf(Blocks.CRACKED_NETHER_BRICKS)));
    public static final Block RED_NETHER_BRICKS_COLUMN = registerBlock("red_nether_bricks_column", new ColumnBlock(FabricBlockSettings.copyOf(Blocks.RED_NETHER_BRICKS)));
    public static final Block BASALT_COLUMN = registerBlock("basalt_column", new ColumnBlock(FabricBlockSettings.copyOf(Blocks.BASALT)));
    public static final Block SMOOTH_BASALT_COLUMN = registerBlock("smooth_basalt_column", new ColumnBlock(FabricBlockSettings.copyOf(Blocks.SMOOTH_BASALT)));
    public static final Block POLISHED_BASALT_COLUMN = registerBlock("polished_basalt_column", new ColumnBlock(FabricBlockSettings.copyOf(Blocks.POLISHED_BASALT)));
    public static final Block BLACKSTONE_COLUMN = registerBlock("blackstone_column", new ColumnBlock(FabricBlockSettings.copyOf(Blocks.BLACKSTONE)));
    public static final Block POLISHED_BLACKSTONE_COLUMN = registerBlock("polished_blackstone_column", new ColumnBlock(FabricBlockSettings.copyOf(Blocks.POLISHED_BLACKSTONE)));
    public static final Block POLISHED_BLACKSTONE_BRICKS_COLUMN = registerBlock("polished_blackstone_bricks_column", new ColumnBlock(FabricBlockSettings.copyOf(Blocks.POLISHED_BLACKSTONE_BRICKS)));
    public static final Block CRACKED_POLISHED_BLACKSTONE_BRICKS_COLUMN = registerBlock("cracked_polished_blackstone_bricks_column", new ColumnBlock(FabricBlockSettings.copyOf(Blocks.CRACKED_POLISHED_BLACKSTONE_BRICKS)));
    public static final Block END_STONE_BRICKS_COLUMN = registerBlock("end_stone_bricks_column", new ColumnBlock(FabricBlockSettings.copyOf(Blocks.END_STONE_BRICKS)));
    public static final Block PURPUR_BLOCK_COLUMN = registerBlock("purpur_block_column", new ColumnBlock(FabricBlockSettings.copyOf(Blocks.PURPUR_BLOCK)));
    public static final Block PURPUR_PILLAR_COLUMN = registerBlock("purpur_pillar_column", new ColumnBlock(FabricBlockSettings.copyOf(Blocks.PURPUR_PILLAR)));
    public static final Block CUT_IRON_COLUMN = registerBlock("cut_iron_column", new ColumnBlock(FabricBlockSettings.copyOf(CUT_IRON_BLOCK)));
    public static final Block CUT_GOLD_COLUMN = registerBlock("cut_gold_column", new ColumnBlock(FabricBlockSettings.copyOf(CUT_GOLD_BLOCK)));
    public static final Block CUT_EMERALD_COLUMN = registerBlock("cut_emerald_column", new ColumnBlock(FabricBlockSettings.copyOf(CUT_EMERALD_BLOCK)));
    public static final Block CUT_LAPIS_COLUMN = registerBlock("cut_lapis_column", new ColumnBlock(FabricBlockSettings.copyOf(CUT_LAPIS_BLOCK)));
    public static final Block CUT_DIAMOND_COLUMN = registerBlock("cut_diamond_column", new ColumnBlock(FabricBlockSettings.copyOf(CUT_DIAMOND_BLOCK)));
    public static final Block CUT_NETHERITE_COLUMN = registerBlock("cut_netherite_column", new ColumnBlock(FabricBlockSettings.copyOf(CUT_NETHERITE_BLOCK)));
    public static final Block QUARTZ_BLOCK_COLUMN = registerBlock("quartz_block_column", new ColumnBlock(FabricBlockSettings.copyOf(Blocks.QUARTZ_BLOCK)));
    public static final Block QUARTZ_BRICKS_COLUMN = registerBlock("quartz_bricks_column", new ColumnBlock(FabricBlockSettings.copyOf(Blocks.QUARTZ_BRICKS)));
    public static final Block QUARTZ_PILLAR_COLUMN = registerBlock("quartz_pillar_column", new ColumnBlock(FabricBlockSettings.copyOf(Blocks.QUARTZ_PILLAR)));
    public static final Block SMOOTH_QUARTZ_COLUMN = registerBlock("smooth_quartz_column", new ColumnBlock(FabricBlockSettings.copyOf(Blocks.SMOOTH_QUARTZ)));
    public static final Block CHARRED_QUARTZ_BLOCK_COLUMN = registerBlock("charred_quartz_block_column", new ColumnBlock(FabricBlockSettings.copyOf(CHARRED_QUARTZ_BLOCK)));
    public static final Block CHARRED_QUARTZ_BRICKS_COLUMN = registerBlock("charred_quartz_bricks_column", new ColumnBlock(FabricBlockSettings.copyOf(CHARRED_QUARTZ_BRICKS)));
    public static final Block CHARRED_QUARTZ_PILLAR_COLUMN = registerBlock("charred_quartz_pillar_column", new ColumnBlock(FabricBlockSettings.copyOf(CHARRED_QUARTZ_PILLAR)));
    public static final Block SMOOTH_CHARRED_QUARTZ_COLUMN = registerBlock("smooth_charred_quartz_block_column", new ColumnBlock(FabricBlockSettings.copyOf(SMOOTH_CHARRED_QUARTZ_BLOCK)));
    public static final Block POLISHED_AMETHYST_COLUMN = registerBlock("polished_amethyst_column", new ColumnBlock(FabricBlockSettings.copyOf(POLISHED_AMETHYST_BLOCK)));
    public static final Block POLISHED_AMETHYST_BRICKS_COLUMN = registerBlock("polished_amethyst_bricks_column", new ColumnBlock(FabricBlockSettings.copyOf(POLISHED_AMETHYST_BRICKS)));
    public static final Block CUT_POLISHED_AMETHYST_COLUMN = registerBlock("cut_polished_amethyst_column", new ColumnBlock(FabricBlockSettings.copyOf(CUT_POLISHED_AMETHYST)));
    public static final Block CUT_COPPER_COLUMN = registerBlock("cut_copper_column", new OxidizableColumnBlock(FabricBlockSettings.copyOf(Blocks.CUT_COPPER), Oxidizable.OxidationLevel.UNAFFECTED));
    public static final Block WAXED_CUT_COPPER_COLUMN = registerBlock("waxed_cut_copper_column", new ColumnBlock(FabricBlockSettings.copyOf(Blocks.WAXED_CUT_COPPER)));
    public static final Block EXPOSED_CUT_COPPER_COLUMN = registerBlock("exposed_cut_copper_column", new OxidizableColumnBlock(FabricBlockSettings.copyOf(Blocks.EXPOSED_CUT_COPPER), Oxidizable.OxidationLevel.EXPOSED));
    public static final Block WAXED_EXPOSED_CUT_COPPER_COLUMN = registerBlock("waxed_exposed_cut_copper_column", new ColumnBlock(FabricBlockSettings.copyOf(Blocks.WAXED_EXPOSED_CUT_COPPER)));
    public static final Block WEATHERED_CUT_COPPER_COLUMN = registerBlock("weathered_cut_copper_column", new OxidizableColumnBlock(FabricBlockSettings.copyOf(Blocks.WEATHERED_CUT_COPPER), Oxidizable.OxidationLevel.WEATHERED));
    public static final Block WAXED_WEATHERED_CUT_COPPER_COLUMN = registerBlock("waxed_weathered_cut_copper_column", new ColumnBlock(FabricBlockSettings.copyOf(Blocks.WAXED_WEATHERED_CUT_COPPER)));
    public static final Block OXIDIZED_CUT_COPPER_COLUMN = registerBlock("oxidized_cut_copper_column", new OxidizableColumnBlock(FabricBlockSettings.copyOf(Blocks.OXIDIZED_CUT_COPPER), Oxidizable.OxidationLevel.OXIDIZED));
    public static final Block WAXED_OXIDIZED_CUT_COPPER_COLUMN = registerBlock("waxed_oxidized_cut_copper_column", new ColumnBlock(FabricBlockSettings.copyOf(Blocks.WAXED_OXIDIZED_CUT_COPPER)));
    public static final Block PACKED_ICE_BRICKS_COLUMN = registerBlock("packed_ice_bricks_column", new ColumnBlock(FabricBlockSettings.copyOf(PACKED_ICE_BRICKS)));
    public static final Block CRACKED_PACKED_ICE_BRICKS_COLUMN = registerBlock("cracked_packed_ice_bricks_column", new ColumnBlock(FabricBlockSettings.copyOf(CRACKED_PACKED_ICE_BRICKS)));
    public static final Block LARGE_PACKED_ICE_BRICKS_COLUMN = registerBlock("large_packed_ice_bricks_column", new ColumnBlock(FabricBlockSettings.copyOf(LARGE_PACKED_ICE_BRICKS)));
    public static final Block LARGE_CRACKED_PACKED_ICE_BRICKS_COLUMN = registerBlock("large_cracked_packed_ice_bricks_column", new ColumnBlock(FabricBlockSettings.copyOf(LARGE_CRACKED_PACKED_ICE_BRICKS)));
    public static final Block POLISHED_PACKED_ICE_COLUMN = registerBlock("polished_packed_ice_column", new ColumnBlock(FabricBlockSettings.copyOf(POLISHED_PACKED_ICE)));
    public static final Block BLUE_ICE_BRICKS_COLUMN = registerBlock("blue_ice_bricks_column", new ColumnBlock(FabricBlockSettings.copyOf(BLUE_ICE_BRICKS)));
    public static final Block CRACKED_BLUE_ICE_BRICKS_COLUMN = registerBlock("cracked_blue_ice_bricks_column", new ColumnBlock(FabricBlockSettings.copyOf(CRACKED_BLUE_ICE_BRICKS)));
    public static final Block LARGE_BLUE_ICE_BRICKS_COLUMN = registerBlock("large_blue_ice_bricks_column", new ColumnBlock(FabricBlockSettings.copyOf(LARGE_BLUE_ICE_BRICKS)));
    public static final Block LARGE_CRACKED_BLUE_ICE_BRICKS_COLUMN = registerBlock("large_cracked_blue_ice_bricks_column", new ColumnBlock(FabricBlockSettings.copyOf(LARGE_CRACKED_BLUE_ICE_BRICKS)));
    public static final Block POLISHED_BLUE_ICE_COLUMN = registerBlock("polished_blue_ice_column", new ColumnBlock(FabricBlockSettings.copyOf(POLISHED_BLUE_ICE)));
    public static final Block POLISHED_OBSIDIAN_COLUMN = registerBlock("polished_obsidian_column", new ColumnBlock(FabricBlockSettings.copyOf(POLISHED_OBSIDIAN)));
    public static final Block WAXED_POLISHED_OBSIDIAN_COLUMN = registerBlock("waxed_polished_obsidian_column", new ColumnBlock(FabricBlockSettings.copyOf(WAXED_POLISHED_OBSIDIAN)));
    public static final Block BONE_BLOCK_COLUMN = registerBlock("bone_block_column", new ColumnBlock(FabricBlockSettings.copyOf(Blocks.BONE_BLOCK)));
    public static final Block DRIPSTONE_BLOCK_COLUMN = registerBlock("dripstone_block_column", new ColumnBlock(FabricBlockSettings.copyOf(Blocks.DRIPSTONE_BLOCK)));
    public static final Block POLISHED_DRIPSTONE_COLUMN = registerBlock("polished_dripstone_column", new ColumnBlock(FabricBlockSettings.copyOf(POLISHED_DRIPSTONE)));
    public static final Block DRIPSTONE_BRICKS_COLUMN = registerBlock("dripstone_bricks_column", new ColumnBlock(FabricBlockSettings.copyOf(DRIPSTONE_BRICKS)));
    public static final Block CRACKED_DRIPSTONE_BRICKS_COLUMN = registerBlock("cracked_dripstone_bricks_column", new ColumnBlock(FabricBlockSettings.copyOf(CRACKED_DRIPSTONE_BRICKS)));
    public static final Block MOSSY_DRIPSTONE_BRICKS_COLUMN = registerBlock("mossy_dripstone_bricks_column", new ColumnBlock(FabricBlockSettings.copyOf(MOSSY_DRIPSTONE_BRICKS)));
    public static final Block MOSSY_COBBLED_DEEPSLATE_COLUMN = registerBlock("mossy_cobbled_deepslate_column", new ColumnBlock(FabricBlockSettings.copyOf(MOSSY_COBBLED_DEEPSLATE)));
    public static final Block MOSSY_DEEPSLATE_BRICKS_COLUMN = registerBlock("mossy_deepslate_bricks_column", new ColumnBlock(FabricBlockSettings.copyOf(MOSSY_DEEPSLATE_BRICKS)));
    public static final Block CRACKED_TUFF_BRICKS_COLUMN = registerBlock("cracked_tuff_bricks_column", new ColumnBlock(FabricBlockSettings.copyOf(Blocks.TUFF_BRICKS)));
    public static final Block MOSSY_TUFF_BRICKS_COLUMN = registerBlock("mossy_tuff_bricks_column", new ColumnBlock(FabricBlockSettings.copyOf(Blocks.TUFF_BRICKS)));
    public static final Block TUFF_BRICKS_COLUMN = registerBlock("tuff_bricks_column", new ColumnBlock(FabricBlockSettings.copyOf(Blocks.TUFF_BRICKS)));
    public static final Block POLISHED_TUFF_COLUMN = registerBlock("polished_tuff_column", new ColumnBlock(FabricBlockSettings.copyOf(Blocks.POLISHED_TUFF)));
    public static final Block TUFF_COLUMN = registerBlock("tuff_column", new ColumnBlock(FabricBlockSettings.copyOf(Blocks.TUFF)));


    public static Block registerBlock(String name, Block block) {
        registerBlockItem(name, block, false);
        return Registry.register(Registries.BLOCK, new Identifier(MOD_ID, name), block);
    }

    public static Block registerBlock(String name, Block block, boolean fireproof) {
        registerBlockItem(name, block, fireproof);
        return Registry.register(Registries.BLOCK, new Identifier(MOD_ID, name), block);
    }

    private static Item registerBlockItem(String name, Block block, boolean fireproof) {
        FabricItemSettings settings = fireproof ? new FabricItemSettings().fireproof() : new FabricItemSettings();
        return Registry.register(Registries.ITEM, new Identifier(MOD_ID, name), new BlockItem(block, settings));
    }

    private static void registerStrippableBlocks() {
        StrippableBlockRegistry.register(AZALEA_LOG, STRIPPED_AZALEA_LOG);
        StrippableBlockRegistry.register(AZALEA_WOOD, STRIPPED_AZALEA_WOOD);
        StrippableBlockRegistry.register(SWAMP_LOG, STRIPPED_SWAMP_LOG);
        StrippableBlockRegistry.register(SWAMP_WOOD, STRIPPED_SWAMP_WOOD);
    }

    private static void registerFlammableBlocks() {
        FlammableBlockRegistry instance = FlammableBlockRegistry.getDefaultInstance();

        instance.add(AZALEA_LOG, 5, 5);
        instance.add(AZALEA_WOOD, 5, 5);
        instance.add(STRIPPED_AZALEA_LOG, 5, 5);
        instance.add(STRIPPED_AZALEA_WOOD, 5, 5);
        instance.add(AZALEA_PLANKS, 20, 5);
        instance.add(AZALEA_SLAB, 20, 5);
        instance.add(AZALEA_STAIRS, 20, 5);
        instance.add(AZALEA_FENCE, 20, 5);
        instance.add(AZALEA_PRESSURE_PLATE, 20, 5);
        instance.add(AZALEA_DOOR, 20, 5);
        instance.add(AZALEA_TRAPDOOR, 20, 5);
        instance.add(SWAMP_LEAVES, 30, 60);
        instance.add(SWAMP_LOG, 5, 5);
        instance.add(SWAMP_WOOD, 5, 5);
        instance.add(STRIPPED_SWAMP_LOG, 5, 5);
        instance.add(STRIPPED_SWAMP_WOOD, 5, 5);
        instance.add(SWAMP_PLANKS, 20, 5);
        instance.add(SWAMP_SLAB, 20, 5);
        instance.add(SWAMP_STAIRS, 20, 5);
        instance.add(SWAMP_FENCE, 20, 5);
        instance.add(SWAMP_PRESSURE_PLATE, 20, 5);
        instance.add(SWAMP_DOOR, 20, 5);
        instance.add(SWAMP_TRAPDOOR, 20, 5);
        instance.add(CHARCOAL_BLOCK, 5, 5);
        instance.add(BLAZING_COAL_BLOCK, 5, 5);

        instance.add(OAK_LOG_COLUMN, 5, 5);
        instance.add(STRIPPED_OAK_LOG_COLUMN, 5, 5);
        instance.add(OAK_WOOD_COLUMN, 5, 5);
        instance.add(STRIPPED_OAK_WOOD_COLUMN, 5, 5);
        instance.add(OAK_PLANKS_COLUMN, 20, 5);
        instance.add(SPRUCE_LOG_COLUMN, 5, 5);
        instance.add(STRIPPED_SPRUCE_LOG_COLUMN, 5, 5);
        instance.add(SPRUCE_WOOD_COLUMN, 5, 5);
        instance.add(STRIPPED_SPRUCE_WOOD_COLUMN, 5, 5);
        instance.add(SPRUCE_PLANKS_COLUMN, 20, 5);
        instance.add(BIRCH_LOG_COLUMN, 5, 5);
        instance.add(STRIPPED_BIRCH_LOG_COLUMN, 5, 5);
        instance.add(BIRCH_WOOD_COLUMN, 5, 5);
        instance.add(STRIPPED_BIRCH_WOOD_COLUMN, 5, 5);
        instance.add(BIRCH_PLANKS_COLUMN, 20, 5);
        instance.add(JUNGLE_LOG_COLUMN, 5, 5);
        instance.add(STRIPPED_JUNGLE_LOG_COLUMN, 5, 5);
        instance.add(JUNGLE_WOOD_COLUMN, 5, 5);
        instance.add(STRIPPED_JUNGLE_WOOD_COLUMN, 5, 5);
        instance.add(JUNGLE_PLANKS_COLUMN, 20, 5);
        instance.add(ACACIA_LOG_COLUMN, 5, 5);
        instance.add(STRIPPED_ACACIA_LOG_COLUMN, 5, 5);
        instance.add(ACACIA_WOOD_COLUMN, 5, 5);
        instance.add(STRIPPED_ACACIA_WOOD_COLUMN, 5, 5);
        instance.add(ACACIA_PLANKS_COLUMN, 20, 5);
        instance.add(DARK_OAK_LOG_COLUMN, 5, 5);
        instance.add(STRIPPED_DARK_OAK_LOG_COLUMN, 5, 5);
        instance.add(DARK_OAK_WOOD_COLUMN, 5, 5);
        instance.add(STRIPPED_DARK_OAK_WOOD_COLUMN, 5, 5);
        instance.add(DARK_OAK_PLANKS_COLUMN, 20, 5);
        instance.add(MANGROVE_LOG_COLUMN, 5, 5);
        instance.add(STRIPPED_MANGROVE_LOG_COLUMN, 5, 5);
        instance.add(MANGROVE_WOOD_COLUMN, 5, 5);
        instance.add(STRIPPED_MANGROVE_WOOD_COLUMN, 5, 5);
        instance.add(MANGROVE_PLANKS_COLUMN, 20, 5);
        instance.add(CHERRY_LOG_COLUMN, 5, 5);
        instance.add(STRIPPED_CHERRY_LOG_COLUMN, 5, 5);
        instance.add(CHERRY_WOOD_COLUMN, 5, 5);
        instance.add(STRIPPED_CHERRY_WOOD_COLUMN, 5, 5);
        instance.add(CHERRY_PLANKS_COLUMN, 20, 5);
        instance.add(BAMBOO_BLOCK_COLUMN, 5, 5);
        instance.add(STRIPPED_BAMBOO_BLOCK_COLUMN, 5, 5);
        instance.add(BAMBOO_PLANKS_COLUMN, 20, 5);
        instance.add(BAMBOO_MOSAIC_COLUMN, 20, 5);
        instance.add(AZALEA_LOG_COLUMN, 5, 5);
        instance.add(STRIPPED_AZALEA_LOG_COLUMN, 5, 5);
        instance.add(AZALEA_WOOD_COLUMN, 5, 5);
        instance.add(STRIPPED_AZALEA_WOOD_COLUMN, 5, 5);
        instance.add(AZALEA_PLANKS_COLUMN, 20, 5);
        instance.add(SWAMP_LOG_COLUMN, 5, 5);
        instance.add(STRIPPED_SWAMP_LOG_COLUMN, 5, 5);
        instance.add(SWAMP_WOOD_COLUMN, 5, 5);
        instance.add(STRIPPED_SWAMP_WOOD_COLUMN, 5, 5);
        instance.add(SWAMP_PLANKS_COLUMN, 20, 5);
    }

    private static FabricBlockSettings logSettings(MapColor color, BlockSoundGroup soundGroup) {
        return FabricBlockSettings.create().mapColor(state -> color).instrument(Instrument.BASS).strength(2.0f).sounds(soundGroup).burnable();
    }

    private static FabricBlockSettings logSettings(MapColor color) {
        return FabricBlockSettings.create().mapColor(state -> color).instrument(Instrument.BASS).strength(2.0f).sounds(BlockSoundGroup.WOOD).burnable();
    }

    private static FabricBlockSettings netherLogSettings(MapColor color) {
        return FabricBlockSettings.create().mapColor(state -> color).instrument(Instrument.BASS).strength(2.0f).sounds(BlockSoundGroup.NETHER_STEM);
    }

    private static FabricBlockSettings createLogSettings(MapColor top, MapColor side) {
        return FabricBlockSettings.create().mapColor(state -> state.get(PillarBlock.AXIS) == Direction.Axis.Y ? top : side).instrument(Instrument.BASS).strength(2.0f).sounds(BlockSoundGroup.WOOD).burnable();
    }

    public static void registerBlocks() {
        DeployAndDestroy.LOGGER.info("Registering Blocks For " + MOD_ID);
        registerStrippableBlocks();
        registerFlammableBlocks();
    }

    public static class WoodTypes {

        public static final WoodType AZALEA = new WoodTypeBuilder().register(new Identifier(MOD_ID, "azalea"), DDBlocks.BlockSetTypes.AZALEA);
        public static final WoodType SWAMP = new WoodTypeBuilder().register(new Identifier(MOD_ID, "swamp"), DDBlocks.BlockSetTypes.SWAMP);
    }

    public static class BlockSetTypes {

        public static final BlockSetType AZALEA = new BlockSetType("azalea");
        public static final BlockSetType SWAMP = new BlockSetType("swamp");
    }
}
