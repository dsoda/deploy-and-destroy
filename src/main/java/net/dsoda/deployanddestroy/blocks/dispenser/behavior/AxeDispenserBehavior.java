package net.dsoda.deployanddestroy.blocks.dispenser.behavior;

import com.google.common.collect.ImmutableMap;
import net.dsoda.deployanddestroy.blocks.DDBlocks;
import net.dsoda.deployanddestroy.util.ColumnStripperHelper;
import net.dsoda.deployanddestroy.util.DDOxidationHelper;
import net.minecraft.block.*;
import net.minecraft.block.dispenser.FallibleItemDispenserBehavior;
import net.minecraft.block.enums.PistonType;
import net.minecraft.client.util.ParticleUtil;
import net.minecraft.item.HoneycombItem;
import net.minecraft.item.ItemStack;
import net.minecraft.particle.ParticleTypes;
import net.minecraft.sound.SoundCategory;
import net.minecraft.sound.SoundEvents;
import net.minecraft.state.property.Properties;
import net.minecraft.util.math.BlockPointer;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.intprovider.UniformIntProvider;
import net.minecraft.world.World;
import net.minecraft.world.event.GameEvent;

import java.util.Map;
import java.util.Optional;

public class AxeDispenserBehavior extends FallibleItemDispenserBehavior {

    @Override
    protected ItemStack dispenseSilently(BlockPointer pointer, ItemStack stack) {
        World serverWorld = pointer.world();
        if (!serverWorld.isClient()) {
            BlockPos blockPos = pointer.pos().offset(pointer.state().get(DispenserBlock.FACING));
            this.setSuccess(
                tryRemoveSlime(serverWorld, blockPos)
            ||  tryDoVanillaAxeBehaviors(serverWorld.getBlockState(blockPos), blockPos, serverWorld)
            ||  tryUnwaxDDBlock(blockPos, serverWorld)
            ||  tryDecreaseOxidation(blockPos, serverWorld)
            ||  tryStripColumn(blockPos, serverWorld)
            );
            if (this.isSuccess() && stack.damage(1, serverWorld.getRandom(), null)) {
                stack.setCount(0);
            }
        }
        return stack;
    }

    // axe behavior methods

    private static boolean tryRemoveSlime(World world, BlockPos pos) {
        BlockState state = world.getBlockState(pos);
        boolean notExtended = state.isOf(Blocks.STICKY_PISTON)
                && !state.get(Properties.EXTENDED);
        boolean extended = state.isOf(Blocks.PISTON_HEAD)
                && state.get(Properties.PISTON_TYPE).asString().equals("sticky");
        if(notExtended || extended) {
            if (notExtended) replaceStickyPistonWithNormalAtPos(pos, world);
            else replaceExtendedStickyPistonWithNormalAtPos(pos, world);
            world.playSound(null, pos, SoundEvents.ITEM_AXE_STRIP, SoundCategory.BLOCKS, 1f, 1f);
            world.emitGameEvent(null, GameEvent.BLOCK_CHANGE, pos);
            return true;
        }
        return false;
    }

    private static boolean tryDoVanillaAxeBehaviors(BlockState state, BlockPos pos, World world) {
        Optional<BlockState> optional = getStrippedState(state);
        Optional<BlockState> optional2 = Oxidizable.getDecreasedOxidationState(state);
        Optional<BlockState> optional3 = Optional.ofNullable(HoneycombItem.WAXED_TO_UNWAXED_BLOCKS.get().get(state.getBlock())).map(block -> block.getStateWithProperties(state));
        Optional<BlockState> optional5 = Optional.empty();
        if (optional.isPresent()) {
            world.playSound(null, pos, SoundEvents.ITEM_AXE_STRIP, SoundCategory.BLOCKS, 1f, 1f);
            optional5 = optional;
        }
        if (optional2.isPresent()) {
            world.playSound(null, pos, SoundEvents.ITEM_AXE_SCRAPE, SoundCategory.BLOCKS, 1f, 1f);
            optional5 = optional2;
        }
        if(optional3.isPresent()) {
            world.playSound(null, pos, SoundEvents.ITEM_AXE_WAX_OFF, SoundCategory.BLOCKS, 1f, 1f);
            optional5 = optional3;
        }
        if (optional5.isPresent()) {
            world.setBlockState(pos, optional5.get(), Block.NOTIFY_ALL | Block.REDRAW_ON_MAIN_THREAD);
            world.emitGameEvent(null, GameEvent.BLOCK_CHANGE, pos);
            ParticleUtil.spawnParticle(world, pos, ParticleTypes.WAX_OFF, UniformIntProvider.create(3, 5));
            return true;
        }
        return false;
    }

    private static boolean tryUnwaxDDBlock(BlockPos pos, World world) {
        BlockState oldState = world.getBlockState(pos);
        Optional<Block> optional = Optional.ofNullable(DDOxidationHelper.WAXED_TO_UNWAXED.get(oldState.getBlock()));
        if (optional.isPresent()) {
            world.playSound(null, pos, SoundEvents.ITEM_AXE_WAX_OFF, SoundCategory.BLOCKS, 1f, 1f);
            world.setBlockState(pos, optional.get().getStateWithProperties(oldState), Block.NOTIFY_ALL | Block.REDRAW_ON_MAIN_THREAD);
            world.emitGameEvent(null, GameEvent.BLOCK_CHANGE, pos);
            ParticleUtil.spawnParticle(world, pos, ParticleTypes.WAX_OFF, UniformIntProvider.create(3, 5));
            return true;
        }
        return false;
    }

    private static boolean tryStripColumn(BlockPos pos, World world) {
        BlockState state = world.getBlockState(pos);
        BlockState newState;
        Optional<Block> optional = ColumnStripperHelper.getStrippedColumn(state.getBlock());
        if (optional.isPresent()) {
            newState = optional.get().getStateWithProperties(state);
            world.playSound(null, pos, SoundEvents.ITEM_AXE_STRIP, SoundCategory.BLOCKS, 1f, 1f);
            world.setBlockState(pos, newState, Block.NOTIFY_ALL | Block.REDRAW_ON_MAIN_THREAD);
            world.emitGameEvent(null, GameEvent.BLOCK_CHANGE, pos);
            return true;
        }
        return false;
    }

    private static boolean tryDecreaseOxidation(BlockPos pos, World world) {
        BlockState oldState = world.getBlockState(pos);
        Optional<Block> optional = Optional.ofNullable(DDOxidationHelper.OXIDIZABLE_DECREASE.get(oldState.getBlock()));
        if (optional.isPresent()) {
            world.playSound(null, pos, SoundEvents.ITEM_AXE_SCRAPE, SoundCategory.BLOCKS, 1f, 1f);
            world.setBlockState(pos, optional.get().getStateWithProperties(oldState), Block.NOTIFY_ALL | Block.REDRAW_ON_MAIN_THREAD);
            world.emitGameEvent(null, GameEvent.BLOCK_CHANGE, pos);
            return true;
        }
        return false;
    }

    // helper methods for stripping sticky pistons

    private static void replaceStickyPistonWithNormalAtPos(BlockPos pos, World world) {
        BlockState state = world.getBlockState(pos); // piston retracts briefly if state is passed in instead of set here
        world.setBlockState(pos, Blocks.PISTON.getStateWithProperties(state), Block.NOTIFY_ALL | Block.REDRAW_ON_MAIN_THREAD);
    }

    private static void replaceExtendedStickyPistonWithNormalAtPos(BlockPos pos, World world) {
        BlockState state = world.getBlockState(pos); // piston retracts briefly if state is passed in instead of set here
        BlockPos connected = pos.offset(state.get(PistonBlock.FACING), -1);
        world.setBlockState(pos,Blocks.PISTON_HEAD.getStateWithProperties(state).with(Properties.PISTON_TYPE, PistonType.DEFAULT));
        replaceStickyPistonWithNormalAtPos(connected, world);
    }

    // end sticky piston stripping

    // log stripping

    private static Optional<BlockState> getStrippedState(BlockState state) {
        final Map<Block, Block> STRIPPED_BLOCKS = new ImmutableMap.Builder<Block, Block>()
                .put(Blocks.OAK_WOOD, Blocks.STRIPPED_OAK_WOOD)
                .put(Blocks.OAK_LOG, Blocks.STRIPPED_OAK_LOG)
                .put(Blocks.DARK_OAK_WOOD, Blocks.STRIPPED_DARK_OAK_WOOD)
                .put(Blocks.DARK_OAK_LOG, Blocks.STRIPPED_DARK_OAK_LOG)
                .put(Blocks.ACACIA_WOOD, Blocks.STRIPPED_ACACIA_WOOD)
                .put(Blocks.ACACIA_LOG, Blocks.STRIPPED_ACACIA_LOG)
                .put(Blocks.CHERRY_WOOD, Blocks.STRIPPED_CHERRY_WOOD)
                .put(Blocks.CHERRY_LOG, Blocks.STRIPPED_CHERRY_LOG)
                .put(Blocks.BIRCH_WOOD, Blocks.STRIPPED_BIRCH_WOOD)
                .put(Blocks.BIRCH_LOG, Blocks.STRIPPED_BIRCH_LOG)
                .put(Blocks.JUNGLE_WOOD, Blocks.STRIPPED_JUNGLE_WOOD)
                .put(Blocks.JUNGLE_LOG, Blocks.STRIPPED_JUNGLE_LOG)
                .put(Blocks.SPRUCE_WOOD, Blocks.STRIPPED_SPRUCE_WOOD)
                .put(Blocks.SPRUCE_LOG, Blocks.STRIPPED_SPRUCE_LOG)
                .put(Blocks.WARPED_STEM, Blocks.STRIPPED_WARPED_STEM)
                .put(Blocks.WARPED_HYPHAE, Blocks.STRIPPED_WARPED_HYPHAE)
                .put(Blocks.CRIMSON_STEM, Blocks.STRIPPED_CRIMSON_STEM)
                .put(Blocks.CRIMSON_HYPHAE, Blocks.STRIPPED_CRIMSON_HYPHAE)
                .put(Blocks.MANGROVE_WOOD, Blocks.STRIPPED_MANGROVE_WOOD)
                .put(Blocks.MANGROVE_LOG, Blocks.STRIPPED_MANGROVE_LOG)
                .put(Blocks.BAMBOO_BLOCK, Blocks.STRIPPED_BAMBOO_BLOCK)
                .put(DDBlocks.AZALEA_LOG, DDBlocks.STRIPPED_AZALEA_LOG)
                .put(DDBlocks.AZALEA_WOOD, DDBlocks.STRIPPED_AZALEA_WOOD)
                .put(DDBlocks.SWAMP_LOG, DDBlocks.STRIPPED_SWAMP_LOG)
                .put(DDBlocks.SWAMP_WOOD, DDBlocks.STRIPPED_SWAMP_WOOD)
                .build();
        return Optional.ofNullable(STRIPPED_BLOCKS.get(state.getBlock()))
                .map(block -> block.getDefaultState()
                        .with(PillarBlock.AXIS, state.get(PillarBlock.AXIS)));
    }

    // end log stripping

}
