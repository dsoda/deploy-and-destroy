package net.dsoda.deployanddestroy.blocks.dispenser.behavior;

import net.dsoda.deployanddestroy.util.behavior.DDGeneralBehaviorHelper;
import net.minecraft.block.DispenserBlock;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.passive.AnimalEntity;
import net.minecraft.entity.passive.CowEntity;
import net.minecraft.entity.passive.GoatEntity;
import net.minecraft.entity.passive.SquidEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Items;
import net.minecraft.predicate.entity.EntityPredicates;
import net.minecraft.server.world.ServerWorld;
import net.minecraft.sound.SoundCategory;
import net.minecraft.sound.SoundEvent;
import net.minecraft.sound.SoundEvents;
import net.minecraft.util.math.BlockPointer;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Box;

import java.util.List;

public class MilkAnimalDispenserBehavior extends DDGeneralBehaviorHelper {

    @Override
    protected ItemStack dispenseSilently(BlockPointer pointer, ItemStack stack) {
        this.setSuccess(true);
        ServerWorld world = pointer.world();
        BlockPos facing = pointer.pos().offset(pointer.state().get(DispenserBlock.FACING));
        Box facingBox = new Box(facing);

        List<AnimalEntity> milkable = world.getEntitiesByClass(AnimalEntity.class, facingBox, EntityPredicates.VALID_LIVING_ENTITY.and((milkableEntity)
            -> !((AnimalEntity) milkableEntity).isBaby() && (milkableEntity instanceof CowEntity || milkableEntity instanceof GoatEntity)));

        List<SquidEntity> squids = world.getEntitiesByClass(SquidEntity.class, facingBox, EntityPredicates.VALID_LIVING_ENTITY);

        if (!milkable.isEmpty()) {
            Entity milked = milkable.get(world.random.nextInt(milkable.size()));
            world.playSound(null, facing, milkSound(milked), SoundCategory.NEUTRAL, 1.0f, 1.0f);
            return this.addOrDispense(pointer, stack, new ItemStack(Items.MILK_BUCKET));
        }

        if (!squids.isEmpty()) {
            Entity squid = squids.get(world.random.nextInt(squids.size()));
            world.playSound(null, facing, milkSound(squid), SoundCategory.NEUTRAL, 1.0f, 1.0f);
            return this.addOrDispense(pointer, stack, new ItemStack(Items.MILK_BUCKET));
        }

        this.setSuccess(false);
        return stack;
    }

    private static SoundEvent milkSound(Entity creature) {
        if(creature.getType() == EntityType.GOAT)
            return ((GoatEntity) creature).isScreaming() ? SoundEvents.ENTITY_GOAT_SCREAMING_MILK : SoundEvents.ENTITY_GOAT_MILK;
        return SoundEvents.ENTITY_COW_MILK;
    }
}
