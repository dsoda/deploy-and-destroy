package net.dsoda.deployanddestroy.blocks.dispenser.behavior;

import net.dsoda.deployanddestroy.util.behavior.DDUnmossHelper;
import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.block.Blocks;
import net.minecraft.block.DispenserBlock;
import net.minecraft.block.dispenser.FallibleItemDispenserBehavior;
import net.minecraft.entity.ItemEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Items;
import net.minecraft.server.world.ServerWorld;
import net.minecraft.sound.SoundCategory;
import net.minecraft.sound.SoundEvents;
import net.minecraft.util.math.BlockPointer;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Direction;
import net.minecraft.world.event.GameEvent;

import java.util.Optional;

public class DDShearsDispenserBehavior extends FallibleItemDispenserBehavior {

    @Override
    protected ItemStack dispenseSilently(BlockPointer pointer, ItemStack stack) {
        this.setSuccess(true);
        ServerWorld world = pointer.world();
        Direction direction = pointer.state().get(DispenserBlock.FACING);
        BlockPos pos = pointer.pos().offset(direction);
        if (direction == Direction.UP || direction == Direction.DOWN) { direction = Direction.NORTH; }
        BlockState state = world.getBlockState(pos);
        Optional<BlockState> optional = DDUnmossHelper.getMossyBlock(world, pos, state);
        if(state.getBlock() == Blocks.PUMPKIN) {
            world.playSound(null, pos, SoundEvents.BLOCK_PUMPKIN_CARVE, SoundCategory.BLOCKS, 1.0f, 1.0f);
            world.setBlockState(pos, Blocks.CARVED_PUMPKIN.getStateWithProperties(state), Block.NOTIFY_ALL | Block.REDRAW_ON_MAIN_THREAD);
            ItemEntity itemEntity = new ItemEntity(world, (double)pos.getX() + 0.5 + (double)direction.getOffsetX() * 0.65, (double)pos.getY() + 0.1, (double)pos.getZ() + 0.5 + (double)direction.getOffsetZ() * 0.65, new ItemStack(Items.PUMPKIN_SEEDS, 4));
            itemEntity.setVelocity(0.05 * (double)direction.getOffsetX() + world.random.nextDouble() * 0.02, 0.05, 0.05 * (double)direction.getOffsetZ() + world.random.nextDouble() * 0.02);
            world.spawnEntity(itemEntity);
            world.emitGameEvent(null, GameEvent.BLOCK_CHANGE, pos);
        } else if (optional.isPresent()) {
            DDUnmossHelper.removeMoss(world, direction, pos, optional.get());
        } else {
            this.setSuccess(false);
        }
        if (this.isSuccess() && stack.damage(1, world.random, null)) {
            stack.setCount(0);
        }
        return stack;
    }
}
