package net.dsoda.deployanddestroy.blocks.dispenser.behavior;

import net.dsoda.deployanddestroy.blocks.DDBlocks;
import net.minecraft.block.Block;
import net.minecraft.block.DispenserBlock;
import net.minecraft.block.dispenser.FallibleItemDispenserBehavior;
import net.minecraft.item.ItemStack;
import net.minecraft.sound.SoundCategory;
import net.minecraft.sound.SoundEvents;
import net.minecraft.util.math.BlockPointer;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraft.world.event.GameEvent;

public class HoneyCombDispenserBehavior extends FallibleItemDispenserBehavior {

    @Override
    protected ItemStack dispenseSilently(BlockPointer pointer, ItemStack stack) {
        this.setSuccess(false);
        World world = pointer.world();
        BlockPos pos = pointer.pos().offset(pointer.state().get(DispenserBlock.FACING));
        if (world.getBlockState(pos).isOf(DDBlocks.POLISHED_OBSIDIAN)) {
            world.playSound(null, pos, SoundEvents.ITEM_HONEYCOMB_WAX_ON, SoundCategory.BLOCKS, 1f, 1f);
            world.setBlockState(pos, DDBlocks.WAXED_POLISHED_OBSIDIAN.getDefaultState(), Block.NOTIFY_ALL | Block.REDRAW_ON_MAIN_THREAD);
            world.emitGameEvent(null, GameEvent.BLOCK_CHANGE, pos);
            stack.decrement(1);
            this.setSuccess(true);
        }
        return stack;
    }
}
