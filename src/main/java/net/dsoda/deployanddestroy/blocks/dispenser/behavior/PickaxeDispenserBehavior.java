package net.dsoda.deployanddestroy.blocks.dispenser.behavior;

import net.dsoda.deployanddestroy.util.CrackedBlockCraftingHelper;
import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.block.DispenserBlock;
import net.minecraft.block.dispenser.FallibleItemDispenserBehavior;
import net.minecraft.item.ItemStack;
import net.minecraft.sound.SoundCategory;
import net.minecraft.sound.SoundEvents;
import net.minecraft.util.math.BlockPointer;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

import java.util.Optional;

public class PickaxeDispenserBehavior extends FallibleItemDispenserBehavior {

    @Override
    protected ItemStack dispenseSilently(BlockPointer pointer, ItemStack stack) {
        this.setSuccess(true);
        World world = pointer.world();
        BlockPos pos = pointer.pos().offset(pointer.state().get(DispenserBlock.FACING));
        BlockState state1 = pointer.state();
        Optional<Block> optional = CrackedBlockCraftingHelper.getCrackedBlock(world, pos);
        if (optional.isPresent()) {
            BlockState state = optional.get().getStateWithProperties(state1);
            world.setBlockState(pos, state, Block.NOTIFY_ALL | Block.REDRAW_ON_MAIN_THREAD);
            world.playSound(null, pos, SoundEvents.BLOCK_ANVIL_LAND, SoundCategory.BLOCKS, 1, 1);
        }
        else {
            this.setSuccess(false);
        }
        if (this.isSuccess() && stack.damage(1, world.random, null)) { stack.setCount(0); }
        return stack;
    }
}
