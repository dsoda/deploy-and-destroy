package net.dsoda.deployanddestroy.blocks.dispenser.behavior;

import net.minecraft.block.DispenserBlock;
import net.minecraft.block.dispenser.ItemDispenserBehavior;
import net.minecraft.item.FluidModificationItem;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Items;
import net.minecraft.server.world.ServerWorld;
import net.minecraft.util.math.BlockPointer;
import net.minecraft.util.math.BlockPos;

public class FillPlaceableBucketBehavior extends ItemDispenserBehavior {

    private final ItemDispenserBehavior fallbackBehavior = new ItemDispenserBehavior();

    @Override
    public ItemStack dispenseSilently(BlockPointer pointer, ItemStack stack) {
        FluidModificationItem fluidModificationItem = (FluidModificationItem)((Object)stack.getItem());
        BlockPos blockPos = pointer.pos().offset(pointer.state().get(DispenserBlock.FACING));
        ServerWorld world = pointer.world();
        if (fluidModificationItem.placeFluid(null, world, blockPos, null)) {
            fluidModificationItem.onEmptied(null, world, stack, blockPos);
            stack.decrement(1);
        }
        if (pointer.blockEntity().addToFirstFreeSlot(new ItemStack(Items.BUCKET)) < 0) {
            this.fallbackBehavior.dispense(pointer, new ItemStack(Items.BUCKET));
        }
        return stack;
    }
}
