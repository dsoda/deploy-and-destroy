package net.dsoda.deployanddestroy.blocks.dispenser.behavior;

import net.dsoda.deployanddestroy.util.tag.DDBlockTags;
import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.block.Blocks;
import net.minecraft.block.DispenserBlock;
import net.minecraft.block.dispenser.FallibleItemDispenserBehavior;
import net.minecraft.entity.ItemEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Items;
import net.minecraft.server.world.ServerWorld;
import net.minecraft.sound.SoundCategory;
import net.minecraft.sound.SoundEvents;
import net.minecraft.util.math.BlockPointer;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Direction;
import net.minecraft.world.event.GameEvent;

public class
HoeDispenserBehavior extends FallibleItemDispenserBehavior {

    @Override
    protected ItemStack dispenseSilently(BlockPointer pointer, ItemStack stack) {
        this.setSuccess(true);
        ServerWorld world = pointer.world();
        BlockPos pos = pointer.pos().offset(pointer.state().get(DispenserBlock.FACING));
        BlockPos above = pos.offset(Direction.UP);
        Direction direction = pointer.state().get(DispenserBlock.FACING);
        if (direction == Direction.UP || direction == Direction.DOWN) { direction = Direction.NORTH; }
        BlockState state = world.getBlockState(pos);
        if (state.isIn(DDBlockTags.CAN_BECOME_FARMLAND) && world.getBlockState(above).isOf(Blocks.AIR)) {
            replaceWithBlockState(world, pos, Blocks.FARMLAND.getDefaultState());
        }
        else if (state.getBlock() == Blocks.COARSE_DIRT) {
            replaceWithBlockState(world, pos, Blocks.DIRT.getDefaultState());
        }
        else if (state.getBlock() == Blocks.ROOTED_DIRT) {
            replaceWithBlockState(world, pos, Blocks.DIRT.getDefaultState());
            ItemEntity itemEntity = new ItemEntity(world, (double)pos.getX() + 0.5 + (double)direction.getOffsetX() * 0.65, (double)pos.getY() + 0.1, (double)pos.getZ() + 0.5 + (double)direction.getOffsetZ() * 0.65, new ItemStack(Items.HANGING_ROOTS, 1));
            itemEntity.setVelocity(0.05 * (double)direction.getOffsetX() + world.random.nextDouble() * 0.02, 0.05, 0.05 * (double)direction.getOffsetZ() + world.random.nextDouble() * 0.02);
            world.spawnEntity(itemEntity);
        }
        else {
            this.setSuccess(false);
        }
        if (this.isSuccess() && stack.damage(1, world.random, null)) { stack.setCount(0); }
        return stack;
    }

    private static void replaceWithBlockState(ServerWorld world, BlockPos pos, BlockState state) {
        world.setBlockState(pos, state, Block.NOTIFY_ALL | Block.REDRAW_ON_MAIN_THREAD);
        world.playSound(null, pos, SoundEvents.ITEM_HOE_TILL, SoundCategory.BLOCKS, 1f, 1f);
        world.emitGameEvent(null, GameEvent.BLOCK_CHANGE, pos);
    }
}
