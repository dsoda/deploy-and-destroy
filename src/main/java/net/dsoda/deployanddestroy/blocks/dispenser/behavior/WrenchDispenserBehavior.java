package net.dsoda.deployanddestroy.blocks.dispenser.behavior;

import net.dsoda.deployanddestroy.items.item.WrenchItem;
import net.minecraft.block.DispenserBlock;
import net.minecraft.block.dispenser.FallibleItemDispenserBehavior;
import net.minecraft.item.ItemStack;
import net.minecraft.util.math.BlockPointer;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Direction;
import net.minecraft.world.World;

public class WrenchDispenserBehavior extends FallibleItemDispenserBehavior {

    @Override
    protected ItemStack dispenseSilently(BlockPointer pointer, ItemStack stack) {
        World world = pointer.world();
        Direction facing = pointer.state().get(DispenserBlock.FACING);
        BlockPos pos = pointer.pos().offset(facing);
        if (!world.isClient()) {
            this.setSuccess(WrenchItem.tryRotateBlock(world, null, pos, facing));
        }
        return stack;
    }
}
