package net.dsoda.deployanddestroy.blocks.dispenser.behavior;

import net.dsoda.deployanddestroy.mixin.entity.accessor.ZombieVillagerConvertingAccessor;
import net.minecraft.block.DispenserBlock;
import net.minecraft.block.dispenser.FallibleItemDispenserBehavior;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.effect.StatusEffects;
import net.minecraft.entity.mob.ZombieVillagerEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.predicate.entity.EntityPredicates;
import net.minecraft.server.world.ServerWorld;
import net.minecraft.util.math.BlockPointer;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Box;

import java.util.Optional;

public class GoldenAppleDispenserBehavior extends FallibleItemDispenserBehavior {

    @Override
    protected ItemStack dispenseSilently(BlockPointer pointer, ItemStack stack) {
        this.setSuccess(true);
        ServerWorld world = pointer.world();
        BlockPos facing = pointer.pos().offset(pointer.state().get(DispenserBlock.FACING));


        Optional<ZombieVillagerEntity> target = world.getEntitiesByType(EntityType.ZOMBIE_VILLAGER, new Box(facing), EntityPredicates.VALID_LIVING_ENTITY).stream()
                .filter(z -> z.hasStatusEffect(StatusEffects.WEAKNESS)).findFirst();

        if (target.isPresent()) {
            ZombieVillagerConvertingAccessor convertingAccessor = (ZombieVillagerConvertingAccessor) target.get();
            convertingAccessor.invokeSetConverting(null, world.random.nextInt(2401) + 3600);
            stack.decrement(1);
            return stack;
        }

        this.setSuccess(false);
        return stack;
    }
}
