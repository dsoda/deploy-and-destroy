package net.dsoda.deployanddestroy.blocks.dispenser.behavior;

import net.dsoda.deployanddestroy.mixin.entity.accessor.MooshroomStatusEffectAccessor;
import net.minecraft.block.DispenserBlock;
import net.minecraft.block.SuspiciousStewIngredient;
import net.minecraft.block.dispenser.FallibleItemDispenserBehavior;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.passive.MooshroomEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.particle.ParticleTypes;
import net.minecraft.predicate.entity.EntityPredicates;
import net.minecraft.server.world.ServerWorld;
import net.minecraft.sound.SoundCategory;
import net.minecraft.sound.SoundEvents;
import net.minecraft.util.math.BlockPointer;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Box;

import java.util.List;
import java.util.Optional;

public class FeedMooshroomDispenserBehavior extends FallibleItemDispenserBehavior {

    @Override
    protected ItemStack dispenseSilently(BlockPointer pointer, ItemStack stack) {
        this.setSuccess(true);
        ServerWorld world = pointer.world();
        BlockPos facing = pointer.pos().offset(pointer.state().get(DispenserBlock.FACING));

        List<MooshroomEntity> mooshrooms = world.getEntitiesByType(EntityType.MOOSHROOM, new Box(facing), EntityPredicates.VALID_LIVING_ENTITY.and((mooshroom)
            -> ((MooshroomEntity) mooshroom).getVariant() == MooshroomEntity.Type.BROWN));

        for (MooshroomEntity mooshroom : mooshrooms) {
            MooshroomStatusEffectAccessor mooshroomAccessor = (MooshroomStatusEffectAccessor) mooshroom;
            if (mooshroomAccessor.getStewEffects() == null) {
                Optional<List<SuspiciousStewIngredient.StewEffect>> optional = mooshroomAccessor.invokeGetStewEffectFrom(stack);
                if (optional.isPresent()) {
                    mooshroomAccessor.setStewEffects(optional.get());
                    world.playSound(null, facing, SoundEvents.ENTITY_MOOSHROOM_EAT, SoundCategory.NEUTRAL, 2.0F, 1.0F);
                    world.spawnParticles(ParticleTypes.EFFECT, mooshroom.getX() + mooshroom.getRandom().nextDouble() / 2.0D, mooshroom.getBodyY(0.5D), mooshroom.getZ() + mooshroom.getRandom().nextDouble() / 2.0D, 1, 0.0D, mooshroom.getRandom().nextDouble() / 5.0D, 0.0D, 0.01D);
                    stack.decrement(1);
                    return stack;
                }
            }
        }
        this.setSuccess(false);
        return stack;
    }
}
