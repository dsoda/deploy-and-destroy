package net.dsoda.deployanddestroy.blocks.dispenser.behavior;

import com.google.common.collect.ImmutableMap;
import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.block.Blocks;
import net.minecraft.block.DispenserBlock;
import net.minecraft.block.dispenser.FallibleItemDispenserBehavior;
import net.minecraft.entity.passive.IronGolemEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.predicate.entity.EntityPredicates;
import net.minecraft.registry.tag.BlockTags;
import net.minecraft.server.world.ServerWorld;
import net.minecraft.sound.SoundCategory;
import net.minecraft.sound.SoundEvents;
import net.minecraft.util.math.BlockPointer;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Box;
import net.minecraft.world.event.GameEvent;

import java.util.List;
import java.util.Map;
import java.util.Optional;

public class IronIngotDispenserBehavior extends FallibleItemDispenserBehavior {

    private static final Map<Block, Block> REPAIR_ANVIL = new ImmutableMap.Builder<Block, Block>()
        .put(Blocks.CHIPPED_ANVIL, Blocks.ANVIL)
        .put(Blocks.DAMAGED_ANVIL, Blocks.CHIPPED_ANVIL)
        .build();

    @Override
    protected ItemStack dispenseSilently(BlockPointer pointer, ItemStack stack) {
        this.setSuccess(true);
        ServerWorld world = pointer.world();
        BlockPos facing = pointer.pos().offset(pointer.state().get(DispenserBlock.FACING));
        BlockState facingState = world.getBlockState(facing);
        Optional<IronGolemEntity> golemOptional = getIronGolemPresent(world, new Box(facing));
        if (facingState.isIn(BlockTags.ANVIL) && facingState.getBlock() != Blocks.ANVIL) {
            setAnvil(world, facingState, facing);
            stack.decrement(1);
            return stack;
        }
        if (golemOptional.isPresent()) {
            IronGolemEntity golem = golemOptional.get();
            float health = golem.getHealth();
            golem.heal(25.0f);
            if (golem.getHealth() == health) {
                this.setSuccess(false);
                return stack;
            }
            float pitch = 1.0f + (world.random.nextFloat() - world.random.nextFloat()) * 0.2f;
            world.playSound(null, facing, SoundEvents.ENTITY_IRON_GOLEM_REPAIR, SoundCategory.BLOCKS, 1.0f, pitch);
            stack.decrement(1);
            return stack;
        }
        this.setSuccess(false);
        return stack;
    }

    private static void setAnvil(ServerWorld world, BlockState state, BlockPos pos) {
        BlockState newState = REPAIR_ANVIL.get(state.getBlock()).getStateWithProperties(state);
        world.setBlockState(pos, newState, Block.NOTIFY_ALL | Block.REDRAW_ON_MAIN_THREAD);
        world.emitGameEvent(null, GameEvent.BLOCK_CHANGE, pos);
        world.playSound(null, pos, SoundEvents.BLOCK_ANVIL_USE, SoundCategory.BLOCKS, 1.0f, 1.0f);
    }

    private static Optional<IronGolemEntity> getIronGolemPresent(ServerWorld world, Box box) {
        IronGolemEntity e = null;
        List<IronGolemEntity> golemList = world.getEntitiesByClass(IronGolemEntity.class, box, EntityPredicates.VALID_LIVING_ENTITY);
        if (!golemList.isEmpty())
            e = golemList.stream().findFirst().orElse(null);
        return Optional.ofNullable(e);
    }
}
