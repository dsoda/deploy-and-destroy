package net.dsoda.deployanddestroy.blocks.dispenser.behavior;

import net.dsoda.deployanddestroy.util.behavior.DDCauldronBehaviorHelper;
import net.dsoda.deployanddestroy.util.behavior.DDGeneralBehaviorHelper;
import net.minecraft.block.*;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Items;
import net.minecraft.server.world.ServerWorld;
import net.minecraft.sound.SoundEvents;
import net.minecraft.util.math.BlockPointer;
import net.minecraft.util.math.BlockPos;

public class EmptyCauldronDispenserBehavior extends DDGeneralBehaviorHelper {

    @Override
    protected ItemStack dispenseSilently(BlockPointer pointer, ItemStack stack) {
        this.setSuccess(true);
        ServerWorld world = pointer.world();
        BlockPos facing = pointer.pos().offset(pointer.state().get(DispenserBlock.FACING));
        BlockState state = world.getBlockState(facing);
        Block facingBlock = state.getBlock();

        if(facingBlock instanceof AbstractCauldronBlock && ((AbstractCauldronBlock) facingBlock).isFull(state)) {
            if(facingBlock == Blocks.LAVA_CAULDRON) {
                DDCauldronBehaviorHelper.setCauldron(world, facing, Blocks.CAULDRON.getDefaultState(), SoundEvents.ITEM_BUCKET_FILL_LAVA);
                return this.addOrDispense(pointer, stack, new ItemStack(Items.LAVA_BUCKET));
            }
            else if(facingBlock == Blocks.WATER_CAULDRON) {
                DDCauldronBehaviorHelper.setCauldron(world, facing, Blocks.CAULDRON.getDefaultState(), SoundEvents.ITEM_BUCKET_FILL);
                return this.addOrDispense(pointer, stack, new ItemStack(Items.WATER_BUCKET));
            }
            else if(facingBlock == Blocks.POWDER_SNOW_CAULDRON) {
                DDCauldronBehaviorHelper.setCauldron(world, facing, Blocks.CAULDRON.getDefaultState(), SoundEvents.ITEM_BUCKET_FILL_POWDER_SNOW);
                return this.addOrDispense(pointer, stack, new ItemStack(Items.POWDER_SNOW_BUCKET));
            }
        }

        this.setSuccess(false);
        return stack;
    }
}
