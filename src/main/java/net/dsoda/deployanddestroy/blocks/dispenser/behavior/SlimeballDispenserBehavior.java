package net.dsoda.deployanddestroy.blocks.dispenser.behavior;

import net.minecraft.block.*;
import net.minecraft.block.dispenser.FallibleItemDispenserBehavior;
import net.minecraft.block.enums.PistonType;
import net.minecraft.item.ItemStack;
import net.minecraft.server.world.ServerWorld;
import net.minecraft.sound.SoundCategory;
import net.minecraft.sound.SoundEvents;
import net.minecraft.state.property.Properties;
import net.minecraft.util.math.BlockPointer;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.event.GameEvent;

// TODO: Figure out slime particle effects, spit slime balls out if no piston
public class SlimeballDispenserBehavior extends FallibleItemDispenserBehavior {
    @Override
    protected ItemStack dispenseSilently(BlockPointer pointer, ItemStack stack) {
        ServerWorld serverWorld = pointer.world();
        if (!serverWorld.isClient()) {
            BlockPos blockPos = pointer.pos().offset(pointer.state().get(DispenserBlock.FACING));
            this.setSuccess(SlimeballDispenserBehavior.tryApplySlime(serverWorld, blockPos));
            if (this.isSuccess()) {
                stack.decrement(1);
            }
        }
        return stack;
    }

    private static boolean tryApplySlime(ServerWorld world, BlockPos pos) {
        BlockState state = world.getBlockState(pos);
        boolean notExtended = state.isOf(Blocks.PISTON)
                && !state.get(Properties.EXTENDED);
        boolean extended = state.isOf(Blocks.PISTON_HEAD)
                && state.get(Properties.PISTON_TYPE).asString().equals("normal");
        if(notExtended || extended) {
            if (notExtended) replacePistonWithStickyAtPos(pos, world);
            else replaceExtendedPistonWithStickyAtPos(pos, world);
            world.playSound(null, pos, SoundEvents.BLOCK_SLIME_BLOCK_BREAK, SoundCategory.BLOCKS, 1f, 1f);
            //world.addBlockBreakParticles(pos, Blocks.SLIME_BLOCK.getDefaultState());
            world.emitGameEvent(null, GameEvent.BLOCK_CHANGE, pos);
            return true;
        }
        return false;
    }

    private static void replacePistonWithStickyAtPos(BlockPos pos, ServerWorld world) {
        BlockState state = world.getBlockState(pos);
        world.setBlockState(pos, Blocks.STICKY_PISTON.getStateWithProperties(state), Block.NOTIFY_ALL | Block.REDRAW_ON_MAIN_THREAD);
    }

    private static void replaceExtendedPistonWithStickyAtPos(BlockPos pos, ServerWorld world) {
        BlockState state = world.getBlockState(pos);
        BlockPos connected = pos.offset(state.get(PistonBlock.FACING), -1);
        world.setBlockState(pos, Blocks.PISTON_HEAD.getStateWithProperties(state).with(Properties.PISTON_TYPE, PistonType.STICKY));
        replacePistonWithStickyAtPos(connected, world);
    }

    private static void spawnSlimeParticles(BlockPos pos, ServerWorld world) {
        world.addBlockBreakParticles(pos, Blocks.SLIME_BLOCK.getDefaultState());
    }
}
