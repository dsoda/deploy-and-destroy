package net.dsoda.deployanddestroy.blocks.dispenser.behavior;

import net.dsoda.deployanddestroy.util.behavior.DDDyeBehaviorHelper;
import net.minecraft.block.*;
import net.minecraft.block.dispenser.FallibleItemDispenserBehavior;
import net.minecraft.entity.passive.SheepEntity;
import net.minecraft.item.DyeItem;
import net.minecraft.item.ItemStack;
import net.minecraft.server.world.ServerWorld;
import net.minecraft.sound.SoundCategory;
import net.minecraft.sound.SoundEvents;
import net.minecraft.util.math.BlockPointer;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.DyeColor;
import net.minecraft.world.event.GameEvent;
import java.util.Optional;

public class DyeDispenserBehavior extends FallibleItemDispenserBehavior {

    @Override
    protected ItemStack dispenseSilently(BlockPointer pointer, ItemStack stack) {
        DyeItem item = (DyeItem)stack.getItem();
        DyeColor color = item.getColor();
        ServerWorld world = pointer.world();
        BlockPos facing = pointer.pos().offset(pointer.state().get(DispenserBlock.FACING));
        BlockState facingState = world.getBlockState(pointer.pos().offset(pointer.state().get(DispenserBlock.FACING)));
        Optional<BlockState> optional   = DDDyeBehaviorHelper.getWoolForDyeColor(color, facingState);
        Optional<BlockState> optional2  = DDDyeBehaviorHelper.getStainedGlassForDyeColor(color, facingState);
        Optional<BlockState> optional3  = DDDyeBehaviorHelper.getStainedGlassPaneForDyeColor(color, facingState);
        Optional<BlockState> optional4  = DDDyeBehaviorHelper.getConcreteForDyeColor(color, facingState);
        Optional<BlockState> optional5  = DDDyeBehaviorHelper.getConcretePowderForDyeColor(color, facingState);
        Optional<BlockState> optional6  = DDDyeBehaviorHelper.getTerracottaForDyeColor(color, facingState);
        Optional<BlockState> optional7  = DDDyeBehaviorHelper.getCandleForDyeColor(color, facingState);
        Optional<BlockState> optional8  = DDDyeBehaviorHelper.getCandleCakeForDyeColor(color, facingState);
        Optional<BlockState> optional9  = DDDyeBehaviorHelper.getCarpetForDyeColor(color, facingState);
        Optional<BlockState> optional10  = DDDyeBehaviorHelper.getLampForDyeColor(color, facingState);
        Optional<BlockState> optional11 = DDDyeBehaviorHelper.getGlazedTerracottaForDyeColor(color, facingState);
        Optional<BlockState> optionalBed  = DDDyeBehaviorHelper.getBedForDyeColor(color, facingState);
        Optional<BlockState> optionalShulker = DDDyeBehaviorHelper.getShulkerBoxForDyeColor(color, facingState);
        Optional<SheepEntity> optionalEntity = DDDyeBehaviorHelper.getDyeableEntityInBox(color, world, facingState, facing);
        Optional<BlockState> optionalSuccess = Optional.empty();
        if (!world.isClient()) {
            if (optional.isPresent()) optionalSuccess = optional;
            if (optional2.isPresent()) optionalSuccess = optional2;
            if (optional3.isPresent()) optionalSuccess = optional3;
            if (optional4.isPresent()) optionalSuccess = optional4;
            if (optional5.isPresent()) optionalSuccess = optional5;
            if (optional6.isPresent()) optionalSuccess = optional6;
            if (optional7.isPresent()) optionalSuccess = optional7;
            if (optional8.isPresent()) optionalSuccess = optional8;
            if (optional9.isPresent()) optionalSuccess = optional9;
            if (optional10.isPresent()) optionalSuccess = optional10;
            if (optional11.isPresent()) optionalSuccess = optional11;
            if (optionalBed.isPresent()) {
                DDDyeBehaviorHelper.setBed(world, facing, optionalBed.get());
                world.playSound(null, facing, SoundEvents.BLOCK_SLIME_BLOCK_PLACE, SoundCategory.BLOCKS, 1f, 1f);
                stack.decrement(1);
                this.setSuccess(true);
            }
            if (optionalEntity.isPresent()) {
                optionalEntity.get().setColor(color);
                world.emitGameEvent(null, GameEvent.ENTITY_ACTION, facing);
                stack.decrement(1);
                this.setSuccess(true);
            }
            else if (optionalSuccess.isPresent()) {
                world.playSound(null, facing, SoundEvents.BLOCK_SLIME_BLOCK_PLACE, SoundCategory.BLOCKS, 1f, 1f);
                world.setBlockState(facing, optionalSuccess.get(), Block.NOTIFY_ALL | Block.REDRAW_ON_MAIN_THREAD);
                world.emitGameEvent(null, GameEvent.BLOCK_CHANGE, facing);
                stack.decrement(1);
                this.setSuccess(true);
            }
            else if (optionalShulker.isPresent()) {
                DDDyeBehaviorHelper.setShulker(world, facing, optionalShulker.get());
                world.playSound(null, facing, SoundEvents.BLOCK_SLIME_BLOCK_PLACE, SoundCategory.BLOCKS, 1f, 1f);
                stack.decrement(1);
                this.setSuccess(true);
            }
        } else { this.setSuccess(false); }
        return stack;
    }
}
