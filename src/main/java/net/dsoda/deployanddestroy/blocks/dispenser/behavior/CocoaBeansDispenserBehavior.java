package net.dsoda.deployanddestroy.blocks.dispenser.behavior;

import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.block.Blocks;
import net.minecraft.block.DispenserBlock;
import net.minecraft.block.dispenser.FallibleItemDispenserBehavior;
import net.minecraft.item.ItemStack;
import net.minecraft.registry.tag.BlockTags;
import net.minecraft.server.world.ServerWorld;
import net.minecraft.sound.SoundCategory;
import net.minecraft.sound.SoundEvents;
import net.minecraft.state.property.Properties;
import net.minecraft.util.math.BlockPointer;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Direction;
import net.minecraft.world.event.GameEvent;

public class CocoaBeansDispenserBehavior extends FallibleItemDispenserBehavior {

    @Override
    protected ItemStack dispenseSilently(BlockPointer pointer, ItemStack stack) {
        this.setSuccess(true);
        ServerWorld world = pointer.world();
        BlockState state = pointer.state();
        Direction direction = state.get(DispenserBlock.FACING);
        BlockPos pos = pointer.pos().offset(direction);
        if(isValidLocation(pos, direction, world)) {
            world.setBlockState(pos, Blocks.COCOA.getStateWithProperties(state).with(Properties.HORIZONTAL_FACING, direction), Block.NOTIFY_ALL | Block.REDRAW_ON_MAIN_THREAD);
            world.playSound(null, pos, SoundEvents.ITEM_CROP_PLANT, SoundCategory.BLOCKS, 1f, 1f);
            world.emitGameEvent(null, GameEvent.BLOCK_PLACE, pos);
            stack.decrement(1);
        } else {
            this.setSuccess(false);
        }
        return stack;
    }

    private boolean isValidLocation(BlockPos pos, Direction direction, ServerWorld world) {
        BlockPos pos2 = pos.offset(direction);
        Block block = world.getBlockState(pos).getBlock();
        BlockState state = world.getBlockState(pos2);
        return direction != Direction.UP && direction != Direction.DOWN && block == Blocks.AIR && state.isIn(BlockTags.JUNGLE_LOGS);
    }
}
