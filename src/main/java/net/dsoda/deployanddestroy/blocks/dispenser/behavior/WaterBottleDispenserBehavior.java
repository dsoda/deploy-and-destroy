package net.dsoda.deployanddestroy.blocks.dispenser.behavior;

import net.dsoda.deployanddestroy.util.behavior.DDCauldronBehaviorHelper;
import net.dsoda.deployanddestroy.util.behavior.DDGeneralBehaviorHelper;
import net.minecraft.block.*;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Items;
import net.minecraft.server.world.ServerWorld;
import net.minecraft.sound.SoundCategory;
import net.minecraft.sound.SoundEvents;
import net.minecraft.util.math.BlockPointer;
import net.minecraft.util.math.BlockPos;

public class WaterBottleDispenserBehavior extends DDGeneralBehaviorHelper {

    @Override
    protected ItemStack dispenseSilently(BlockPointer pointer, ItemStack stack) {
        this.setSuccess(true);
        Item item = stack.getItem();
        ServerWorld world = pointer.world();
        BlockPos facing = pointer.pos().offset(pointer.state().get(DispenserBlock.FACING));
        BlockState state = world.getBlockState(facing);
        Block facingBlock = state.getBlock();

        if (item == Items.GLASS_BOTTLE) {
            if (facingBlock == Blocks.WATER) {
                world.playSound(null, facing, SoundEvents.ITEM_BOTTLE_FILL, SoundCategory.BLOCKS, 1.0f, 1.0f);
                return addOrDispense(pointer, stack, DDCauldronBehaviorHelper.getWaterBottle());
            } else if (facingBlock == Blocks.WATER_CAULDRON) {
                DDCauldronBehaviorHelper.setCauldronWithFillLevel(world, facing, state, SoundEvents.ITEM_BOTTLE_FILL, (byte) 0);
                return addOrDispense(pointer, stack, DDCauldronBehaviorHelper.getWaterBottle());
            }
        } else if (item == DDCauldronBehaviorHelper.getWaterBottle().getItem() && facingBlock instanceof AbstractCauldronBlock) {
            if (facingBlock == Blocks.CAULDRON) {
                DDCauldronBehaviorHelper.setCauldronWithFillLevel(world, facing, state, SoundEvents.ITEM_BOTTLE_FILL, (byte) 1);
                return addOrDispense(pointer, stack, new ItemStack(Items.GLASS_BOTTLE));
            } else if (facingBlock == Blocks.WATER_CAULDRON && !DDCauldronBehaviorHelper.isCauldronFull(state)) {
                DDCauldronBehaviorHelper.setCauldronWithFillLevel(world, facing, state, SoundEvents.ITEM_BOTTLE_FILL, (byte) 1);
                return addOrDispense(pointer, stack, new ItemStack(Items.GLASS_BOTTLE));
            }
        }
        this.setSuccess(false);
        return stack;
    }
}