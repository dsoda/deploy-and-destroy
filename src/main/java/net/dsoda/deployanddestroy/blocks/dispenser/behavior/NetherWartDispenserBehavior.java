package net.dsoda.deployanddestroy.blocks.dispenser.behavior;

import net.minecraft.block.Block;
import net.minecraft.block.Blocks;
import net.minecraft.block.DispenserBlock;
import net.minecraft.block.dispenser.FallibleItemDispenserBehavior;
import net.minecraft.item.ItemStack;
import net.minecraft.server.world.ServerWorld;
import net.minecraft.sound.SoundCategory;
import net.minecraft.sound.SoundEvents;
import net.minecraft.util.math.BlockPointer;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Direction;
import net.minecraft.world.event.GameEvent;

public class NetherWartDispenserBehavior extends FallibleItemDispenserBehavior {

    @Override
    protected ItemStack dispenseSilently(BlockPointer pointer, ItemStack stack) {
        this.setSuccess(true);
        ServerWorld world = pointer.world();
        BlockPos pos = pointer.pos().offset(pointer.state().get(DispenserBlock.FACING));
        if(isValidLocation(pos, world)) {
            world.setBlockState(pos, Blocks.NETHER_WART.getDefaultState(), Block.NOTIFY_ALL | Block.REDRAW_ON_MAIN_THREAD);
            world.playSound(null, pos, SoundEvents.BLOCK_NETHER_WART_BREAK, SoundCategory.BLOCKS, 1f, 1f);
            world.emitGameEvent(null, GameEvent.BLOCK_PLACE, pos);
            stack.decrement(1);
        } else {
            this.setSuccess(false);
        }
        return stack;
    }

    private boolean isValidLocation(BlockPos pos, ServerWorld world) {
        BlockPos pos2 = pos.offset(Direction.DOWN);
        Block block1 = world.getBlockState(pos).getBlock();
        Block block2 = world.getBlockState(pos2).getBlock();
        return block1 == Blocks.AIR && block2 == Blocks.SOUL_SAND;
    }
}
