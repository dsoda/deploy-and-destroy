package net.dsoda.deployanddestroy.blocks.dispenser;

import net.dsoda.deployanddestroy.blocks.dispenser.behavior.*;
import net.dsoda.deployanddestroy.items.DDItems;
import net.dsoda.deployanddestroy.items.item.DyeBucketItem;
import net.dsoda.deployanddestroy.util.behavior.DDCauldronBehaviorHelper;
import net.dsoda.deployanddestroy.util.behavior.DDUnmossHelper;
import net.dsoda.deployanddestroy.util.tag.DDItemTags;
import net.minecraft.block.*;
import net.minecraft.block.dispenser.DispenserBehavior;
import net.minecraft.block.entity.DispenserBlockEntity;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.passive.AnimalEntity;
import net.minecraft.entity.passive.CowEntity;
import net.minecraft.entity.passive.GoatEntity;
import net.minecraft.entity.passive.SquidEntity;
import net.minecraft.item.*;
import net.minecraft.predicate.entity.EntityPredicates;
import net.minecraft.registry.tag.ItemTags;
import net.minecraft.server.world.ServerWorld;
import net.minecraft.util.math.BlockPointer;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Box;
import net.minecraft.util.math.Direction;

import java.util.Map;

public interface DDDispenserBehavior {

    static DispenserBehavior getAdditionalBehaviors(ServerWorld world, BlockPos pos, BlockPointer pointer, DispenserBlockEntity entity, ItemStack stack, Map<Item, DispenserBehavior> EXISTING_BEHAVIORS) {
        Item item = stack.getItem();
        Direction facing = pointer.state().get(DispenserBlock.FACING);
        BlockPos facingBlockPos = pos.offset(facing);
        BlockState facingBlockState = world.getBlockState(facingBlockPos);
        Block facingBlock = facingBlockState.getBlock();
        Box facingBlockBox = new Box(facingBlockPos);

        if (item == Items.FLINT_AND_STEEL && facingBlock == Blocks.WET_SPONGE)
            return new DDFlintAndSteelDispenserBehavior();
        if (item == Items.SLIME_BALL)
            return new SlimeballDispenserBehavior();
        if (stack.isIn(ItemTags.AXES))
            return new AxeDispenserBehavior();
        if (stack.isIn(ItemTags.SHOVELS))
            return new ShovelDispenserBehavior();
        if (stack.isIn(ItemTags.HOES))
            return new HoeDispenserBehavior();
        if (stack.isIn(ItemTags.PICKAXES))
            return new PickaxeDispenserBehavior();
        if (item == Items.NETHER_WART)
            return new NetherWartDispenserBehavior();
        if (item == Items.SHEARS && (facingBlock.equals(Blocks.PUMPKIN) || DDUnmossHelper.isMossy(facingBlock)))
            return new DDShearsDispenserBehavior();
        if (item == Items.COCOA_BEANS)
            return new CocoaBeansDispenserBehavior();
        if (item instanceof DyeItem)
            return new DyeDispenserBehavior();
        if (item instanceof DyeBucketItem)
            return new ColorWandDispenserBehavior();
        if (stack.isIn(DDItemTags.FARMLAND_PLANTABLE))
            return new PlantableDispenserBehavior();
        if (item == Items.GLASS_BOTTLE || item == DDCauldronBehaviorHelper.getWaterBottle().getItem())
            return new WaterBottleDispenserBehavior();
        if (facingBlock instanceof AbstractCauldronBlock) {
            if (item == Items.BUCKET)
                return new EmptyCauldronDispenserBehavior();
            else if (item == Items.WATER_BUCKET || item == Items.LAVA_BUCKET || item == Items.POWDER_SNOW_BUCKET)
                return new FillCauldronDispenserBehavior();
        }
        if ((milkableAnimalInBox(world, facingBlockBox) || squidInBox(world, facingBlockBox)) && item == Items.BUCKET)
            return new MilkAnimalDispenserBehavior();
        if (stewableAnimalInBox(world, facingBlockBox)) {
            if (stack.isIn(ItemTags.SMALL_FLOWERS))
                return new FeedMooshroomDispenserBehavior();
            else if (item == Items.BOWL)
                return new StewAnimalDispenserBehavior();
        }
        if (item == Items.IRON_INGOT)
            return new IronIngotDispenserBehavior();
        if (zombieVillagerInBox(world, facingBlockBox) && item == Items.GOLDEN_APPLE)
            return new GoldenAppleDispenserBehavior();
        if (item == Items.HONEYCOMB)
            return new HoneyCombDispenserBehavior();
        if (item == DDItems.WRENCH)
            return new WrenchDispenserBehavior();

        // do vanilla behaviors
        return null;
    }

    private static boolean milkableAnimalInBox(ServerWorld world, Box box) {
        return !world.getEntitiesByClass(AnimalEntity.class, box, EntityPredicates.VALID_LIVING_ENTITY.and((animalEntity)
            -> animalEntity instanceof CowEntity
            || animalEntity instanceof GoatEntity)).isEmpty();
    }

    private static boolean squidInBox(ServerWorld world, Box box) {
        return !world.getEntitiesByClass(SquidEntity.class, box, EntityPredicates.VALID_LIVING_ENTITY).isEmpty();
    }

    private static boolean stewableAnimalInBox(ServerWorld world, Box box) {
        return !world.getEntitiesByType(EntityType.MOOSHROOM, box, EntityPredicates.VALID_LIVING_ENTITY).isEmpty();
    }

    private static boolean zombieVillagerInBox(ServerWorld world, Box box) {
        return !world.getEntitiesByType(EntityType.ZOMBIE_VILLAGER, box, EntityPredicates.VALID_LIVING_ENTITY).isEmpty();
    }
}
