package net.dsoda.deployanddestroy.blocks.dispenser.behavior;

import net.dsoda.deployanddestroy.util.behavior.DDCauldronBehaviorHelper;
import net.minecraft.block.*;
import net.minecraft.block.dispenser.FallibleItemDispenserBehavior;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Items;
import net.minecraft.server.world.ServerWorld;
import net.minecraft.sound.SoundEvents;
import net.minecraft.util.math.BlockPointer;
import net.minecraft.util.math.BlockPos;

public class FillCauldronDispenserBehavior extends FallibleItemDispenserBehavior {

    @Override
    protected ItemStack dispenseSilently(BlockPointer pointer, ItemStack stack) {
        this.setSuccess(true);
        Item item = stack.getItem();
        ServerWorld world = pointer.world();
        BlockPos facing = pointer.pos().offset(pointer.state().get(DispenserBlock.FACING));
        BlockState state = world.getBlockState(facing);
        Block facingBlock = state.getBlock();

        if (facingBlock instanceof AbstractCauldronBlock) {
            if (item == Items.LAVA_BUCKET) {
                DDCauldronBehaviorHelper.setCauldron(world, facing, Blocks.LAVA_CAULDRON.getDefaultState(), SoundEvents.ITEM_BUCKET_EMPTY_LAVA);
                return new ItemStack(Items.BUCKET);
            }
            else if (item == Items.WATER_BUCKET) {
                DDCauldronBehaviorHelper.setCauldron(world, facing, Blocks.WATER_CAULDRON.getDefaultState().with(LeveledCauldronBlock.LEVEL, 3), SoundEvents.ITEM_BUCKET_EMPTY);
                return new ItemStack(Items.BUCKET);
            }
            else if (item == Items.POWDER_SNOW_BUCKET) {
                DDCauldronBehaviorHelper.setCauldron(world, facing, Blocks.POWDER_SNOW_CAULDRON.getDefaultState().with(LeveledCauldronBlock.LEVEL, 3), SoundEvents.ITEM_BUCKET_EMPTY_POWDER_SNOW);
                return new ItemStack(Items.BUCKET);
            }
        }

        this.setSuccess(false);
        return stack;
    }
}
