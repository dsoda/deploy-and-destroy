package net.dsoda.deployanddestroy.blocks.dispenser.behavior;

import com.google.common.collect.ImmutableMap;
import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.block.Blocks;
import net.minecraft.block.DispenserBlock;
import net.minecraft.block.dispenser.FallibleItemDispenserBehavior;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Items;
import net.minecraft.server.world.ServerWorld;
import net.minecraft.sound.SoundCategory;
import net.minecraft.sound.SoundEvents;
import net.minecraft.util.math.BlockPointer;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Direction;
import net.minecraft.world.event.GameEvent;

import java.util.Map;
import java.util.Optional;

public class PlantableDispenserBehavior extends FallibleItemDispenserBehavior {

    private static final Map<Item, Block> SEED_TO_BLOCK = new ImmutableMap.Builder<Item, Block>()
        .put(Items.WHEAT_SEEDS, Blocks.WHEAT)
        .put(Items.BEETROOT_SEEDS, Blocks.BEETROOTS)
        .put(Items.MELON_SEEDS, Blocks.MELON_STEM)
        .put(Items.PUMPKIN_SEEDS, Blocks.PUMPKIN_STEM)
        .put(Items.TORCHFLOWER_SEEDS, Blocks.TORCHFLOWER_CROP)
        .put(Items.PITCHER_POD, Blocks.PITCHER_CROP)
        .put(Items.POTATO, Blocks.POTATOES)
        .put(Items.CARROT, Blocks.CARROTS)
        .build();

    @Override
    protected ItemStack dispenseSilently(BlockPointer pointer, ItemStack stack) {
        this.setSuccess(true);
        ServerWorld world = pointer.world();
        Direction direction = pointer.state().get(DispenserBlock.FACING);
        Item item = stack.getItem();
        BlockPos pos = pointer.pos().offset(direction);
        BlockPos pos2 = pos.offset(Direction.DOWN);
        BlockState state = world.getBlockState(pos);
        BlockState state2 = world.getBlockState(pos2);
        Optional<Block> plant = getBlockForSeed(item);
        if(plant.isPresent() && (state.getBlock() == Blocks.AIR && state2.getBlock() == Blocks.FARMLAND)) {
            world.playSound(null, pos, SoundEvents.ITEM_CROP_PLANT, SoundCategory.BLOCKS, 1.0f, 1.0f);
            world.setBlockState(pos, plant.get().getDefaultState(), Block.NOTIFY_ALL | Block.REDRAW_ON_MAIN_THREAD);
            world.emitGameEvent(null, GameEvent.BLOCK_PLACE, pos);
            stack.decrement(1);
        } else {
            this.setSuccess(false);
        }
        return stack;
    }

    private static Optional<Block> getBlockForSeed(Item seed) {
        return Optional.ofNullable(SEED_TO_BLOCK.get(seed));
    }
}
