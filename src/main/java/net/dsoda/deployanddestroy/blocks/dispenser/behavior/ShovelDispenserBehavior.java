package net.dsoda.deployanddestroy.blocks.dispenser.behavior;

import net.dsoda.deployanddestroy.util.tag.DDBlockTags;
import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.block.Blocks;
import net.minecraft.block.DispenserBlock;
import net.minecraft.block.dispenser.FallibleItemDispenserBehavior;
import net.minecraft.item.ItemStack;
import net.minecraft.server.world.ServerWorld;
import net.minecraft.sound.SoundCategory;
import net.minecraft.sound.SoundEvents;
import net.minecraft.util.math.BlockPointer;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Direction;
import net.minecraft.world.event.GameEvent;

public class ShovelDispenserBehavior extends FallibleItemDispenserBehavior {

    @Override
    protected ItemStack dispenseSilently(BlockPointer pointer, ItemStack stack) {
        this.setSuccess(true);
        ServerWorld world = pointer.world();
        BlockPos pos = pointer.pos().offset(pointer.state().get(DispenserBlock.FACING));
        BlockPos above = pos.offset(Direction.UP);
        BlockState state = world.getBlockState(pos);
        if(state.isIn(DDBlockTags.CAN_BECOME_PATH) && world.getBlockState(above).isOf(Blocks.AIR)) {
            world.setBlockState(pos, Blocks.DIRT_PATH.getDefaultState(), Block.NOTIFY_ALL | Block.REDRAW_ON_MAIN_THREAD);
            world.playSound(null, pos, SoundEvents.BLOCK_ROOTED_DIRT_PLACE, SoundCategory.BLOCKS, 1f, 1f);
            world.emitGameEvent(null, GameEvent.BLOCK_CHANGE, pos);
        } else { this.setSuccess(false); }
        if (this.isSuccess() && stack.damage(1, world.random, null)) { stack.setCount(0); }
        return stack;
    }
}
