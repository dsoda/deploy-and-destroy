package net.dsoda.deployanddestroy.blocks.dispenser.behavior;

import net.dsoda.deployanddestroy.mixin.entity.accessor.MooshroomStatusEffectAccessor;
import net.dsoda.deployanddestroy.util.behavior.DDGeneralBehaviorHelper;
import net.minecraft.block.DispenserBlock;
import net.minecraft.block.SuspiciousStewIngredient;
import net.minecraft.entity.passive.MooshroomEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Items;
import net.minecraft.item.SuspiciousStewItem;
import net.minecraft.predicate.entity.EntityPredicates;
import net.minecraft.server.world.ServerWorld;
import net.minecraft.sound.SoundCategory;
import net.minecraft.sound.SoundEvent;
import net.minecraft.sound.SoundEvents;
import net.minecraft.util.math.BlockPointer;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Box;

import java.util.List;

public class StewAnimalDispenserBehavior extends DDGeneralBehaviorHelper {

    @Override
    protected ItemStack dispenseSilently(BlockPointer pointer, ItemStack stack) {
        this.setSuccess(true);
        ServerWorld world = pointer.world();
        BlockPos facing = pointer.pos().offset(pointer.state().get(DispenserBlock.FACING));
        Box facingBox = new Box(facing);

        List<MooshroomEntity> stewable = world.getEntitiesByClass(MooshroomEntity.class, facingBox, EntityPredicates.VALID_LIVING_ENTITY.and((stewableEntity)
            -> !((MooshroomEntity) stewableEntity).isBaby()));
        
        if (!stewable.isEmpty()) {
            ItemStack newStack = getStewType(stewable);
            SoundEvent sound = newStack.getItem() == Items.SUSPICIOUS_STEW ? SoundEvents.ENTITY_MOOSHROOM_SUSPICIOUS_MILK : SoundEvents.ENTITY_MOOSHROOM_MILK;
            world.playSound(null, facing, sound, SoundCategory.NEUTRAL, 1.0f, 1.0f);
            return this.addOrDispense(pointer, stack, newStack);
        }

        this.setSuccess(false);
        return stack;
    }

    private static ItemStack getStewType(List<MooshroomEntity> mooshrooms) {
        for (MooshroomEntity mooshroom : mooshrooms) {
            MooshroomStatusEffectAccessor mooshroomAccessor = (MooshroomStatusEffectAccessor) mooshroom;
            List<SuspiciousStewIngredient.StewEffect> stewEffects = mooshroomAccessor.getStewEffects();
            if (stewEffects != null) {
                ItemStack stewStack = new ItemStack(Items.SUSPICIOUS_STEW);
                SuspiciousStewItem.writeEffectsToStew(stewStack, stewEffects);
                mooshroomAccessor.setStewEffects(null);
                return stewStack;
            }
        }
        return new ItemStack(Items.MUSHROOM_STEW);
    }
}
