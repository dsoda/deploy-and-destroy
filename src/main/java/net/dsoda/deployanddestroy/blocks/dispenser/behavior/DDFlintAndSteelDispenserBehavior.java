package net.dsoda.deployanddestroy.blocks.dispenser.behavior;

import net.minecraft.block.Block;
import net.minecraft.block.Blocks;
import net.minecraft.block.DispenserBlock;
import net.minecraft.block.dispenser.FallibleItemDispenserBehavior;
import net.minecraft.item.ItemStack;
import net.minecraft.server.world.ServerWorld;
import net.minecraft.sound.SoundCategory;
import net.minecraft.sound.SoundEvents;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.BlockPointer;
import net.minecraft.world.event.GameEvent;

public class DDFlintAndSteelDispenserBehavior extends FallibleItemDispenserBehavior {
    @Override
    protected ItemStack dispenseSilently(BlockPointer pointer, ItemStack stack) {
        this.setSuccess(true);
        ServerWorld world = pointer.world();
        BlockPos pos = pointer.pos().offset(pointer.state().get(DispenserBlock.FACING));
        Block facingBlock = world.getBlockState(pos).getBlock();
        if(facingBlock == Blocks.WET_SPONGE) {
            world.setBlockState(pos, Blocks.SPONGE.getDefaultState(), Block.NOTIFY_ALL | Block.REDRAW_ON_MAIN_THREAD);
            world.playSound(null, pos, SoundEvents.BLOCK_FIRE_EXTINGUISH, SoundCategory.BLOCKS, 1f, 1f);
            world.emitGameEvent(null, GameEvent.BLOCK_CHANGE, pos);
        } else {
            this.setSuccess(false);
        }
        if (this.isSuccess() && stack.damage(1, world.random, null)) {
            stack.setCount(0);
        }
        return stack;
    }
}
