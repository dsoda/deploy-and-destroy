package net.dsoda.deployanddestroy.blocks;

import net.dsoda.deployanddestroy.DeployAndDestroy;
import net.dsoda.deployanddestroy.blocks.block.entity.*;
import net.dsoda.deployanddestroy.blocks.block.entity.barrel.*;
import net.dsoda.deployanddestroy.blocks.block.entity.sign.DDHangingSignBlockEntity;
import net.dsoda.deployanddestroy.blocks.block.entity.sign.DDSignBlockEntity;
import net.fabricmc.fabric.api.object.builder.v1.block.entity.FabricBlockEntityTypeBuilder;
import net.minecraft.block.entity.BlockEntityType;
import net.minecraft.registry.Registries;
import net.minecraft.registry.Registry;
import net.minecraft.util.Identifier;
import static net.dsoda.deployanddestroy.DeployAndDestroy.MOD_ID;


public class DDBlockEntities {
    public static final BlockEntityType<DeployerBlockEntity> DEPLOYER_BLOCK_ENTITY =
        Registry.register(Registries.BLOCK_ENTITY_TYPE, new Identifier(MOD_ID, "deployer_block_entity"),
            FabricBlockEntityTypeBuilder.create(DeployerBlockEntity::new,
                DDBlocks.DEPLOYER).build());

    public static final BlockEntityType<DestroyerBlockEntity> DESTROYER_BLOCK_ENTITY =
        Registry.register(Registries.BLOCK_ENTITY_TYPE, new Identifier(MOD_ID, "destroyer_block_entity"),
            FabricBlockEntityTypeBuilder.create(DestroyerBlockEntity::new,
                DDBlocks.DESTROYER).build());

    public static final BlockEntityType<CopperBarrelBlockEntity> COPPER_BARREL_BLOCK_ENTITY =
        Registry.register(Registries.BLOCK_ENTITY_TYPE, new Identifier(MOD_ID, "copper_barrel_block_entity"),
            FabricBlockEntityTypeBuilder.create(CopperBarrelBlockEntity::new,
                DDBlocks.COPPER_BARREL).build());

    public static final BlockEntityType<IronBarrelBlockEntity> IRON_BARREL_BLOCK_ENTITY =
        Registry.register(Registries.BLOCK_ENTITY_TYPE, new Identifier(MOD_ID, "iron_barrel_block_entity"),
            FabricBlockEntityTypeBuilder.create(IronBarrelBlockEntity::new,
                DDBlocks.IRON_BARREL).build());

    public static final BlockEntityType<GoldBarrelBlockEntity> GOLD_BARREL_BLOCK_ENTITY =
        Registry.register(Registries.BLOCK_ENTITY_TYPE, new Identifier(MOD_ID, "gold_barrel_block_entity"),
            FabricBlockEntityTypeBuilder.create(GoldBarrelBlockEntity::new,
                DDBlocks.GOLD_BARREL).build());

    public static final BlockEntityType<DiamondBarrelBlockEntity> DIAMOND_BARREL_BLOCK_ENTITY =
        Registry.register(Registries.BLOCK_ENTITY_TYPE, new Identifier(MOD_ID, "diamond_barrel_block_entity"),
            FabricBlockEntityTypeBuilder.create(DiamondBarrelBlockEntity::new,
                DDBlocks.DIAMOND_BARREL).build());

    public static final BlockEntityType<NetheriteBarrelBlockEntity> NETHERITE_BARREL_BLOCK_ENTITY =
        Registry.register(Registries.BLOCK_ENTITY_TYPE, new Identifier(MOD_ID, "netherite_barrel_block_entity"),
            FabricBlockEntityTypeBuilder.create(NetheriteBarrelBlockEntity::new,
                DDBlocks.NETHERITE_BARREL).build());

    public static final BlockEntityType<DDCampfireBlockEntity> DD_CAMPFIRE =
        Registry.register(Registries.BLOCK_ENTITY_TYPE, new Identifier(MOD_ID, "dd_campfire_block_entity"),
            FabricBlockEntityTypeBuilder.create(DDCampfireBlockEntity::new,
                DDBlocks.COPPER_CAMPFIRE, DDBlocks.AMETHYST_CAMPFIRE).build());

    public static final BlockEntityType<FastHopperBlockEntity> FAST_HOPPER_BLOCK_ENTITY =
        Registry.register(Registries.BLOCK_ENTITY_TYPE, new Identifier(MOD_ID, "fast_hopper_block_entity"),
            FabricBlockEntityTypeBuilder.create(FastHopperBlockEntity::new,
                DDBlocks.FAST_HOPPER).build());

    public static final BlockEntityType<DDSignBlockEntity> DD_SIGN_BLOCK_ENTITY = Registry.register(
            Registries.BLOCK_ENTITY_TYPE,
            new Identifier(MOD_ID, "dd_sign_entity"),
            FabricBlockEntityTypeBuilder.create(DDSignBlockEntity::new,
                DDBlocks.AZALEA_SIGN, DDBlocks.AZALEA_WALL_SIGN,
                DDBlocks.SWAMP_SIGN, DDBlocks.SWAMP_WALL_SIGN
            ).build());

    public static final BlockEntityType<DDHangingSignBlockEntity> DD_HANGING_SIGN_BLOCK_ENTITY = Registry.register(
        Registries.BLOCK_ENTITY_TYPE,
            new Identifier(MOD_ID, "dd_hanging_sign_entity"),
            FabricBlockEntityTypeBuilder.create(DDHangingSignBlockEntity::new,
                DDBlocks.AZALEA_HANGING_SIGN, DDBlocks.AZALEA_WALL_HANGING_SIGN,
                DDBlocks.SWAMP_HANGING_SIGN, DDBlocks.SWAMP_WALL_HANGING_SIGN
            ).build());

    public static void registerBlockEntities() {
        DeployAndDestroy.LOGGER.info("Registering Block Entities for " + MOD_ID);
    }
}
