package net.dsoda.deployanddestroy.util;

import net.dsoda.deployanddestroy.DeployAndDestroy;
import net.minecraft.entity.effect.StatusEffectInstance;
import net.minecraft.entity.effect.StatusEffects;
import net.minecraft.item.FoodComponent;

public class DDFoods {

    public static final FoodComponent CHOCOLATE_MILK = new FoodComponent.Builder().hunger(6).saturationModifier(0.3f)
        .statusEffect(new StatusEffectInstance(StatusEffects.SPEED, 100), 0.5f).build();

    public static void registerFoodComponents() {
        DeployAndDestroy.LOGGER.info("Registering foods.");
    }
}
