package net.dsoda.deployanddestroy.util.api;

import com.mojang.datafixers.util.Pair;
import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import net.dsoda.deployanddestroy.util.StructurePoolExtension;
import net.dsoda.deployanddestroy.mixin.misc.accessor.StructurePoolAccessor;
import net.minecraft.registry.RegistryKey;
import net.minecraft.registry.RegistryKeys;
import net.minecraft.registry.entry.RegistryEntry;
import net.minecraft.server.MinecraftServer;
import net.minecraft.structure.pool.StructurePool;
import net.minecraft.structure.pool.StructurePoolElement;
import net.minecraft.structure.processor.StructureProcessorList;
import net.minecraft.util.Identifier;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Optional;

public class StructurePoolAPI {

    public static void injectIntoStructurePool(MinecraftServer server, Identifier poolId, Identifier structureId, int weight, String pList) {

        RegistryEntry<StructureProcessorList> processorList = getProcessorList(server, pList);
        Optional<StructurePool> poolGetter = server.getRegistryManager().get(RegistryKeys.TEMPLATE_POOL).getOrEmpty(poolId);

        if (poolGetter.isEmpty()) {
            System.err.println("StructurePool API: cannot add to " + poolId + " as it cannot be found!");
            return;
        }

        StructurePool pool = poolGetter.get();

        ObjectArrayList<StructurePoolElement> pieceList = ((StructurePoolAccessor) pool).getElements();
        StructurePoolElement piece = StructurePoolElement.ofProcessedSingle(structureId.toString(), processorList).apply(StructurePool.Projection.RIGID);
        ((StructurePoolExtension)pool).remember(piece, structureId);
        ArrayList<Pair<StructurePoolElement, Integer>> list = new ArrayList<>(((StructurePoolAccessor) pool).getElementCounts());
        list.add(Pair.of(piece, weight));
        ((StructurePoolAccessor) pool).setElementCounts(list);

        for (int i = 0; i < weight; ++i) {
            pieceList.add(piece);
        }
    }

    private static RegistryEntry<StructureProcessorList> getProcessorList(MinecraftServer server, String pList) {
        return server.getRegistryManager().get(
            RegistryKeys.PROCESSOR_LIST).entryOf(
                RegistryKey.of(RegistryKeys.PROCESSOR_LIST, new Identifier("minecraft", pList)
            )
        );
    }

    public record SpawnPerk(int limit) { }
    public static HashMap<Identifier, HashMap<Identifier, SpawnPerk>> spawnLimitations = new HashMap<>();

    public static void limitSpawn(Identifier poolId, Identifier structureId, int limit) {
        if (!spawnLimitations.containsKey(poolId)) {
            spawnLimitations.put(poolId, new HashMap<>());
        }
        spawnLimitations.get(poolId).put(structureId, new SpawnPerk(limit));
    }
}
