package net.dsoda.deployanddestroy.util;

import net.dsoda.deployanddestroy.DeployAndDestroy;
import net.minecraft.item.Item;
import net.minecraft.item.Items;

import java.util.HashMap;

import static net.dsoda.deployanddestroy.DeployAndDestroy.MOD_ID;

public class DDMaxStackSizeOverrides {

    public static final HashMap<Item, Integer> OVERRIDES = new HashMap<>();

    public static void initMaxStackSizeOverrides() {
        DeployAndDestroy.LOGGER.info("Overriding recipe remainders for " + MOD_ID);
        OVERRIDES.put(Items.SNOWBALL, 64);
        OVERRIDES.put(Items.EGG, 64);
    }
}
