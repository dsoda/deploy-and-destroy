package net.dsoda.deployanddestroy.util.behavior;

import net.minecraft.block.dispenser.FallibleItemDispenserBehavior;
import net.minecraft.item.ItemStack;
import net.minecraft.util.math.BlockPointer;

public class DDGeneralBehaviorHelper extends FallibleItemDispenserBehavior {

    public ItemStack addOrDispense(BlockPointer pointer, ItemStack stack, ItemStack newStack) {
        stack.decrement(1);
        if (stack.isEmpty()) {
            return newStack;
        }
        else if ((pointer.blockEntity()).addToFirstFreeSlot(newStack) < 0) {
            super.dispenseSilently(pointer, newStack);
        }
        return stack;
    }
}
