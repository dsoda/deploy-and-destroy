package net.dsoda.deployanddestroy.util.behavior;

import net.minecraft.block.BlockState;
import net.minecraft.block.Blocks;
import net.minecraft.block.LeveledCauldronBlock;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Items;
import net.minecraft.potion.PotionUtil;
import net.minecraft.potion.Potions;
import net.minecraft.server.world.ServerWorld;
import net.minecraft.sound.SoundCategory;
import net.minecraft.sound.SoundEvent;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.event.GameEvent;

public class DDCauldronBehaviorHelper {
    public static ItemStack getWaterBottle() {
        return PotionUtil.setPotion(Items.POTION.getDefaultStack(), Potions.WATER);
    }

    public static void setCauldron(ServerWorld world, BlockPos pos, BlockState state, SoundEvent soundEvent) {
        world.setBlockState(pos, state);
        world.playSound(null, pos, soundEvent, SoundCategory.BLOCKS, 1.0F, 1.0F);
        world.emitGameEvent(null, GameEvent.FLUID_PLACE, pos);
    }

    public static void setCauldronWithFillLevel(ServerWorld world, BlockPos pos, BlockState state, SoundEvent soundEvent, byte flag) {
        world.setBlockState(pos, getStateWithNextFillLevel(state, flag));
        world.playSound(null, pos, soundEvent, SoundCategory.BLOCKS, 1.0f, 1.0f);
        world.emitGameEvent(null, GameEvent.FLUID_PLACE, pos);
    }

    public static boolean isCauldronFull(BlockState cauldron) {
        return cauldron.get(LeveledCauldronBlock.LEVEL) >= 3;
    }

    public static boolean isCauldronEmpty(BlockState cauldron) {
        return cauldron == Blocks.CAULDRON.getDefaultState();
    }

    private static BlockState getStateWithNextFillLevel(BlockState cauldron, byte flag) {
        BlockState newCauldron;
        int fillLevel = !isCauldronEmpty(cauldron) ? cauldron.get(LeveledCauldronBlock.LEVEL) : 0;
        return switch (flag) {
            // emptying
            case 0 -> {
                newCauldron = fillLevel < 2 ? Blocks.CAULDRON.getDefaultState() : Blocks.WATER_CAULDRON.getDefaultState().with(LeveledCauldronBlock.LEVEL, fillLevel - 1);
                yield newCauldron;
            }
            // filling
            case 1 -> {
                newCauldron = fillLevel > 2 ? Blocks.WATER_CAULDRON.getDefaultState().with(LeveledCauldronBlock.LEVEL, 3) : Blocks.WATER_CAULDRON.getDefaultState().with(LeveledCauldronBlock.LEVEL, fillLevel + 1);
                yield newCauldron;
            }
            default -> cauldron;
        };
    }
}
