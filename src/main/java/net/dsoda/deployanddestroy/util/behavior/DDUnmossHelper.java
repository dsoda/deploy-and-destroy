package net.dsoda.deployanddestroy.util.behavior;

import com.google.common.collect.ImmutableMap;
import net.dsoda.deployanddestroy.blocks.DDBlocks;
import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.block.Blocks;
import net.minecraft.entity.ItemEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Items;
import net.minecraft.sound.SoundCategory;
import net.minecraft.sound.SoundEvents;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Direction;
import net.minecraft.world.World;
import net.minecraft.world.event.GameEvent;

import java.util.Map;
import java.util.Optional;

public class DDUnmossHelper {

    private static final Map<Block, Block> MOSSY_TO_UNMOSSY = new ImmutableMap.Builder<Block, Block>()
        .put(Blocks.MOSSY_COBBLESTONE, Blocks.COBBLESTONE)
        .put(Blocks.MOSSY_COBBLESTONE_STAIRS, Blocks.COBBLESTONE_STAIRS)
        .put(Blocks.MOSSY_COBBLESTONE_SLAB, Blocks.COBBLESTONE_SLAB)
        .put(Blocks.MOSSY_COBBLESTONE_WALL, Blocks.COBBLESTONE_WALL)
        .put(Blocks.MOSSY_STONE_BRICKS, Blocks.STONE_BRICKS)
        .put(Blocks.MOSSY_STONE_BRICK_STAIRS, Blocks.STONE_BRICK_STAIRS)
        .put(Blocks.MOSSY_STONE_BRICK_SLAB, Blocks.STONE_BRICK_SLAB)
        .put(Blocks.MOSSY_STONE_BRICK_WALL, Blocks.STONE_BRICK_WALL)
        .put(DDBlocks.MOSSY_BRICKS, Blocks.BRICKS)
        .put(DDBlocks.MOSSY_BRICK_STAIRS, Blocks.BRICK_STAIRS)
        .put(DDBlocks.MOSSY_BRICK_SLAB, Blocks.BRICK_SLAB)
        .put(DDBlocks.MOSSY_BRICK_WALL, Blocks.BRICK_WALL)
        .put(DDBlocks.MOSSY_SANDSTONE_BRICKS, DDBlocks.SANDSTONE_BRICKS)
        .put(DDBlocks.MOSSY_SANDSTONE_BRICK_STAIRS, DDBlocks.SANDSTONE_BRICK_STAIRS)
        .put(DDBlocks.MOSSY_SANDSTONE_BRICK_SLAB, DDBlocks.SANDSTONE_BRICK_SLAB)
        .put(DDBlocks.MOSSY_SANDSTONE_BRICK_WALL, DDBlocks.SANDSTONE_BRICK_WALL)
        .put(DDBlocks.MOSSY_RED_SANDSTONE_BRICKS, DDBlocks.RED_SANDSTONE_BRICKS)
        .put(DDBlocks.MOSSY_RED_SANDSTONE_BRICK_STAIRS, DDBlocks.RED_SANDSTONE_BRICK_STAIRS)
        .put(DDBlocks.MOSSY_RED_SANDSTONE_BRICK_SLAB, DDBlocks.RED_SANDSTONE_BRICK_SLAB)
        .put(DDBlocks.MOSSY_RED_SANDSTONE_BRICK_WALL, DDBlocks.RED_SANDSTONE_BRICK_WALL)
        .put(DDBlocks.MOSSY_ANDESITE_BRICKS, DDBlocks.ANDESITE_BRICKS)
        .put(DDBlocks.MOSSY_ANDESITE_BRICK_STAIRS, DDBlocks.ANDESITE_BRICK_STAIRS)
        .put(DDBlocks.MOSSY_ANDESITE_BRICK_SLAB, DDBlocks.ANDESITE_BRICK_SLAB)
        .put(DDBlocks.MOSSY_ANDESITE_BRICK_WALL, DDBlocks.ANDESITE_BRICK_WALL)
        .put(DDBlocks.MOSSY_DIORITE_BRICKS, DDBlocks.DIORITE_BRICKS)
        .put(DDBlocks.MOSSY_DIORITE_BRICK_STAIRS, DDBlocks.DIORITE_BRICK_STAIRS)
        .put(DDBlocks.MOSSY_DIORITE_BRICK_SLAB, DDBlocks.DIORITE_BRICK_SLAB)
        .put(DDBlocks.MOSSY_DIORITE_BRICK_WALL, DDBlocks.DIORITE_BRICK_WALL)
        .put(DDBlocks.MOSSY_GRANITE_BRICKS, DDBlocks.GRANITE_BRICKS)
        .put(DDBlocks.MOSSY_GRANITE_BRICK_STAIRS, DDBlocks.GRANITE_BRICK_STAIRS)
        .put(DDBlocks.MOSSY_GRANITE_BRICK_SLAB, DDBlocks.GRANITE_BRICK_SLAB)
        .put(DDBlocks.MOSSY_GRANITE_BRICK_WALL, DDBlocks.GRANITE_BRICK_WALL)
        .put(DDBlocks.MOSSY_COBBLESTONE_COLUMN, DDBlocks.COBBLESTONE_COLUMN)
        .put(DDBlocks.MOSSY_STONE_BRICKS_COLUMN, DDBlocks.STONE_BRICKS_COLUMN)
        .put(DDBlocks.MOSSY_GRANITE_BRICKS_COLUMN, DDBlocks.GRANITE_BRICKS_COLUMN)
        .put(DDBlocks.MOSSY_DIORITE_BRICKS_COLUMN, DDBlocks.DIORITE_BRICKS_COLUMN)
        .put(DDBlocks.MOSSY_ANDESITE_BRICKS_COLUMN, DDBlocks.ANDESITE_BRICKS_COLUMN)
        .put(DDBlocks.MOSSY_CALCITE_BRICKS_COLUMN, DDBlocks.CALCITE_BRICKS_COLUMN)
        .put(DDBlocks.MOSSY_BRICKS_COLUMN, DDBlocks.BRICKS_COLUMN)
        .put(DDBlocks.MOSSY_SANDSTONE_BRICKS_COLUMN, DDBlocks.SANDSTONE_BRICKS_COLUMN)
        .put(DDBlocks.MOSSY_RED_SANDSTONE_BRICKS_COLUMN, DDBlocks.RED_SANDSTONE_BRICKS_COLUMN)
        .put(DDBlocks.MOSSY_DRIPSTONE_BRICKS, DDBlocks.DRIPSTONE_BRICKS)
        .put(DDBlocks.MOSSY_DRIPSTONE_BRICK_STAIRS, DDBlocks.DRIPSTONE_BRICK_STAIRS)
        .put(DDBlocks.MOSSY_DRIPSTONE_BRICK_SLAB, DDBlocks.DRIPSTONE_BRICK_SLAB)
        .put(DDBlocks.MOSSY_DRIPSTONE_BRICK_WALL, DDBlocks.DRIPSTONE_BRICK_WALL)
        .put(DDBlocks.MOSSY_DRIPSTONE_BRICKS_COLUMN, DDBlocks.DRIPSTONE_BRICKS_COLUMN)
        .put(DDBlocks.MOSSY_COBBLED_DEEPSLATE, Blocks.COBBLED_DEEPSLATE)
        .put(DDBlocks.MOSSY_COBBLED_DEEPSLATE_STAIRS, Blocks.COBBLED_DEEPSLATE_STAIRS)
        .put(DDBlocks.MOSSY_COBBLED_DEEPSLATE_SLAB, Blocks.COBBLED_DEEPSLATE_SLAB)
        .put(DDBlocks.MOSSY_COBBLED_DEEPSLATE_WALL, Blocks.COBBLED_DEEPSLATE_WALL)
        .put(DDBlocks.MOSSY_COBBLED_DEEPSLATE_COLUMN, DDBlocks.COBBLED_DEEPSLATE_COLUMN)
        .put(DDBlocks.MOSSY_DEEPSLATE_BRICKS, Blocks.DEEPSLATE_BRICKS)
        .put(DDBlocks.MOSSY_DEEPSLATE_BRICK_STAIRS, Blocks.DEEPSLATE_BRICK_STAIRS)
        .put(DDBlocks.MOSSY_DEEPSLATE_BRICK_SLAB, Blocks.DEEPSLATE_BRICK_SLAB)
        .put(DDBlocks.MOSSY_DEEPSLATE_BRICK_WALL, Blocks.DEEPSLATE_BRICK_WALL)
        .put(DDBlocks.MOSSY_DEEPSLATE_BRICKS_COLUMN, DDBlocks.DEEPSLATE_BRICKS_COLUMN)
        .build();

    public static boolean isMossy(Block block) {
        return MOSSY_TO_UNMOSSY.containsKey(block);
    }

    public static Optional<BlockState> getMossyBlock(World world, BlockPos pos, BlockState blockState) {
        return Optional.ofNullable(MOSSY_TO_UNMOSSY.get(world.getBlockState(pos).getBlock())).map(state -> state.getStateWithProperties(blockState));
    }

    public static void removeMoss(World world, Direction direction, BlockPos pos, BlockState state) {
        world.setBlockState(pos, state, Block.NOTIFY_ALL | Block.REDRAW_ON_MAIN_THREAD);
        world.emitGameEvent(null, GameEvent.BLOCK_CHANGE, pos);
        world.playSound(null, pos, SoundEvents.ENTITY_SHEEP_SHEAR, SoundCategory.BLOCKS, 1f, 1f);
        ItemEntity itemEntity = new ItemEntity(world, (double)pos.getX() + 0.5 + (double)direction.getOffsetX() * 0.65, (double)pos.getY() + 0.1, (double)pos.getZ() + 0.5 + (double)direction.getOffsetZ() * 0.65, new ItemStack(Items.MOSS_CARPET, world.random.nextBetween(1, 3)));
        itemEntity.setVelocity(0.05 * (double)direction.getOffsetX() + world.random.nextDouble() * 0.02, 0.05, 0.05 * (double)direction.getOffsetZ() + world.random.nextDouble() * 0.02);
        world.spawnEntity(itemEntity);
    }
}
