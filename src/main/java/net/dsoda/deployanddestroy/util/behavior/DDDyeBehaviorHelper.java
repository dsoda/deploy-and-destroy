package net.dsoda.deployanddestroy.util.behavior;

import com.google.common.collect.ImmutableMap;
import net.dsoda.deployanddestroy.blocks.DDBlocks;
import net.dsoda.deployanddestroy.util.tag.DDBlockTags;
import net.minecraft.block.*;
import net.minecraft.block.entity.BlockEntity;
import net.minecraft.block.entity.ShulkerBoxBlockEntity;
import net.minecraft.block.enums.BedPart;
import net.minecraft.entity.passive.SheepEntity;
import net.minecraft.nbt.NbtCompound;
import net.minecraft.predicate.entity.EntityPredicates;
import net.minecraft.registry.tag.BlockTags;
import net.minecraft.server.world.ServerWorld;
import net.minecraft.state.property.Properties;
import net.minecraft.text.Text;
import net.minecraft.util.DyeColor;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Box;
import net.minecraft.util.math.Direction;
import net.minecraft.world.World;
import net.minecraft.world.event.GameEvent;

import java.util.List;
import java.util.Map;
import java.util.Optional;

public class DDDyeBehaviorHelper {
    private static final Map<DyeColor, Block> DYE_TO_WOOL = new ImmutableMap.Builder<DyeColor, Block>()
        .put(DyeColor.WHITE, Blocks.WHITE_WOOL)
        .put(DyeColor.LIGHT_GRAY, Blocks.LIGHT_GRAY_WOOL)
        .put(DyeColor.GRAY, Blocks.GRAY_WOOL)
        .put(DyeColor.BLACK, Blocks.BLACK_WOOL)
        .put(DyeColor.BROWN, Blocks.BROWN_WOOL)
        .put(DyeColor.RED, Blocks.RED_WOOL)
        .put(DyeColor.ORANGE, Blocks.ORANGE_WOOL)
        .put(DyeColor.YELLOW, Blocks.YELLOW_WOOL)
        .put(DyeColor.LIME, Blocks.LIME_WOOL)
        .put(DyeColor.GREEN, Blocks.GREEN_WOOL)
        .put(DyeColor.CYAN, Blocks.CYAN_WOOL)
        .put(DyeColor.LIGHT_BLUE, Blocks.LIGHT_BLUE_WOOL)
        .put(DyeColor.BLUE, Blocks.BLUE_WOOL)
        .put(DyeColor.PURPLE, Blocks.PURPLE_WOOL)
        .put(DyeColor.MAGENTA, Blocks.MAGENTA_WOOL)
        .put(DyeColor.PINK, Blocks.PINK_WOOL)
        .build();

    private static final Map<DyeColor, Block> DYE_TO_GLASS = new ImmutableMap.Builder<DyeColor, Block>()
        .put(DyeColor.WHITE, Blocks.WHITE_STAINED_GLASS)
        .put(DyeColor.LIGHT_GRAY, Blocks.LIGHT_GRAY_STAINED_GLASS)
        .put(DyeColor.GRAY, Blocks.GRAY_STAINED_GLASS)
        .put(DyeColor.BLACK, Blocks.BLACK_STAINED_GLASS)
        .put(DyeColor.BROWN, Blocks.BROWN_STAINED_GLASS)
        .put(DyeColor.RED, Blocks.RED_STAINED_GLASS)
        .put(DyeColor.ORANGE, Blocks.ORANGE_STAINED_GLASS)
        .put(DyeColor.YELLOW, Blocks.YELLOW_STAINED_GLASS)
        .put(DyeColor.LIME, Blocks.LIME_STAINED_GLASS)
        .put(DyeColor.GREEN, Blocks.GREEN_STAINED_GLASS)
        .put(DyeColor.CYAN, Blocks.CYAN_STAINED_GLASS)
        .put(DyeColor.LIGHT_BLUE, Blocks.LIGHT_BLUE_STAINED_GLASS)
        .put(DyeColor.BLUE, Blocks.BLUE_STAINED_GLASS)
        .put(DyeColor.PURPLE, Blocks.PURPLE_STAINED_GLASS)
        .put(DyeColor.MAGENTA, Blocks.MAGENTA_STAINED_GLASS)
        .put(DyeColor.PINK, Blocks.PINK_STAINED_GLASS)
        .build();

    private static final Map<DyeColor, Block> DYE_TO_GLASS_PANE = new ImmutableMap.Builder<DyeColor, Block>()
        .put(DyeColor.WHITE, Blocks.WHITE_STAINED_GLASS_PANE)
        .put(DyeColor.LIGHT_GRAY, Blocks.LIGHT_GRAY_STAINED_GLASS_PANE)
        .put(DyeColor.GRAY, Blocks.GRAY_STAINED_GLASS_PANE)
        .put(DyeColor.BLACK, Blocks.BLACK_STAINED_GLASS_PANE)
        .put(DyeColor.BROWN, Blocks.BROWN_STAINED_GLASS_PANE)
        .put(DyeColor.RED, Blocks.RED_STAINED_GLASS_PANE)
        .put(DyeColor.ORANGE, Blocks.ORANGE_STAINED_GLASS_PANE)
        .put(DyeColor.YELLOW, Blocks.YELLOW_STAINED_GLASS_PANE)
        .put(DyeColor.LIME, Blocks.LIME_STAINED_GLASS_PANE)
        .put(DyeColor.GREEN, Blocks.GREEN_STAINED_GLASS_PANE)
        .put(DyeColor.CYAN, Blocks.CYAN_STAINED_GLASS_PANE)
        .put(DyeColor.LIGHT_BLUE, Blocks.LIGHT_BLUE_STAINED_GLASS_PANE)
        .put(DyeColor.BLUE, Blocks.BLUE_STAINED_GLASS_PANE)
        .put(DyeColor.PURPLE, Blocks.PURPLE_STAINED_GLASS_PANE)
        .put(DyeColor.MAGENTA, Blocks.MAGENTA_STAINED_GLASS_PANE)
        .put(DyeColor.PINK, Blocks.PINK_STAINED_GLASS_PANE)
        .build();

    private static final Map<DyeColor, Block> DYE_TO_CONCRETE = new ImmutableMap.Builder<DyeColor, Block>()
        .put(DyeColor.WHITE, Blocks.WHITE_CONCRETE)
        .put(DyeColor.LIGHT_GRAY, Blocks.LIGHT_GRAY_CONCRETE)
        .put(DyeColor.GRAY, Blocks.GRAY_CONCRETE)
        .put(DyeColor.BLACK, Blocks.BLACK_CONCRETE)
        .put(DyeColor.BROWN, Blocks.BROWN_CONCRETE)
        .put(DyeColor.RED, Blocks.RED_CONCRETE)
        .put(DyeColor.ORANGE, Blocks.ORANGE_CONCRETE)
        .put(DyeColor.YELLOW, Blocks.YELLOW_CONCRETE)
        .put(DyeColor.LIME, Blocks.LIME_CONCRETE)
        .put(DyeColor.GREEN, Blocks.GREEN_CONCRETE)
        .put(DyeColor.CYAN, Blocks.CYAN_CONCRETE)
        .put(DyeColor.LIGHT_BLUE, Blocks.LIGHT_BLUE_CONCRETE)
        .put(DyeColor.BLUE, Blocks.BLUE_CONCRETE)
        .put(DyeColor.PURPLE, Blocks.PURPLE_CONCRETE)
        .put(DyeColor.MAGENTA, Blocks.MAGENTA_CONCRETE)
        .put(DyeColor.PINK, Blocks.PINK_CONCRETE)
        .build();

    private static final Map<DyeColor, Block> DYE_TO_CONCRETE_POWDER = new ImmutableMap.Builder<DyeColor, Block>()
        .put(DyeColor.WHITE, Blocks.WHITE_CONCRETE_POWDER)
        .put(DyeColor.LIGHT_GRAY, Blocks.LIGHT_GRAY_CONCRETE_POWDER)
        .put(DyeColor.GRAY, Blocks.GRAY_CONCRETE_POWDER)
        .put(DyeColor.BLACK, Blocks.BLACK_CONCRETE_POWDER)
        .put(DyeColor.BROWN, Blocks.BROWN_CONCRETE_POWDER)
        .put(DyeColor.RED, Blocks.RED_CONCRETE_POWDER)
        .put(DyeColor.ORANGE, Blocks.ORANGE_CONCRETE_POWDER)
        .put(DyeColor.YELLOW, Blocks.YELLOW_CONCRETE_POWDER)
        .put(DyeColor.LIME, Blocks.LIME_CONCRETE_POWDER)
        .put(DyeColor.GREEN, Blocks.GREEN_CONCRETE_POWDER)
        .put(DyeColor.CYAN, Blocks.CYAN_CONCRETE_POWDER)
        .put(DyeColor.LIGHT_BLUE, Blocks.LIGHT_BLUE_CONCRETE_POWDER)
        .put(DyeColor.BLUE, Blocks.BLUE_CONCRETE_POWDER)
        .put(DyeColor.PURPLE, Blocks.PURPLE_CONCRETE_POWDER)
        .put(DyeColor.MAGENTA, Blocks.MAGENTA_CONCRETE_POWDER)
        .put(DyeColor.PINK, Blocks.PINK_CONCRETE_POWDER)
        .build();

    private static final Map<DyeColor, Block> DYE_TO_TERRACOTTA = new ImmutableMap.Builder<DyeColor, Block>()
        .put(DyeColor.WHITE, Blocks.WHITE_TERRACOTTA)
        .put(DyeColor.LIGHT_GRAY, Blocks.LIGHT_GRAY_TERRACOTTA)
        .put(DyeColor.GRAY, Blocks.GRAY_TERRACOTTA)
        .put(DyeColor.BLACK, Blocks.BLACK_TERRACOTTA)
        .put(DyeColor.BROWN, Blocks.BROWN_TERRACOTTA)
        .put(DyeColor.RED, Blocks.RED_TERRACOTTA)
        .put(DyeColor.ORANGE, Blocks.ORANGE_TERRACOTTA)
        .put(DyeColor.YELLOW, Blocks.YELLOW_TERRACOTTA)
        .put(DyeColor.LIME, Blocks.LIME_TERRACOTTA)
        .put(DyeColor.GREEN, Blocks.GREEN_TERRACOTTA)
        .put(DyeColor.CYAN, Blocks.CYAN_TERRACOTTA)
        .put(DyeColor.LIGHT_BLUE, Blocks.LIGHT_BLUE_TERRACOTTA)
        .put(DyeColor.BLUE, Blocks.BLUE_TERRACOTTA)
        .put(DyeColor.PURPLE, Blocks.PURPLE_TERRACOTTA)
        .put(DyeColor.MAGENTA, Blocks.MAGENTA_TERRACOTTA)
        .put(DyeColor.PINK, Blocks.PINK_TERRACOTTA)
        .build();

    private static final Map<DyeColor, Block> DYE_TO_CANDLE = new ImmutableMap.Builder<DyeColor, Block>()
        .put(DyeColor.WHITE, Blocks.WHITE_CANDLE)
        .put(DyeColor.LIGHT_GRAY, Blocks.LIGHT_GRAY_CANDLE)
        .put(DyeColor.GRAY, Blocks.GRAY_CANDLE)
        .put(DyeColor.BLACK, Blocks.BLACK_CANDLE)
        .put(DyeColor.BROWN, Blocks.BROWN_CANDLE)
        .put(DyeColor.RED, Blocks.RED_CANDLE)
        .put(DyeColor.ORANGE, Blocks.ORANGE_CANDLE)
        .put(DyeColor.YELLOW, Blocks.YELLOW_CANDLE)
        .put(DyeColor.LIME, Blocks.LIME_CANDLE)
        .put(DyeColor.GREEN, Blocks.GREEN_CANDLE)
        .put(DyeColor.CYAN, Blocks.CYAN_CANDLE)
        .put(DyeColor.LIGHT_BLUE, Blocks.LIGHT_BLUE_CANDLE)
        .put(DyeColor.BLUE, Blocks.BLUE_CANDLE)
        .put(DyeColor.PURPLE, Blocks.PURPLE_CANDLE)
        .put(DyeColor.MAGENTA, Blocks.MAGENTA_CANDLE)
        .put(DyeColor.PINK, Blocks.PINK_CANDLE)
        .build();

    private static final Map<DyeColor, Block> DYE_TO_CANDLE_CAKE = new ImmutableMap.Builder<DyeColor, Block>()
        .put(DyeColor.WHITE, Blocks.WHITE_CANDLE_CAKE)
        .put(DyeColor.LIGHT_GRAY, Blocks.LIGHT_GRAY_CANDLE_CAKE)
        .put(DyeColor.GRAY, Blocks.GRAY_CANDLE_CAKE)
        .put(DyeColor.BLACK, Blocks.BLACK_CANDLE_CAKE)
        .put(DyeColor.BROWN, Blocks.BROWN_CANDLE_CAKE)
        .put(DyeColor.RED, Blocks.RED_CANDLE_CAKE)
        .put(DyeColor.ORANGE, Blocks.ORANGE_CANDLE_CAKE)
        .put(DyeColor.YELLOW, Blocks.YELLOW_CANDLE_CAKE)
        .put(DyeColor.LIME, Blocks.LIME_CANDLE_CAKE)
        .put(DyeColor.GREEN, Blocks.GREEN_CANDLE_CAKE)
        .put(DyeColor.CYAN, Blocks.CYAN_CANDLE_CAKE)
        .put(DyeColor.LIGHT_BLUE, Blocks.LIGHT_BLUE_CANDLE_CAKE)
        .put(DyeColor.BLUE, Blocks.BLUE_CANDLE_CAKE)
        .put(DyeColor.PURPLE, Blocks.PURPLE_CANDLE_CAKE)
        .put(DyeColor.MAGENTA, Blocks.MAGENTA_CANDLE_CAKE)
        .put(DyeColor.PINK, Blocks.PINK_CANDLE_CAKE)
        .build();

    private static final Map<DyeColor, Block> DYE_TO_CARPET = new ImmutableMap.Builder<DyeColor, Block>()
        .put(DyeColor.WHITE, Blocks.WHITE_CARPET)
        .put(DyeColor.LIGHT_GRAY, Blocks.LIGHT_GRAY_CARPET)
        .put(DyeColor.GRAY, Blocks.GRAY_CARPET)
        .put(DyeColor.BLACK, Blocks.BLACK_CARPET)
        .put(DyeColor.BROWN, Blocks.BROWN_CARPET)
        .put(DyeColor.RED, Blocks.RED_CARPET)
        .put(DyeColor.ORANGE, Blocks.ORANGE_CARPET)
        .put(DyeColor.YELLOW, Blocks.YELLOW_CARPET)
        .put(DyeColor.LIME, Blocks.LIME_CARPET)
        .put(DyeColor.GREEN, Blocks.GREEN_CARPET)
        .put(DyeColor.CYAN, Blocks.CYAN_CARPET)
        .put(DyeColor.LIGHT_BLUE, Blocks.LIGHT_BLUE_CARPET)
        .put(DyeColor.BLUE, Blocks.BLUE_CARPET)
        .put(DyeColor.PURPLE, Blocks.PURPLE_CARPET)
        .put(DyeColor.MAGENTA, Blocks.MAGENTA_CARPET)
        .put(DyeColor.PINK, Blocks.PINK_CARPET)
        .build();

    private static final Map<DyeColor, Block> DYE_TO_LAMP = new ImmutableMap.Builder<DyeColor, Block>()
        .put(DyeColor.WHITE, DDBlocks.WHITE_LAMP)
        .put(DyeColor.LIGHT_GRAY, DDBlocks.LIGHT_GRAY_LAMP)
        .put(DyeColor.GRAY, DDBlocks.GRAY_LAMP)
        .put(DyeColor.BLACK, DDBlocks.BLACK_LAMP)
        .put(DyeColor.BROWN, DDBlocks.BROWN_LAMP)
        .put(DyeColor.RED, DDBlocks.RED_LAMP)
        .put(DyeColor.ORANGE, DDBlocks.ORANGE_LAMP)
        .put(DyeColor.YELLOW, DDBlocks.YELLOW_LAMP)
        .put(DyeColor.LIME, DDBlocks.LIME_LAMP)
        .put(DyeColor.GREEN, DDBlocks.GREEN_LAMP)
        .put(DyeColor.CYAN, DDBlocks.CYAN_LAMP)
        .put(DyeColor.LIGHT_BLUE, DDBlocks.LIGHT_BLUE_LAMP)
        .put(DyeColor.BLUE, DDBlocks.BLUE_LAMP)
        .put(DyeColor.PURPLE, DDBlocks.PURPLE_LAMP)
        .put(DyeColor.MAGENTA, DDBlocks.MAGENTA_LAMP)
        .put(DyeColor.PINK, DDBlocks.PINK_LAMP)
        .build();

    private static final Map<DyeColor, Block> DYE_TO_GLAZED_TERRACOTTA = new ImmutableMap.Builder<DyeColor, Block>()
        .put(DyeColor.WHITE, Blocks.WHITE_GLAZED_TERRACOTTA)
        .put(DyeColor.LIGHT_GRAY, Blocks.LIGHT_GRAY_GLAZED_TERRACOTTA)
        .put(DyeColor.GRAY, Blocks.GRAY_GLAZED_TERRACOTTA)
        .put(DyeColor.BLACK, Blocks.BLACK_GLAZED_TERRACOTTA)
        .put(DyeColor.BROWN, Blocks.BROWN_GLAZED_TERRACOTTA)
        .put(DyeColor.RED, Blocks.RED_GLAZED_TERRACOTTA)
        .put(DyeColor.ORANGE, Blocks.ORANGE_GLAZED_TERRACOTTA)
        .put(DyeColor.YELLOW, Blocks.YELLOW_GLAZED_TERRACOTTA)
        .put(DyeColor.LIME, Blocks.LIME_GLAZED_TERRACOTTA)
        .put(DyeColor.GREEN, Blocks.GREEN_GLAZED_TERRACOTTA)
        .put(DyeColor.CYAN, Blocks.CYAN_GLAZED_TERRACOTTA)
        .put(DyeColor.LIGHT_BLUE, Blocks.LIGHT_BLUE_GLAZED_TERRACOTTA)
        .put(DyeColor.BLUE, Blocks.BLUE_GLAZED_TERRACOTTA)
        .put(DyeColor.PURPLE, Blocks.PURPLE_GLAZED_TERRACOTTA)
        .put(DyeColor.MAGENTA, Blocks.MAGENTA_GLAZED_TERRACOTTA)
        .put(DyeColor.PINK, Blocks.PINK_GLAZED_TERRACOTTA)
        .build();

    private static final Map<DyeColor, Block> DYE_TO_BED = new ImmutableMap.Builder<DyeColor, Block>()
        .put(DyeColor.WHITE, Blocks.WHITE_BED)
        .put(DyeColor.LIGHT_GRAY, Blocks.LIGHT_GRAY_BED)
        .put(DyeColor.GRAY, Blocks.GRAY_BED)
        .put(DyeColor.BLACK, Blocks.BLACK_BED)
        .put(DyeColor.BROWN, Blocks.BROWN_BED)
        .put(DyeColor.RED, Blocks.RED_BED)
        .put(DyeColor.ORANGE, Blocks.ORANGE_BED)
        .put(DyeColor.YELLOW, Blocks.YELLOW_BED)
        .put(DyeColor.LIME, Blocks.LIME_BED)
        .put(DyeColor.GREEN, Blocks.GREEN_BED)
        .put(DyeColor.CYAN, Blocks.CYAN_BED)
        .put(DyeColor.LIGHT_BLUE, Blocks.LIGHT_BLUE_BED)
        .put(DyeColor.BLUE, Blocks.BLUE_BED)
        .put(DyeColor.PURPLE, Blocks.PURPLE_BED)
        .put(DyeColor.MAGENTA, Blocks.MAGENTA_BED)
        .put(DyeColor.PINK, Blocks.PINK_BED)
        .build();

    public static Optional<BlockState> getWoolForDyeColor(DyeColor color, BlockState state) {
        if (!state.isIn(BlockTags.WOOL)) return Optional.empty();
        Block woolBlock = DYE_TO_WOOL.get(color);
        if (state.getBlock() == woolBlock) return Optional.empty();
        return Optional.ofNullable(woolBlock).map(Block::getDefaultState);
    }

    public static Optional<BlockState> getStainedGlassForDyeColor(DyeColor color, BlockState state) {
        if (!state.isIn(DDBlockTags.GLASS)) return Optional.empty();
        Block glassBlock = DYE_TO_GLASS.get(color);
        if (state.getBlock() == glassBlock) return Optional.empty();
        return Optional.ofNullable(glassBlock).map(Block::getDefaultState);
    }

    public static Optional<BlockState> getStainedGlassPaneForDyeColor(DyeColor color, BlockState state) {
        if (!state.isIn(DDBlockTags.GLASS_PANES)) return Optional.empty();
        Block glassPane = DYE_TO_GLASS_PANE.get(color);
        if (state.getBlock() == glassPane) return Optional.empty();
        return Optional.ofNullable(glassPane).map(block -> block.getStateWithProperties(state));
    }

    public static Optional<BlockState> getConcreteForDyeColor(DyeColor color, BlockState state) {
        if (!state.isIn(DDBlockTags.CONCRETE)) return Optional.empty();
        Block conk = DYE_TO_CONCRETE.get(color);
        if (state.getBlock() == conk) return Optional.empty();
        return Optional.ofNullable(conk).map(Block::getDefaultState);
    }

    public static Optional<BlockState> getConcretePowderForDyeColor(DyeColor color, BlockState state) {
        if (!state.isIn(BlockTags.CONCRETE_POWDER)) return Optional.empty();
        Block powder = DYE_TO_CONCRETE_POWDER.get(color);
        if (state.getBlock() == powder) return Optional.empty();
        return Optional.ofNullable(powder).map(Block::getDefaultState);
    }

    public static Optional<BlockState> getTerracottaForDyeColor(DyeColor color, BlockState state) {
        if (!state.isIn(BlockTags.TERRACOTTA)) return Optional.empty();
        Block cotta = DYE_TO_TERRACOTTA.get(color);
        if (state.getBlock() == cotta) return Optional.empty();
        return Optional.ofNullable(cotta).map(Block::getDefaultState);
    }

    public static Optional<BlockState> getCandleForDyeColor(DyeColor color, BlockState state) {
        if (!state.isIn(BlockTags.CANDLES)) return Optional.empty();
        Block candle = DYE_TO_CANDLE.get(color);
        if (state.getBlock() == candle) return Optional.empty();
        return Optional.ofNullable(candle).map(block -> block.getStateWithProperties(state));
    }

    public static Optional<BlockState> getCandleCakeForDyeColor(DyeColor color, BlockState state) {
        if (!state.isIn(BlockTags.CANDLE_CAKES)) return Optional.empty();
        Block candle_cake = DYE_TO_CANDLE_CAKE.get(color);
        if (state.getBlock() == candle_cake) return Optional.empty();
        return Optional.ofNullable(candle_cake).map(block -> block.getStateWithProperties(state));
    }

    public static Optional<BlockState> getCarpetForDyeColor(DyeColor color, BlockState state) {
        if (!state.isIn(BlockTags.WOOL_CARPETS)) return Optional.empty();
        Block carpet = DYE_TO_CARPET.get(color);
        if (state.getBlock() == carpet) return Optional.empty();
        return Optional.ofNullable(carpet).map(Block::getDefaultState);
    }

    public static Optional<BlockState> getLampForDyeColor(DyeColor color, BlockState state) {
        if (!state.isIn(DDBlockTags.LAMPS)) return Optional.empty();
        Block lamp = DYE_TO_LAMP.get(color);
        if (state.getBlock() == lamp) return Optional.empty();
        return Optional.ofNullable(lamp).map(block -> block.getStateWithProperties(state));
    }

    public static Optional<BlockState> getGlazedTerracottaForDyeColor(DyeColor color, BlockState state) {
        if (!state.isIn(DDBlockTags.GLAZED_TERRACOTTA)) return Optional.empty();
        Block glazed = DYE_TO_GLAZED_TERRACOTTA.get(color);
        if (state.getBlock() == glazed) return Optional.empty();
        return Optional.ofNullable(glazed).map(block -> block.getStateWithProperties(state));
    }

    public static Optional<BlockState> getShulkerBoxForDyeColor(DyeColor color, BlockState state) {
        if (!state.isIn(BlockTags.SHULKER_BOXES) || state.getBlock() == ShulkerBoxBlock.get(color)) return Optional.empty();
        return Optional.ofNullable(ShulkerBoxBlock.get(color).getStateWithProperties(state));
    }

    public static void setShulker(World world, BlockPos pos, BlockState oldState) {
        BlockEntity oldShulkerEntity = world.getBlockEntity(pos);
        ShulkerBoxBlockEntity shulker = (ShulkerBoxBlockEntity) oldShulkerEntity;
        Text shulkerName = shulker.getCustomName();
        NbtCompound nbt = shulker.createNbt();
        world.removeBlockEntity(pos);
        world.removeBlock(pos, false);
        world.setBlockState(pos, oldState, Block.NOTIFY_ALL | Block.REDRAW_ON_MAIN_THREAD);
        world.updateListeners(pos, oldState, oldState, Block.NOTIFY_ALL | Block.REDRAW_ON_MAIN_THREAD);
        world.getBlockEntity(pos).readNbt(nbt);
        if (shulkerName != null) ((ShulkerBoxBlockEntity) world.getBlockEntity(pos)).setCustomName(shulkerName);
    }

    public static Optional<BlockState> getBedForDyeColor(DyeColor color, BlockState state) {
        Block bed = DYE_TO_BED.get(color);
        if (!state.isIn(BlockTags.BEDS) || state.getBlock().equals(bed)) return Optional.empty();
        return Optional.ofNullable(bed.getStateWithProperties(state));
    }

    public static void setBed(World world, BlockPos pos, BlockState state) {
        BedPart facingPart = state.get(Properties.BED_PART);
        BedPart connectedPart = facingPart == BedPart.FOOT ? BedPart.HEAD : BedPart.FOOT;
        Direction bedFacing = state.get(Properties.HORIZONTAL_FACING);
        BlockPos connected = pos.offset(BedBlock.getOppositePartDirection(state));
        world.setBlockState(connected, state.getBlock().getDefaultState().with(Properties.BED_PART, connectedPart).with(Properties.HORIZONTAL_FACING, bedFacing), Block.NOTIFY_ALL | Block.REDRAW_ON_MAIN_THREAD | Block.FORCE_STATE);
        world.setBlockState(pos, state, Block.NOTIFY_ALL | Block.REDRAW_ON_MAIN_THREAD | Block.FORCE_STATE);
        world.emitGameEvent(null, GameEvent.BLOCK_CHANGE, pos);
        world.emitGameEvent(null, GameEvent.BLOCK_CHANGE, connected);
    }

    public static Optional<SheepEntity> getDyeableEntityInBox(DyeColor color, ServerWorld world, BlockState state, BlockPos pos) {
        if (state.getBlock() != Blocks.AIR) return Optional.empty();
        SheepEntity e = null;
        List<SheepEntity> sheepList = world.getEntitiesByClass(SheepEntity.class, new Box(pos), EntityPredicates.VALID_LIVING_ENTITY.and((sheep)
                -> !((SheepEntity) sheep).isBaby()));
        if (!sheepList.isEmpty()) {
            e = sheepList.stream().filter(x -> x.getColor() != color).findFirst().orElse(null);
        }
        return Optional.ofNullable(e);
    }
}
