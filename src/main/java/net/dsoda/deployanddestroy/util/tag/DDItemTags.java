package net.dsoda.deployanddestroy.util.tag;

import net.dsoda.deployanddestroy.DeployAndDestroy;
import net.minecraft.item.Item;
import net.minecraft.registry.RegistryKeys;
import net.minecraft.registry.tag.TagKey;
import net.minecraft.util.Identifier;

public class DDItemTags {

    public static final TagKey<Item> FARMLAND_PLANTABLE = createTag("farmland_plantable");
    public static final TagKey<Item> AZALEA_LOGS = createTag("azalea_logs");
    public static final TagKey<Item> SWAMP_LOGS = createTag("swamp_logs");
    public static final TagKey<Item> DEFAULT_TORCHES = createTag("default_torches");
    public static final TagKey<Item> SOUL_TORCHES = createTag("soul_torches");
    public static final TagKey<Item> REDSTONE_TORCHES = createTag("redstone_torches");
    public static final TagKey<Item> COPPER_TORCHES = createTag("copper_torches");
    public static final TagKey<Item> AMETHYST_TORCHES = createTag("amethyst_torches");
    public static final TagKey<Item> MOSS_FOR_RECIPE = createTag("moss_for_recipe");
    public static final TagKey<Item> BURNABLE_LOG_COLUMNS = createTag("burnable_log_columns");
    public static final TagKey<Item> BURNABLE_PLANK_COLUMNS = createTag("burnable_plank_columns");

    public static final TagKey<Item> BARREL_UPGRADE_ITEMS = createTag("barrel_upgrade_items");

    private static TagKey<Item> createTag(String tag_id) {
        return TagKey.of(RegistryKeys.ITEM, new Identifier(DeployAndDestroy.MOD_ID, tag_id));
    }
}
