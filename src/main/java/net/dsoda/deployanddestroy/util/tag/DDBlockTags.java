package net.dsoda.deployanddestroy.util.tag;

import net.dsoda.deployanddestroy.DeployAndDestroy;
import net.minecraft.block.Block;
import net.minecraft.registry.RegistryKeys;
import net.minecraft.registry.tag.TagKey;
import net.minecraft.util.Identifier;

public class DDBlockTags {
    public static final TagKey<Block> CAN_BECOME_PATH = createTag("can_become_path");
    public static final TagKey<Block> CAN_BECOME_FARMLAND = createTag("can_become_farmland");
    public static final TagKey<Block> GLASS = createTag("glass");
    public static final TagKey<Block> GLASS_PANES = createTag("glass_panes");
    public static final TagKey<Block> CONCRETE = createTag("concrete");
    public static final TagKey<Block> LAMPS = createTag("lamps");
    public static final TagKey<Block> COLORED_LAMPS = createTag("colored_lamps");
    public static final TagKey<Block> GLAZED_TERRACOTTA = createTag("glazed_terracotta");
    public static final TagKey<Block> PISTON_NON_MOVABLE = createTag("piston_non_movable");
    public static final TagKey<Block> SHEARS_MINEABLE = createTag("shears_mineable");
    public static final TagKey<Block> COPPER_FIRE_BASE_BLOCKS = createTag("copper_fire_base_blocks");
    public static final TagKey<Block> AMETHYST_FIRE_BASE_BLOCKS = createTag("amethyst_fire_base_blocks");
    public static final TagKey<Block> DETECTOR_EXCLUDE = createTag("detector_exclude");

    private static TagKey<Block> createTag(String tag_id) {
        return TagKey.of(RegistryKeys.BLOCK, new Identifier(DeployAndDestroy.MOD_ID, tag_id));
    }
}
