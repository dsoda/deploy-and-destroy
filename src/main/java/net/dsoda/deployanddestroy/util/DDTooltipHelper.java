package net.dsoda.deployanddestroy.util;

import net.minecraft.text.Text;
import net.minecraft.util.DyeColor;

public class DDTooltipHelper {

    public static Text getTooltipText(DyeColor color) {
        return Text.literal(formatDyeColor(color)).setStyle(DDColorHelper.getStyleForDyeColor(color).withBold(true));
    }

    private static String formatDyeColor(DyeColor color) {
        String[] colorSplit = color.getName().split("_");
        StringBuilder r = new StringBuilder();
        for (String c : colorSplit) r.append(capitalize(c)).append(" ");
        return r.substring(0, r.length() - 1);
    }

    private static String capitalize(String s) {
        return s.substring(0, 1).toUpperCase() + s.substring(1);
    }

}
