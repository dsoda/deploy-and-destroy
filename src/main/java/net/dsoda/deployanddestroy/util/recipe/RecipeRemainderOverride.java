package net.dsoda.deployanddestroy.util.recipe;

import net.minecraft.item.Item;
import net.minecraft.item.Items;

import java.util.HashMap;

public class RecipeRemainderOverride {

    public static Item EMPTY = Items.AIR;

    public static boolean SHOULD_OVERRIDE;

    public static RecipeRemainderOverride OVERRIDE;

    private final Item output;

    private final HashMap<Item, Item> newRemainders;

    public RecipeRemainderOverride(Item output) {
        this.output = output;
        this.newRemainders = new HashMap<>();
        DDRecipeRemainderOverrides.OVERRIDES.add(this);
    }

    public Item getOutputItem() {
        return this.output;
    }

    public RecipeRemainderOverride add(Item itemWithRemainder, Item newRemainder) {
        this.newRemainders.put(itemWithRemainder, newRemainder);
        return this;
    }

    public Item getRemainderOverride(Item item) {
        return this.newRemainders.getOrDefault(item, item.hasRecipeRemainder() ? item.getRecipeRemainder() : Items.AIR);
    }
}
