package net.dsoda.deployanddestroy.util.recipe;

import com.google.common.collect.ImmutableMap;
import net.dsoda.deployanddestroy.blocks.DDBlocks;
import net.minecraft.block.Block;
import net.minecraft.item.Item;
import net.minecraft.item.Items;

import java.util.List;
import java.util.Map;

public class DDRecipeHelper {

    public static final List<Block> COLORED_LAMPS = List.of(
        DDBlocks.WHITE_LAMP
    ,   DDBlocks.LIGHT_GRAY_LAMP
    ,   DDBlocks.GRAY_LAMP
    ,   DDBlocks.BLACK_LAMP
    ,   DDBlocks.BROWN_LAMP
    ,   DDBlocks.RED_LAMP
    ,   DDBlocks.ORANGE_LAMP
    ,   DDBlocks.YELLOW_LAMP
    ,   DDBlocks.LIME_LAMP
    ,   DDBlocks.GREEN_LAMP
    ,   DDBlocks.CYAN_LAMP
    ,   DDBlocks.LIGHT_BLUE_LAMP
    ,   DDBlocks.BLUE_LAMP
    ,   DDBlocks.PURPLE_LAMP
    ,   DDBlocks.MAGENTA_LAMP
    ,   DDBlocks.PINK_LAMP
    );

    public static final List<Item> LAMPS = List.of(
        DDBlocks.WHITE_LAMP.asItem()
    ,   DDBlocks.LIGHT_GRAY_LAMP.asItem()
    ,   DDBlocks.GRAY_LAMP.asItem()
    ,   DDBlocks.BLACK_LAMP.asItem()
    ,   DDBlocks.BROWN_LAMP.asItem()
    ,   DDBlocks.RED_LAMP.asItem()
    ,   DDBlocks.ORANGE_LAMP.asItem()
    ,   DDBlocks.YELLOW_LAMP.asItem()
    ,   DDBlocks.LIME_LAMP.asItem()
    ,   DDBlocks.GREEN_LAMP.asItem()
    ,   DDBlocks.CYAN_LAMP.asItem()
    ,   DDBlocks.LIGHT_BLUE_LAMP.asItem()
    ,   DDBlocks.BLUE_LAMP.asItem()
    ,   DDBlocks.PURPLE_LAMP.asItem()
    ,   DDBlocks.MAGENTA_LAMP.asItem()
    ,   DDBlocks.PINK_LAMP.asItem()
    ,   Items.REDSTONE_LAMP
    );

    public static final List<Item> DYES = List.of(
        Items.WHITE_DYE
    ,   Items.LIGHT_GRAY_DYE
    ,   Items.GRAY_DYE
    ,   Items.BLACK_DYE
    ,   Items.BROWN_DYE
    ,   Items.RED_DYE
    ,   Items.ORANGE_DYE
    ,   Items.YELLOW_DYE
    ,   Items.LIME_DYE
    ,   Items.GREEN_DYE
    ,   Items.CYAN_DYE
    ,   Items.LIGHT_BLUE_DYE
    ,   Items.BLUE_DYE
    ,   Items.PURPLE_DYE
    ,   Items.MAGENTA_DYE
    ,   Items.PINK_DYE
    );

    public static final Map<Block, Item> DYE_ITEM_FOR_LAMP = new ImmutableMap.Builder<Block, Item>()
        .put(DDBlocks.WHITE_LAMP, Items.WHITE_DYE)
        .put(DDBlocks.LIGHT_GRAY_LAMP, Items.LIGHT_GRAY_DYE)
        .put(DDBlocks.GRAY_LAMP, Items.GRAY_DYE)
        .put(DDBlocks.BLACK_LAMP, Items.BLACK_DYE)
        .put(DDBlocks.BROWN_LAMP, Items.BROWN_DYE)
        .put(DDBlocks.RED_LAMP, Items.RED_DYE)
        .put(DDBlocks.ORANGE_LAMP, Items.ORANGE_DYE)
        .put(DDBlocks.YELLOW_LAMP, Items.YELLOW_DYE)
        .put(DDBlocks.LIME_LAMP, Items.LIME_DYE)
        .put(DDBlocks.GREEN_LAMP, Items.GREEN_DYE)
        .put(DDBlocks.CYAN_LAMP, Items.CYAN_DYE)
        .put(DDBlocks.LIGHT_BLUE_LAMP, Items.LIGHT_BLUE_DYE)
        .put(DDBlocks.BLUE_LAMP, Items.BLUE_DYE)
        .put(DDBlocks.PURPLE_LAMP, Items.PURPLE_DYE)
        .put(DDBlocks.MAGENTA_LAMP, Items.MAGENTA_DYE)
        .put(DDBlocks.PINK_LAMP, Items.PINK_DYE)
        .build();
}
