package net.dsoda.deployanddestroy.util.recipe;

import net.dsoda.deployanddestroy.DeployAndDestroy;

import java.util.ArrayList;
import java.util.List;

import static net.dsoda.deployanddestroy.DeployAndDestroy.MOD_ID;

public class DDRecipeRemainderOverrides {

    public static List<RecipeRemainderOverride> OVERRIDES = new ArrayList<>();

    //public static final RecipeRemainderOverride CHOCOLATE_MILK_BUCKET = new RecipeRemainderOverride(DDItems.CHOCOLATE_MILK).add(Items.MILK_BUCKET, RecipeRemainderOverride.EMPTY);

    public static void initRecipeRemainderOverrides() {
        DeployAndDestroy.LOGGER.info("Overriding recipe remainders for " + MOD_ID);
    }
}
