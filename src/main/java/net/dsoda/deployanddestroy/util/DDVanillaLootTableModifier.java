package net.dsoda.deployanddestroy.util;

import net.dsoda.deployanddestroy.items.DDItems;
import net.fabricmc.fabric.api.loot.v2.LootTableEvents;
import net.minecraft.enchantment.Enchantments;
import net.minecraft.entity.EntityType;
import net.minecraft.loot.LootPool;
import net.minecraft.loot.entry.ItemEntry;
import net.minecraft.loot.function.ApplyBonusLootFunction;
import net.minecraft.loot.function.SetCountLootFunction;
import net.minecraft.loot.provider.number.UniformLootNumberProvider;

public class DDVanillaLootTableModifier {

    public static void modifyLootTables() {
        LootTableEvents.MODIFY.register((resourceManager, lootManager, id, tableBuilder, source) -> {
            if (source.isBuiltin() && EntityType.HUSK.getLootTableId().equals(id)) {
                LootPool.Builder poolBuilder = LootPool.builder()
                    .with(ItemEntry.builder(DDItems.SAND_PILE))
                    .apply(SetCountLootFunction.builder(UniformLootNumberProvider.create(0f, 1f)))
                    .apply(ApplyBonusLootFunction.uniformBonusCount(Enchantments.LOOTING));
                tableBuilder.pool(poolBuilder);
            }
        });

        LootTableEvents.MODIFY.register((resourceManager, lootManager, id, tableBuilder, source) -> {
            if (source.isBuiltin() && EntityType.STRAY.getLootTableId().equals(id)) {
                LootPool.Builder poolBuilder = LootPool.builder()
                    .with(ItemEntry.builder(DDItems.ICE_SHARD))
                    .apply(SetCountLootFunction.builder(UniformLootNumberProvider.create(0f, 1f)))
                    .apply(ApplyBonusLootFunction.uniformBonusCount(Enchantments.LOOTING));
                tableBuilder.pool(poolBuilder);
            }
        });
    }
}
