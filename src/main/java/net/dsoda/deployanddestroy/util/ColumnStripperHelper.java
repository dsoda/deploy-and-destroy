package net.dsoda.deployanddestroy.util;

import com.google.common.collect.ImmutableMap;
import net.dsoda.deployanddestroy.blocks.DDBlocks;
import net.minecraft.block.Block;

import java.util.Map;
import java.util.Optional;

public class ColumnStripperHelper {

    private static final Map<Block, Block> UNSTRIPPED_TO_STRIPPED_COLUMN = new ImmutableMap.Builder<Block, Block>()
        .put(DDBlocks.OAK_LOG_COLUMN, DDBlocks.STRIPPED_OAK_LOG_COLUMN)
        .put(DDBlocks.OAK_WOOD_COLUMN, DDBlocks.STRIPPED_OAK_WOOD_COLUMN)
        .put(DDBlocks.SPRUCE_LOG_COLUMN, DDBlocks.STRIPPED_SPRUCE_LOG_COLUMN)
        .put(DDBlocks.SPRUCE_WOOD_COLUMN, DDBlocks.STRIPPED_SPRUCE_WOOD_COLUMN)
        .put(DDBlocks.BIRCH_LOG_COLUMN, DDBlocks.STRIPPED_BIRCH_LOG_COLUMN)
        .put(DDBlocks.BIRCH_WOOD_COLUMN, DDBlocks.STRIPPED_BIRCH_WOOD_COLUMN)
        .put(DDBlocks.JUNGLE_LOG_COLUMN, DDBlocks.STRIPPED_JUNGLE_LOG_COLUMN)
        .put(DDBlocks.JUNGLE_WOOD_COLUMN, DDBlocks.STRIPPED_JUNGLE_WOOD_COLUMN)
        .put(DDBlocks.ACACIA_LOG_COLUMN, DDBlocks.STRIPPED_ACACIA_LOG_COLUMN)
        .put(DDBlocks.ACACIA_WOOD_COLUMN, DDBlocks.STRIPPED_ACACIA_WOOD_COLUMN)
        .put(DDBlocks.DARK_OAK_LOG_COLUMN, DDBlocks.STRIPPED_DARK_OAK_LOG_COLUMN)
        .put(DDBlocks.DARK_OAK_WOOD_COLUMN, DDBlocks.STRIPPED_DARK_OAK_WOOD_COLUMN)
        .put(DDBlocks.MANGROVE_LOG_COLUMN, DDBlocks.STRIPPED_MANGROVE_LOG_COLUMN)
        .put(DDBlocks.MANGROVE_WOOD_COLUMN, DDBlocks.STRIPPED_MANGROVE_WOOD_COLUMN)
        .put(DDBlocks.CHERRY_LOG_COLUMN, DDBlocks.STRIPPED_CHERRY_LOG_COLUMN)
        .put(DDBlocks.CHERRY_WOOD_COLUMN, DDBlocks.STRIPPED_CHERRY_WOOD_COLUMN)
        .put(DDBlocks.AZALEA_LOG_COLUMN, DDBlocks.STRIPPED_AZALEA_LOG_COLUMN)
        .put(DDBlocks.AZALEA_WOOD_COLUMN, DDBlocks.STRIPPED_AZALEA_WOOD_COLUMN)
        .put(DDBlocks.SWAMP_LOG_COLUMN, DDBlocks.STRIPPED_SWAMP_LOG_COLUMN)
        .put(DDBlocks.SWAMP_WOOD_COLUMN, DDBlocks.STRIPPED_SWAMP_WOOD_COLUMN)
        .put(DDBlocks.BAMBOO_BLOCK_COLUMN, DDBlocks.STRIPPED_BAMBOO_BLOCK_COLUMN)
        .put(DDBlocks.CRIMSON_STEM_COLUMN, DDBlocks.STRIPPED_CRIMSON_STEM_COLUMN)
        .put(DDBlocks.CRIMSON_HYPHAE_COLUMN, DDBlocks.STRIPPED_CRIMSON_HYPHAE_COLUMN)
        .put(DDBlocks.WARPED_STEM_COLUMN, DDBlocks.STRIPPED_WARPED_STEM_COLUMN)
        .put(DDBlocks.WARPED_HYPHAE_COLUMN, DDBlocks.STRIPPED_WARPED_HYPHAE_COLUMN)
        .build();

    public static Optional<Block> getStrippedColumn(Block unstripped) {
        return Optional.ofNullable(UNSTRIPPED_TO_STRIPPED_COLUMN.get(unstripped));
    }
}
