package net.dsoda.deployanddestroy.util;

import com.google.common.collect.ImmutableMap;
import net.dsoda.deployanddestroy.blocks.block.ColoredLampBlock;
import net.minecraft.block.*;
import net.minecraft.block.entity.SignBlockEntity;
import net.minecraft.block.entity.SignText;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.text.Style;
import net.minecraft.util.DyeColor;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

import java.util.Map;
import java.util.Optional;

public class DDColorHelper {

    public static final Style WHITE = Style.EMPTY.withColor(0x16FFFFFF);
    public static final Style LIGHT_GRAY = Style.EMPTY.withColor(0x16C8C8C8);
    public static final Style GRAY = Style.EMPTY.withColor(0x16848484);
    public static final Style BLACK = Style.EMPTY.withColor(0x16100D14);
    public static final Style BROWN = Style.EMPTY.withColor(0x16632C04);
    public static final Style RED = Style.EMPTY.withColor(0x16D2443F);
    public static final Style ORANGE = Style.EMPTY.withColor(0x16DB8B2A);
    public static final Style YELLOW = Style.EMPTY.withColor(0x16E7E72A);
    public static final Style LIME = Style.EMPTY.withColor(0x1676C610);
    public static final Style GREEN = Style.EMPTY.withColor(0x164A6B18);
    public static final Style CYAN = Style.EMPTY.withColor(0x16236F8F);
    public static final Style LIGHT_BLUE = Style.EMPTY.withColor(0x166F9BDB);
    public static final Style BLUE = Style.EMPTY.withColor(0x16345EC3);
    public static final Style PURPLE = Style.EMPTY.withColor(0x168C39BC);
    public static final Style MAGENTA = Style.EMPTY.withColor(0x16BE5CB8);
    public static final Style PINK = Style.EMPTY.withColor(0x16DB8BB4);


    private static final Map<DyeColor, Style> TEXT_STYLE_FOR_DYE_COLOR = new ImmutableMap.Builder<DyeColor, Style>()
        .put(DyeColor.WHITE, WHITE).put(DyeColor.LIGHT_GRAY, LIGHT_GRAY).put(DyeColor.GRAY, GRAY)
        .put(DyeColor.BLACK, BLACK).put(DyeColor.BROWN, BROWN).put(DyeColor.RED, RED).put(DyeColor.ORANGE, ORANGE)
        .put(DyeColor.YELLOW, YELLOW).put(DyeColor.LIME, LIME).put(DyeColor.GREEN, GREEN).put(DyeColor.CYAN, CYAN)
        .put(DyeColor.LIGHT_BLUE, LIGHT_BLUE).put(DyeColor.BLUE, BLUE).put(DyeColor.PURPLE, PURPLE)
        .put(DyeColor.MAGENTA, MAGENTA).put(DyeColor.PINK, PINK).build();

    private static final Map<Block, DyeColor> COLOR_FROM_WOOL = new ImmutableMap.Builder<Block, DyeColor>()
        .put(Blocks.WHITE_WOOL, DyeColor.WHITE).put(Blocks.LIGHT_GRAY_WOOL, DyeColor.LIGHT_GRAY).put(Blocks.GRAY_WOOL, DyeColor.GRAY)
        .put(Blocks.BLACK_WOOL, DyeColor.BLACK).put(Blocks.BROWN_WOOL, DyeColor.BROWN).put(Blocks.RED_WOOL, DyeColor.RED)
        .put(Blocks.ORANGE_WOOL, DyeColor.ORANGE).put(Blocks.YELLOW_WOOL, DyeColor.YELLOW).put(Blocks.LIME_WOOL, DyeColor.LIME)
        .put(Blocks.GREEN_WOOL, DyeColor.GREEN).put(Blocks.CYAN_WOOL, DyeColor.CYAN).put(Blocks.LIGHT_BLUE_WOOL, DyeColor.LIGHT_BLUE)
        .put(Blocks.BLUE_WOOL, DyeColor.BLUE).put(Blocks.PURPLE_WOOL, DyeColor.PURPLE).put(Blocks.MAGENTA_WOOL, DyeColor.MAGENTA)
        .put(Blocks.PINK_WOOL, DyeColor.PINK).build();

    private static final Map<Block, DyeColor> COLOR_FROM_TERRACOTTA = new ImmutableMap.Builder<Block, DyeColor>()
        .put(Blocks.WHITE_TERRACOTTA, DyeColor.WHITE).put(Blocks.LIGHT_GRAY_TERRACOTTA, DyeColor.LIGHT_GRAY).put(Blocks.GRAY_TERRACOTTA, DyeColor.GRAY)
        .put(Blocks.BLACK_TERRACOTTA, DyeColor.BLACK).put(Blocks.BROWN_TERRACOTTA, DyeColor.BROWN).put(Blocks.RED_TERRACOTTA, DyeColor.RED)
        .put(Blocks.ORANGE_TERRACOTTA, DyeColor.ORANGE).put(Blocks.YELLOW_TERRACOTTA, DyeColor.YELLOW).put(Blocks.LIME_TERRACOTTA, DyeColor.LIME)
        .put(Blocks.GREEN_TERRACOTTA, DyeColor.GREEN).put(Blocks.CYAN_TERRACOTTA, DyeColor.CYAN).put(Blocks.LIGHT_BLUE_TERRACOTTA, DyeColor.LIGHT_BLUE)
        .put(Blocks.BLUE_TERRACOTTA, DyeColor.BLUE).put(Blocks.PURPLE_TERRACOTTA, DyeColor.PURPLE).put(Blocks.MAGENTA_TERRACOTTA, DyeColor.MAGENTA)
        .put(Blocks.PINK_TERRACOTTA, DyeColor.PINK).build();

    private static final Map<Block, DyeColor> COLOR_FROM_GLAZED_TERRACOTTA = new ImmutableMap.Builder<Block, DyeColor>()
        .put(Blocks.WHITE_GLAZED_TERRACOTTA, DyeColor.WHITE).put(Blocks.LIGHT_GRAY_GLAZED_TERRACOTTA, DyeColor.LIGHT_GRAY).put(Blocks.GRAY_GLAZED_TERRACOTTA, DyeColor.GRAY)
        .put(Blocks.BLACK_GLAZED_TERRACOTTA, DyeColor.BLACK).put(Blocks.BROWN_GLAZED_TERRACOTTA, DyeColor.BROWN).put(Blocks.RED_GLAZED_TERRACOTTA, DyeColor.RED)
        .put(Blocks.ORANGE_GLAZED_TERRACOTTA, DyeColor.ORANGE).put(Blocks.YELLOW_GLAZED_TERRACOTTA, DyeColor.YELLOW).put(Blocks.LIME_GLAZED_TERRACOTTA, DyeColor.LIME)
        .put(Blocks.GREEN_GLAZED_TERRACOTTA, DyeColor.GREEN).put(Blocks.CYAN_GLAZED_TERRACOTTA, DyeColor.CYAN).put(Blocks.LIGHT_BLUE_GLAZED_TERRACOTTA, DyeColor.LIGHT_BLUE)
        .put(Blocks.BLUE_GLAZED_TERRACOTTA, DyeColor.BLUE).put(Blocks.PURPLE_GLAZED_TERRACOTTA, DyeColor.PURPLE).put(Blocks.MAGENTA_GLAZED_TERRACOTTA, DyeColor.MAGENTA)
        .put(Blocks.PINK_GLAZED_TERRACOTTA, DyeColor.PINK).build();

    private static final Map<Block, DyeColor> COLOR_FROM_CONCRETE = new ImmutableMap.Builder<Block, DyeColor>()
        .put(Blocks.WHITE_CONCRETE, DyeColor.WHITE).put(Blocks.LIGHT_GRAY_CONCRETE, DyeColor.LIGHT_GRAY).put(Blocks.GRAY_CONCRETE, DyeColor.GRAY)
        .put(Blocks.BLACK_CONCRETE, DyeColor.BLACK).put(Blocks.BROWN_CONCRETE, DyeColor.BROWN).put(Blocks.RED_CONCRETE, DyeColor.RED)
        .put(Blocks.ORANGE_CONCRETE, DyeColor.ORANGE).put(Blocks.YELLOW_CONCRETE, DyeColor.YELLOW).put(Blocks.LIME_CONCRETE, DyeColor.LIME)
        .put(Blocks.GREEN_CONCRETE, DyeColor.GREEN).put(Blocks.CYAN_CONCRETE, DyeColor.CYAN).put(Blocks.LIGHT_BLUE_CONCRETE, DyeColor.LIGHT_BLUE)
        .put(Blocks.BLUE_CONCRETE, DyeColor.BLUE).put(Blocks.PURPLE_CONCRETE, DyeColor.PURPLE).put(Blocks.MAGENTA_CONCRETE, DyeColor.MAGENTA)
        .put(Blocks.PINK_CONCRETE, DyeColor.PINK).build();

    private static final Map<Block, DyeColor> COLOR_FROM_CONCRETE_POWDER = new ImmutableMap.Builder<Block, DyeColor>()
        .put(Blocks.WHITE_CONCRETE_POWDER, DyeColor.WHITE).put(Blocks.LIGHT_GRAY_CONCRETE_POWDER, DyeColor.LIGHT_GRAY).put(Blocks.GRAY_CONCRETE_POWDER, DyeColor.GRAY)
        .put(Blocks.BLACK_CONCRETE_POWDER, DyeColor.BLACK).put(Blocks.BROWN_CONCRETE_POWDER, DyeColor.BROWN).put(Blocks.RED_CONCRETE_POWDER, DyeColor.RED)
        .put(Blocks.ORANGE_CONCRETE_POWDER, DyeColor.ORANGE).put(Blocks.YELLOW_CONCRETE_POWDER, DyeColor.YELLOW).put(Blocks.LIME_CONCRETE_POWDER, DyeColor.LIME)
        .put(Blocks.GREEN_CONCRETE_POWDER, DyeColor.GREEN).put(Blocks.CYAN_CONCRETE_POWDER, DyeColor.CYAN).put(Blocks.LIGHT_BLUE_CONCRETE_POWDER, DyeColor.LIGHT_BLUE)
        .put(Blocks.BLUE_CONCRETE_POWDER, DyeColor.BLUE).put(Blocks.PURPLE_CONCRETE_POWDER, DyeColor.PURPLE).put(Blocks.MAGENTA_CONCRETE_POWDER, DyeColor.MAGENTA)
        .put(Blocks.PINK_CONCRETE_POWDER, DyeColor.PINK).build();

    private static final Map<Block, DyeColor> COLOR_FROM_CANDLE = new ImmutableMap.Builder<Block, DyeColor>()
        .put(Blocks.WHITE_CANDLE, DyeColor.WHITE).put(Blocks.LIGHT_GRAY_CANDLE, DyeColor.LIGHT_GRAY).put(Blocks.GRAY_CANDLE, DyeColor.GRAY)
        .put(Blocks.BLACK_CANDLE, DyeColor.BLACK).put(Blocks.BROWN_CANDLE, DyeColor.BROWN).put(Blocks.RED_CANDLE, DyeColor.RED)
        .put(Blocks.ORANGE_CANDLE, DyeColor.ORANGE).put(Blocks.YELLOW_CANDLE, DyeColor.YELLOW).put(Blocks.LIME_CANDLE, DyeColor.LIME)
        .put(Blocks.GREEN_CANDLE, DyeColor.GREEN).put(Blocks.CYAN_CANDLE, DyeColor.CYAN).put(Blocks.LIGHT_BLUE_CANDLE, DyeColor.LIGHT_BLUE)
        .put(Blocks.BLUE_CANDLE, DyeColor.BLUE).put(Blocks.PURPLE_CANDLE, DyeColor.PURPLE).put(Blocks.MAGENTA_CANDLE, DyeColor.MAGENTA)
        .put(Blocks.PINK_CANDLE, DyeColor.PINK).build();

    private static final Map<Block, DyeColor> COLOR_FROM_CANDLE_CAKE = new ImmutableMap.Builder<Block, DyeColor>()
        .put(Blocks.WHITE_CANDLE_CAKE, DyeColor.WHITE).put(Blocks.LIGHT_GRAY_CANDLE_CAKE, DyeColor.LIGHT_GRAY).put(Blocks.GRAY_CANDLE_CAKE, DyeColor.GRAY)
        .put(Blocks.BLACK_CANDLE_CAKE, DyeColor.BLACK).put(Blocks.BROWN_CANDLE_CAKE, DyeColor.BROWN).put(Blocks.RED_CANDLE_CAKE, DyeColor.RED)
        .put(Blocks.ORANGE_CANDLE_CAKE, DyeColor.ORANGE).put(Blocks.YELLOW_CANDLE_CAKE, DyeColor.YELLOW).put(Blocks.LIME_CANDLE_CAKE, DyeColor.LIME)
        .put(Blocks.GREEN_CANDLE_CAKE, DyeColor.GREEN).put(Blocks.CYAN_CANDLE_CAKE, DyeColor.CYAN).put(Blocks.LIGHT_BLUE_CANDLE_CAKE, DyeColor.LIGHT_BLUE)
        .put(Blocks.BLUE_CANDLE_CAKE, DyeColor.BLUE).put(Blocks.PURPLE_CANDLE_CAKE, DyeColor.PURPLE).put(Blocks.MAGENTA_CANDLE_CAKE, DyeColor.MAGENTA)
        .put(Blocks.PINK_CANDLE_CAKE, DyeColor.PINK).build();

    public static Style getStyleForDyeColor(DyeColor color) {
        return TEXT_STYLE_FOR_DYE_COLOR.get(color);
    }

    public static Optional<DyeColor> getColorFromWool(Block block) {
        return Optional.ofNullable(COLOR_FROM_WOOL.get(block));
    }

    public static Optional<DyeColor> getColorFromCarpet(Block block) {
        return (block instanceof DyedCarpetBlock) ? Optional.ofNullable(((DyedCarpetBlock) block).getDyeColor()) : Optional.empty();
    }

    public static Optional<DyeColor> getColorFromTerracotta(Block block) {
        return Optional.ofNullable(COLOR_FROM_TERRACOTTA.get(block));
    }

    public static Optional<DyeColor> getColorFromGlazedTerracotta(Block block) {
        return Optional.ofNullable(COLOR_FROM_GLAZED_TERRACOTTA.get(block));
    }

    public static Optional<DyeColor> getColorFromConcrete(Block block) {
        return Optional.ofNullable(COLOR_FROM_CONCRETE.get(block));
    }

    public static Optional<DyeColor> getColorFromConcretePowder(Block block) {
        return Optional.ofNullable(COLOR_FROM_CONCRETE_POWDER.get(block));
    }

    public static Optional<DyeColor> getColorFromStainedGlass(Block block) {
        return (block instanceof StainedGlassBlock) ? Optional.ofNullable(((StainedGlassBlock) block).getColor()) : Optional.empty();
    }

    public static Optional<DyeColor> getColorFromStainedGlassPane(Block block) {
        return (block instanceof StainedGlassPaneBlock) ? Optional.ofNullable(((StainedGlassPaneBlock) block).getColor()) : Optional.empty();
    }

    public static Optional<DyeColor> getColorFromShulkerBox(Block block) {
        return (block instanceof ShulkerBoxBlock) ? Optional.ofNullable(((ShulkerBoxBlock) block).getColor()) : Optional.empty();
    }

    public static Optional<DyeColor> getColorFromBed(Block block) {
        return (block instanceof BedBlock) ? Optional.ofNullable(((BedBlock) block).getColor()) : Optional.empty();
    }

    public static Optional<DyeColor> getColorFromCandle(Block block) {
        return Optional.ofNullable(COLOR_FROM_CANDLE.get(block));
    }

    public static Optional<DyeColor> getColorFromCandleCake(Block block) {
        return Optional.ofNullable(COLOR_FROM_CANDLE_CAKE.get(block));
    }

    public static Optional<DyeColor> getColorFromLamp(Block block) {
        return (block instanceof ColoredLampBlock) ? Optional.ofNullable(((ColoredLampBlock) block).getColor()) : Optional.empty();
    }

    public static Optional<DyeColor> getColorFromBanner(Block block) {
        return (block instanceof AbstractBannerBlock) ? Optional.ofNullable(((AbstractBannerBlock) block).getColor()) : Optional.empty();
    }

    public static Optional<DyeColor> getColorFromSign(World world, PlayerEntity player, BlockPos pos) {
        if (!(world.getBlockState(pos).getBlock() instanceof AbstractSignBlock)) return Optional.empty();
        SignBlockEntity sign = (SignBlockEntity) world.getBlockEntity(pos);
        return sign != null ? Optional.ofNullable(colorFromSignText(sign, player)) : Optional.empty();
    }

    private static DyeColor colorFromSignText(SignBlockEntity sign, PlayerEntity player) {
        SignText facingText = sign.isPlayerFacingFront(player) ? sign.getFrontText() : sign.getBackText();
        return facingText.getColor();
    }
}
