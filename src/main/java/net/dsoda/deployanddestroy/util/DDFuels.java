package net.dsoda.deployanddestroy.util;

import net.dsoda.deployanddestroy.blocks.DDBlocks;
import net.dsoda.deployanddestroy.items.DDItems;
import net.dsoda.deployanddestroy.util.tag.DDItemTags;
import net.fabricmc.fabric.api.registry.FuelRegistry;

public class DDFuels {

    public static void registerFuels() {
        FuelRegistry.INSTANCE.add(DDBlocks.CHARCOAL_BLOCK, 16000);
        FuelRegistry.INSTANCE.add(DDItems.BLAZING_COAL, 3200);
        FuelRegistry.INSTANCE.add(DDBlocks.BLAZING_COAL_BLOCK, 64000);
        FuelRegistry.INSTANCE.add(DDItemTags.BURNABLE_LOG_COLUMNS, 300);
        FuelRegistry.INSTANCE.add(DDItemTags.BURNABLE_PLANK_COLUMNS, 300);
    }
}
