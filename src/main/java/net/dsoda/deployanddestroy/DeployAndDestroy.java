package net.dsoda.deployanddestroy;

import net.dsoda.deployanddestroy.blocks.DDBlockEntities;
import net.dsoda.deployanddestroy.blocks.DDBlockFamilies;
import net.dsoda.deployanddestroy.blocks.DDBlocks;
import net.dsoda.deployanddestroy.entity.DDBoats;
import net.dsoda.deployanddestroy.entity.DDTrades;
import net.dsoda.deployanddestroy.particle.DDParticles;
import net.dsoda.deployanddestroy.recipe.DDRecipes;
import net.dsoda.deployanddestroy.sound.DDSounds;
import net.dsoda.deployanddestroy.stats.DDStats;
import net.dsoda.deployanddestroy.util.*;
import net.dsoda.deployanddestroy.items.DDItemGroups;
import net.dsoda.deployanddestroy.items.DDItems;
import net.dsoda.deployanddestroy.events.DDUseBlock;
import net.dsoda.deployanddestroy.client.screens.DDScreenHandlers;
import net.dsoda.deployanddestroy.util.recipe.DDRecipeRemainderOverrides;
import net.dsoda.deployanddestroy.worldgen.DDOreGeneration;
import net.dsoda.deployanddestroy.worldgen.DDVillagerHouses;
import net.fabricmc.api.ModInitializer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DeployAndDestroy implements ModInitializer {

	public static final String MOD_ID = "deployanddestroy";
	public static final Logger LOGGER = LoggerFactory.getLogger(MOD_ID);

	@Override
	public void onInitialize() {
		DDVillagerHouses.generateVillagerHouses();
		DDParticles.registerParticles();
		DDItems.registerItems();
		DDBlocks.registerBlocks();
		DDBlockFamilies.registerBlockFamilies();
		DDBlockEntities.registerBlockEntities();
		DDUseBlock.registerUseBlockEvents();
		DDItemGroups.registerItemGroups();
		DDBoats.registerBoats();
		DDFuels.registerFuels();
		DDScreenHandlers.registerScreenHandlers();
		DDFoods.registerFoodComponents();
		DDRecipeRemainderOverrides.initRecipeRemainderOverrides();
		DDMaxStackSizeOverrides.initMaxStackSizeOverrides();
		DDVanillaLootTableModifier.modifyLootTables();
		DDOreGeneration.generateOres();
		DDRecipes.registerRecipes();
		DDTrades.registerTrades();
		DDStats.registerStats();
		DDSounds.registerSounds();
	}
}