package net.dsoda.deployanddestroy.datagen;

import net.dsoda.deployanddestroy.blocks.DDBlocks;
import net.dsoda.deployanddestroy.items.DDItems;
import net.dsoda.deployanddestroy.recipe.WoodcuttingRecipe;
import net.dsoda.deployanddestroy.util.recipe.DDRecipeHelper;
import net.dsoda.deployanddestroy.util.tag.DDItemTags;
import net.fabricmc.fabric.api.datagen.v1.FabricDataOutput;
import net.fabricmc.fabric.api.datagen.v1.provider.FabricRecipeProvider;
import net.minecraft.block.Blocks;
import net.minecraft.data.server.recipe.*;
import net.minecraft.item.Item;
import net.minecraft.item.ItemConvertible;
import net.minecraft.item.Items;
import net.minecraft.recipe.Ingredient;
import net.minecraft.recipe.book.RecipeCategory;
import net.minecraft.registry.tag.ItemTags;
import net.minecraft.util.Identifier;

import java.util.List;

public class DDRecipeProvider extends FabricRecipeProvider {
    public DDRecipeProvider(FabricDataOutput dataOutput) {
        super(dataOutput);
    }

    @Override
    public void generate(RecipeExporter exporter) {
        generateShapelessRecipes(exporter);
        generateShapedRecipes(exporter);
        generateSmeltingRecipes(exporter);
        generateBlastSmeltingRecipes(exporter);
        generateStonecuttingRecipes(exporter);
        generateWoodcuttingRecipes(exporter);
        generateSmithingRecipes(exporter);
        generateTorchRecipes(exporter);
        generateLanternRecipes(exporter);
        generateCampfireRecipes(exporter);
        generateVanillaItemRecipesFromModItems(exporter);
    }

    private static void generateShapelessRecipes(RecipeExporter exporter) {
        RecipeProvider.offerPlanksRecipe(exporter, DDBlocks.AZALEA_PLANKS, DDItemTags.AZALEA_LOGS, 4);
        RecipeProvider.offerPlanksRecipe(exporter, DDBlocks.SWAMP_PLANKS, DDItemTags.SWAMP_LOGS, 4);

        ShapelessRecipeJsonBuilder.create(RecipeCategory.REDSTONE, DDBlocks.AZALEA_BUTTON, 1)
            .input(DDBlocks.AZALEA_PLANKS)
            .criterion(hasItem(DDBlocks.AZALEA_PLANKS), conditionsFromItem(DDBlocks.AZALEA_PLANKS))
            .offerTo(exporter, new Identifier(getRecipeName(DDBlocks.AZALEA_BUTTON)));

        ShapelessRecipeJsonBuilder.create(RecipeCategory.REDSTONE, DDBlocks.SWAMP_BUTTON, 1)
            .input(DDBlocks.SWAMP_PLANKS)
            .criterion(hasItem(DDBlocks.SWAMP_PLANKS), conditionsFromItem(DDBlocks.SWAMP_PLANKS))
            .offerTo(exporter, new Identifier(getRecipeName(DDBlocks.SWAMP_BUTTON)));

        ShapelessRecipeJsonBuilder.create(RecipeCategory.MISC, Items.IRON_INGOT, 9)
            .input(DDBlocks.CUT_IRON_BLOCK)
            .criterion(hasItem(DDBlocks.CUT_IRON_BLOCK), conditionsFromItem(DDBlocks.CUT_IRON_BLOCK))
            .offerTo(exporter, new Identifier(getRecipeName(Items.IRON_INGOT)) + "_from_cut_block");

        ShapelessRecipeJsonBuilder.create(RecipeCategory.MISC, Items.GOLD_INGOT, 9)
            .input(DDBlocks.CUT_GOLD_BLOCK)
            .criterion(hasItem(DDBlocks.CUT_GOLD_BLOCK), conditionsFromItem(DDBlocks.CUT_GOLD_BLOCK))
            .offerTo(exporter, new Identifier(getRecipeName(Items.GOLD_INGOT)) + "_from_cut_block");

        ShapelessRecipeJsonBuilder.create(RecipeCategory.MISC, Items.DIAMOND, 9)
            .input(DDBlocks.CUT_DIAMOND_BLOCK)
            .criterion(hasItem(DDBlocks.CUT_DIAMOND_BLOCK), conditionsFromItem(DDBlocks.CUT_DIAMOND_BLOCK))
            .offerTo(exporter, new Identifier(getRecipeName(Items.DIAMOND)) + "_from_cut_block");

        ShapelessRecipeJsonBuilder.create(RecipeCategory.MISC, Items.EMERALD, 9)
            .input(DDBlocks.CUT_EMERALD_BLOCK)
            .criterion(hasItem(DDBlocks.CUT_EMERALD_BLOCK), conditionsFromItem(DDBlocks.CUT_DIAMOND_BLOCK))
            .offerTo(exporter, new Identifier(getRecipeName(Items.EMERALD)) + "_from_cut_block");

        ShapelessRecipeJsonBuilder.create(RecipeCategory.MISC, Items.NETHERITE_INGOT, 9)
            .input(DDBlocks.CUT_NETHERITE_BLOCK)
            .criterion(hasItem(DDBlocks.CUT_NETHERITE_BLOCK), conditionsFromItem(DDBlocks.CUT_NETHERITE_BLOCK))
            .offerTo(exporter, new Identifier(getRecipeName(Items.NETHERITE_INGOT)) + "_from_cut_block");

        ShapelessRecipeJsonBuilder.create(RecipeCategory.MISC, Items.LAPIS_LAZULI, 9)
            .input(DDBlocks.CUT_LAPIS_BLOCK)
            .criterion(hasItem(DDBlocks.CUT_LAPIS_BLOCK), conditionsFromItem(DDBlocks.CUT_LAPIS_BLOCK))
            .offerTo(exporter, new Identifier(getRecipeName(Items.LAPIS_LAZULI)) + "_from_cut_block");

        ShapelessRecipeJsonBuilder.create(RecipeCategory.MISC, Items.BONE_MEAL, 9)
            .input(DDBlocks.BONE_BLOCK_COLUMN)
            .criterion(hasItem(DDBlocks.BONE_BLOCK_COLUMN), conditionsFromItem(DDBlocks.BONE_BLOCK_COLUMN))
            .offerTo(exporter, new Identifier(getRecipeName(Items.BONE_MEAL)) + "_from_column");

        ShapelessRecipeJsonBuilder.create(RecipeCategory.BUILDING_BLOCKS, Items.CALCITE, 1)
            .input(Items.STONE)
            .input(Items.QUARTZ)
            .criterion(hasItem(Items.STONE), conditionsFromItem(Items.STONE))
            .criterion(hasItem(Items.QUARTZ), conditionsFromItem(Items.QUARTZ))
            .offerTo(exporter, new Identifier(getRecipeName(Items.CALCITE) + "_shapeless"));

        offerMossyBlockRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.SANDSTONE_BRICKS, DDBlocks.MOSSY_SANDSTONE_BRICKS);
        offerMossyBlockRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.RED_SANDSTONE_BRICKS, DDBlocks.MOSSY_RED_SANDSTONE_BRICKS);
        offerMossyBlockRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Items.BRICKS, DDBlocks.MOSSY_BRICKS);
        offerMossyBlockRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.ANDESITE_BRICKS, DDBlocks.MOSSY_ANDESITE_BRICKS);
        offerMossyBlockRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.DIORITE_BRICKS, DDBlocks.MOSSY_DIORITE_BRICKS);
        offerMossyBlockRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.GRANITE_BRICKS, DDBlocks.MOSSY_GRANITE_BRICKS);
        offerMossyBlockRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.CALCITE_BRICKS, DDBlocks.MOSSY_CALCITE_BRICKS);
        offerMossyBlockRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.DRIPSTONE_BRICKS, DDBlocks.MOSSY_DRIPSTONE_BRICKS);
        offerMossyBlockRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.COBBLED_DEEPSLATE, DDBlocks.MOSSY_COBBLED_DEEPSLATE);
        offerMossyBlockRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.DEEPSLATE_BRICKS, DDBlocks.MOSSY_DEEPSLATE_BRICKS);
        offerMossyBlockRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.TUFF_BRICKS, DDBlocks.MOSSY_TUFF_BRICKS);

        offerWaxBlockRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.POLISHED_OBSIDIAN, DDBlocks.WAXED_POLISHED_OBSIDIAN);
        offerWaxBlockRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.POLISHED_OBSIDIAN_COLUMN, DDBlocks.WAXED_POLISHED_OBSIDIAN_COLUMN);
        offerWaxBlockRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.COPPER_PRESSURE_PLATE, DDBlocks.WAXED_COPPER_PRESSURE_PLATE);
        offerWaxBlockRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.EXPOSED_COPPER_PRESSURE_PLATE, DDBlocks.WAXED_EXPOSED_COPPER_PRESSURE_PLATE);
        offerWaxBlockRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.WEATHERED_COPPER_PRESSURE_PLATE, DDBlocks.WAXED_WEATHERED_COPPER_PRESSURE_PLATE);
        offerWaxBlockRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.OXIDIZED_COPPER_PRESSURE_PLATE, DDBlocks.WAXED_OXIDIZED_COPPER_PRESSURE_PLATE);
        offerWaxBlockRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.CUT_COPPER_COLUMN, DDBlocks.WAXED_CUT_COPPER_COLUMN);
        offerWaxBlockRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.EXPOSED_CUT_COPPER_COLUMN, DDBlocks.WAXED_EXPOSED_CUT_COPPER_COLUMN);
        offerWaxBlockRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.WEATHERED_CUT_COPPER_COLUMN, DDBlocks.WAXED_WEATHERED_CUT_COPPER_COLUMN);
        offerWaxBlockRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.OXIDIZED_CUT_COPPER_COLUMN, DDBlocks.WAXED_OXIDIZED_CUT_COPPER_COLUMN);
        offerWaxBlockRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.CUT_COPPER_WALL, DDBlocks.WAXED_CUT_COPPER_WALL);
        offerWaxBlockRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.EXPOSED_CUT_COPPER_WALL, DDBlocks.WAXED_EXPOSED_CUT_COPPER_WALL);
        offerWaxBlockRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.WEATHERED_CUT_COPPER_WALL, DDBlocks.WAXED_WEATHERED_CUT_COPPER_WALL);
        offerWaxBlockRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.OXIDIZED_CUT_COPPER_WALL, DDBlocks.WAXED_OXIDIZED_CUT_COPPER_WALL);

        offer3ItemShapelessCompactingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDItems.SAND_PILE, Items.SAND, 1);
        offer3ItemShapelessCompactingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDItems.ICE_SHARD, Items.ICE, 1);

        RecipeProvider.offerDyeableRecipes(exporter, DDRecipeHelper.DYES, DDRecipeHelper.LAMPS, "dye_lamps");
    }

    private static void generateShapedRecipes(RecipeExporter exporter) {
        RecipeProvider.offer2x2CompactingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.CHARRED_QUARTZ_BLOCK, DDItems.CHARRED_QUARTZ);
        RecipeProvider.offerReversibleCompactingRecipes(exporter, RecipeCategory.MISC, Items.SHORT_GRASS, RecipeCategory.BUILDING_BLOCKS, DDBlocks.GRASS_BUNDLE);
        RecipeProvider.offerReversibleCompactingRecipes(exporter, RecipeCategory.MISC, Items.CHARCOAL, RecipeCategory.BUILDING_BLOCKS, DDBlocks.CHARCOAL_BLOCK);
        RecipeProvider.offerReversibleCompactingRecipes(exporter, RecipeCategory.MISC, DDItems.BLAZING_COAL, RecipeCategory.BUILDING_BLOCKS, DDBlocks.BLAZING_COAL_BLOCK);
        RecipeProvider.offerSlabRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.AZALEA_SLAB, DDBlocks.AZALEA_PLANKS);
        RecipeProvider.offerPressurePlateRecipe(exporter, DDBlocks.AZALEA_PRESSURE_PLATE, DDBlocks.AZALEA_PLANKS);
        RecipeProvider.offerSlabRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.SWAMP_SLAB, DDBlocks.SWAMP_PLANKS);
        RecipeProvider.offerPressurePlateRecipe(exporter, DDBlocks.SWAMP_PRESSURE_PLATE, DDBlocks.SWAMP_PLANKS);

        RecipeProvider.offerPressurePlateRecipe(exporter, DDBlocks.COPPER_PRESSURE_PLATE, Blocks.COPPER_BLOCK);
        RecipeProvider.offerPressurePlateRecipe(exporter, DDBlocks.EXPOSED_COPPER_PRESSURE_PLATE, Blocks.EXPOSED_COPPER);
        RecipeProvider.offerPressurePlateRecipe(exporter, DDBlocks.WEATHERED_COPPER_PRESSURE_PLATE, Blocks.WEATHERED_COPPER);
        RecipeProvider.offerPressurePlateRecipe(exporter, DDBlocks.OXIDIZED_COPPER_PRESSURE_PLATE, Blocks.OXIDIZED_COPPER);
        RecipeProvider.offerPressurePlateRecipe(exporter, DDBlocks.WAXED_COPPER_PRESSURE_PLATE, Blocks.WAXED_COPPER_BLOCK);
        RecipeProvider.offerPressurePlateRecipe(exporter, DDBlocks.WAXED_EXPOSED_COPPER_PRESSURE_PLATE, Blocks.WAXED_EXPOSED_COPPER);
        RecipeProvider.offerPressurePlateRecipe(exporter, DDBlocks.WAXED_WEATHERED_COPPER_PRESSURE_PLATE, Blocks.WAXED_WEATHERED_COPPER);
        RecipeProvider.offerPressurePlateRecipe(exporter, DDBlocks.WAXED_OXIDIZED_COPPER_PRESSURE_PLATE, Blocks.WAXED_OXIDIZED_COPPER);

        RecipeProvider.createStairsRecipe(DDBlocks.AZALEA_STAIRS, Ingredient.ofItems(DDBlocks.AZALEA_PLANKS))
            .criterion(hasItem(DDBlocks.AZALEA_PLANKS), conditionsFromItem(DDBlocks.AZALEA_PLANKS))
            .offerTo(exporter);

        RecipeProvider.createStairsRecipe(DDBlocks.SWAMP_STAIRS, Ingredient.ofItems(DDBlocks.SWAMP_PLANKS))
            .criterion(hasItem(DDBlocks.SWAMP_PLANKS), conditionsFromItem(DDBlocks.SWAMP_PLANKS))
            .offerTo(exporter);

        RecipeProvider.offerBarkBlockRecipe(exporter, DDBlocks.AZALEA_WOOD, DDBlocks.AZALEA_LOG);
        RecipeProvider.offerBarkBlockRecipe(exporter, DDBlocks.STRIPPED_AZALEA_WOOD, DDBlocks.STRIPPED_AZALEA_LOG);
        RecipeProvider.offerBarkBlockRecipe(exporter, DDBlocks.SWAMP_WOOD, DDBlocks.SWAMP_LOG);
        RecipeProvider.offerBarkBlockRecipe(exporter, DDBlocks.STRIPPED_SWAMP_WOOD, DDBlocks.STRIPPED_SWAMP_LOG);

        RecipeProvider.createFenceRecipe(DDBlocks.AZALEA_FENCE, Ingredient.ofItems(DDBlocks.AZALEA_PLANKS))
            .criterion(hasItem(DDBlocks.AZALEA_PLANKS), conditionsFromItem(DDBlocks.AZALEA_PLANKS))
            .offerTo(exporter);

        RecipeProvider.createFenceRecipe(DDBlocks.SWAMP_FENCE, Ingredient.ofItems(DDBlocks.SWAMP_PLANKS))
            .criterion(hasItem(DDBlocks.SWAMP_PLANKS), conditionsFromItem(DDBlocks.SWAMP_PLANKS))
            .offerTo(exporter);

        RecipeProvider.createFenceGateRecipe(DDBlocks.AZALEA_FENCE_GATE, Ingredient.ofItems(DDBlocks.AZALEA_PLANKS))
            .criterion(hasItem(DDBlocks.AZALEA_PLANKS), conditionsFromItem(DDBlocks.AZALEA_PLANKS))
            .offerTo(exporter);

        RecipeProvider.createFenceGateRecipe(DDBlocks.SWAMP_FENCE_GATE, Ingredient.ofItems(DDBlocks.SWAMP_PLANKS))
            .criterion(hasItem(DDBlocks.SWAMP_PLANKS), conditionsFromItem(DDBlocks.SWAMP_PLANKS))
            .offerTo(exporter);

        RecipeProvider.createDoorRecipe(DDBlocks.AZALEA_DOOR, Ingredient.ofItems(DDBlocks.AZALEA_PLANKS))
            .criterion(hasItem(DDBlocks.AZALEA_PLANKS), conditionsFromItem(DDBlocks.AZALEA_PLANKS))
            .offerTo(exporter);

        RecipeProvider.createDoorRecipe(DDBlocks.SWAMP_DOOR, Ingredient.ofItems(DDBlocks.SWAMP_PLANKS))
            .criterion(hasItem(DDBlocks.SWAMP_PLANKS), conditionsFromItem(DDBlocks.SWAMP_PLANKS))
            .offerTo(exporter);

        RecipeProvider.createTrapdoorRecipe(DDBlocks.AZALEA_TRAPDOOR, Ingredient.ofItems(DDBlocks.AZALEA_PLANKS))
            .criterion(hasItem(DDBlocks.AZALEA_PLANKS), conditionsFromItem(DDBlocks.AZALEA_PLANKS))
            .offerTo(exporter);

        RecipeProvider.createTrapdoorRecipe(DDBlocks.SWAMP_TRAPDOOR, Ingredient.ofItems(DDBlocks.SWAMP_PLANKS))
            .criterion(hasItem(DDBlocks.SWAMP_PLANKS), conditionsFromItem(DDBlocks.SWAMP_PLANKS))
            .offerTo(exporter);

        RecipeProvider.createSignRecipe(DDBlocks.AZALEA_SIGN, Ingredient.ofItems(DDBlocks.AZALEA_PLANKS))
            .criterion(hasItem(DDBlocks.AZALEA_PLANKS), conditionsFromItem(DDBlocks.AZALEA_PLANKS))
            .offerTo(exporter);

        RecipeProvider.createSignRecipe(DDBlocks.SWAMP_SIGN, Ingredient.ofItems(DDBlocks.SWAMP_PLANKS))
            .criterion(hasItem(DDBlocks.SWAMP_PLANKS), conditionsFromItem(DDBlocks.SWAMP_PLANKS))
            .offerTo(exporter);

        RecipeProvider.offerHangingSignRecipe(exporter, DDBlocks.AZALEA_HANGING_SIGN, DDBlocks.STRIPPED_AZALEA_LOG);
        RecipeProvider.offerBoatRecipe(exporter, DDItems.AZALEA_BOAT, DDBlocks.AZALEA_PLANKS);
        RecipeProvider.offerChestBoatRecipe(exporter,DDItems.AZALEA_CHEST_BOAT, DDItems.AZALEA_BOAT);
        RecipeProvider.offerHangingSignRecipe(exporter, DDBlocks.SWAMP_HANGING_SIGN, DDBlocks.STRIPPED_SWAMP_LOG);
        RecipeProvider.offerBoatRecipe(exporter, DDItems.SWAMP_BOAT, DDBlocks.SWAMP_PLANKS);
        RecipeProvider.offerChestBoatRecipe(exporter,DDItems.SWAMP_CHEST_BOAT, DDItems.SWAMP_BOAT);
        RecipeProvider.offerWallRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.NETHERRACK_WALL, Blocks.NETHERRACK);
        RecipeProvider.offerSlabRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.NETHERRACK_SLAB, Blocks.NETHERRACK);
        RecipeProvider.offerWallRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.SMOOTH_STONE_WALL, Blocks.SMOOTH_STONE);
        RecipeProvider.offerWallRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.STONE_WALL, Blocks.STONE);
        RecipeProvider.offerWallRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.POLISHED_ANDESITE_WALL, Blocks.POLISHED_ANDESITE);
        RecipeProvider.offerWallRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.POLISHED_DIORITE_WALL, Blocks.POLISHED_DIORITE);
        RecipeProvider.offerWallRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.POLISHED_GRANITE_WALL, Blocks.POLISHED_GRANITE);
        RecipeProvider.offerWallRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.PRISMARINE_BRICK_WALL, Blocks.PRISMARINE_BRICKS);
        RecipeProvider.offerWallRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.DARK_PRISMARINE_WALL, Blocks.DARK_PRISMARINE);
        RecipeProvider.offerWallRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.PURPUR_WALL, Blocks.PURPUR_BLOCK);
        RecipeProvider.offerWallRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.POLISHED_SANDSTONE_WALL, DDBlocks.POLISHED_SANDSTONE);
        RecipeProvider.offerSlabRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.POLISHED_SANDSTONE_SLAB, DDBlocks.POLISHED_SANDSTONE);
        RecipeProvider.offerWallRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.SMOOTH_SANDSTONE_WALL, Blocks.SMOOTH_SANDSTONE);
        RecipeProvider.offerWallRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.SMOOTH_RED_SANDSTONE_WALL, Blocks.SMOOTH_RED_SANDSTONE);
        RecipeProvider.offerSlabRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.SANDSTONE_BRICK_SLAB, DDBlocks.SANDSTONE_BRICKS);
        RecipeProvider.offerWallRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.SANDSTONE_BRICK_WALL, DDBlocks.SANDSTONE_BRICKS);
        RecipeProvider.offerSlabRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.CRACKED_SANDSTONE_BRICK_SLAB, DDBlocks.CRACKED_SANDSTONE_BRICKS);
        RecipeProvider.offerWallRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.CRACKED_SANDSTONE_BRICK_WALL, DDBlocks.CRACKED_SANDSTONE_BRICKS);
        RecipeProvider.offerSlabRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.MOSSY_SANDSTONE_BRICK_SLAB, DDBlocks.MOSSY_SANDSTONE_BRICKS);
        RecipeProvider.offerWallRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.MOSSY_SANDSTONE_BRICK_WALL, DDBlocks.MOSSY_SANDSTONE_BRICKS);
        RecipeProvider.offerWallRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.POLISHED_RED_SANDSTONE_WALL, DDBlocks.POLISHED_RED_SANDSTONE);
        RecipeProvider.offerSlabRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.POLISHED_RED_SANDSTONE_SLAB, DDBlocks.POLISHED_RED_SANDSTONE);
        RecipeProvider.offerSlabRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.RED_SANDSTONE_BRICK_SLAB, DDBlocks.RED_SANDSTONE_BRICKS);
        RecipeProvider.offerWallRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.RED_SANDSTONE_BRICK_WALL, DDBlocks.RED_SANDSTONE_BRICKS);
        RecipeProvider.offerSlabRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.CRACKED_RED_SANDSTONE_BRICK_SLAB, DDBlocks.CRACKED_RED_SANDSTONE_BRICKS);
        RecipeProvider.offerWallRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.CRACKED_RED_SANDSTONE_BRICK_WALL, DDBlocks.CRACKED_RED_SANDSTONE_BRICKS);
        RecipeProvider.offerSlabRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.MOSSY_RED_SANDSTONE_BRICK_SLAB, DDBlocks.MOSSY_RED_SANDSTONE_BRICKS);
        RecipeProvider.offerWallRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.MOSSY_RED_SANDSTONE_BRICK_WALL, DDBlocks.MOSSY_RED_SANDSTONE_BRICKS);
        RecipeProvider.offerSlabRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.CRACKED_BRICK_SLAB, DDBlocks.CRACKED_BRICKS);
        RecipeProvider.offerWallRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.CRACKED_BRICK_WALL, DDBlocks.CRACKED_BRICKS);
        RecipeProvider.offerSlabRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.MOSSY_BRICK_SLAB, DDBlocks.MOSSY_BRICKS);
        RecipeProvider.offerWallRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.MOSSY_BRICK_WALL, DDBlocks.MOSSY_BRICKS);
        RecipeProvider.offerSlabRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.QUARTZ_BRICK_SLAB, Items.QUARTZ_BRICKS);
        RecipeProvider.offerWallRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.QUARTZ_BRICK_WALL, Items.QUARTZ_BRICKS);
        RecipeProvider.offerWallRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.QUARTZ_WALL, Items.QUARTZ_BLOCK);
        RecipeProvider.offerWallRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.SMOOTH_QUARTZ_WALL, Items.SMOOTH_QUARTZ);
        RecipeProvider.offerSlabRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.CHARRED_QUARTZ_SLAB, DDBlocks.CHARRED_QUARTZ_BLOCK);
        RecipeProvider.offerWallRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.CHARRED_QUARTZ_WALL, DDBlocks.CHARRED_QUARTZ_BLOCK);
        RecipeProvider.offerSlabRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.SMOOTH_CHARRED_QUARTZ_SLAB, DDBlocks.SMOOTH_CHARRED_QUARTZ_BLOCK);
        RecipeProvider.offerWallRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.SMOOTH_CHARRED_QUARTZ_WALL, DDBlocks.SMOOTH_CHARRED_QUARTZ_BLOCK);
        RecipeProvider.offerSlabRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.CHARRED_QUARTZ_BRICK_SLAB, DDBlocks.CHARRED_QUARTZ_BRICKS);
        RecipeProvider.offerWallRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.CHARRED_QUARTZ_BRICK_WALL, DDBlocks.CHARRED_QUARTZ_BRICKS);
        RecipeProvider.offerSlabRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.ANDESITE_BRICK_SLAB, DDBlocks.ANDESITE_BRICKS);
        RecipeProvider.offerWallRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.ANDESITE_BRICK_WALL, DDBlocks.ANDESITE_BRICKS);
        RecipeProvider.offerSlabRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.DIORITE_BRICK_SLAB, DDBlocks.DIORITE_BRICKS);
        RecipeProvider.offerWallRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.DIORITE_BRICK_WALL, DDBlocks.DIORITE_BRICKS);
        RecipeProvider.offerSlabRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.GRANITE_BRICK_SLAB, DDBlocks.GRANITE_BRICKS);
        RecipeProvider.offerWallRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.GRANITE_BRICK_WALL, DDBlocks.GRANITE_BRICKS);
        RecipeProvider.offerSlabRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.CRACKED_ANDESITE_BRICK_SLAB, DDBlocks.CRACKED_ANDESITE_BRICKS);
        RecipeProvider.offerWallRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.CRACKED_ANDESITE_BRICK_WALL, DDBlocks.CRACKED_ANDESITE_BRICKS);
        RecipeProvider.offerSlabRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.CRACKED_DIORITE_BRICK_SLAB, DDBlocks.CRACKED_DIORITE_BRICKS);
        RecipeProvider.offerWallRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.CRACKED_DIORITE_BRICK_WALL, DDBlocks.CRACKED_DIORITE_BRICKS);
        RecipeProvider.offerSlabRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.CRACKED_GRANITE_BRICK_SLAB, DDBlocks.CRACKED_GRANITE_BRICKS);
        RecipeProvider.offerWallRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.CRACKED_GRANITE_BRICK_WALL, DDBlocks.CRACKED_GRANITE_BRICKS);
        RecipeProvider.offerSlabRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.MOSSY_ANDESITE_BRICK_SLAB, DDBlocks.MOSSY_ANDESITE_BRICKS);
        RecipeProvider.offerWallRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.MOSSY_ANDESITE_BRICK_WALL, DDBlocks.MOSSY_ANDESITE_BRICKS);
        RecipeProvider.offerSlabRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.MOSSY_DIORITE_BRICK_SLAB, DDBlocks.MOSSY_DIORITE_BRICKS);
        RecipeProvider.offerWallRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.MOSSY_DIORITE_BRICK_WALL, DDBlocks.MOSSY_DIORITE_BRICKS);
        RecipeProvider.offerSlabRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.MOSSY_GRANITE_BRICK_SLAB, DDBlocks.MOSSY_GRANITE_BRICKS);
        RecipeProvider.offerWallRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.MOSSY_GRANITE_BRICK_WALL, DDBlocks.MOSSY_GRANITE_BRICKS);
        RecipeProvider.offerSlabRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.POLISHED_CALCITE_SLAB, DDBlocks.POLISHED_CALCITE);
        RecipeProvider.offerWallRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.POLISHED_CALCITE_WALL, DDBlocks.POLISHED_CALCITE);
        RecipeProvider.offerSlabRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.CALCITE_BRICK_SLAB, DDBlocks.CALCITE_BRICKS);
        RecipeProvider.offerWallRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.CALCITE_BRICK_WALL, DDBlocks.CALCITE_BRICKS);
        RecipeProvider.offerSlabRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.CRACKED_CALCITE_BRICK_SLAB, DDBlocks.CRACKED_CALCITE_BRICKS);
        RecipeProvider.offerWallRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.CRACKED_CALCITE_BRICK_WALL, DDBlocks.CRACKED_CALCITE_BRICKS);
        RecipeProvider.offerSlabRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.MOSSY_CALCITE_BRICK_SLAB, DDBlocks.MOSSY_CALCITE_BRICKS);
        RecipeProvider.offerWallRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.MOSSY_CALCITE_BRICK_WALL, DDBlocks.MOSSY_CALCITE_BRICKS);
        RecipeProvider.offerSlabRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.CALCITE_SLAB, Blocks.CALCITE);
        RecipeProvider.offerWallRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.CALCITE_WALL, Blocks.CALCITE);
        RecipeProvider.offerSlabRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.CUT_IRON_SLAB, DDBlocks.CUT_IRON_BLOCK);
        RecipeProvider.offerWallRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.CUT_IRON_WALL, DDBlocks.CUT_IRON_BLOCK);
        RecipeProvider.offerSlabRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.CUT_GOLD_SLAB, DDBlocks.CUT_GOLD_BLOCK);
        RecipeProvider.offerWallRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.CUT_GOLD_WALL, DDBlocks.CUT_GOLD_BLOCK);
        RecipeProvider.offerSlabRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.CUT_DIAMOND_SLAB, DDBlocks.CUT_DIAMOND_BLOCK);
        RecipeProvider.offerWallRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.CUT_DIAMOND_WALL, DDBlocks.CUT_DIAMOND_BLOCK);
        RecipeProvider.offerSlabRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.CUT_EMERALD_SLAB, DDBlocks.CUT_EMERALD_BLOCK);
        RecipeProvider.offerWallRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.CUT_EMERALD_WALL, DDBlocks.CUT_EMERALD_BLOCK);
        RecipeProvider.offerSlabRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.CUT_NETHERITE_SLAB, DDBlocks.CUT_NETHERITE_BLOCK);
        RecipeProvider.offerWallRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.CUT_NETHERITE_WALL, DDBlocks.CUT_NETHERITE_BLOCK);
        RecipeProvider.offerSlabRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.CUT_LAPIS_SLAB, DDBlocks.CUT_LAPIS_BLOCK);
        RecipeProvider.offerWallRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.CUT_LAPIS_WALL, DDBlocks.CUT_LAPIS_BLOCK);
        RecipeProvider.offerSlabRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.POLISHED_AMETHYST_SLAB, DDBlocks.POLISHED_AMETHYST_BLOCK);
        RecipeProvider.offerWallRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.POLISHED_AMETHYST_WALL, DDBlocks.POLISHED_AMETHYST_BLOCK);
        RecipeProvider.offerSlabRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.POLISHED_AMETHYST_BRICK_SLAB, DDBlocks.POLISHED_AMETHYST_BRICKS);
        RecipeProvider.offerWallRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.POLISHED_AMETHYST_BRICK_WALL, DDBlocks.POLISHED_AMETHYST_BRICKS);
        RecipeProvider.offerSlabRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.CUT_POLISHED_AMETHYST_SLAB, DDBlocks.CUT_POLISHED_AMETHYST);
        RecipeProvider.offerWallRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.CUT_POLISHED_AMETHYST_WALL, DDBlocks.CUT_POLISHED_AMETHYST);
        RecipeProvider.offerSlabRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.PACKED_ICE_BRICK_SLAB, DDBlocks.PACKED_ICE_BRICKS);
        RecipeProvider.offerWallRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.PACKED_ICE_BRICK_WALL, DDBlocks.PACKED_ICE_BRICKS);
        RecipeProvider.offerSlabRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.CRACKED_PACKED_ICE_BRICK_SLAB, DDBlocks.CRACKED_PACKED_ICE_BRICKS);
        RecipeProvider.offerWallRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.CRACKED_PACKED_ICE_BRICK_WALL, DDBlocks.CRACKED_PACKED_ICE_BRICKS);
        RecipeProvider.offerSlabRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.LARGE_PACKED_ICE_BRICK_SLAB, DDBlocks.LARGE_PACKED_ICE_BRICKS);
        RecipeProvider.offerWallRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.LARGE_PACKED_ICE_BRICK_WALL, DDBlocks.LARGE_PACKED_ICE_BRICKS);
        RecipeProvider.offerSlabRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.LARGE_CRACKED_PACKED_ICE_BRICK_SLAB, DDBlocks.LARGE_CRACKED_PACKED_ICE_BRICKS);
        RecipeProvider.offerWallRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.LARGE_CRACKED_PACKED_ICE_BRICK_WALL, DDBlocks.LARGE_CRACKED_PACKED_ICE_BRICKS);
        RecipeProvider.offerSlabRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.POLISHED_PACKED_ICE_SLAB, DDBlocks.POLISHED_PACKED_ICE);
        RecipeProvider.offerWallRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.POLISHED_PACKED_ICE_WALL, DDBlocks.POLISHED_PACKED_ICE);
        RecipeProvider.offerSlabRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.BLUE_ICE_BRICK_SLAB, DDBlocks.BLUE_ICE_BRICKS);
        RecipeProvider.offerWallRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.BLUE_ICE_BRICK_WALL, DDBlocks.BLUE_ICE_BRICKS);
        RecipeProvider.offerSlabRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.CRACKED_BLUE_ICE_BRICK_SLAB, DDBlocks.CRACKED_BLUE_ICE_BRICKS);
        RecipeProvider.offerWallRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.CRACKED_BLUE_ICE_BRICK_WALL, DDBlocks.CRACKED_BLUE_ICE_BRICKS);
        RecipeProvider.offerSlabRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.LARGE_BLUE_ICE_BRICK_SLAB, DDBlocks.LARGE_BLUE_ICE_BRICKS);
        RecipeProvider.offerWallRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.LARGE_BLUE_ICE_BRICK_WALL, DDBlocks.LARGE_BLUE_ICE_BRICKS);
        RecipeProvider.offerSlabRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.LARGE_CRACKED_BLUE_ICE_BRICK_SLAB, DDBlocks.LARGE_CRACKED_BLUE_ICE_BRICKS);
        RecipeProvider.offerWallRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.LARGE_CRACKED_BLUE_ICE_BRICK_WALL, DDBlocks.LARGE_CRACKED_BLUE_ICE_BRICKS);
        RecipeProvider.offerSlabRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.POLISHED_BLUE_ICE_SLAB, DDBlocks.POLISHED_BLUE_ICE);
        RecipeProvider.offerWallRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.POLISHED_BLUE_ICE_WALL, DDBlocks.POLISHED_BLUE_ICE);
        RecipeProvider.offerSlabRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.CRACKED_DEEPSLATE_BRICK_SLAB, Blocks.CRACKED_DEEPSLATE_BRICKS);
        RecipeProvider.offerWallRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.CRACKED_DEEPSLATE_BRICK_WALL, Blocks.CRACKED_DEEPSLATE_BRICKS);
        RecipeProvider.offerSlabRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.CRACKED_DEEPSLATE_TILE_SLAB, Blocks.CRACKED_DEEPSLATE_TILES);
        RecipeProvider.offerWallRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.CRACKED_DEEPSLATE_TILE_WALL, Blocks.CRACKED_DEEPSLATE_TILES);
        RecipeProvider.offerSlabRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.CRACKED_NETHER_BRICK_SLAB, Blocks.CRACKED_NETHER_BRICKS);
        RecipeProvider.offerWallRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.CRACKED_NETHER_BRICK_WALL, Blocks.CRACKED_NETHER_BRICKS);
        RecipeProvider.offerSlabRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.CRACKED_POLISHED_BLACKSTONE_BRICK_SLAB, Blocks.CRACKED_POLISHED_BLACKSTONE_BRICKS);
        RecipeProvider.offerWallRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.CRACKED_POLISHED_BLACKSTONE_BRICK_WALL, Blocks.CRACKED_POLISHED_BLACKSTONE_BRICKS);
        RecipeProvider.offerSlabRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.SMOOTH_BASALT_SLAB, Blocks.SMOOTH_BASALT);
        RecipeProvider.offerWallRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.SMOOTH_BASALT_WALL, Blocks.SMOOTH_BASALT);
        RecipeProvider.offerSlabRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.DRIPSTONE_SLAB, Blocks.DRIPSTONE_BLOCK);
        RecipeProvider.offerWallRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.DRIPSTONE_WALL, Blocks.DRIPSTONE_BLOCK);
        RecipeProvider.offerSlabRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.POLISHED_DRIPSTONE_SLAB, DDBlocks.POLISHED_DRIPSTONE);
        RecipeProvider.offerWallRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.POLISHED_DRIPSTONE_WALL, DDBlocks.POLISHED_DRIPSTONE);
        RecipeProvider.offerSlabRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.DRIPSTONE_BRICK_SLAB, DDBlocks.DRIPSTONE_BRICKS);
        RecipeProvider.offerWallRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.DRIPSTONE_BRICK_WALL, DDBlocks.DRIPSTONE_BRICKS);
        RecipeProvider.offerSlabRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.CRACKED_DRIPSTONE_BRICK_SLAB, DDBlocks.CRACKED_DRIPSTONE_BRICKS);
        RecipeProvider.offerWallRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.CRACKED_DRIPSTONE_BRICK_WALL, DDBlocks.CRACKED_DRIPSTONE_BRICKS);
        RecipeProvider.offerSlabRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.MOSSY_DRIPSTONE_BRICK_SLAB, DDBlocks.MOSSY_DRIPSTONE_BRICKS);
        RecipeProvider.offerWallRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.MOSSY_DRIPSTONE_BRICK_WALL, DDBlocks.MOSSY_DRIPSTONE_BRICKS);
        RecipeProvider.offerWallRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.CUT_COPPER_WALL, Blocks.CUT_COPPER);
        RecipeProvider.offerWallRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.EXPOSED_CUT_COPPER_WALL, Blocks.EXPOSED_CUT_COPPER);
        RecipeProvider.offerWallRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.WEATHERED_CUT_COPPER_WALL, Blocks.WEATHERED_CUT_COPPER);
        RecipeProvider.offerWallRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.OXIDIZED_CUT_COPPER_WALL, Blocks.OXIDIZED_CUT_COPPER);
        RecipeProvider.offerWallRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.WAXED_CUT_COPPER_WALL, Blocks.WAXED_CUT_COPPER);
        RecipeProvider.offerWallRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.WAXED_EXPOSED_CUT_COPPER_WALL, Blocks.WAXED_EXPOSED_CUT_COPPER);
        RecipeProvider.offerWallRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.WAXED_WEATHERED_CUT_COPPER_WALL, Blocks.WAXED_WEATHERED_CUT_COPPER);
        RecipeProvider.offerWallRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.WAXED_OXIDIZED_CUT_COPPER_WALL, Blocks.WAXED_OXIDIZED_CUT_COPPER);
        RecipeProvider.offerSlabRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.MOSSY_COBBLED_DEEPSLATE_SLAB, DDBlocks.MOSSY_COBBLED_DEEPSLATE);
        RecipeProvider.offerWallRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.MOSSY_COBBLED_DEEPSLATE_WALL, DDBlocks.MOSSY_COBBLED_DEEPSLATE);
        RecipeProvider.offerSlabRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.MOSSY_DEEPSLATE_BRICK_SLAB, DDBlocks.MOSSY_DEEPSLATE_BRICKS);
        RecipeProvider.offerWallRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.MOSSY_DEEPSLATE_BRICK_WALL, DDBlocks.MOSSY_DEEPSLATE_BRICKS);
        RecipeProvider.offerSlabRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.CHISELED_GRANITE_SLAB, DDBlocks.CHISELED_GRANITE);
        RecipeProvider.offerSlabRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.CHISELED_DIORITE_SLAB, DDBlocks.CHISELED_DIORITE);
        RecipeProvider.offerSlabRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.CHISELED_ANDESITE_SLAB, DDBlocks.CHISELED_ANDESITE);
        RecipeProvider.offerSlabRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.CHISELED_CALCITE_SLAB, DDBlocks.CHISELED_CALCITE);
        RecipeProvider.offerSlabRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.CHISELED_DRIPSTONE_SLAB, DDBlocks.CHISELED_DRIPSTONE);
        RecipeProvider.offerSlabRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.CRACKED_TUFF_BRICK_SLAB, DDBlocks.CRACKED_TUFF_BRICKS);
        RecipeProvider.offerSlabRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.MOSSY_TUFF_BRICK_SLAB, DDBlocks.MOSSY_TUFF_BRICKS);

        RecipeProvider.createStairsRecipe(DDBlocks.NETHERRACK_STAIRS, Ingredient.ofItems(Items.NETHERRACK))
            .criterion(hasItem(Items.NETHERRACK), conditionsFromItem(Items.NETHERRACK))
            .offerTo(exporter);

        RecipeProvider.createStairsRecipe(DDBlocks.SMOOTH_STONE_STAIRS, Ingredient.ofItems(Items.SMOOTH_STONE))
            .criterion(hasItem(Items.SMOOTH_STONE), conditionsFromItem(Items.SMOOTH_STONE))
            .offerTo(exporter);

        RecipeProvider.createStairsRecipe(DDBlocks.POLISHED_SANDSTONE_STAIRS, Ingredient.ofItems(DDBlocks.POLISHED_SANDSTONE))
            .criterion(hasItem(DDBlocks.POLISHED_SANDSTONE), conditionsFromItem(DDBlocks.POLISHED_SANDSTONE))
            .offerTo(exporter);

        RecipeProvider.createStairsRecipe(DDBlocks.SANDSTONE_BRICK_STAIRS, Ingredient.ofItems(DDBlocks.SANDSTONE_BRICKS))
            .criterion(hasItem(DDBlocks.SANDSTONE_BRICKS), conditionsFromItem(DDBlocks.SANDSTONE_BRICKS))
            .offerTo(exporter);

        RecipeProvider.createStairsRecipe(DDBlocks.CRACKED_SANDSTONE_BRICK_STAIRS, Ingredient.ofItems(DDBlocks.CRACKED_SANDSTONE_BRICKS))
            .criterion(hasItem(DDBlocks.CRACKED_SANDSTONE_BRICKS), conditionsFromItem(DDBlocks.CRACKED_SANDSTONE_BRICKS))
            .offerTo(exporter);

        RecipeProvider.createStairsRecipe(DDBlocks.MOSSY_SANDSTONE_BRICK_STAIRS, Ingredient.ofItems(DDBlocks.MOSSY_SANDSTONE_BRICKS))
            .criterion(hasItem(DDBlocks.MOSSY_SANDSTONE_BRICKS), conditionsFromItem(DDBlocks.MOSSY_SANDSTONE_BRICKS))
            .offerTo(exporter);

        RecipeProvider.createStairsRecipe(DDBlocks.POLISHED_RED_SANDSTONE_STAIRS, Ingredient.ofItems(DDBlocks.POLISHED_RED_SANDSTONE))
            .criterion(hasItem(DDBlocks.POLISHED_RED_SANDSTONE), conditionsFromItem(DDBlocks.POLISHED_RED_SANDSTONE))
            .offerTo(exporter);

        RecipeProvider.createStairsRecipe(DDBlocks.RED_SANDSTONE_BRICK_STAIRS, Ingredient.ofItems(DDBlocks.RED_SANDSTONE_BRICKS))
            .criterion(hasItem(DDBlocks.RED_SANDSTONE_BRICKS), conditionsFromItem(DDBlocks.RED_SANDSTONE_BRICKS))
            .offerTo(exporter);

        RecipeProvider.createStairsRecipe(DDBlocks.CRACKED_RED_SANDSTONE_BRICK_STAIRS, Ingredient.ofItems(DDBlocks.CRACKED_RED_SANDSTONE_BRICKS))
            .criterion(hasItem(DDBlocks.CRACKED_RED_SANDSTONE_BRICKS), conditionsFromItem(DDBlocks.CRACKED_RED_SANDSTONE_BRICKS))
            .offerTo(exporter);

        RecipeProvider.createStairsRecipe(DDBlocks.MOSSY_RED_SANDSTONE_BRICK_STAIRS, Ingredient.ofItems(DDBlocks.MOSSY_RED_SANDSTONE_BRICKS))
            .criterion(hasItem(DDBlocks.MOSSY_RED_SANDSTONE_BRICKS), conditionsFromItem(DDBlocks.MOSSY_RED_SANDSTONE_BRICKS))
            .offerTo(exporter);

        RecipeProvider.createStairsRecipe(DDBlocks.CRACKED_BRICK_STAIRS, Ingredient.ofItems(DDBlocks.CRACKED_BRICKS))
            .criterion(hasItem(DDBlocks.CRACKED_BRICKS), conditionsFromItem(DDBlocks.CRACKED_BRICKS))
            .offerTo(exporter);

        RecipeProvider.createStairsRecipe(DDBlocks.MOSSY_BRICK_STAIRS, Ingredient.ofItems(DDBlocks.MOSSY_BRICKS))
            .criterion(hasItem(DDBlocks.MOSSY_BRICKS), conditionsFromItem(DDBlocks.MOSSY_BRICKS))
            .offerTo(exporter);

        RecipeProvider.createStairsRecipe(DDBlocks.QUARTZ_BRICK_STAIRS, Ingredient.ofItems(Items.QUARTZ_BRICKS))
            .criterion(hasItem(Items.QUARTZ_BRICKS), conditionsFromItem(Items.QUARTZ_BRICKS))
            .offerTo(exporter);

        RecipeProvider.createStairsRecipe(DDBlocks.CHARRED_QUARTZ_STAIRS, Ingredient.ofItems(DDBlocks.CHARRED_QUARTZ_BLOCK))
            .criterion(hasItem(DDBlocks.CHARRED_QUARTZ_BLOCK), conditionsFromItem(DDBlocks.CHARRED_QUARTZ_BLOCK))
            .offerTo(exporter);

        RecipeProvider.createStairsRecipe(DDBlocks.SMOOTH_CHARRED_QUARTZ_STAIRS, Ingredient.ofItems(DDBlocks.SMOOTH_CHARRED_QUARTZ_BLOCK))
            .criterion(hasItem(DDBlocks.SMOOTH_CHARRED_QUARTZ_BLOCK), conditionsFromItem(DDBlocks.SMOOTH_CHARRED_QUARTZ_BLOCK))
            .offerTo(exporter);

        RecipeProvider.createStairsRecipe(DDBlocks.CHARRED_QUARTZ_BRICK_STAIRS, Ingredient.ofItems(DDBlocks.CHARRED_QUARTZ_BRICKS))
            .criterion(hasItem(DDBlocks.CHARRED_QUARTZ_BRICKS), conditionsFromItem(DDBlocks.CHARRED_QUARTZ_BRICKS))
            .offerTo(exporter);

        RecipeProvider.createStairsRecipe(DDBlocks.ANDESITE_BRICK_STAIRS, Ingredient.ofItems(DDBlocks.ANDESITE_BRICKS))
            .criterion(hasItem(DDBlocks.ANDESITE_BRICKS), conditionsFromItem(DDBlocks.ANDESITE_BRICKS))
            .offerTo(exporter);

        RecipeProvider.createStairsRecipe(DDBlocks.DIORITE_BRICK_STAIRS, Ingredient.ofItems(DDBlocks.DIORITE_BRICKS))
            .criterion(hasItem(DDBlocks.DIORITE_BRICKS), conditionsFromItem(DDBlocks.DIORITE_BRICKS))
            .offerTo(exporter);

        RecipeProvider.createStairsRecipe(DDBlocks.GRANITE_BRICK_STAIRS, Ingredient.ofItems(DDBlocks.GRANITE_BRICKS))
            .criterion(hasItem(DDBlocks.GRANITE_BRICKS), conditionsFromItem(DDBlocks.GRANITE_BRICKS))
            .offerTo(exporter);

        RecipeProvider.createStairsRecipe(DDBlocks.CRACKED_ANDESITE_BRICK_STAIRS, Ingredient.ofItems(DDBlocks.CRACKED_ANDESITE_BRICKS))
            .criterion(hasItem(DDBlocks.CRACKED_ANDESITE_BRICKS), conditionsFromItem(DDBlocks.CRACKED_ANDESITE_BRICKS))
            .offerTo(exporter);

        RecipeProvider.createStairsRecipe(DDBlocks.CRACKED_DIORITE_BRICK_STAIRS, Ingredient.ofItems(DDBlocks.CRACKED_DIORITE_BRICKS))
            .criterion(hasItem(DDBlocks.CRACKED_DIORITE_BRICKS), conditionsFromItem(DDBlocks.CRACKED_DIORITE_BRICKS))
            .offerTo(exporter);

        RecipeProvider.createStairsRecipe(DDBlocks.CRACKED_GRANITE_BRICK_STAIRS, Ingredient.ofItems(DDBlocks.CRACKED_GRANITE_BRICKS))
            .criterion(hasItem(DDBlocks.CRACKED_GRANITE_BRICKS), conditionsFromItem(DDBlocks.CRACKED_GRANITE_BRICKS))
            .offerTo(exporter);

        RecipeProvider.createStairsRecipe(DDBlocks.MOSSY_ANDESITE_BRICK_STAIRS, Ingredient.ofItems(DDBlocks.MOSSY_ANDESITE_BRICKS))
            .criterion(hasItem(DDBlocks.MOSSY_ANDESITE_BRICKS), conditionsFromItem(DDBlocks.MOSSY_ANDESITE_BRICKS))
            .offerTo(exporter);

        RecipeProvider.createStairsRecipe(DDBlocks.MOSSY_DIORITE_BRICK_STAIRS, Ingredient.ofItems(DDBlocks.MOSSY_DIORITE_BRICKS))
            .criterion(hasItem(DDBlocks.MOSSY_DIORITE_BRICKS), conditionsFromItem(DDBlocks.MOSSY_DIORITE_BRICKS))
            .offerTo(exporter);

        RecipeProvider.createStairsRecipe(DDBlocks.MOSSY_GRANITE_BRICK_STAIRS, Ingredient.ofItems(DDBlocks.MOSSY_GRANITE_BRICKS))
            .criterion(hasItem(DDBlocks.MOSSY_GRANITE_BRICKS), conditionsFromItem(DDBlocks.MOSSY_GRANITE_BRICKS))
            .offerTo(exporter);

        RecipeProvider.createStairsRecipe(DDBlocks.CALCITE_STAIRS, Ingredient.ofItems(Blocks.CALCITE))
            .criterion(hasItem(Blocks.CALCITE), conditionsFromItem(Blocks.CALCITE))
            .offerTo(exporter);

        RecipeProvider.createStairsRecipe(DDBlocks.POLISHED_CALCITE_STAIRS, Ingredient.ofItems(DDBlocks.POLISHED_CALCITE))
            .criterion(hasItem(DDBlocks.POLISHED_CALCITE), conditionsFromItem(DDBlocks.POLISHED_CALCITE))
            .offerTo(exporter);

        RecipeProvider.createStairsRecipe(DDBlocks.CALCITE_BRICK_STAIRS, Ingredient.ofItems(DDBlocks.CALCITE_BRICKS))
            .criterion(hasItem(DDBlocks.CALCITE_BRICKS), conditionsFromItem(DDBlocks.CALCITE_BRICKS))
            .offerTo(exporter);

        RecipeProvider.createStairsRecipe(DDBlocks.CRACKED_CALCITE_BRICK_STAIRS, Ingredient.ofItems(DDBlocks.CRACKED_CALCITE_BRICKS))
            .criterion(hasItem(DDBlocks.CRACKED_CALCITE_BRICKS), conditionsFromItem(DDBlocks.CRACKED_CALCITE_BRICKS))
            .offerTo(exporter);

        RecipeProvider.createStairsRecipe(DDBlocks.MOSSY_CALCITE_BRICK_STAIRS, Ingredient.ofItems(DDBlocks.MOSSY_CALCITE_BRICKS))
            .criterion(hasItem(DDBlocks.MOSSY_CALCITE_BRICKS), conditionsFromItem(DDBlocks.MOSSY_CALCITE_BRICKS))
            .offerTo(exporter);

        RecipeProvider.createStairsRecipe(DDBlocks.CUT_IRON_STAIRS, Ingredient.ofItems(DDBlocks.CUT_IRON_BLOCK))
            .criterion(hasItem(DDBlocks.CUT_IRON_BLOCK), conditionsFromItem(DDBlocks.CUT_IRON_BLOCK))
            .offerTo(exporter);

        RecipeProvider.createStairsRecipe(DDBlocks.CUT_GOLD_STAIRS, Ingredient.ofItems(DDBlocks.CUT_GOLD_BLOCK))
            .criterion(hasItem(DDBlocks.CUT_GOLD_BLOCK), conditionsFromItem(DDBlocks.CUT_GOLD_BLOCK))
            .offerTo(exporter);

        RecipeProvider.createStairsRecipe(DDBlocks.CUT_DIAMOND_STAIRS, Ingredient.ofItems(DDBlocks.CUT_DIAMOND_BLOCK))
            .criterion(hasItem(DDBlocks.CUT_DIAMOND_BLOCK), conditionsFromItem(DDBlocks.CUT_DIAMOND_BLOCK))
            .offerTo(exporter);

        RecipeProvider.createStairsRecipe(DDBlocks.CUT_EMERALD_STAIRS, Ingredient.ofItems(DDBlocks.CUT_EMERALD_BLOCK))
            .criterion(hasItem(DDBlocks.CUT_EMERALD_BLOCK), conditionsFromItem(DDBlocks.CUT_EMERALD_BLOCK))
            .offerTo(exporter);

        RecipeProvider.createStairsRecipe(DDBlocks.CUT_NETHERITE_STAIRS, Ingredient.ofItems(DDBlocks.CUT_NETHERITE_BLOCK))
            .criterion(hasItem(DDBlocks.CUT_NETHERITE_BLOCK), conditionsFromItem(DDBlocks.CUT_NETHERITE_BLOCK))
            .offerTo(exporter);

        RecipeProvider.createStairsRecipe(DDBlocks.CUT_LAPIS_STAIRS, Ingredient.ofItems(DDBlocks.CUT_LAPIS_BLOCK))
            .criterion(hasItem(DDBlocks.CUT_LAPIS_BLOCK), conditionsFromItem(DDBlocks.CUT_LAPIS_BLOCK))
            .offerTo(exporter);

        RecipeProvider.createStairsRecipe(DDBlocks.POLISHED_AMETHYST_STAIRS, Ingredient.ofItems(DDBlocks.POLISHED_AMETHYST_BLOCK))
            .criterion(hasItem(DDBlocks.POLISHED_AMETHYST_BLOCK), conditionsFromItem(DDBlocks.POLISHED_AMETHYST_BLOCK))
            .offerTo(exporter);

        RecipeProvider.createStairsRecipe(DDBlocks.POLISHED_AMETHYST_BRICK_STAIRS, Ingredient.ofItems(DDBlocks.POLISHED_AMETHYST_BRICKS))
            .criterion(hasItem(DDBlocks.POLISHED_AMETHYST_BRICKS), conditionsFromItem(DDBlocks.POLISHED_AMETHYST_BRICKS))
            .offerTo(exporter);

        RecipeProvider.createStairsRecipe(DDBlocks.CUT_POLISHED_AMETHYST_STAIRS, Ingredient.ofItems(DDBlocks.CUT_POLISHED_AMETHYST))
            .criterion(hasItem(DDBlocks.CUT_POLISHED_AMETHYST), conditionsFromItem(DDBlocks.CUT_POLISHED_AMETHYST))
            .offerTo(exporter);

        RecipeProvider.createStairsRecipe(DDBlocks.PACKED_ICE_BRICK_STAIRS, Ingredient.ofItems(DDBlocks.PACKED_ICE_BRICKS))
            .criterion(hasItem(DDBlocks.PACKED_ICE_BRICKS), conditionsFromItem(DDBlocks.PACKED_ICE_BRICKS))
            .offerTo(exporter);

        RecipeProvider.createStairsRecipe(DDBlocks.CRACKED_PACKED_ICE_BRICK_STAIRS, Ingredient.ofItems(DDBlocks.CRACKED_PACKED_ICE_BRICKS))
            .criterion(hasItem(DDBlocks.CRACKED_PACKED_ICE_BRICKS), conditionsFromItem(DDBlocks.CRACKED_PACKED_ICE_BRICKS))
            .offerTo(exporter);

        RecipeProvider.createStairsRecipe(DDBlocks.LARGE_PACKED_ICE_BRICK_STAIRS, Ingredient.ofItems(DDBlocks.LARGE_PACKED_ICE_BRICKS))
            .criterion(hasItem(DDBlocks.LARGE_PACKED_ICE_BRICKS), conditionsFromItem(DDBlocks.LARGE_PACKED_ICE_BRICKS))
            .offerTo(exporter);

        RecipeProvider.createStairsRecipe(DDBlocks.LARGE_CRACKED_PACKED_ICE_BRICK_STAIRS, Ingredient.ofItems(DDBlocks.LARGE_CRACKED_PACKED_ICE_BRICKS))
            .criterion(hasItem(DDBlocks.LARGE_CRACKED_PACKED_ICE_BRICKS), conditionsFromItem(DDBlocks.LARGE_CRACKED_PACKED_ICE_BRICKS))
            .offerTo(exporter);

        RecipeProvider.createStairsRecipe(DDBlocks.POLISHED_PACKED_ICE_STAIRS, Ingredient.ofItems(DDBlocks.POLISHED_PACKED_ICE))
            .criterion(hasItem(DDBlocks.POLISHED_PACKED_ICE), conditionsFromItem(DDBlocks.POLISHED_PACKED_ICE))
            .offerTo(exporter);

        RecipeProvider.createStairsRecipe(DDBlocks.BLUE_ICE_BRICK_STAIRS, Ingredient.ofItems(DDBlocks.BLUE_ICE_BRICKS))
            .criterion(hasItem(DDBlocks.BLUE_ICE_BRICKS), conditionsFromItem(DDBlocks.BLUE_ICE_BRICKS))
            .offerTo(exporter);

        RecipeProvider.createStairsRecipe(DDBlocks.CRACKED_BLUE_ICE_BRICK_STAIRS, Ingredient.ofItems(DDBlocks.CRACKED_BLUE_ICE_BRICKS))
            .criterion(hasItem(DDBlocks.CRACKED_BLUE_ICE_BRICKS), conditionsFromItem(DDBlocks.CRACKED_BLUE_ICE_BRICKS))
            .offerTo(exporter);

        RecipeProvider.createStairsRecipe(DDBlocks.LARGE_BLUE_ICE_BRICK_STAIRS, Ingredient.ofItems(DDBlocks.LARGE_BLUE_ICE_BRICKS))
            .criterion(hasItem(DDBlocks.LARGE_BLUE_ICE_BRICKS), conditionsFromItem(DDBlocks.LARGE_BLUE_ICE_BRICKS))
            .offerTo(exporter);

        RecipeProvider.createStairsRecipe(DDBlocks.LARGE_CRACKED_BLUE_ICE_BRICK_STAIRS, Ingredient.ofItems(DDBlocks.LARGE_CRACKED_BLUE_ICE_BRICKS))
            .criterion(hasItem(DDBlocks.LARGE_CRACKED_BLUE_ICE_BRICKS), conditionsFromItem(DDBlocks.LARGE_CRACKED_BLUE_ICE_BRICKS))
            .offerTo(exporter);

        RecipeProvider.createStairsRecipe(DDBlocks.POLISHED_BLUE_ICE_STAIRS, Ingredient.ofItems(DDBlocks.POLISHED_BLUE_ICE))
            .criterion(hasItem(DDBlocks.POLISHED_BLUE_ICE), conditionsFromItem(DDBlocks.POLISHED_BLUE_ICE))
            .offerTo(exporter);

        RecipeProvider.createStairsRecipe(DDBlocks.CRACKED_DEEPSLATE_BRICK_STAIRS, Ingredient.ofItems(Blocks.CRACKED_DEEPSLATE_BRICKS))
            .criterion(hasItem(Blocks.CRACKED_DEEPSLATE_BRICKS), conditionsFromItem(Blocks.CRACKED_DEEPSLATE_BRICKS))
            .offerTo(exporter);

        RecipeProvider.createStairsRecipe(DDBlocks.CRACKED_DEEPSLATE_TILE_STAIRS, Ingredient.ofItems(Blocks.CRACKED_DEEPSLATE_TILES))
            .criterion(hasItem(Blocks.CRACKED_DEEPSLATE_TILES), conditionsFromItem(Blocks.CRACKED_DEEPSLATE_TILES))
            .offerTo(exporter);

        RecipeProvider.createStairsRecipe(DDBlocks.CRACKED_NETHER_BRICK_STAIRS, Ingredient.ofItems(Blocks.CRACKED_NETHER_BRICKS))
            .criterion(hasItem(Blocks.CRACKED_NETHER_BRICKS), conditionsFromItem(Blocks.CRACKED_NETHER_BRICKS))
            .offerTo(exporter);

        RecipeProvider.createStairsRecipe(DDBlocks.CRACKED_POLISHED_BLACKSTONE_BRICK_STAIRS, Ingredient.ofItems(Blocks.CRACKED_POLISHED_BLACKSTONE_BRICKS))
            .criterion(hasItem(Blocks.CRACKED_POLISHED_BLACKSTONE_BRICKS), conditionsFromItem(Blocks.CRACKED_POLISHED_BLACKSTONE_BRICKS))
            .offerTo(exporter);

        RecipeProvider.createStairsRecipe(DDBlocks.SMOOTH_BASALT_STAIRS, Ingredient.ofItems(Blocks.SMOOTH_BASALT))
            .criterion(hasItem(Blocks.SMOOTH_BASALT), conditionsFromItem(Blocks.SMOOTH_BASALT))
            .offerTo(exporter);

        RecipeProvider.createStairsRecipe(DDBlocks.DRIPSTONE_STAIRS, Ingredient.ofItems(Blocks.DRIPSTONE_BLOCK))
            .criterion(hasItem(Blocks.DRIPSTONE_BLOCK), conditionsFromItem(Blocks.DRIPSTONE_BLOCK))
            .offerTo(exporter);

        RecipeProvider.createStairsRecipe(DDBlocks.POLISHED_DRIPSTONE_STAIRS, Ingredient.ofItems(DDBlocks.POLISHED_DRIPSTONE))
            .criterion(hasItem(DDBlocks.POLISHED_DRIPSTONE), conditionsFromItem(DDBlocks.POLISHED_DRIPSTONE))
            .offerTo(exporter);

        RecipeProvider.createStairsRecipe(DDBlocks.DRIPSTONE_BRICK_STAIRS, Ingredient.ofItems(DDBlocks.DRIPSTONE_BRICKS))
            .criterion(hasItem(DDBlocks.DRIPSTONE_BRICKS), conditionsFromItem(DDBlocks.DRIPSTONE_BRICKS))
            .offerTo(exporter);

        RecipeProvider.createStairsRecipe(DDBlocks.CRACKED_DRIPSTONE_BRICK_STAIRS, Ingredient.ofItems(DDBlocks.CRACKED_DRIPSTONE_BRICKS))
            .criterion(hasItem(DDBlocks.CRACKED_DRIPSTONE_BRICKS), conditionsFromItem(DDBlocks.CRACKED_DRIPSTONE_BRICKS))
            .offerTo(exporter);

        RecipeProvider.createStairsRecipe(DDBlocks.MOSSY_DRIPSTONE_BRICK_STAIRS, Ingredient.ofItems(DDBlocks.MOSSY_DRIPSTONE_BRICKS))
            .criterion(hasItem(DDBlocks.MOSSY_DRIPSTONE_BRICKS), conditionsFromItem(DDBlocks.MOSSY_DRIPSTONE_BRICKS))
            .offerTo(exporter);

        RecipeProvider.createStairsRecipe(DDBlocks.MOSSY_COBBLED_DEEPSLATE_STAIRS, Ingredient.ofItems(DDBlocks.MOSSY_COBBLED_DEEPSLATE))
            .criterion(hasItem(DDBlocks.MOSSY_COBBLED_DEEPSLATE), conditionsFromItem(DDBlocks.MOSSY_COBBLED_DEEPSLATE))
            .offerTo(exporter);

        RecipeProvider.createStairsRecipe(DDBlocks.MOSSY_DEEPSLATE_BRICK_STAIRS, Ingredient.ofItems(DDBlocks.MOSSY_DEEPSLATE_BRICKS))
            .criterion(hasItem(DDBlocks.MOSSY_DEEPSLATE_BRICKS), conditionsFromItem(DDBlocks.MOSSY_DEEPSLATE_BRICKS))
            .offerTo(exporter);

        RecipeProvider.createStairsRecipe(DDBlocks.CRACKED_TUFF_BRICK_STAIRS, Ingredient.ofItems(DDBlocks.CRACKED_TUFF_BRICKS))
            .criterion(hasItem(DDBlocks.CRACKED_TUFF_BRICKS), conditionsFromItem(DDBlocks.CRACKED_TUFF_BRICKS))
            .offerTo(exporter);

        RecipeProvider.createStairsRecipe(DDBlocks.MOSSY_TUFF_BRICK_STAIRS, Ingredient.ofItems(DDBlocks.MOSSY_TUFF_BRICKS))
            .criterion(hasItem(DDBlocks.MOSSY_TUFF_BRICKS), conditionsFromItem(DDBlocks.MOSSY_TUFF_BRICKS))
            .offerTo(exporter);

        ShapedRecipeJsonBuilder.create(RecipeCategory.MISC, DDItems.BLAZING_COAL)
            .pattern(" N ")
            .pattern("NCN")
            .pattern(" N ")
            .input('N', Items.NETHERRACK)
            .input('C', ItemTags.COALS)
            .criterion(FabricRecipeProvider.hasItem(Items.NETHERRACK), FabricRecipeProvider.conditionsFromItem(Items.NETHERRACK))
            .criterion(FabricRecipeProvider.hasItem(DDItems.BLAZING_COAL), FabricRecipeProvider.conditionsFromItem(DDItems.BLAZING_COAL))
            .offerTo(exporter, new Identifier(getRecipeName(DDItems.BLAZING_COAL)) + "_from_coal");

        ShapedRecipeJsonBuilder.create(RecipeCategory.BUILDING_BLOCKS, DDBlocks.CRACKED_NETHER_BRICK_FENCE)
            .pattern("CBC")
            .pattern("CBC")
            .input('B', Items.NETHER_BRICK)
            .input('C', Blocks.CRACKED_NETHER_BRICKS)
            .criterion(FabricRecipeProvider.hasItem(Items.NETHER_BRICK), FabricRecipeProvider.conditionsFromItem(Items.NETHER_BRICK))
            .criterion(FabricRecipeProvider.hasItem(Blocks.CRACKED_NETHER_BRICKS), FabricRecipeProvider.conditionsFromItem(Blocks.CRACKED_NETHER_BRICKS))
            .offerTo(exporter, new Identifier(getRecipeName(DDBlocks.CRACKED_NETHER_BRICK_FENCE)));

        ShapedRecipeJsonBuilder.create(RecipeCategory.BUILDING_BLOCKS, DDBlocks.SOUL_JACK_O_LANTERN)
            .pattern("P")
            .pattern("T")
            .input('P', Items.CARVED_PUMPKIN)
            .input('T', DDItemTags.SOUL_TORCHES)
            .criterion(FabricRecipeProvider.hasItem(Items.CARVED_PUMPKIN), FabricRecipeProvider.conditionsFromItem(Items.CARVED_PUMPKIN))
            .criterion("soul_torches", FabricRecipeProvider.conditionsFromTag(DDItemTags.SOUL_TORCHES))
            .offerTo(exporter, new Identifier(getRecipeName(DDBlocks.SOUL_JACK_O_LANTERN)));

        ShapedRecipeJsonBuilder.create(RecipeCategory.MISC, Items.BUNDLE)
            .pattern("SLS")
            .pattern("L L")
            .pattern("LLL")
            .input('L', Items.LEATHER)
            .input('S', Items.STRING)
            .criterion(FabricRecipeProvider.hasItem(Items.LEATHER), FabricRecipeProvider.conditionsFromItem(Items.LEATHER))
            .criterion(FabricRecipeProvider.hasItem(Items.STRING), FabricRecipeProvider.conditionsFromItem(Items.STRING))
            .offerTo(exporter, new Identifier(getRecipeName(Items.BUNDLE)));

        ShapedRecipeJsonBuilder.create(RecipeCategory.BUILDING_BLOCKS, DDBlocks.COPPER_JACK_O_LANTERN)
            .pattern("P")
            .pattern("T")
            .input('P', Items.CARVED_PUMPKIN)
            .input('T', DDItemTags.COPPER_TORCHES)
            .criterion(FabricRecipeProvider.hasItem(Items.CARVED_PUMPKIN), FabricRecipeProvider.conditionsFromItem(Items.CARVED_PUMPKIN))
            .criterion("copper_torches", FabricRecipeProvider.conditionsFromTag(DDItemTags.COPPER_TORCHES))
            .offerTo(exporter, new Identifier(getRecipeName(DDBlocks.COPPER_JACK_O_LANTERN)));

        ShapedRecipeJsonBuilder.create(RecipeCategory.BUILDING_BLOCKS, DDBlocks.AMETHYST_JACK_O_LANTERN)
            .pattern("P")
            .pattern("T")
            .input('P', Items.CARVED_PUMPKIN)
            .input('T', DDItemTags.AMETHYST_TORCHES)
            .criterion(FabricRecipeProvider.hasItem(Items.CARVED_PUMPKIN), FabricRecipeProvider.conditionsFromItem(Items.CARVED_PUMPKIN))
            .criterion("amethyst_torches", FabricRecipeProvider.conditionsFromTag(DDItemTags.AMETHYST_TORCHES))
            .offerTo(exporter, new Identifier(getRecipeName(DDBlocks.AMETHYST_JACK_O_LANTERN)));

        offer2x2BrickRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Items.SMOOTH_SANDSTONE, DDBlocks.POLISHED_SANDSTONE);
        offer2x2BrickRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.POLISHED_SANDSTONE, DDBlocks.SANDSTONE_BRICKS);
        offer2x2BrickRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Items.SMOOTH_RED_SANDSTONE, DDBlocks.POLISHED_RED_SANDSTONE);
        offer2x2BrickRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.POLISHED_RED_SANDSTONE, DDBlocks.RED_SANDSTONE_BRICKS);
        offer2x2BrickRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Items.ANDESITE, DDBlocks.ANDESITE_BRICKS);
        offer2x2BrickRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Items.DIORITE, DDBlocks.DIORITE_BRICKS);
        offer2x2BrickRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Items.GRANITE, DDBlocks.GRANITE_BRICKS);
        offer2x2BrickRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Items.CALCITE, DDBlocks.POLISHED_CALCITE);
        offer2x2BrickRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.POLISHED_CALCITE, DDBlocks.CALCITE_BRICKS);
        offer2x2BrickRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Items.OBSIDIAN, DDBlocks.POLISHED_OBSIDIAN);
        offer2x2BrickRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Items.IRON_BLOCK, DDBlocks.CUT_IRON_BLOCK);
        offer2x2BrickRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Items.GOLD_BLOCK, DDBlocks.CUT_GOLD_BLOCK);
        offer2x2BrickRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Items.DIAMOND_BLOCK, DDBlocks.CUT_DIAMOND_BLOCK);
        offer2x2BrickRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Items.EMERALD_BLOCK, DDBlocks.CUT_EMERALD_BLOCK);
        offer2x2BrickRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Items.NETHERITE_BLOCK, DDBlocks.CUT_NETHERITE_BLOCK);
        offer2x2BrickRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Items.LAPIS_BLOCK, DDBlocks.CUT_LAPIS_BLOCK);
        offer2x2BrickRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Items.AMETHYST_BLOCK, DDBlocks.POLISHED_AMETHYST_BLOCK);
        offer2x2BrickRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.POLISHED_AMETHYST_BLOCK, DDBlocks.POLISHED_AMETHYST_BRICKS);
        offer2x2BrickRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Items.PACKED_ICE, DDBlocks.POLISHED_PACKED_ICE);
        offer2x2BrickRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.POLISHED_PACKED_ICE, DDBlocks.PACKED_ICE_BRICKS);
        offer2x2BrickRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.PACKED_ICE_BRICKS, DDBlocks.LARGE_PACKED_ICE_BRICKS);
        offer2x2BrickRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Items.BLUE_ICE, DDBlocks.POLISHED_BLUE_ICE);
        offer2x2BrickRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.POLISHED_BLUE_ICE, DDBlocks.BLUE_ICE_BRICKS);
        offer2x2BrickRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.BLUE_ICE_BRICKS, DDBlocks.LARGE_BLUE_ICE_BRICKS);
        offer2x2BrickRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.DRIPSTONE_BLOCK, DDBlocks.POLISHED_DRIPSTONE);
        offer2x2BrickRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.POLISHED_DRIPSTONE, DDBlocks.DRIPSTONE_BRICKS);

        RecipeProvider.createChiseledBlockRecipe(RecipeCategory.BUILDING_BLOCKS, DDBlocks.CHISELED_POLISHED_SANDSTONE, Ingredient.ofItems(DDBlocks.SANDSTONE_BRICK_SLAB.asItem()))
            .criterion(FabricRecipeProvider.hasItem(DDBlocks.SANDSTONE_BRICK_SLAB), FabricRecipeProvider.conditionsFromItem(DDBlocks.SANDSTONE_BRICK_SLAB))
            .offerTo(exporter);

        RecipeProvider.createChiseledBlockRecipe(RecipeCategory.BUILDING_BLOCKS, DDBlocks.CHISELED_POLISHED_RED_SANDSTONE, Ingredient.ofItems(DDBlocks.RED_SANDSTONE_BRICK_SLAB.asItem()))
            .criterion(FabricRecipeProvider.hasItem(DDBlocks.RED_SANDSTONE_BRICK_SLAB), FabricRecipeProvider.conditionsFromItem(DDBlocks.RED_SANDSTONE_BRICK_SLAB))
            .offerTo(exporter);

        RecipeProvider.createChiseledBlockRecipe(RecipeCategory.BUILDING_BLOCKS, DDBlocks.CHISELED_CHARRED_QUARTZ_BLOCK, Ingredient.ofItems(DDBlocks.CHARRED_QUARTZ_SLAB.asItem()))
            .criterion(FabricRecipeProvider.hasItem(DDBlocks.CHARRED_QUARTZ_SLAB), FabricRecipeProvider.conditionsFromItem(DDBlocks.CHARRED_QUARTZ_SLAB))
            .offerTo(exporter);

        RecipeProvider.createChiseledBlockRecipe(RecipeCategory.BUILDING_BLOCKS, DDBlocks.CUT_POLISHED_AMETHYST, Ingredient.ofItems(DDBlocks.POLISHED_AMETHYST_SLAB.asItem()))
            .criterion(FabricRecipeProvider.hasItem(DDBlocks.POLISHED_AMETHYST_SLAB), FabricRecipeProvider.conditionsFromItem(DDBlocks.POLISHED_AMETHYST_SLAB))
            .offerTo(exporter);

        RecipeProvider.createChiseledBlockRecipe(RecipeCategory.BUILDING_BLOCKS, DDBlocks.CHISELED_PACKED_ICE_BRICKS, Ingredient.ofItems(DDBlocks.PACKED_ICE_BRICK_SLAB.asItem()))
            .criterion(FabricRecipeProvider.hasItem(DDBlocks.PACKED_ICE_BRICK_SLAB), FabricRecipeProvider.conditionsFromItem(DDBlocks.PACKED_ICE_BRICK_SLAB))
            .offerTo(exporter);

        RecipeProvider.createChiseledBlockRecipe(RecipeCategory.BUILDING_BLOCKS, DDBlocks.CHISELED_BLUE_ICE_BRICKS, Ingredient.ofItems(DDBlocks.BLUE_ICE_BRICK_SLAB.asItem()))
            .criterion(FabricRecipeProvider.hasItem(DDBlocks.BLUE_ICE_BRICK_SLAB), FabricRecipeProvider.conditionsFromItem(DDBlocks.BLUE_ICE_BRICK_SLAB))
            .offerTo(exporter);

        RecipeProvider.offerChiseledBlockRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.CHISELED_GRANITE, Blocks.POLISHED_GRANITE_SLAB);
        RecipeProvider.offerChiseledBlockRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.CHISELED_DIORITE, Blocks.POLISHED_DIORITE_SLAB);
        RecipeProvider.offerChiseledBlockRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.CHISELED_ANDESITE, Blocks.POLISHED_ANDESITE_SLAB);
        RecipeProvider.offerChiseledBlockRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.CHISELED_CALCITE, DDBlocks.POLISHED_CALCITE_SLAB);
        RecipeProvider.offerChiseledBlockRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.CHISELED_DRIPSTONE, DDBlocks.POLISHED_DRIPSTONE_SLAB);

        ShapedRecipeJsonBuilder.create(RecipeCategory.BUILDING_BLOCKS, DDBlocks.COPPER_BARREL, 1)
            .pattern("CCC")
            .pattern("CBC")
            .pattern("CCC")
            .input('C', Items.COPPER_INGOT)
            .input('B', Items.BARREL)
            .criterion(FabricRecipeProvider.hasItem(Items.COPPER_INGOT), FabricRecipeProvider.conditionsFromItem(Items.COPPER_INGOT))
            .criterion(FabricRecipeProvider.hasItem(Items.BARREL), FabricRecipeProvider.conditionsFromItem(Items.BARREL))
            .offerTo(exporter);

        ShapedRecipeJsonBuilder.create(RecipeCategory.BUILDING_BLOCKS, DDBlocks.IRON_BARREL, 1)
            .pattern("III")
            .pattern("IBI")
            .pattern("III")
            .input('I', Items.IRON_INGOT)
            .input('B', DDBlocks.COPPER_BARREL)
            .criterion(FabricRecipeProvider.hasItem(Items.IRON_INGOT), FabricRecipeProvider.conditionsFromItem(Items.IRON_INGOT))
            .criterion(FabricRecipeProvider.hasItem(DDBlocks.COPPER_BARREL), FabricRecipeProvider.conditionsFromItem(DDBlocks.COPPER_BARREL))
            .offerTo(exporter);

        ShapedRecipeJsonBuilder.create(RecipeCategory.BUILDING_BLOCKS, DDBlocks.GOLD_BARREL, 1)
            .pattern("GGG")
            .pattern("GBG")
            .pattern("GGG")
            .input('G', Items.GOLD_INGOT)
            .input('B', DDBlocks.IRON_BARREL)
            .criterion(FabricRecipeProvider.hasItem(Items.GOLD_INGOT), FabricRecipeProvider.conditionsFromItem(Items.GOLD_INGOT))
            .criterion(FabricRecipeProvider.hasItem(DDBlocks.IRON_BARREL), FabricRecipeProvider.conditionsFromItem(DDBlocks.IRON_BARREL))
            .offerTo(exporter);

        ShapedRecipeJsonBuilder.create(RecipeCategory.BUILDING_BLOCKS, DDBlocks.DIAMOND_BARREL, 1)
            .pattern(" D ")
            .pattern("DBD")
            .pattern(" D ")
            .input('D', Items.DIAMOND)
            .input('B', DDBlocks.GOLD_BARREL)
            .criterion(FabricRecipeProvider.hasItem(Items.DIAMOND), FabricRecipeProvider.conditionsFromItem(Items.DIAMOND))
            .criterion(FabricRecipeProvider.hasItem(DDBlocks.GOLD_BARREL), FabricRecipeProvider.conditionsFromItem(DDBlocks.GOLD_BARREL))
            .offerTo(exporter);

        ShapedRecipeJsonBuilder.create(RecipeCategory.BUILDING_BLOCKS, DDItems.COPPER_BARREL_UPGRADE, 1)
            .pattern("CCC")
            .pattern("CPC")
            .pattern("CCC")
            .input('C', Items.COPPER_INGOT)
            .input('P', ItemTags.PLANKS)
            .criterion(FabricRecipeProvider.hasItem(Items.COPPER_INGOT), FabricRecipeProvider.conditionsFromItem(Items.COPPER_INGOT))
            .offerTo(exporter);

        ShapedRecipeJsonBuilder.create(RecipeCategory.BUILDING_BLOCKS, DDItems.IRON_BARREL_UPGRADE, 1)
            .pattern("III")
            .pattern("ICI")
            .pattern("III")
            .input('I', Items.IRON_INGOT)
            .input('C', Items.COPPER_INGOT)
            .criterion(FabricRecipeProvider.hasItem(Items.IRON_INGOT), FabricRecipeProvider.conditionsFromItem(Items.IRON_INGOT))
            .criterion(FabricRecipeProvider.hasItem(Items.COPPER_INGOT), FabricRecipeProvider.conditionsFromItem(Items.COPPER_INGOT))
            .offerTo(exporter);

        ShapedRecipeJsonBuilder.create(RecipeCategory.BUILDING_BLOCKS, DDItems.GOLD_BARREL_UPGRADE, 1)
            .pattern("GGG")
            .pattern("GIG")
            .pattern("GGG")
            .input('G', Items.GOLD_INGOT)
            .input('I', Items.IRON_INGOT)
            .criterion(FabricRecipeProvider.hasItem(Items.GOLD_INGOT), FabricRecipeProvider.conditionsFromItem(Items.GOLD_INGOT))
            .criterion(FabricRecipeProvider.hasItem(Items.IRON_INGOT), FabricRecipeProvider.conditionsFromItem(Items.IRON_INGOT))
            .offerTo(exporter);

        ShapedRecipeJsonBuilder.create(RecipeCategory.BUILDING_BLOCKS, DDItems.DIAMOND_BARREL_UPGRADE, 1)
            .pattern(" D ")
            .pattern("DGD")
            .pattern(" D ")
            .input('D', Items.DIAMOND)
            .input('G', Items.GOLD_INGOT)
            .criterion(FabricRecipeProvider.hasItem(Items.DIAMOND), FabricRecipeProvider.conditionsFromItem(Items.DIAMOND))
            .criterion(FabricRecipeProvider.hasItem(Items.GOLD_INGOT), FabricRecipeProvider.conditionsFromItem(Items.GOLD_INGOT))
            .offerTo(exporter);

        ShapedRecipeJsonBuilder.create(RecipeCategory.FOOD, DDItems.CHOCOLATE_MILK, 1)
            .pattern(" C ")
            .pattern("CBC")
            .pattern(" M ")
            .input('C', Items.COCOA_BEANS)
            .input('B', Items.GLASS_BOTTLE)
            .input('M', Items.MILK_BUCKET)
            .criterion(FabricRecipeProvider.hasItem(Items.COCOA_BEANS), FabricRecipeProvider.conditionsFromItem(Items.COCOA_BEANS))
            .criterion(FabricRecipeProvider.hasItem(Items.GLASS_BOTTLE), FabricRecipeProvider.conditionsFromItem(Items.GLASS_BOTTLE))
            .criterion(FabricRecipeProvider.hasItem(Items.MILK_BUCKET), FabricRecipeProvider.conditionsFromItem(Items.MILK_BUCKET))
            .offerTo(exporter);

        ShapedRecipeJsonBuilder.create(RecipeCategory.TOOLS, DDItems.DYE_BUCKET, 1)
            .pattern("@@@")
            .pattern("@B@")
            .pattern("@@@")
            .input('B', Items.BUCKET)
            .input('@', Items.WHITE_DYE) // placeholder until updating to 1.20.5+
            .criterion(FabricRecipeProvider.hasItem(Items.BUCKET), FabricRecipeProvider.conditionsFromItem(Items.BUCKET))
            .criterion(FabricRecipeProvider.hasItem(Items.WHITE_DYE), FabricRecipeProvider.conditionsFromItem(Items.WHITE_DYE))
            .offerTo(exporter);

        ShapedRecipeJsonBuilder.create(RecipeCategory.BUILDING_BLOCKS, DDBlocks.WOODCUTTER, 1)
            .pattern(" I ")
            .pattern("CCC")
            .input('C', Items.COBBLESTONE)
            .input('I', Items.IRON_INGOT)
            .criterion(FabricRecipeProvider.hasItem(Items.COBBLESTONE), FabricRecipeProvider.conditionsFromItem(Items.COBBLESTONE))
            .criterion(FabricRecipeProvider.hasItem(Items.IRON_INGOT), FabricRecipeProvider.conditionsFromItem(Items.IRON_INGOT))
            .offerTo(exporter, new Identifier(getRecipeName(DDBlocks.WOODCUTTER)));

        ShapedRecipeJsonBuilder.create(RecipeCategory.REDSTONE, DDBlocks.DETECTOR, 1)
            .pattern("CCC")
            .pattern("TRQ")
            .pattern("CCC")
            .input('C', Items.COBBLESTONE)
            .input('T', Items.TINTED_GLASS)
            .input('R', Items.REDSTONE)
            .input('Q', Items.QUARTZ)
            .criterion(FabricRecipeProvider.hasItem(Items.COBBLESTONE), FabricRecipeProvider.conditionsFromItem(Items.COBBLESTONE))
            .criterion(FabricRecipeProvider.hasItem(Items.TINTED_GLASS), FabricRecipeProvider.conditionsFromItem(Items.TINTED_GLASS))
            .criterion(FabricRecipeProvider.hasItem(Items.REDSTONE), FabricRecipeProvider.conditionsFromItem(Items.REDSTONE))
            .criterion(FabricRecipeProvider.hasItem(Items.QUARTZ), FabricRecipeProvider.conditionsFromItem(Items.QUARTZ))
            .offerTo(exporter, new Identifier(getRecipeName(DDBlocks.DETECTOR)));

        ShapedRecipeJsonBuilder.create(RecipeCategory.TOOLS, DDItems.WRENCH, 1)
            .pattern(" C ")
            .pattern(" CC")
            .pattern("C  ")
            .input('C', Items.COPPER_INGOT)
            .criterion(FabricRecipeProvider.hasItem(Items.COPPER_INGOT), FabricRecipeProvider.conditionsFromItem(Items.COPPER_INGOT))
            .offerTo(exporter, new Identifier(getRecipeName(DDItems.WRENCH)));

        ShapedRecipeJsonBuilder.create(RecipeCategory.BUILDING_BLOCKS, DDBlocks.FAST_HOPPER, 1)
            .pattern("@ @")
            .pattern("#C#")
            .pattern(" @ ")
            .input('C', Items.CHEST)
            .input('#', Items.GOLD_BLOCK)
            .input('@', Items.GOLD_INGOT)
            .criterion(FabricRecipeProvider.hasItem(Items.CHEST), FabricRecipeProvider.conditionsFromItem(Items.CHEST))
            .criterion(FabricRecipeProvider.hasItem(Items.GOLD_BLOCK), FabricRecipeProvider.conditionsFromItem(Items.GOLD_BLOCK))
            .criterion(FabricRecipeProvider.hasItem(Items.GOLD_INGOT), FabricRecipeProvider.conditionsFromItem(Items.GOLD_INGOT))
            .offerTo(exporter, new Identifier(getRecipeName(DDBlocks.FAST_HOPPER)));

        RecipeProvider.offerReversibleCompactingRecipes(exporter, RecipeCategory.MISC, DDItems.NETHERITE_NUGGET, RecipeCategory.BUILDING_BLOCKS, Items.NETHERITE_INGOT);
        offerDispenserShapedRecipe(exporter, Items.PISTON, DDBlocks.DEPLOYER);
        offerDispenserShapedRecipe(exporter, Items.IRON_PICKAXE, DDBlocks.DESTROYER);

        offerPillarRecipe(exporter, DDBlocks.CHARRED_QUARTZ_BLOCK, DDBlocks.CHARRED_QUARTZ_PILLAR);

        offer3x3CompactingNoNameConflict(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.CHISELED_BLUE_ICE_BRICKS, DDBlocks.CHISELED_PACKED_ICE_BRICKS);
        offer3x3CompactingNoNameConflict(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.POLISHED_BLUE_ICE, DDBlocks.POLISHED_PACKED_ICE);
        offer3x3CompactingNoNameConflict(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.POLISHED_BLUE_ICE_STAIRS, DDBlocks.POLISHED_PACKED_ICE_STAIRS);
        offer3x3CompactingNoNameConflict(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.POLISHED_BLUE_ICE_SLAB, DDBlocks.POLISHED_PACKED_ICE_SLAB);
        offer3x3CompactingNoNameConflict(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.POLISHED_BLUE_ICE_WALL, DDBlocks.POLISHED_PACKED_ICE_WALL);
        offer3x3CompactingNoNameConflict(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.BLUE_ICE_BRICKS, DDBlocks.PACKED_ICE_BRICKS);
        offer3x3CompactingNoNameConflict(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.BLUE_ICE_BRICK_STAIRS, DDBlocks.PACKED_ICE_BRICK_STAIRS);
        offer3x3CompactingNoNameConflict(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.BLUE_ICE_BRICK_SLAB, DDBlocks.PACKED_ICE_BRICK_SLAB);
        offer3x3CompactingNoNameConflict(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.BLUE_ICE_BRICK_WALL, DDBlocks.PACKED_ICE_BRICK_WALL);
        offer3x3CompactingNoNameConflict(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.LARGE_BLUE_ICE_BRICKS, DDBlocks.LARGE_PACKED_ICE_BRICKS);
        offer3x3CompactingNoNameConflict(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.LARGE_BLUE_ICE_BRICK_STAIRS, DDBlocks.LARGE_PACKED_ICE_BRICK_STAIRS);
        offer3x3CompactingNoNameConflict(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.LARGE_BLUE_ICE_BRICK_SLAB, DDBlocks.LARGE_PACKED_ICE_BRICK_SLAB);
        offer3x3CompactingNoNameConflict(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.LARGE_BLUE_ICE_BRICK_WALL, DDBlocks.LARGE_PACKED_ICE_BRICK_WALL);
        offer3x3CompactingNoNameConflict(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.LARGE_CRACKED_BLUE_ICE_BRICKS, DDBlocks.LARGE_CRACKED_PACKED_ICE_BRICKS);
        offer3x3CompactingNoNameConflict(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.LARGE_CRACKED_BLUE_ICE_BRICK_STAIRS, DDBlocks.LARGE_CRACKED_PACKED_ICE_BRICK_STAIRS);
        offer3x3CompactingNoNameConflict(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.LARGE_CRACKED_BLUE_ICE_BRICK_SLAB, DDBlocks.LARGE_CRACKED_PACKED_ICE_BRICK_SLAB);
        offer3x3CompactingNoNameConflict(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.LARGE_CRACKED_BLUE_ICE_BRICK_WALL, DDBlocks.LARGE_CRACKED_PACKED_ICE_BRICK_WALL);
        offer3x3CompactingNoNameConflict(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.CRACKED_BLUE_ICE_BRICKS, DDBlocks.CRACKED_PACKED_ICE_BRICKS);
        offer3x3CompactingNoNameConflict(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.CRACKED_BLUE_ICE_BRICK_STAIRS, DDBlocks.CRACKED_PACKED_ICE_BRICK_STAIRS);
        offer3x3CompactingNoNameConflict(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.CRACKED_BLUE_ICE_BRICK_SLAB, DDBlocks.CRACKED_PACKED_ICE_BRICK_SLAB);
        offer3x3CompactingNoNameConflict(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.CRACKED_BLUE_ICE_BRICK_WALL, DDBlocks.CRACKED_PACKED_ICE_BRICK_WALL);

        offerColumnRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.OAK_LOG_COLUMN, Blocks.OAK_LOG);
        offerColumnRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.OAK_WOOD_COLUMN, Blocks.OAK_WOOD);
        offerColumnRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.STRIPPED_OAK_LOG_COLUMN, Blocks.STRIPPED_OAK_LOG);
        offerColumnRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.STRIPPED_OAK_WOOD_COLUMN, Blocks.STRIPPED_OAK_WOOD);
        offerColumnRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.OAK_PLANKS_COLUMN, Blocks.OAK_PLANKS);
        offerColumnRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.SPRUCE_LOG_COLUMN, Blocks.SPRUCE_LOG);
        offerColumnRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.SPRUCE_WOOD_COLUMN, Blocks.SPRUCE_WOOD);
        offerColumnRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.STRIPPED_SPRUCE_LOG_COLUMN, Blocks.STRIPPED_SPRUCE_LOG);
        offerColumnRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.STRIPPED_SPRUCE_WOOD_COLUMN, Blocks.STRIPPED_SPRUCE_WOOD);
        offerColumnRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.SPRUCE_PLANKS_COLUMN, Blocks.SPRUCE_PLANKS);
        offerColumnRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.BIRCH_LOG_COLUMN, Blocks.BIRCH_LOG);
        offerColumnRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.BIRCH_WOOD_COLUMN, Blocks.BIRCH_WOOD);
        offerColumnRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.STRIPPED_BIRCH_LOG_COLUMN, Blocks.STRIPPED_BIRCH_LOG);
        offerColumnRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.STRIPPED_BIRCH_WOOD_COLUMN, Blocks.STRIPPED_BIRCH_WOOD);
        offerColumnRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.BIRCH_PLANKS_COLUMN, Blocks.BIRCH_PLANKS);
        offerColumnRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.JUNGLE_LOG_COLUMN, Blocks.JUNGLE_LOG);
        offerColumnRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.JUNGLE_WOOD_COLUMN, Blocks.JUNGLE_WOOD);
        offerColumnRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.STRIPPED_JUNGLE_LOG_COLUMN, Blocks.STRIPPED_JUNGLE_LOG);
        offerColumnRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.STRIPPED_JUNGLE_WOOD_COLUMN, Blocks.STRIPPED_JUNGLE_WOOD);
        offerColumnRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.JUNGLE_PLANKS_COLUMN, Blocks.JUNGLE_PLANKS);
        offerColumnRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.ACACIA_LOG_COLUMN, Blocks.ACACIA_LOG);
        offerColumnRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.ACACIA_WOOD_COLUMN, Blocks.ACACIA_WOOD);
        offerColumnRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.STRIPPED_ACACIA_LOG_COLUMN, Blocks.STRIPPED_ACACIA_LOG);
        offerColumnRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.STRIPPED_ACACIA_WOOD_COLUMN, Blocks.STRIPPED_ACACIA_WOOD);
        offerColumnRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.ACACIA_PLANKS_COLUMN, Blocks.ACACIA_PLANKS);
        offerColumnRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.DARK_OAK_LOG_COLUMN, Blocks.DARK_OAK_LOG);
        offerColumnRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.DARK_OAK_WOOD_COLUMN, Blocks.DARK_OAK_WOOD);
        offerColumnRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.STRIPPED_DARK_OAK_LOG_COLUMN, Blocks.STRIPPED_DARK_OAK_LOG);
        offerColumnRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.STRIPPED_DARK_OAK_WOOD_COLUMN, Blocks.STRIPPED_DARK_OAK_WOOD);
        offerColumnRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.DARK_OAK_PLANKS_COLUMN, Blocks.DARK_OAK_PLANKS);
        offerColumnRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.MANGROVE_LOG_COLUMN, Blocks.MANGROVE_LOG);
        offerColumnRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.MANGROVE_WOOD_COLUMN, Blocks.MANGROVE_WOOD);
        offerColumnRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.STRIPPED_MANGROVE_LOG_COLUMN, Blocks.STRIPPED_MANGROVE_LOG);
        offerColumnRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.STRIPPED_MANGROVE_WOOD_COLUMN, Blocks.STRIPPED_MANGROVE_WOOD);
        offerColumnRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.MANGROVE_PLANKS_COLUMN, Blocks.MANGROVE_PLANKS);
        offerColumnRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.CHERRY_LOG_COLUMN, Blocks.CHERRY_LOG);
        offerColumnRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.CHERRY_WOOD_COLUMN, Blocks.CHERRY_WOOD);
        offerColumnRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.STRIPPED_CHERRY_LOG_COLUMN, Blocks.STRIPPED_CHERRY_LOG);
        offerColumnRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.STRIPPED_CHERRY_WOOD_COLUMN, Blocks.STRIPPED_CHERRY_WOOD);
        offerColumnRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.CHERRY_PLANKS_COLUMN, Blocks.CHERRY_PLANKS);
        offerColumnRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.BAMBOO_BLOCK_COLUMN, Blocks.BAMBOO_BLOCK);
        offerColumnRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.STRIPPED_BAMBOO_BLOCK_COLUMN, Blocks.STRIPPED_BAMBOO_BLOCK);
        offerColumnRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.BAMBOO_PLANKS_COLUMN, Blocks.BAMBOO_PLANKS);
        offerColumnRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.BAMBOO_MOSAIC_COLUMN, Blocks.BAMBOO_MOSAIC);
        offerColumnRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.AZALEA_LOG_COLUMN, DDBlocks.AZALEA_LOG);
        offerColumnRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.AZALEA_WOOD_COLUMN, DDBlocks.AZALEA_WOOD);
        offerColumnRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.STRIPPED_AZALEA_LOG_COLUMN, DDBlocks.STRIPPED_AZALEA_LOG);
        offerColumnRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.STRIPPED_AZALEA_WOOD_COLUMN, DDBlocks.STRIPPED_AZALEA_WOOD);
        offerColumnRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.AZALEA_PLANKS_COLUMN, DDBlocks.AZALEA_PLANKS);
        offerColumnRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.SWAMP_LOG_COLUMN, DDBlocks.SWAMP_LOG);
        offerColumnRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.SWAMP_WOOD_COLUMN, DDBlocks.SWAMP_WOOD);
        offerColumnRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.STRIPPED_SWAMP_LOG_COLUMN, DDBlocks.STRIPPED_SWAMP_LOG);
        offerColumnRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.STRIPPED_SWAMP_WOOD_COLUMN, DDBlocks.STRIPPED_SWAMP_WOOD);
        offerColumnRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.SWAMP_PLANKS_COLUMN, DDBlocks.SWAMP_PLANKS);
        offerColumnRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.CRIMSON_STEM_COLUMN, Blocks.CRIMSON_STEM);
        offerColumnRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.CRIMSON_HYPHAE_COLUMN, Blocks.CRIMSON_HYPHAE);
        offerColumnRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.STRIPPED_CRIMSON_STEM_COLUMN, Blocks.STRIPPED_CRIMSON_STEM);
        offerColumnRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.STRIPPED_CRIMSON_HYPHAE_COLUMN, Blocks.STRIPPED_CRIMSON_HYPHAE);
        offerColumnRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.CRIMSON_PLANKS_COLUMN, Blocks.CRIMSON_PLANKS);
        offerColumnRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.WARPED_STEM_COLUMN, Blocks.WARPED_STEM);
        offerColumnRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.WARPED_HYPHAE_COLUMN, Blocks.WARPED_HYPHAE);
        offerColumnRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.STRIPPED_WARPED_STEM_COLUMN, Blocks.STRIPPED_WARPED_STEM);
        offerColumnRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.STRIPPED_WARPED_HYPHAE_COLUMN, Blocks.STRIPPED_WARPED_HYPHAE);
        offerColumnRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.WARPED_PLANKS_COLUMN, Blocks.WARPED_PLANKS);
        offerColumnRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.STONE_COLUMN, Blocks.STONE);
        offerColumnRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.COBBLESTONE_COLUMN, Blocks.COBBLESTONE);
        offerColumnRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.MOSSY_COBBLESTONE_COLUMN, Blocks.MOSSY_COBBLESTONE);
        offerColumnRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.SMOOTH_STONE_COLUMN, Blocks.SMOOTH_STONE);
        offerColumnRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.STONE_BRICKS_COLUMN, Blocks.STONE_BRICKS);
        offerColumnRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.CRACKED_STONE_BRICKS_COLUMN, Blocks.CRACKED_STONE_BRICKS);
        offerColumnRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.MOSSY_STONE_BRICKS_COLUMN, Blocks.MOSSY_STONE_BRICKS);
        offerColumnRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.GRANITE_COLUMN, Blocks.GRANITE);
        offerColumnRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.POLISHED_GRANITE_COLUMN, Blocks.POLISHED_GRANITE);
        offerColumnRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.GRANITE_BRICKS_COLUMN, DDBlocks.GRANITE_BRICKS);
        offerColumnRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.CRACKED_GRANITE_BRICKS_COLUMN, DDBlocks.CRACKED_GRANITE_BRICKS);
        offerColumnRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.MOSSY_GRANITE_BRICKS_COLUMN, DDBlocks.MOSSY_GRANITE_BRICKS);
        offerColumnRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.DIORITE_COLUMN, Blocks.DIORITE);
        offerColumnRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.POLISHED_DIORITE_COLUMN, Blocks.POLISHED_DIORITE);
        offerColumnRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.DIORITE_BRICKS_COLUMN, DDBlocks.DIORITE_BRICKS);
        offerColumnRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.CRACKED_DIORITE_BRICKS_COLUMN, DDBlocks.CRACKED_DIORITE_BRICKS);
        offerColumnRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.MOSSY_DIORITE_BRICKS_COLUMN, DDBlocks.MOSSY_DIORITE_BRICKS);
        offerColumnRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.ANDESITE_COLUMN, Blocks.ANDESITE);
        offerColumnRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.POLISHED_ANDESITE_COLUMN, Blocks.POLISHED_ANDESITE);
        offerColumnRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.ANDESITE_BRICKS_COLUMN, DDBlocks.ANDESITE_BRICKS);
        offerColumnRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.CRACKED_ANDESITE_BRICKS_COLUMN, DDBlocks.CRACKED_ANDESITE_BRICKS);
        offerColumnRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.MOSSY_ANDESITE_BRICKS_COLUMN, DDBlocks.MOSSY_ANDESITE_BRICKS);
        offerColumnRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.CALCITE_COLUMN, Blocks.CALCITE);
        offerColumnRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.POLISHED_CALCITE_COLUMN, DDBlocks.POLISHED_CALCITE);
        offerColumnRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.CALCITE_BRICKS_COLUMN, DDBlocks.CALCITE_BRICKS);
        offerColumnRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.CRACKED_CALCITE_BRICKS_COLUMN, DDBlocks.CRACKED_CALCITE_BRICKS);
        offerColumnRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.MOSSY_CALCITE_BRICKS_COLUMN, DDBlocks.MOSSY_CALCITE_BRICKS);
        offerColumnRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.COBBLED_DEEPSLATE_COLUMN, Blocks.COBBLED_DEEPSLATE);
        offerColumnRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.POLISHED_DEEPSLATE_COLUMN, Blocks.POLISHED_DEEPSLATE);
        offerColumnRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.DEEPSLATE_BRICKS_COLUMN, Blocks.DEEPSLATE_BRICKS);
        offerColumnRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.CRACKED_DEEPSLATE_BRICKS_COLUMN, Blocks.CRACKED_DEEPSLATE_BRICKS);
        offerColumnRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.DEEPSLATE_TILES_COLUMN, Blocks.DEEPSLATE_TILES);
        offerColumnRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.CRACKED_DEEPSLATE_TILES_COLUMN, Blocks.CRACKED_DEEPSLATE_TILES);
        offerColumnRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.BRICKS_COLUMN, Blocks.BRICKS);
        offerColumnRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.CRACKED_BRICKS_COLUMN, DDBlocks.CRACKED_BRICKS);
        offerColumnRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.MOSSY_BRICKS_COLUMN, DDBlocks.MOSSY_BRICKS);
        offerColumnRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.MUD_BRICKS_COLUMN, Blocks.MUD_BRICKS);
        offerColumnRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.SANDSTONE_COLUMN, Blocks.SANDSTONE);
        offerColumnRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.SMOOTH_SANDSTONE_COLUMN, Blocks.SMOOTH_SANDSTONE);
        offerColumnRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.SANDSTONE_BRICKS_COLUMN, DDBlocks.MOSSY_CALCITE_BRICKS);
        offerColumnRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.CRACKED_SANDSTONE_BRICKS_COLUMN, DDBlocks.CRACKED_SANDSTONE_BRICKS);
        offerColumnRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.MOSSY_SANDSTONE_BRICKS_COLUMN, DDBlocks.MOSSY_SANDSTONE_BRICKS);
        offerColumnRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.RED_SANDSTONE_COLUMN, Blocks.RED_SANDSTONE);
        offerColumnRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.SMOOTH_RED_SANDSTONE_COLUMN, Blocks.SMOOTH_RED_SANDSTONE);
        offerColumnRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.RED_SANDSTONE_BRICKS_COLUMN, DDBlocks.RED_SANDSTONE_BRICKS);
        offerColumnRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.CRACKED_RED_SANDSTONE_BRICKS_COLUMN, DDBlocks.CRACKED_RED_SANDSTONE_BRICKS);
        offerColumnRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.MOSSY_RED_SANDSTONE_BRICKS_COLUMN, DDBlocks.MOSSY_RED_SANDSTONE_BRICKS);
        offerColumnRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.PRISMARINE_COLUMN, Blocks.PRISMARINE);
        offerColumnRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.PRISMARINE_BRICKS_COLUMN, Blocks.PRISMARINE_BRICKS);
        offerColumnRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.DARK_PRISMARINE_COLUMN, Blocks.DARK_PRISMARINE);
        offerColumnRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.NETHERRACK_COLUMN, Blocks.NETHERRACK);
        offerColumnRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.NETHER_BRICKS_COLUMN, Blocks.NETHER_BRICKS);
        offerColumnRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.CRACKED_NETHER_BRICKS_COLUMN, Blocks.CRACKED_NETHER_BRICKS);
        offerColumnRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.RED_NETHER_BRICKS_COLUMN, DDBlocks.RED_NETHER_BRICKS_COLUMN);
        offerColumnRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.BASALT_COLUMN, Blocks.BASALT);
        offerColumnRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.SMOOTH_BASALT_COLUMN, Blocks.SMOOTH_BASALT);
        offerColumnRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.POLISHED_BASALT_COLUMN, Blocks.POLISHED_BASALT);
        offerColumnRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.BLACKSTONE_COLUMN, Blocks.BLACKSTONE);
        offerColumnRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.POLISHED_BLACKSTONE_COLUMN, Blocks.POLISHED_BLACKSTONE);
        offerColumnRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.POLISHED_BLACKSTONE_BRICKS_COLUMN, Blocks.POLISHED_BLACKSTONE_BRICKS);
        offerColumnRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.CRACKED_POLISHED_BLACKSTONE_BRICKS_COLUMN, Blocks.CRACKED_POLISHED_BLACKSTONE_BRICKS);
        offerColumnRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.END_STONE_BRICKS_COLUMN, Blocks.END_STONE_BRICKS);
        offerColumnRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.PURPUR_BLOCK_COLUMN, Blocks.PURPUR_BLOCK);
        offerColumnRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.PURPUR_PILLAR_COLUMN, Blocks.PURPUR_PILLAR);
        offerColumnRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.CUT_IRON_COLUMN, DDBlocks.CUT_IRON_BLOCK);
        offerColumnRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.CUT_GOLD_COLUMN, DDBlocks.CUT_GOLD_BLOCK);
        offerColumnRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.CUT_EMERALD_COLUMN, DDBlocks.CUT_EMERALD_BLOCK);
        offerColumnRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.CUT_LAPIS_COLUMN, DDBlocks.CUT_LAPIS_BLOCK);
        offerColumnRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.CUT_DIAMOND_COLUMN, DDBlocks.CUT_DIAMOND_BLOCK);
        offerColumnRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.CUT_NETHERITE_COLUMN, DDBlocks.CUT_NETHERITE_BLOCK);
        offerColumnRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.QUARTZ_BLOCK_COLUMN, Blocks.QUARTZ_BLOCK);
        offerColumnRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.QUARTZ_BRICKS_COLUMN, Blocks.QUARTZ_BRICKS);
        offerColumnRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.QUARTZ_PILLAR_COLUMN, Blocks.QUARTZ_PILLAR);
        offerColumnRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.SMOOTH_QUARTZ_COLUMN, Blocks.SMOOTH_QUARTZ);
        offerColumnRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.CHARRED_QUARTZ_BLOCK_COLUMN, DDBlocks.CHARRED_QUARTZ_BLOCK);
        offerColumnRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.CHARRED_QUARTZ_BRICKS_COLUMN, DDBlocks.CHARRED_QUARTZ_BRICKS);
        offerColumnRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.CHARRED_QUARTZ_PILLAR_COLUMN, DDBlocks.CHARRED_QUARTZ_PILLAR);
        offerColumnRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.SMOOTH_CHARRED_QUARTZ_COLUMN, DDBlocks.SMOOTH_CHARRED_QUARTZ_BLOCK);
        offerColumnRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.POLISHED_AMETHYST_COLUMN, DDBlocks.POLISHED_AMETHYST_BLOCK);
        offerColumnRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.POLISHED_AMETHYST_BRICKS_COLUMN, DDBlocks.POLISHED_AMETHYST_BRICKS);
        offerColumnRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.CUT_POLISHED_AMETHYST_COLUMN, DDBlocks.CUT_POLISHED_AMETHYST);
        offerColumnRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.CUT_COPPER_COLUMN, Blocks.CUT_COPPER);
        offerColumnRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.WAXED_CUT_COPPER_COLUMN, Blocks.WAXED_CUT_COPPER);
        offerColumnRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.EXPOSED_CUT_COPPER_COLUMN, Blocks.EXPOSED_CUT_COPPER);
        offerColumnRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.WAXED_EXPOSED_CUT_COPPER_COLUMN, Blocks.WAXED_EXPOSED_CUT_COPPER);
        offerColumnRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.WEATHERED_CUT_COPPER_COLUMN, Blocks.WEATHERED_CUT_COPPER);
        offerColumnRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.WAXED_WEATHERED_CUT_COPPER_COLUMN, Blocks.WAXED_WEATHERED_CUT_COPPER);
        offerColumnRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.OXIDIZED_CUT_COPPER_COLUMN, Blocks.OXIDIZED_CUT_COPPER);
        offerColumnRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.WAXED_OXIDIZED_CUT_COPPER_COLUMN, Blocks.WAXED_OXIDIZED_CUT_COPPER);
        offerColumnRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.PACKED_ICE_BRICKS_COLUMN, DDBlocks.PACKED_ICE_BRICKS);
        offerColumnRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.CRACKED_PACKED_ICE_BRICKS_COLUMN, DDBlocks.CRACKED_PACKED_ICE_BRICKS);
        offerColumnRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.LARGE_PACKED_ICE_BRICKS_COLUMN, DDBlocks.LARGE_PACKED_ICE_BRICKS);
        offerColumnRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.LARGE_CRACKED_PACKED_ICE_BRICKS_COLUMN, DDBlocks.LARGE_CRACKED_PACKED_ICE_BRICKS);
        offerColumnRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.POLISHED_PACKED_ICE_COLUMN, DDBlocks.POLISHED_PACKED_ICE);
        offerColumnRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.BLUE_ICE_BRICKS_COLUMN, DDBlocks.BLUE_ICE_BRICKS);
        offerColumnRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.CRACKED_BLUE_ICE_BRICKS_COLUMN, DDBlocks.CRACKED_BLUE_ICE_BRICKS);
        offerColumnRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.LARGE_BLUE_ICE_BRICKS_COLUMN, DDBlocks.LARGE_BLUE_ICE_BRICKS);
        offerColumnRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.LARGE_CRACKED_BLUE_ICE_BRICKS_COLUMN, DDBlocks.LARGE_CRACKED_BLUE_ICE_BRICKS);
        offerColumnRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.POLISHED_BLUE_ICE_COLUMN, DDBlocks.POLISHED_BLUE_ICE);
        offerColumnRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.POLISHED_OBSIDIAN_COLUMN, DDBlocks.POLISHED_OBSIDIAN);
        offerColumnRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.WAXED_POLISHED_OBSIDIAN_COLUMN,DDBlocks.WAXED_POLISHED_OBSIDIAN);
        offerColumnRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.BONE_BLOCK_COLUMN, Blocks.BONE_BLOCK);
        offerColumnRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.DRIPSTONE_BLOCK_COLUMN, Blocks.DRIPSTONE_BLOCK);
        offerColumnRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.POLISHED_DRIPSTONE_COLUMN, DDBlocks.POLISHED_DRIPSTONE);
        offerColumnRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.DRIPSTONE_BRICKS_COLUMN, DDBlocks.DRIPSTONE_BRICKS);
        offerColumnRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.CRACKED_DRIPSTONE_BRICKS_COLUMN, DDBlocks.CRACKED_DRIPSTONE_BRICKS);
        offerColumnRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.MOSSY_DRIPSTONE_BRICKS_COLUMN, DDBlocks.MOSSY_DRIPSTONE_BRICKS);
        offerColumnRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.MOSSY_COBBLED_DEEPSLATE_COLUMN, DDBlocks.MOSSY_COBBLED_DEEPSLATE);
        offerColumnRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.MOSSY_DEEPSLATE_BRICKS_COLUMN, DDBlocks.MOSSY_DEEPSLATE_BRICKS);
        offerColumnRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.CRACKED_TUFF_BRICKS_COLUMN, DDBlocks.CRACKED_TUFF_BRICKS);
        offerColumnRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.MOSSY_TUFF_BRICKS_COLUMN, DDBlocks.MOSSY_TUFF_BRICKS);
    }

    private static void generateSmeltingRecipes(RecipeExporter exporter) {
        RecipeProvider.offerSmelting(exporter, List.of(DDBlocks.SANDSTONE_BRICKS), RecipeCategory.BUILDING_BLOCKS, DDBlocks.CRACKED_SANDSTONE_BRICKS, 0.1f, 200, "cracked_sandstone_bricks");
        RecipeProvider.offerSmelting(exporter, List.of(DDBlocks.SANDSTONE_BRICK_STAIRS), RecipeCategory.BUILDING_BLOCKS, DDBlocks.CRACKED_SANDSTONE_BRICK_STAIRS, 0.1f, 200, "cracked_sandstone_bricks");
        RecipeProvider.offerSmelting(exporter, List.of(DDBlocks.SANDSTONE_BRICK_WALL), RecipeCategory.BUILDING_BLOCKS, DDBlocks.CRACKED_SANDSTONE_BRICK_WALL, 0.1f, 200, "cracked_sandstone_bricks");
        RecipeProvider.offerSmelting(exporter, List.of(DDBlocks.SANDSTONE_BRICK_SLAB), RecipeCategory.BUILDING_BLOCKS, DDBlocks.CRACKED_SANDSTONE_BRICK_SLAB, 0.05f, 200, "cracked_sandstone_bricks");
        RecipeProvider.offerSmelting(exporter, List.of(DDBlocks.RED_SANDSTONE_BRICKS), RecipeCategory.BUILDING_BLOCKS, DDBlocks.CRACKED_RED_SANDSTONE_BRICKS, 0.1f, 200, "cracked_sandstone_bricks");
        RecipeProvider.offerSmelting(exporter, List.of(DDBlocks.RED_SANDSTONE_BRICK_STAIRS), RecipeCategory.BUILDING_BLOCKS, DDBlocks.CRACKED_RED_SANDSTONE_BRICK_STAIRS, 0.1f, 200, "cracked_sandstone_bricks");
        RecipeProvider.offerSmelting(exporter, List.of(DDBlocks.RED_SANDSTONE_BRICK_WALL), RecipeCategory.BUILDING_BLOCKS, DDBlocks.CRACKED_RED_SANDSTONE_BRICK_WALL, 0.1f, 200, "cracked_sandstone_bricks");
        RecipeProvider.offerSmelting(exporter, List.of(DDBlocks.RED_SANDSTONE_BRICK_SLAB), RecipeCategory.BUILDING_BLOCKS, DDBlocks.CRACKED_RED_SANDSTONE_BRICK_SLAB, 0.05f, 200, "cracked_sandstone_bricks");
        RecipeProvider.offerSmelting(exporter, List.of(Items.STONE_BRICK_STAIRS), RecipeCategory.BUILDING_BLOCKS, DDBlocks.CRACKED_STONE_BRICK_STAIRS, 0.1f, 200, "cracked_stone_bricks");
        RecipeProvider.offerSmelting(exporter, List.of(Items.STONE_BRICK_WALL), RecipeCategory.BUILDING_BLOCKS, DDBlocks.CRACKED_STONE_BRICK_WALL, 0.1f, 200, "cracked_stone_bricks");
        RecipeProvider.offerSmelting(exporter, List.of(Items.STONE_BRICK_SLAB), RecipeCategory.BUILDING_BLOCKS, DDBlocks.CRACKED_STONE_BRICK_SLAB, 0.05f, 200, "cracked_stone_bricks");
        RecipeProvider.offerSmelting(exporter, List.of(Items.COBBLESTONE_STAIRS), RecipeCategory.BUILDING_BLOCKS, Items.STONE_STAIRS, 0.1f, 200, "stone");
        RecipeProvider.offerSmelting(exporter, List.of(Items.COBBLESTONE_WALL), RecipeCategory.BUILDING_BLOCKS, DDBlocks.STONE_WALL, 0.1f, 200, "stone");
        RecipeProvider.offerSmelting(exporter, List.of(Items.COBBLESTONE_SLAB), RecipeCategory.BUILDING_BLOCKS, Items.STONE_SLAB, 0.05f, 200, "stone");
        RecipeProvider.offerSmelting(exporter, List.of(Items.STONE_STAIRS), RecipeCategory.BUILDING_BLOCKS, DDBlocks.SMOOTH_STONE_STAIRS, 0.1f, 200, "smooth_stone");
        RecipeProvider.offerSmelting(exporter, List.of(DDBlocks.STONE_WALL), RecipeCategory.BUILDING_BLOCKS, DDBlocks.SMOOTH_STONE_WALL, 0.1f, 200, "smooth_stone");
        RecipeProvider.offerSmelting(exporter, List.of(Items.STONE_SLAB), RecipeCategory.BUILDING_BLOCKS, Items.SMOOTH_STONE_SLAB, 0.05f, 200, "smooth_stone");
        RecipeProvider.offerSmelting(exporter, List.of(Items.SANDSTONE_STAIRS), RecipeCategory.BUILDING_BLOCKS, Items.SMOOTH_SANDSTONE_STAIRS, 0.1f, 200, "smooth_sandstone");
        RecipeProvider.offerSmelting(exporter, List.of(Items.SANDSTONE_WALL), RecipeCategory.BUILDING_BLOCKS, DDBlocks.SMOOTH_SANDSTONE_WALL, 0.1f, 200, "smooth_sandstone");
        RecipeProvider.offerSmelting(exporter, List.of(Items.SANDSTONE_SLAB), RecipeCategory.BUILDING_BLOCKS, Items.SMOOTH_SANDSTONE_SLAB, 0.05f, 200, "smooth_sandstone");
        RecipeProvider.offerSmelting(exporter, List.of(Items.RED_SANDSTONE_STAIRS), RecipeCategory.BUILDING_BLOCKS, Items.SMOOTH_RED_SANDSTONE_STAIRS, 0.1f, 200, "smooth_red_sandstone");
        RecipeProvider.offerSmelting(exporter, List.of(Items.RED_SANDSTONE_WALL), RecipeCategory.BUILDING_BLOCKS, DDBlocks.SMOOTH_RED_SANDSTONE_WALL, 0.1f, 200, "smooth_red_sandstone");
        RecipeProvider.offerSmelting(exporter, List.of(Items.RED_SANDSTONE_SLAB), RecipeCategory.BUILDING_BLOCKS, Items.SMOOTH_RED_SANDSTONE_SLAB, 0.05f, 200, "smooth_red_sandstone");
        RecipeProvider.offerSmelting(exporter, List.of(Items.BRICKS), RecipeCategory.BUILDING_BLOCKS, DDBlocks.CRACKED_BRICKS, 0.1f, 200, "cracked_bricks");
        RecipeProvider.offerSmelting(exporter, List.of(Items.BRICK_STAIRS), RecipeCategory.BUILDING_BLOCKS, DDBlocks.CRACKED_BRICK_STAIRS, 0.1f, 200, "cracked_bricks");
        RecipeProvider.offerSmelting(exporter, List.of(Items.BRICK_SLAB), RecipeCategory.BUILDING_BLOCKS, DDBlocks.CRACKED_BRICK_SLAB, 0.05f, 200, "cracked_bricks");
        RecipeProvider.offerSmelting(exporter, List.of(Items.BRICK_WALL), RecipeCategory.BUILDING_BLOCKS, DDBlocks.CRACKED_BRICK_WALL, 0.05f, 200, "cracked_bricks");
        RecipeProvider.offerSmelting(exporter, List.of(Items.RAW_GOLD_BLOCK), RecipeCategory.BUILDING_BLOCKS, Items.GOLD_BLOCK, 6.3f, 200, "raw_ore_blocks");
        RecipeProvider.offerSmelting(exporter, List.of(Items.RAW_COPPER_BLOCK), RecipeCategory.BUILDING_BLOCKS, Items.COPPER_BLOCK, 6.3f, 200, "raw_ore_blocks");
        RecipeProvider.offerSmelting(exporter, List.of(Items.RAW_IRON_BLOCK), RecipeCategory.BUILDING_BLOCKS, Items.IRON_BLOCK, 6.3f, 200, "raw_ore_blocks");
        RecipeProvider.offerSmelting(exporter, List.of(Items.GHAST_TEAR), RecipeCategory.MISC, DDItems.QUARTZ_CHUNK, 0.7f, 200, "quartz_chunk");
        RecipeProvider.offerSmelting(exporter, List.of(Items.QUARTZ_BLOCK), RecipeCategory.BUILDING_BLOCKS, Items.SMOOTH_QUARTZ, 0.1f, 200, "smooth_quartz");
        RecipeProvider.offerSmelting(exporter, List.of(Items.QUARTZ_STAIRS), RecipeCategory.BUILDING_BLOCKS, Items.SMOOTH_QUARTZ_STAIRS, 0.1f, 200, "smooth_quartz");
        RecipeProvider.offerSmelting(exporter, List.of(Items.QUARTZ_SLAB), RecipeCategory.BUILDING_BLOCKS, Items.SMOOTH_QUARTZ_SLAB, 0.05f, 200, "smooth_quartz");
        RecipeProvider.offerSmelting(exporter, List.of(DDBlocks.QUARTZ_WALL), RecipeCategory.BUILDING_BLOCKS, DDBlocks.SMOOTH_QUARTZ_WALL, 0.1f, 200, "smooth_quartz");
        RecipeProvider.offerSmelting(exporter, List.of(DDBlocks.CHARRED_QUARTZ_BLOCK), RecipeCategory.BUILDING_BLOCKS, DDBlocks.SMOOTH_CHARRED_QUARTZ_BLOCK, 0.1f, 200, "smooth_charred_quartz");
        RecipeProvider.offerSmelting(exporter, List.of(DDBlocks.CHARRED_QUARTZ_STAIRS), RecipeCategory.BUILDING_BLOCKS, DDBlocks.SMOOTH_CHARRED_QUARTZ_STAIRS, 0.1f, 200, "smooth_charred_quartz");
        RecipeProvider.offerSmelting(exporter, List.of(DDBlocks.CHARRED_QUARTZ_SLAB), RecipeCategory.BUILDING_BLOCKS, DDBlocks.SMOOTH_CHARRED_QUARTZ_SLAB, 0.05f, 200, "smooth_charred_quartz");
        RecipeProvider.offerSmelting(exporter, List.of(DDBlocks.CHARRED_QUARTZ_WALL), RecipeCategory.BUILDING_BLOCKS, DDBlocks.SMOOTH_CHARRED_QUARTZ_WALL, 0.1f, 200, "smooth_charred_quartz");
        RecipeProvider.offerSmelting(exporter, List.of(DDBlocks.ANDESITE_BRICKS), RecipeCategory.BUILDING_BLOCKS, DDBlocks.CRACKED_ANDESITE_BRICKS, 0.1f, 200, "cracked_andesite_bricks");
        RecipeProvider.offerSmelting(exporter, List.of(DDBlocks.ANDESITE_BRICK_STAIRS), RecipeCategory.BUILDING_BLOCKS, DDBlocks.CRACKED_ANDESITE_BRICK_STAIRS, 0.1f, 200, "cracked_andesite_bricks");
        RecipeProvider.offerSmelting(exporter, List.of(DDBlocks.ANDESITE_BRICK_WALL), RecipeCategory.BUILDING_BLOCKS, DDBlocks.CRACKED_ANDESITE_BRICK_WALL, 0.1f, 200, "cracked_andesite_bricks");
        RecipeProvider.offerSmelting(exporter, List.of(DDBlocks.ANDESITE_BRICK_SLAB), RecipeCategory.BUILDING_BLOCKS, DDBlocks.CRACKED_ANDESITE_BRICK_SLAB, 0.05f, 200, "cracked_andesite_bricks");
        RecipeProvider.offerSmelting(exporter, List.of(DDBlocks.DIORITE_BRICKS), RecipeCategory.BUILDING_BLOCKS, DDBlocks.CRACKED_DIORITE_BRICKS, 0.1f, 200, "cracked_diorite_bricks");
        RecipeProvider.offerSmelting(exporter, List.of(DDBlocks.DIORITE_BRICK_STAIRS), RecipeCategory.BUILDING_BLOCKS, DDBlocks.CRACKED_DIORITE_BRICK_STAIRS, 0.1f, 200, "cracked_diorite_bricks");
        RecipeProvider.offerSmelting(exporter, List.of(DDBlocks.DIORITE_BRICK_WALL), RecipeCategory.BUILDING_BLOCKS, DDBlocks.CRACKED_DIORITE_BRICK_WALL, 0.1f, 200, "cracked_diorite_bricks");
        RecipeProvider.offerSmelting(exporter, List.of(DDBlocks.DIORITE_BRICK_SLAB), RecipeCategory.BUILDING_BLOCKS, DDBlocks.CRACKED_DIORITE_BRICK_SLAB, 0.05f, 200, "cracked_diorite_bricks");
        RecipeProvider.offerSmelting(exporter, List.of(DDBlocks.GRANITE_BRICKS), RecipeCategory.BUILDING_BLOCKS, DDBlocks.CRACKED_GRANITE_BRICKS, 0.1f, 200, "cracked_granite_bricks");
        RecipeProvider.offerSmelting(exporter, List.of(DDBlocks.GRANITE_BRICK_STAIRS), RecipeCategory.BUILDING_BLOCKS, DDBlocks.CRACKED_GRANITE_BRICK_STAIRS, 0.1f, 200, "cracked_granite_bricks");
        RecipeProvider.offerSmelting(exporter, List.of(DDBlocks.GRANITE_BRICK_WALL), RecipeCategory.BUILDING_BLOCKS, DDBlocks.CRACKED_GRANITE_BRICK_WALL, 0.1f, 200, "cracked_granite_bricks");
        RecipeProvider.offerSmelting(exporter, List.of(DDBlocks.GRANITE_BRICK_SLAB), RecipeCategory.BUILDING_BLOCKS, DDBlocks.CRACKED_GRANITE_BRICK_SLAB, 0.05f, 200, "cracked_granite_bricks");
        RecipeProvider.offerSmelting(exporter, List.of(DDBlocks.CALCITE_BRICKS), RecipeCategory.BUILDING_BLOCKS, DDBlocks.CRACKED_CALCITE_BRICKS, 0.1f, 200, "cracked_calcite_bricks");
        RecipeProvider.offerSmelting(exporter, List.of(DDBlocks.CALCITE_BRICK_STAIRS), RecipeCategory.BUILDING_BLOCKS, DDBlocks.CRACKED_CALCITE_BRICK_STAIRS, 0.1f, 200, "cracked_calcite_bricks");
        RecipeProvider.offerSmelting(exporter, List.of(DDBlocks.CALCITE_BRICK_WALL), RecipeCategory.BUILDING_BLOCKS, DDBlocks.CRACKED_CALCITE_BRICK_WALL, 0.1f, 200, "cracked_calcite_bricks");
        RecipeProvider.offerSmelting(exporter, List.of(DDBlocks.CALCITE_BRICK_SLAB), RecipeCategory.BUILDING_BLOCKS, DDBlocks.CRACKED_CALCITE_BRICK_SLAB, 0.05f, 200, "cracked_calcite_bricks");
        RecipeProvider.offerSmelting(exporter, List.of(Blocks.DEEPSLATE_BRICK_STAIRS), RecipeCategory.BUILDING_BLOCKS, DDBlocks.CRACKED_DEEPSLATE_BRICK_STAIRS, 0.1f, 200, "cracked_deepslate_bricks");
        RecipeProvider.offerSmelting(exporter, List.of(Blocks.DEEPSLATE_BRICK_WALL), RecipeCategory.BUILDING_BLOCKS, DDBlocks.CRACKED_DEEPSLATE_BRICK_WALL, 0.1f, 200, "cracked_deepslate_bricks");
        RecipeProvider.offerSmelting(exporter, List.of(Blocks.DEEPSLATE_BRICK_SLAB), RecipeCategory.BUILDING_BLOCKS, DDBlocks.CRACKED_DEEPSLATE_BRICK_SLAB, 0.05f, 200, "cracked_deepslate_bricks");
        RecipeProvider.offerSmelting(exporter, List.of(Blocks.DEEPSLATE_TILE_STAIRS), RecipeCategory.BUILDING_BLOCKS, DDBlocks.CRACKED_DEEPSLATE_TILE_STAIRS, 0.1f, 200, "cracked_deepslate_tiles");
        RecipeProvider.offerSmelting(exporter, List.of(Blocks.DEEPSLATE_TILE_WALL), RecipeCategory.BUILDING_BLOCKS, DDBlocks.CRACKED_DEEPSLATE_TILE_WALL, 0.1f, 200, "cracked_deepslate_tiles");
        RecipeProvider.offerSmelting(exporter, List.of(Blocks.DEEPSLATE_TILE_SLAB), RecipeCategory.BUILDING_BLOCKS, DDBlocks.CRACKED_DEEPSLATE_TILE_SLAB, 0.05f, 200, "cracked_deepslate_tiles");
        RecipeProvider.offerSmelting(exporter, List.of(Blocks.NETHER_BRICK_STAIRS), RecipeCategory.BUILDING_BLOCKS, DDBlocks.CRACKED_NETHER_BRICK_STAIRS, 0.1f, 200, "cracked_nether_bricks");
        RecipeProvider.offerSmelting(exporter, List.of(Blocks.NETHER_BRICK_WALL), RecipeCategory.BUILDING_BLOCKS, DDBlocks.CRACKED_NETHER_BRICK_WALL, 0.1f, 200, "cracked_nether_bricks");
        RecipeProvider.offerSmelting(exporter, List.of(Blocks.NETHER_BRICK_FENCE), RecipeCategory.BUILDING_BLOCKS, DDBlocks.CRACKED_NETHER_BRICK_FENCE, 0.1f, 200, "cracked_nether_bricks");
        RecipeProvider.offerSmelting(exporter, List.of(Blocks.NETHER_BRICK_SLAB), RecipeCategory.BUILDING_BLOCKS, DDBlocks.CRACKED_NETHER_BRICK_SLAB, 0.05f, 200, "cracked_nether_bricks");
        RecipeProvider.offerSmelting(exporter, List.of(Blocks.POLISHED_BLACKSTONE_BRICK_STAIRS), RecipeCategory.BUILDING_BLOCKS, DDBlocks.CRACKED_POLISHED_BLACKSTONE_BRICK_STAIRS, 0.1f, 200, "cracked_polished_blackstone");
        RecipeProvider.offerSmelting(exporter, List.of(Blocks.POLISHED_BLACKSTONE_BRICK_WALL), RecipeCategory.BUILDING_BLOCKS, DDBlocks.CRACKED_POLISHED_BLACKSTONE_BRICK_WALL, 0.1f, 200, "cracked_polished_blackstone");
        RecipeProvider.offerSmelting(exporter, List.of(Blocks.POLISHED_BLACKSTONE_BRICK_SLAB), RecipeCategory.BUILDING_BLOCKS, DDBlocks.CRACKED_POLISHED_BLACKSTONE_BRICK_SLAB, 0.05f, 200, "cracked_polished_blackstone");
        RecipeProvider.offerSmelting(exporter, List.of(DDBlocks.COBBLESTONE_COLUMN), RecipeCategory.BUILDING_BLOCKS, DDBlocks.STONE_COLUMN, 0.1f, 200, "stone");
        RecipeProvider.offerSmelting(exporter, List.of(DDBlocks.STONE_COLUMN), RecipeCategory.BUILDING_BLOCKS, DDBlocks.SMOOTH_STONE_COLUMN, 0.1f, 200, "smooth_stone");
        RecipeProvider.offerSmelting(exporter, List.of(DDBlocks.STONE_BRICKS_COLUMN), RecipeCategory.BUILDING_BLOCKS, DDBlocks.CRACKED_STONE_BRICKS_COLUMN, 0.1f, 200, "cracked_bricks");
        RecipeProvider.offerSmelting(exporter, List.of(DDBlocks.GRANITE_BRICKS_COLUMN), RecipeCategory.BUILDING_BLOCKS, DDBlocks.CRACKED_GRANITE_BRICKS_COLUMN, 0.1f, 200, "cracked_bricks");
        RecipeProvider.offerSmelting(exporter, List.of(DDBlocks.DIORITE_BRICKS_COLUMN), RecipeCategory.BUILDING_BLOCKS, DDBlocks.CRACKED_DIORITE_BRICKS_COLUMN, 0.1f, 200, "cracked_bricks");
        RecipeProvider.offerSmelting(exporter, List.of(DDBlocks.ANDESITE_BRICKS_COLUMN), RecipeCategory.BUILDING_BLOCKS, DDBlocks.CRACKED_ANDESITE_BRICKS_COLUMN, 0.1f, 200, "cracked_bricks");
        RecipeProvider.offerSmelting(exporter, List.of(DDBlocks.CALCITE_BRICKS_COLUMN), RecipeCategory.BUILDING_BLOCKS, DDBlocks.CRACKED_CALCITE_BRICKS_COLUMN, 0.1f, 200, "cracked_bricks");
        RecipeProvider.offerSmelting(exporter, List.of(DDBlocks.DEEPSLATE_BRICKS_COLUMN), RecipeCategory.BUILDING_BLOCKS, DDBlocks.CRACKED_DEEPSLATE_BRICKS_COLUMN, 0.1f, 200, "cracked_bricks");
        RecipeProvider.offerSmelting(exporter, List.of(DDBlocks.DEEPSLATE_TILES_COLUMN), RecipeCategory.BUILDING_BLOCKS, DDBlocks.CRACKED_DEEPSLATE_TILES_COLUMN, 0.1f, 200, "cracked_bricks");
        RecipeProvider.offerSmelting(exporter, List.of(DDBlocks.BRICKS_COLUMN), RecipeCategory.BUILDING_BLOCKS, DDBlocks.CRACKED_BRICKS_COLUMN, 0.1f, 200, "cracked_bricks");
        RecipeProvider.offerSmelting(exporter, List.of(DDBlocks.SANDSTONE_COLUMN), RecipeCategory.BUILDING_BLOCKS, DDBlocks.SMOOTH_SANDSTONE_COLUMN, 0.1f, 200, "stone");
        RecipeProvider.offerSmelting(exporter, List.of(DDBlocks.SANDSTONE_BRICKS_COLUMN), RecipeCategory.BUILDING_BLOCKS, DDBlocks.CRACKED_SANDSTONE_BRICKS_COLUMN, 0.1f, 200, "cracked_bricks");
        RecipeProvider.offerSmelting(exporter, List.of(DDBlocks.RED_SANDSTONE_COLUMN), RecipeCategory.BUILDING_BLOCKS, DDBlocks.SMOOTH_RED_SANDSTONE_COLUMN, 0.1f, 200, "stone");
        RecipeProvider.offerSmelting(exporter, List.of(DDBlocks.RED_SANDSTONE_BRICKS_COLUMN), RecipeCategory.BUILDING_BLOCKS, DDBlocks.CRACKED_RED_SANDSTONE_BRICKS_COLUMN, 0.1f, 200, "cracked_bricks");
        RecipeProvider.offerSmelting(exporter, List.of(DDBlocks.BASALT_COLUMN), RecipeCategory.BUILDING_BLOCKS, DDBlocks.SMOOTH_BASALT_COLUMN, 0.1f, 200, "stone");
        RecipeProvider.offerSmelting(exporter, List.of(DDBlocks.POLISHED_BLACKSTONE_BRICKS_COLUMN), RecipeCategory.BUILDING_BLOCKS, DDBlocks.CRACKED_POLISHED_BLACKSTONE_BRICKS_COLUMN, 0.1f, 200, "cracked_bricks");
        RecipeProvider.offerSmelting(exporter, List.of(DDBlocks.QUARTZ_BLOCK_COLUMN), RecipeCategory.BUILDING_BLOCKS, DDBlocks.SMOOTH_QUARTZ_COLUMN, 0.1f, 200, "stone");
        RecipeProvider.offerSmelting(exporter, List.of(DDBlocks.CHARRED_QUARTZ_BLOCK_COLUMN), RecipeCategory.BUILDING_BLOCKS, DDBlocks.SMOOTH_CHARRED_QUARTZ_COLUMN, 0.1f, 200, "stone");
        RecipeProvider.offerSmelting(exporter, List.of(DDBlocks.DRIPSTONE_BRICKS), RecipeCategory.BUILDING_BLOCKS, DDBlocks.CRACKED_DRIPSTONE_BRICKS, 0.1f, 200, "cracked_bricks");
        RecipeProvider.offerSmelting(exporter, List.of(DDBlocks.DRIPSTONE_BRICK_STAIRS), RecipeCategory.BUILDING_BLOCKS, DDBlocks.CRACKED_DRIPSTONE_BRICK_STAIRS, 0.1f, 200, "cracked_bricks");
        RecipeProvider.offerSmelting(exporter, List.of(DDBlocks.DRIPSTONE_BRICK_SLAB), RecipeCategory.BUILDING_BLOCKS, DDBlocks.CRACKED_DRIPSTONE_BRICK_SLAB, 0.1f, 200, "cracked_bricks");
        RecipeProvider.offerSmelting(exporter, List.of(DDBlocks.DRIPSTONE_BRICK_WALL), RecipeCategory.BUILDING_BLOCKS, DDBlocks.CRACKED_DRIPSTONE_BRICK_WALL, 0.1f, 200, "cracked_bricks");
        RecipeProvider.offerSmelting(exporter, List.of(DDBlocks.DRIPSTONE_BRICKS_COLUMN), RecipeCategory.BUILDING_BLOCKS, DDBlocks.CRACKED_DRIPSTONE_BRICKS_COLUMN, 0.1f, 200, "cracked_bricks");
        RecipeProvider.offerSmelting(exporter, List.of(DDBlocks.END_REDSTONE_ORE), RecipeCategory.MISC, Items.REDSTONE, 0.7f, 200, "redstone");
        RecipeProvider.offerSmelting(exporter, List.of(DDBlocks.CRACKED_TUFF_BRICKS), RecipeCategory.BUILDING_BLOCKS, Blocks.TUFF_BRICKS, 0.1f, 200, "cracked_bricks");
        RecipeProvider.offerSmelting(exporter, List.of(DDBlocks.CRACKED_TUFF_BRICK_STAIRS), RecipeCategory.BUILDING_BLOCKS, Blocks.TUFF_BRICK_STAIRS, 0.1f, 200, "cracked_bricks");
        RecipeProvider.offerSmelting(exporter, List.of(DDBlocks.CRACKED_TUFF_BRICK_SLAB), RecipeCategory.BUILDING_BLOCKS, Blocks.TUFF_BRICK_SLAB, 0.1f, 200, "cracked_bricks");
        RecipeProvider.offerSmelting(exporter, List.of(DDBlocks.CRACKED_TUFF_BRICK_WALL), RecipeCategory.BUILDING_BLOCKS, Blocks.TUFF_BRICK_WALL, 0.1f, 200, "cracked_bricks");
        RecipeProvider.offerSmelting(exporter, List.of(DDBlocks.CRACKED_TUFF_BRICKS_COLUMN), RecipeCategory.BUILDING_BLOCKS, DDBlocks.TUFF_BRICKS_COLUMN, 0.1f, 200, "cracked_bricks");
    }

    private static void generateBlastSmeltingRecipes(RecipeExporter exporter) {
        RecipeProvider.offerBlasting(exporter, List.of(Items.RAW_GOLD_BLOCK), RecipeCategory.BUILDING_BLOCKS, Items.GOLD_BLOCK, 6.3f, 100, "raw_ore_blocks_blasting");
        RecipeProvider.offerBlasting(exporter, List.of(Items.RAW_COPPER_BLOCK), RecipeCategory.BUILDING_BLOCKS, Items.COPPER_BLOCK, 6.3f, 100, "raw_ore_blocks_blasting");
        RecipeProvider.offerBlasting(exporter, List.of(Items.RAW_IRON_BLOCK), RecipeCategory.BUILDING_BLOCKS, Items.IRON_BLOCK, 6.3f, 100, "raw_ore_blocks_blasting");
        RecipeProvider.offerBlasting(exporter, List.of(Items.QUARTZ), RecipeCategory.MISC, DDItems.CHARRED_QUARTZ, 0.7f, 100, "charred_quartz");
        RecipeProvider.offerBlasting(exporter, List.of(Items.QUARTZ_BLOCK), RecipeCategory.BUILDING_BLOCKS, DDBlocks.CHARRED_QUARTZ_BLOCK, 0.1f, 100, "charred_quartz");
        RecipeProvider.offerBlasting(exporter, List.of(Items.QUARTZ_STAIRS), RecipeCategory.BUILDING_BLOCKS, DDBlocks.CHARRED_QUARTZ_STAIRS, 0.1f, 100, "charred_quartz");
        RecipeProvider.offerBlasting(exporter, List.of(Items.QUARTZ_SLAB), RecipeCategory.BUILDING_BLOCKS, DDBlocks.CHARRED_QUARTZ_SLAB, 0.05f, 100, "charred_quartz");
        RecipeProvider.offerBlasting(exporter, List.of(DDBlocks.QUARTZ_WALL), RecipeCategory.BUILDING_BLOCKS, DDBlocks.CHARRED_QUARTZ_WALL, 0.1f, 100, "charred_quartz");
        RecipeProvider.offerBlasting(exporter, List.of(Items.SMOOTH_QUARTZ), RecipeCategory.BUILDING_BLOCKS, DDBlocks.SMOOTH_CHARRED_QUARTZ_BLOCK, 0.1f, 100, "smooth_charred_quartz");
        RecipeProvider.offerBlasting(exporter, List.of(Items.SMOOTH_QUARTZ_STAIRS), RecipeCategory.BUILDING_BLOCKS, DDBlocks.SMOOTH_CHARRED_QUARTZ_STAIRS, 0.1f, 100, "smooth_charred_quartz");
        RecipeProvider.offerBlasting(exporter, List.of(Items.SMOOTH_QUARTZ_SLAB), RecipeCategory.BUILDING_BLOCKS, DDBlocks.SMOOTH_CHARRED_QUARTZ_SLAB, 0.05f, 100, "smooth_charred_quartz");
        RecipeProvider.offerBlasting(exporter, List.of(DDBlocks.SMOOTH_QUARTZ_WALL), RecipeCategory.BUILDING_BLOCKS, DDBlocks.SMOOTH_CHARRED_QUARTZ_WALL, 0.1f, 100, "smooth_charred_quartz");
        RecipeProvider.offerBlasting(exporter, List.of(Items.QUARTZ_BRICKS), RecipeCategory.BUILDING_BLOCKS, DDBlocks.CHARRED_QUARTZ_BRICKS, 0.1f, 100, "charred_quartz_bricks");
        RecipeProvider.offerBlasting(exporter, List.of(DDBlocks.QUARTZ_BRICK_STAIRS), RecipeCategory.BUILDING_BLOCKS, DDBlocks.CHARRED_QUARTZ_BRICK_STAIRS, 0.1f, 100, "charred_quartz_bricks");
        RecipeProvider.offerBlasting(exporter, List.of(DDBlocks.QUARTZ_BRICK_SLAB), RecipeCategory.BUILDING_BLOCKS, DDBlocks.CHARRED_QUARTZ_BRICK_SLAB, 0.05f, 100, "charred_quartz_bricks");
        RecipeProvider.offerBlasting(exporter, List.of(DDBlocks.QUARTZ_BRICK_WALL), RecipeCategory.BUILDING_BLOCKS, DDBlocks.CHARRED_QUARTZ_BRICK_WALL, 0.1f, 100, "charred_quartz_bricks");
        RecipeProvider.offerBlasting(exporter, List.of(Items.CHISELED_QUARTZ_BLOCK), RecipeCategory.BUILDING_BLOCKS, DDBlocks.CHISELED_CHARRED_QUARTZ_BLOCK, 0.1f, 100, "chiseled_charred_quartz");
        RecipeProvider.offerBlasting(exporter, List.of(Items.QUARTZ_PILLAR), RecipeCategory.BUILDING_BLOCKS, DDBlocks.CHARRED_QUARTZ_PILLAR, 0.1f, 100, "quartz_pillar");
        RecipeProvider.offerBlasting(exporter, List.of(DDBlocks.ANDESITE_BRICKS), RecipeCategory.BUILDING_BLOCKS, DDBlocks.CRACKED_ANDESITE_BRICKS, 0.1f, 100, "cracked_andesite_bricks");
        RecipeProvider.offerBlasting(exporter, List.of(DDBlocks.ANDESITE_BRICK_STAIRS), RecipeCategory.BUILDING_BLOCKS, DDBlocks.CRACKED_ANDESITE_BRICK_STAIRS, 0.1f, 100, "cracked_andesite_bricks");
        RecipeProvider.offerBlasting(exporter, List.of(DDBlocks.ANDESITE_BRICK_WALL), RecipeCategory.BUILDING_BLOCKS, DDBlocks.CRACKED_ANDESITE_BRICK_WALL, 0.1f, 100, "cracked_andesite_bricks");
        RecipeProvider.offerBlasting(exporter, List.of(DDBlocks.ANDESITE_BRICK_SLAB), RecipeCategory.BUILDING_BLOCKS, DDBlocks.CRACKED_ANDESITE_BRICK_SLAB, 0.05f, 100, "cracked_andesite_bricks");
        RecipeProvider.offerBlasting(exporter, List.of(DDBlocks.DIORITE_BRICKS), RecipeCategory.BUILDING_BLOCKS, DDBlocks.CRACKED_DIORITE_BRICKS, 0.1f, 100, "cracked_diorite_bricks");
        RecipeProvider.offerBlasting(exporter, List.of(DDBlocks.DIORITE_BRICK_STAIRS), RecipeCategory.BUILDING_BLOCKS, DDBlocks.CRACKED_DIORITE_BRICK_STAIRS, 0.1f, 100, "cracked_diorite_bricks");
        RecipeProvider.offerBlasting(exporter, List.of(DDBlocks.DIORITE_BRICK_WALL), RecipeCategory.BUILDING_BLOCKS, DDBlocks.CRACKED_DIORITE_BRICK_WALL, 0.1f, 100, "cracked_diorite_bricks");
        RecipeProvider.offerBlasting(exporter, List.of(DDBlocks.DIORITE_BRICK_SLAB), RecipeCategory.BUILDING_BLOCKS, DDBlocks.CRACKED_DIORITE_BRICK_SLAB, 0.05f, 100, "cracked_diorite_bricks");
        RecipeProvider.offerBlasting(exporter, List.of(DDBlocks.GRANITE_BRICKS), RecipeCategory.BUILDING_BLOCKS, DDBlocks.CRACKED_GRANITE_BRICKS, 0.1f, 100, "cracked_granite_bricks");
        RecipeProvider.offerBlasting(exporter, List.of(DDBlocks.GRANITE_BRICK_STAIRS), RecipeCategory.BUILDING_BLOCKS, DDBlocks.CRACKED_GRANITE_BRICK_STAIRS, 0.1f, 100, "cracked_granite_bricks");
        RecipeProvider.offerBlasting(exporter, List.of(DDBlocks.GRANITE_BRICK_WALL), RecipeCategory.BUILDING_BLOCKS, DDBlocks.CRACKED_GRANITE_BRICK_WALL, 0.1f, 100, "cracked_granite_bricks");
        RecipeProvider.offerBlasting(exporter, List.of(DDBlocks.GRANITE_BRICK_SLAB), RecipeCategory.BUILDING_BLOCKS, DDBlocks.CRACKED_GRANITE_BRICK_SLAB, 0.05f, 100, "cracked_granite_bricks");
        RecipeProvider.offerBlasting(exporter, List.of(DDBlocks.CALCITE_BRICKS), RecipeCategory.BUILDING_BLOCKS, DDBlocks.CRACKED_CALCITE_BRICKS, 0.1f, 100, "cracked_calcite_bricks");
        RecipeProvider.offerBlasting(exporter, List.of(DDBlocks.CALCITE_BRICK_STAIRS), RecipeCategory.BUILDING_BLOCKS, DDBlocks.CRACKED_CALCITE_BRICK_STAIRS, 0.1f, 100, "cracked_calcite_bricks");
        RecipeProvider.offerBlasting(exporter, List.of(DDBlocks.CALCITE_BRICK_WALL), RecipeCategory.BUILDING_BLOCKS, DDBlocks.CRACKED_CALCITE_BRICK_WALL, 0.1f, 100, "cracked_calcite_bricks");
        RecipeProvider.offerBlasting(exporter, List.of(DDBlocks.CALCITE_BRICK_SLAB), RecipeCategory.BUILDING_BLOCKS, DDBlocks.CRACKED_CALCITE_BRICK_SLAB, 0.05f, 100, "cracked_calcite_bricks");
        RecipeProvider.offerBlasting(exporter, List.of(Blocks.DEEPSLATE_BRICK_STAIRS), RecipeCategory.BUILDING_BLOCKS, DDBlocks.CRACKED_DEEPSLATE_BRICK_STAIRS, 0.1f, 100, "cracked_deepslate_bricks");
        RecipeProvider.offerBlasting(exporter, List.of(Blocks.DEEPSLATE_BRICK_WALL), RecipeCategory.BUILDING_BLOCKS, DDBlocks.CRACKED_DEEPSLATE_BRICK_WALL, 0.1f, 100, "cracked_deepslate_bricks");
        RecipeProvider.offerBlasting(exporter, List.of(Blocks.DEEPSLATE_BRICK_SLAB), RecipeCategory.BUILDING_BLOCKS, DDBlocks.CRACKED_DEEPSLATE_BRICK_SLAB, 0.05f, 100, "cracked_deepslate_bricks");
        RecipeProvider.offerBlasting(exporter, List.of(Blocks.DEEPSLATE_TILE_STAIRS), RecipeCategory.BUILDING_BLOCKS, DDBlocks.CRACKED_DEEPSLATE_TILE_STAIRS, 0.1f, 100, "cracked_deepslate_tiles");
        RecipeProvider.offerBlasting(exporter, List.of(Blocks.DEEPSLATE_TILE_WALL), RecipeCategory.BUILDING_BLOCKS, DDBlocks.CRACKED_DEEPSLATE_TILE_WALL, 0.1f, 100, "cracked_deepslate_tiles");
        RecipeProvider.offerBlasting(exporter, List.of(Blocks.DEEPSLATE_TILE_SLAB), RecipeCategory.BUILDING_BLOCKS, DDBlocks.CRACKED_DEEPSLATE_TILE_SLAB, 0.05f, 100, "cracked_deepslate_tiles");
        RecipeProvider.offerBlasting(exporter, List.of(Blocks.NETHER_BRICK_STAIRS), RecipeCategory.BUILDING_BLOCKS, DDBlocks.CRACKED_NETHER_BRICK_STAIRS, 0.1f, 100, "cracked_nether_bricks");
        RecipeProvider.offerBlasting(exporter, List.of(Blocks.NETHER_BRICK_WALL), RecipeCategory.BUILDING_BLOCKS, DDBlocks.CRACKED_NETHER_BRICK_WALL, 0.1f, 100, "cracked_nether_bricks");
        RecipeProvider.offerBlasting(exporter, List.of(Blocks.NETHER_BRICK_FENCE), RecipeCategory.BUILDING_BLOCKS, DDBlocks.CRACKED_NETHER_BRICK_FENCE, 0.1f, 100, "cracked_nether_bricks");
        RecipeProvider.offerBlasting(exporter, List.of(Blocks.NETHER_BRICK_SLAB), RecipeCategory.BUILDING_BLOCKS, DDBlocks.CRACKED_NETHER_BRICK_SLAB, 0.05f, 100, "cracked_nether_bricks");
        RecipeProvider.offerBlasting(exporter, List.of(Blocks.POLISHED_BLACKSTONE_BRICK_STAIRS), RecipeCategory.BUILDING_BLOCKS, DDBlocks.CRACKED_POLISHED_BLACKSTONE_BRICK_STAIRS, 0.1f, 100, "cracked_polished_blackstone");
        RecipeProvider.offerBlasting(exporter, List.of(Blocks.POLISHED_BLACKSTONE_BRICK_WALL), RecipeCategory.BUILDING_BLOCKS, DDBlocks.CRACKED_POLISHED_BLACKSTONE_BRICK_WALL, 0.1f, 100, "cracked_polished_blackstone");
        RecipeProvider.offerBlasting(exporter, List.of(Blocks.POLISHED_BLACKSTONE_BRICK_SLAB), RecipeCategory.BUILDING_BLOCKS, DDBlocks.CRACKED_POLISHED_BLACKSTONE_BRICK_SLAB, 0.05f, 100, "cracked_polished_blackstone");
        RecipeProvider.offerBlasting(exporter, List.of(DDBlocks.COBBLESTONE_COLUMN), RecipeCategory.BUILDING_BLOCKS, DDBlocks.STONE_COLUMN, 0.1f, 100, "stone");
        RecipeProvider.offerBlasting(exporter, List.of(DDBlocks.STONE_COLUMN), RecipeCategory.BUILDING_BLOCKS, DDBlocks.SMOOTH_STONE_COLUMN, 0.1f, 100, "smooth_stone");
        RecipeProvider.offerBlasting(exporter, List.of(DDBlocks.STONE_BRICKS_COLUMN), RecipeCategory.BUILDING_BLOCKS, DDBlocks.CRACKED_STONE_BRICKS_COLUMN, 0.1f, 100, "cracked_bricks");
        RecipeProvider.offerBlasting(exporter, List.of(DDBlocks.GRANITE_BRICKS_COLUMN), RecipeCategory.BUILDING_BLOCKS, DDBlocks.CRACKED_GRANITE_BRICKS_COLUMN, 0.1f, 100, "cracked_bricks");
        RecipeProvider.offerBlasting(exporter, List.of(DDBlocks.DIORITE_BRICKS_COLUMN), RecipeCategory.BUILDING_BLOCKS, DDBlocks.CRACKED_DIORITE_BRICKS_COLUMN, 0.1f, 100, "cracked_bricks");
        RecipeProvider.offerBlasting(exporter, List.of(DDBlocks.ANDESITE_BRICKS_COLUMN), RecipeCategory.BUILDING_BLOCKS, DDBlocks.CRACKED_ANDESITE_BRICKS_COLUMN, 0.1f, 100, "cracked_bricks");
        RecipeProvider.offerBlasting(exporter, List.of(DDBlocks.CALCITE_BRICKS_COLUMN), RecipeCategory.BUILDING_BLOCKS, DDBlocks.CRACKED_CALCITE_BRICKS_COLUMN, 0.1f, 100, "cracked_bricks");
        RecipeProvider.offerBlasting(exporter, List.of(DDBlocks.DEEPSLATE_BRICKS_COLUMN), RecipeCategory.BUILDING_BLOCKS, DDBlocks.CRACKED_DEEPSLATE_BRICKS_COLUMN, 0.1f, 100, "cracked_bricks");
        RecipeProvider.offerBlasting(exporter, List.of(DDBlocks.DEEPSLATE_TILES_COLUMN), RecipeCategory.BUILDING_BLOCKS, DDBlocks.CRACKED_DEEPSLATE_TILES_COLUMN, 0.1f, 100, "cracked_bricks");
        RecipeProvider.offerBlasting(exporter, List.of(DDBlocks.BRICKS_COLUMN), RecipeCategory.BUILDING_BLOCKS, DDBlocks.CRACKED_BRICKS_COLUMN, 0.1f, 100, "cracked_bricks");
        RecipeProvider.offerBlasting(exporter, List.of(DDBlocks.SANDSTONE_COLUMN), RecipeCategory.BUILDING_BLOCKS, DDBlocks.SMOOTH_SANDSTONE_COLUMN, 0.1f, 100, "stone");
        RecipeProvider.offerBlasting(exporter, List.of(DDBlocks.SANDSTONE_BRICKS_COLUMN), RecipeCategory.BUILDING_BLOCKS, DDBlocks.CRACKED_SANDSTONE_BRICKS_COLUMN, 0.1f, 100, "cracked_bricks");
        RecipeProvider.offerBlasting(exporter, List.of(DDBlocks.RED_SANDSTONE_COLUMN), RecipeCategory.BUILDING_BLOCKS, DDBlocks.SMOOTH_RED_SANDSTONE_COLUMN, 0.1f, 100, "stone");
        RecipeProvider.offerBlasting(exporter, List.of(DDBlocks.RED_SANDSTONE_BRICKS_COLUMN), RecipeCategory.BUILDING_BLOCKS, DDBlocks.CRACKED_RED_SANDSTONE_BRICKS_COLUMN, 0.1f, 100, "cracked_bricks");
        RecipeProvider.offerBlasting(exporter, List.of(DDBlocks.BASALT_COLUMN), RecipeCategory.BUILDING_BLOCKS, DDBlocks.SMOOTH_BASALT_COLUMN, 0.1f, 100, "stone");
        RecipeProvider.offerBlasting(exporter, List.of(DDBlocks.POLISHED_BLACKSTONE_BRICKS_COLUMN), RecipeCategory.BUILDING_BLOCKS, DDBlocks.CRACKED_POLISHED_BLACKSTONE_BRICKS_COLUMN, 0.1f, 100, "cracked_bricks");
        RecipeProvider.offerBlasting(exporter, List.of(DDBlocks.QUARTZ_BLOCK_COLUMN), RecipeCategory.BUILDING_BLOCKS, DDBlocks.CHARRED_QUARTZ_BLOCK_COLUMN, 0.1f, 100, "stone");
        RecipeProvider.offerBlasting(exporter, List.of(DDBlocks.QUARTZ_BRICKS_COLUMN), RecipeCategory.BUILDING_BLOCKS, DDBlocks.CHARRED_QUARTZ_BRICKS_COLUMN, 0.1f, 100, "stone");
        RecipeProvider.offerBlasting(exporter, List.of(DDBlocks.QUARTZ_PILLAR_COLUMN), RecipeCategory.BUILDING_BLOCKS, DDBlocks.CHARRED_QUARTZ_PILLAR_COLUMN, 0.1f, 100, "stone");
        RecipeProvider.offerBlasting(exporter, List.of(DDBlocks.SMOOTH_QUARTZ_COLUMN), RecipeCategory.BUILDING_BLOCKS, DDBlocks.SMOOTH_CHARRED_QUARTZ_COLUMN, 0.1f, 100, "stone");
        RecipeProvider.offerBlasting(exporter, List.of(DDBlocks.DRIPSTONE_BRICKS), RecipeCategory.BUILDING_BLOCKS, DDBlocks.CRACKED_DRIPSTONE_BRICKS, 0.1f, 100, "cracked_bricks");
        RecipeProvider.offerBlasting(exporter, List.of(DDBlocks.DRIPSTONE_BRICK_STAIRS), RecipeCategory.BUILDING_BLOCKS, DDBlocks.CRACKED_DRIPSTONE_BRICK_STAIRS, 0.1f, 100, "cracked_bricks");
        RecipeProvider.offerBlasting(exporter, List.of(DDBlocks.DRIPSTONE_BRICK_SLAB), RecipeCategory.BUILDING_BLOCKS, DDBlocks.CRACKED_DRIPSTONE_BRICK_SLAB, 0.1f, 100, "cracked_bricks");
        RecipeProvider.offerBlasting(exporter, List.of(DDBlocks.DRIPSTONE_BRICK_WALL), RecipeCategory.BUILDING_BLOCKS, DDBlocks.CRACKED_DRIPSTONE_BRICK_WALL, 0.1f, 100, "cracked_bricks");
        RecipeProvider.offerBlasting(exporter, List.of(DDBlocks.DRIPSTONE_BRICKS_COLUMN), RecipeCategory.BUILDING_BLOCKS, DDBlocks.CRACKED_DRIPSTONE_BRICKS_COLUMN, 0.1f, 100, "cracked_bricks");
        RecipeProvider.offerBlasting(exporter, List.of(DDBlocks.END_REDSTONE_ORE), RecipeCategory.MISC, Items.REDSTONE, 0.7f, 100, "redstone");
        RecipeProvider.offerBlasting(exporter, List.of(DDBlocks.CRACKED_TUFF_BRICKS), RecipeCategory.BUILDING_BLOCKS, Blocks.TUFF_BRICKS, 0.1f, 100, "cracked_bricks");
        RecipeProvider.offerBlasting(exporter, List.of(DDBlocks.CRACKED_TUFF_BRICK_STAIRS), RecipeCategory.BUILDING_BLOCKS, Blocks.TUFF_BRICK_STAIRS, 0.1f, 100, "cracked_bricks");
        RecipeProvider.offerBlasting(exporter, List.of(DDBlocks.CRACKED_TUFF_BRICK_SLAB), RecipeCategory.BUILDING_BLOCKS, Blocks.TUFF_BRICK_SLAB, 0.1f, 100, "cracked_bricks");
        RecipeProvider.offerBlasting(exporter, List.of(DDBlocks.CRACKED_TUFF_BRICK_WALL), RecipeCategory.BUILDING_BLOCKS, Blocks.TUFF_BRICK_WALL, 0.1f, 100, "cracked_bricks");
        RecipeProvider.offerBlasting(exporter, List.of(DDBlocks.CRACKED_TUFF_BRICKS_COLUMN), RecipeCategory.BUILDING_BLOCKS, DDBlocks.TUFF_BRICKS_COLUMN, 0.1f, 100, "cracked_bricks");
    }

    private static void generateStonecuttingRecipes(RecipeExporter exporter) {
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.NETHERRACK_STAIRS, Blocks.NETHERRACK, 1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.NETHERRACK_WALL, Blocks.NETHERRACK,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.NETHERRACK_SLAB, Blocks.NETHERRACK,2);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.SMOOTH_STONE_STAIRS, Blocks.SMOOTH_STONE, 1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.SMOOTH_STONE_WALL, Blocks.SMOOTH_STONE,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.STONE_WALL, Blocks.STONE,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.POLISHED_ANDESITE_WALL, Blocks.POLISHED_ANDESITE,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.POLISHED_DIORITE_WALL, Blocks.POLISHED_DIORITE,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.POLISHED_GRANITE_WALL, Blocks.POLISHED_GRANITE,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.PRISMARINE_BRICK_WALL, Blocks.PRISMARINE_BRICKS,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.DARK_PRISMARINE_WALL, Blocks.DARK_PRISMARINE,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.PURPUR_WALL, Blocks.PURPUR_BLOCK,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.SMOOTH_SANDSTONE_WALL, Blocks.SMOOTH_SANDSTONE,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.SMOOTH_RED_SANDSTONE_WALL, Blocks.SMOOTH_RED_SANDSTONE,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.POLISHED_SANDSTONE, Blocks.SMOOTH_SANDSTONE,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.POLISHED_SANDSTONE_STAIRS, DDBlocks.POLISHED_SANDSTONE,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.POLISHED_SANDSTONE_WALL, DDBlocks.POLISHED_SANDSTONE,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.POLISHED_SANDSTONE_SLAB, DDBlocks.POLISHED_SANDSTONE,2);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.POLISHED_SANDSTONE_STAIRS, Blocks.SMOOTH_SANDSTONE,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.POLISHED_SANDSTONE_WALL, Blocks.SMOOTH_SANDSTONE,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.POLISHED_SANDSTONE_SLAB, Blocks.SMOOTH_SANDSTONE,2);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.CHISELED_POLISHED_SANDSTONE, DDBlocks.POLISHED_SANDSTONE,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.CHISELED_POLISHED_SANDSTONE, DDBlocks.SANDSTONE_BRICKS,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.SANDSTONE_BRICKS, DDBlocks.POLISHED_SANDSTONE,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.SANDSTONE_BRICK_STAIRS, DDBlocks.POLISHED_SANDSTONE,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.SANDSTONE_BRICK_STAIRS, DDBlocks.SANDSTONE_BRICKS,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.SANDSTONE_BRICK_SLAB, DDBlocks.POLISHED_SANDSTONE,2);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.SANDSTONE_BRICK_SLAB, DDBlocks.SANDSTONE_BRICKS,2);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.SANDSTONE_BRICK_WALL, DDBlocks.POLISHED_SANDSTONE,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.SANDSTONE_BRICK_WALL, DDBlocks.SANDSTONE_BRICKS,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.CRACKED_SANDSTONE_BRICK_STAIRS, DDBlocks.CRACKED_SANDSTONE_BRICKS,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.CRACKED_SANDSTONE_BRICK_SLAB, DDBlocks.CRACKED_SANDSTONE_BRICKS,2);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.CRACKED_SANDSTONE_BRICK_WALL, DDBlocks.CRACKED_SANDSTONE_BRICKS,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.MOSSY_SANDSTONE_BRICK_STAIRS, DDBlocks.MOSSY_SANDSTONE_BRICKS,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.MOSSY_SANDSTONE_BRICK_SLAB, DDBlocks.MOSSY_SANDSTONE_BRICKS,2);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.MOSSY_SANDSTONE_BRICK_WALL, DDBlocks.MOSSY_SANDSTONE_BRICKS,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.POLISHED_RED_SANDSTONE, Blocks.SMOOTH_RED_SANDSTONE,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.POLISHED_RED_SANDSTONE_STAIRS, DDBlocks.POLISHED_RED_SANDSTONE,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.POLISHED_RED_SANDSTONE_WALL, DDBlocks.POLISHED_RED_SANDSTONE,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.POLISHED_RED_SANDSTONE_SLAB, DDBlocks.POLISHED_RED_SANDSTONE,2);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.POLISHED_RED_SANDSTONE_STAIRS, Blocks.SMOOTH_RED_SANDSTONE,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.POLISHED_RED_SANDSTONE_WALL, Blocks.SMOOTH_RED_SANDSTONE,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.POLISHED_RED_SANDSTONE_SLAB, Blocks.SMOOTH_RED_SANDSTONE,2);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.CHISELED_POLISHED_RED_SANDSTONE, DDBlocks.POLISHED_RED_SANDSTONE,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.CHISELED_POLISHED_RED_SANDSTONE, DDBlocks.RED_SANDSTONE_BRICKS,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.RED_SANDSTONE_BRICKS, DDBlocks.POLISHED_RED_SANDSTONE,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.RED_SANDSTONE_BRICK_STAIRS, DDBlocks.POLISHED_RED_SANDSTONE,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.RED_SANDSTONE_BRICK_STAIRS, DDBlocks.RED_SANDSTONE_BRICKS,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.RED_SANDSTONE_BRICK_SLAB, DDBlocks.POLISHED_RED_SANDSTONE,2);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.RED_SANDSTONE_BRICK_SLAB, DDBlocks.RED_SANDSTONE_BRICKS,2);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.RED_SANDSTONE_BRICK_WALL, DDBlocks.POLISHED_RED_SANDSTONE,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.RED_SANDSTONE_BRICK_WALL, DDBlocks.RED_SANDSTONE_BRICKS,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.CRACKED_RED_SANDSTONE_BRICK_STAIRS, DDBlocks.CRACKED_RED_SANDSTONE_BRICKS,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.CRACKED_RED_SANDSTONE_BRICK_SLAB, DDBlocks.CRACKED_RED_SANDSTONE_BRICKS,2);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.CRACKED_RED_SANDSTONE_BRICK_WALL, DDBlocks.CRACKED_RED_SANDSTONE_BRICKS,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.MOSSY_RED_SANDSTONE_BRICK_STAIRS, DDBlocks.MOSSY_RED_SANDSTONE_BRICKS,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.MOSSY_RED_SANDSTONE_BRICK_SLAB, DDBlocks.MOSSY_RED_SANDSTONE_BRICKS,2);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.MOSSY_RED_SANDSTONE_BRICK_WALL, DDBlocks.MOSSY_RED_SANDSTONE_BRICKS,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.CRACKED_BRICK_STAIRS, DDBlocks.CRACKED_BRICKS,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.CRACKED_BRICK_SLAB, DDBlocks.CRACKED_BRICKS,2);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.CRACKED_BRICK_WALL, DDBlocks.CRACKED_BRICKS,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.MOSSY_BRICK_STAIRS, DDBlocks.MOSSY_BRICKS,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.MOSSY_BRICK_SLAB, DDBlocks.MOSSY_BRICKS,2);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.MOSSY_BRICK_WALL, DDBlocks.MOSSY_BRICKS,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.CHARRED_QUARTZ_STAIRS, DDBlocks.CHARRED_QUARTZ_BLOCK,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.CHARRED_QUARTZ_SLAB, DDBlocks.CHARRED_QUARTZ_BLOCK,2);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.CHARRED_QUARTZ_WALL, DDBlocks.CHARRED_QUARTZ_BLOCK,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.SMOOTH_CHARRED_QUARTZ_STAIRS, DDBlocks.SMOOTH_CHARRED_QUARTZ_BLOCK,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.SMOOTH_CHARRED_QUARTZ_SLAB, DDBlocks.SMOOTH_CHARRED_QUARTZ_BLOCK,2);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.SMOOTH_CHARRED_QUARTZ_WALL, DDBlocks.SMOOTH_CHARRED_QUARTZ_BLOCK,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.CHARRED_QUARTZ_BRICKS, DDBlocks.CHARRED_QUARTZ_BLOCK,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.CHARRED_QUARTZ_BRICK_STAIRS, DDBlocks.CHARRED_QUARTZ_BLOCK,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.CHARRED_QUARTZ_BRICK_SLAB, DDBlocks.CHARRED_QUARTZ_BLOCK,2);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.CHARRED_QUARTZ_BRICK_WALL, DDBlocks.CHARRED_QUARTZ_BLOCK,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.CHARRED_QUARTZ_BRICKS, DDBlocks.SMOOTH_CHARRED_QUARTZ_BLOCK,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.CHARRED_QUARTZ_BRICK_STAIRS, DDBlocks.SMOOTH_CHARRED_QUARTZ_BLOCK,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.CHARRED_QUARTZ_BRICK_SLAB, DDBlocks.SMOOTH_CHARRED_QUARTZ_BLOCK,2);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.CHARRED_QUARTZ_BRICK_WALL, DDBlocks.SMOOTH_CHARRED_QUARTZ_BLOCK,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.CHISELED_CHARRED_QUARTZ_BLOCK, DDBlocks.CHARRED_QUARTZ_BLOCK,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.CHARRED_QUARTZ_PILLAR, DDBlocks.CHARRED_QUARTZ_BLOCK,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.CHISELED_CHARRED_QUARTZ_BLOCK, DDBlocks.SMOOTH_CHARRED_QUARTZ_BLOCK,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.CHARRED_QUARTZ_PILLAR, DDBlocks.SMOOTH_CHARRED_QUARTZ_BLOCK,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.QUARTZ_BRICK_STAIRS, Items.QUARTZ_BRICKS,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.QUARTZ_BRICK_SLAB, Items.QUARTZ_BRICKS,2);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.QUARTZ_BRICK_WALL, Items.QUARTZ_BRICKS,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Items.QUARTZ_BRICKS, Items.SMOOTH_QUARTZ,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Items.CHISELED_QUARTZ_BLOCK, Items.SMOOTH_QUARTZ,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Items.QUARTZ_PILLAR, Items.SMOOTH_QUARTZ,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.QUARTZ_BRICK_STAIRS, Items.SMOOTH_QUARTZ,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.QUARTZ_BRICK_SLAB, Items.SMOOTH_QUARTZ,2);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.QUARTZ_BRICK_WALL, Items.SMOOTH_QUARTZ,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.QUARTZ_WALL, Items.QUARTZ_BLOCK,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.SMOOTH_QUARTZ_WALL, Items.SMOOTH_QUARTZ,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.ANDESITE_BRICKS, Blocks.ANDESITE,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.ANDESITE_BRICK_STAIRS, Blocks.ANDESITE,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.ANDESITE_BRICK_SLAB, Blocks.ANDESITE,2);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.ANDESITE_BRICK_WALL, Blocks.ANDESITE,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.ANDESITE_BRICK_STAIRS, DDBlocks.ANDESITE_BRICKS,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.ANDESITE_BRICK_SLAB, DDBlocks.ANDESITE_BRICKS,2);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.ANDESITE_BRICK_WALL, DDBlocks.ANDESITE_BRICKS,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.CRACKED_ANDESITE_BRICK_STAIRS, DDBlocks.CRACKED_ANDESITE_BRICKS,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.CRACKED_ANDESITE_BRICK_SLAB, DDBlocks.CRACKED_ANDESITE_BRICKS,2);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.CRACKED_ANDESITE_BRICK_WALL, DDBlocks.CRACKED_ANDESITE_BRICKS,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.MOSSY_ANDESITE_BRICK_STAIRS, DDBlocks.MOSSY_ANDESITE_BRICKS,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.MOSSY_ANDESITE_BRICK_SLAB, DDBlocks.MOSSY_ANDESITE_BRICKS,2);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.MOSSY_ANDESITE_BRICK_WALL, DDBlocks.MOSSY_ANDESITE_BRICKS,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.DIORITE_BRICKS, Blocks.DIORITE,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.DIORITE_BRICK_STAIRS, Blocks.DIORITE,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.DIORITE_BRICK_SLAB, Blocks.DIORITE,2);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.DIORITE_BRICK_WALL, Blocks.DIORITE,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.DIORITE_BRICK_STAIRS, DDBlocks.DIORITE_BRICKS,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.DIORITE_BRICK_SLAB, DDBlocks.DIORITE_BRICKS,2);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.DIORITE_BRICK_WALL, DDBlocks.DIORITE_BRICKS,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.CRACKED_DIORITE_BRICK_STAIRS, DDBlocks.CRACKED_DIORITE_BRICKS,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.CRACKED_DIORITE_BRICK_SLAB, DDBlocks.CRACKED_DIORITE_BRICKS,2);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.CRACKED_DIORITE_BRICK_WALL, DDBlocks.CRACKED_DIORITE_BRICKS,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.MOSSY_DIORITE_BRICK_STAIRS, DDBlocks.MOSSY_DIORITE_BRICKS,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.MOSSY_DIORITE_BRICK_SLAB, DDBlocks.MOSSY_DIORITE_BRICKS,2);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.MOSSY_DIORITE_BRICK_WALL, DDBlocks.MOSSY_DIORITE_BRICKS,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.GRANITE_BRICKS, Blocks.GRANITE,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.GRANITE_BRICK_STAIRS, Blocks.GRANITE,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.GRANITE_BRICK_SLAB, Blocks.GRANITE,2);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.GRANITE_BRICK_WALL, Blocks.GRANITE,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.GRANITE_BRICK_STAIRS, DDBlocks.GRANITE_BRICKS,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.GRANITE_BRICK_SLAB, DDBlocks.GRANITE_BRICKS,2);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.GRANITE_BRICK_WALL, DDBlocks.GRANITE_BRICKS,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.CRACKED_GRANITE_BRICK_STAIRS, DDBlocks.CRACKED_GRANITE_BRICKS,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.CRACKED_GRANITE_BRICK_SLAB, DDBlocks.CRACKED_GRANITE_BRICKS,2);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.CRACKED_GRANITE_BRICK_WALL, DDBlocks.CRACKED_GRANITE_BRICKS,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.MOSSY_GRANITE_BRICK_STAIRS, DDBlocks.MOSSY_GRANITE_BRICKS,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.MOSSY_GRANITE_BRICK_SLAB, DDBlocks.MOSSY_GRANITE_BRICKS,2);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.MOSSY_GRANITE_BRICK_WALL, DDBlocks.MOSSY_GRANITE_BRICKS,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.CALCITE_STAIRS, Blocks.CALCITE,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.CALCITE_SLAB, Blocks.CALCITE,2);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.CALCITE_WALL, Blocks.CALCITE,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.POLISHED_CALCITE, Blocks.CALCITE,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.POLISHED_CALCITE_STAIRS, Blocks.CALCITE,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.POLISHED_CALCITE_SLAB, Blocks.CALCITE,2);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.POLISHED_CALCITE_WALL, Blocks.CALCITE,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.POLISHED_CALCITE_STAIRS, DDBlocks.POLISHED_CALCITE,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.POLISHED_CALCITE_SLAB, DDBlocks.POLISHED_CALCITE,2);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.POLISHED_CALCITE_WALL, DDBlocks.POLISHED_CALCITE,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.CALCITE_BRICKS, Blocks.CALCITE,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.CALCITE_BRICK_STAIRS, Blocks.CALCITE,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.CALCITE_BRICK_SLAB, Blocks.CALCITE,2);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.CALCITE_BRICK_WALL, Blocks.CALCITE,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.CALCITE_BRICK_STAIRS, DDBlocks.CALCITE_BRICKS,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.CALCITE_BRICK_SLAB, DDBlocks.CALCITE_BRICKS,2);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.CALCITE_BRICK_WALL, DDBlocks.CALCITE_BRICKS,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.CRACKED_CALCITE_BRICK_STAIRS, DDBlocks.CRACKED_CALCITE_BRICKS,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.CRACKED_CALCITE_BRICK_SLAB, DDBlocks.CRACKED_CALCITE_BRICKS,2);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.CRACKED_CALCITE_BRICK_WALL, DDBlocks.CRACKED_CALCITE_BRICKS,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.MOSSY_CALCITE_BRICK_STAIRS, DDBlocks.MOSSY_CALCITE_BRICKS,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.MOSSY_CALCITE_BRICK_SLAB, DDBlocks.MOSSY_CALCITE_BRICKS,2);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.MOSSY_CALCITE_BRICK_WALL, DDBlocks.MOSSY_CALCITE_BRICKS,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.POLISHED_OBSIDIAN, Items.OBSIDIAN, 1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.CUT_IRON_BLOCK, Items.IRON_BLOCK,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.CUT_IRON_STAIRS, Items.IRON_BLOCK,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.CUT_IRON_SLAB, Items.IRON_BLOCK,2);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.CUT_IRON_WALL, Items.IRON_BLOCK,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.CUT_IRON_STAIRS, DDBlocks.CUT_IRON_BLOCK,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.CUT_IRON_SLAB, DDBlocks.CUT_IRON_BLOCK,2);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.CUT_IRON_WALL, DDBlocks.CUT_IRON_BLOCK,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.CUT_GOLD_BLOCK, Items.GOLD_BLOCK,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.CUT_GOLD_STAIRS, Items.GOLD_BLOCK,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.CUT_GOLD_SLAB, Items.GOLD_BLOCK,2);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.CUT_GOLD_WALL, Items.GOLD_BLOCK,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.CUT_GOLD_STAIRS, DDBlocks.CUT_GOLD_BLOCK,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.CUT_GOLD_SLAB, DDBlocks.CUT_GOLD_BLOCK,2);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.CUT_GOLD_WALL, DDBlocks.CUT_GOLD_BLOCK,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.CUT_DIAMOND_BLOCK, Items.DIAMOND_BLOCK,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.CUT_DIAMOND_STAIRS, Items.DIAMOND_BLOCK,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.CUT_DIAMOND_SLAB, Items.DIAMOND_BLOCK,2);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.CUT_DIAMOND_WALL, Items.DIAMOND_BLOCK,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.CUT_DIAMOND_STAIRS, DDBlocks.CUT_DIAMOND_BLOCK,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.CUT_DIAMOND_SLAB, DDBlocks.CUT_DIAMOND_BLOCK,2);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.CUT_DIAMOND_WALL, DDBlocks.CUT_DIAMOND_BLOCK,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.CUT_EMERALD_BLOCK, Items.EMERALD_BLOCK,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.CUT_EMERALD_STAIRS, Items.EMERALD_BLOCK,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.CUT_EMERALD_SLAB, Items.EMERALD_BLOCK,2);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.CUT_EMERALD_WALL, Items.EMERALD_BLOCK,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.CUT_EMERALD_STAIRS, DDBlocks.CUT_EMERALD_BLOCK,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.CUT_EMERALD_SLAB, DDBlocks.CUT_EMERALD_BLOCK,2);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.CUT_EMERALD_WALL, DDBlocks.CUT_EMERALD_BLOCK,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.CUT_NETHERITE_BLOCK, Items.NETHERITE_BLOCK,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.CUT_NETHERITE_STAIRS, Items.NETHERITE_BLOCK,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.CUT_NETHERITE_SLAB, Items.NETHERITE_BLOCK,2);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.CUT_NETHERITE_WALL, Items.NETHERITE_BLOCK,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.CUT_NETHERITE_STAIRS, DDBlocks.CUT_NETHERITE_BLOCK,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.CUT_NETHERITE_SLAB, DDBlocks.CUT_NETHERITE_BLOCK,2);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.CUT_NETHERITE_WALL, DDBlocks.CUT_NETHERITE_BLOCK,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.CUT_LAPIS_BLOCK, Items.LAPIS_BLOCK,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.CUT_LAPIS_STAIRS, Items.LAPIS_BLOCK,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.CUT_LAPIS_SLAB, Items.LAPIS_BLOCK,2);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.CUT_LAPIS_WALL, Items.LAPIS_BLOCK,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.CUT_LAPIS_STAIRS, DDBlocks.CUT_LAPIS_BLOCK,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.CUT_LAPIS_SLAB, DDBlocks.CUT_LAPIS_BLOCK,2);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.CUT_LAPIS_WALL, DDBlocks.CUT_LAPIS_BLOCK,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.POLISHED_AMETHYST_BLOCK, Items.AMETHYST_BLOCK,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.POLISHED_AMETHYST_WALL, Items.AMETHYST_BLOCK,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.POLISHED_AMETHYST_STAIRS, Items.AMETHYST_BLOCK,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.POLISHED_AMETHYST_SLAB, Items.AMETHYST_BLOCK,2);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.POLISHED_AMETHYST_WALL, DDBlocks.POLISHED_AMETHYST_BLOCK,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.POLISHED_AMETHYST_STAIRS, DDBlocks.POLISHED_AMETHYST_BLOCK,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.POLISHED_AMETHYST_SLAB, DDBlocks.POLISHED_AMETHYST_BLOCK,2);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.POLISHED_AMETHYST_BRICKS, Items.AMETHYST_BLOCK,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.POLISHED_AMETHYST_BRICK_WALL, Items.AMETHYST_BLOCK,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.POLISHED_AMETHYST_BRICK_SLAB, Items.AMETHYST_BLOCK,2);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.POLISHED_AMETHYST_BRICK_STAIRS, Items.AMETHYST_BLOCK,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.POLISHED_AMETHYST_BRICKS, DDBlocks.POLISHED_AMETHYST_BLOCK,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.POLISHED_AMETHYST_BRICK_WALL, DDBlocks.POLISHED_AMETHYST_BLOCK,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.POLISHED_AMETHYST_BRICK_SLAB, DDBlocks.POLISHED_AMETHYST_BLOCK,2);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.POLISHED_AMETHYST_BRICK_STAIRS, DDBlocks.POLISHED_AMETHYST_BLOCK,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.POLISHED_AMETHYST_BRICK_WALL, DDBlocks.POLISHED_AMETHYST_BRICKS,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.POLISHED_AMETHYST_BRICK_SLAB, DDBlocks.POLISHED_AMETHYST_BRICKS,2);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.POLISHED_AMETHYST_BRICK_STAIRS, DDBlocks.POLISHED_AMETHYST_BRICKS,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.CUT_POLISHED_AMETHYST, Items.AMETHYST_BLOCK,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.CUT_POLISHED_AMETHYST_WALL, Items.AMETHYST_BLOCK,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.CUT_POLISHED_AMETHYST_SLAB, Items.AMETHYST_BLOCK,2);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.CUT_POLISHED_AMETHYST_STAIRS, Items.AMETHYST_BLOCK,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.CUT_POLISHED_AMETHYST, DDBlocks.POLISHED_AMETHYST_BLOCK,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.CUT_POLISHED_AMETHYST_WALL, DDBlocks.POLISHED_AMETHYST_BLOCK,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.CUT_POLISHED_AMETHYST_SLAB, DDBlocks.POLISHED_AMETHYST_BLOCK,2);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.CUT_POLISHED_AMETHYST_STAIRS, DDBlocks.POLISHED_AMETHYST_BLOCK,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.CUT_POLISHED_AMETHYST_WALL, DDBlocks.CUT_POLISHED_AMETHYST,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.CUT_POLISHED_AMETHYST_SLAB, DDBlocks.CUT_POLISHED_AMETHYST,2);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.CUT_POLISHED_AMETHYST_STAIRS, DDBlocks.CUT_POLISHED_AMETHYST,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.CHISELED_PACKED_ICE_BRICKS, Items.PACKED_ICE,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.POLISHED_PACKED_ICE, Items.PACKED_ICE,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.POLISHED_PACKED_ICE_WALL, Items.PACKED_ICE,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.POLISHED_PACKED_ICE_SLAB, Items.PACKED_ICE,2);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.POLISHED_PACKED_ICE_STAIRS, Items.PACKED_ICE,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.PACKED_ICE_BRICKS, Items.PACKED_ICE,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.PACKED_ICE_BRICK_STAIRS, Items.PACKED_ICE,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.PACKED_ICE_BRICK_SLAB, Items.PACKED_ICE,2);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.PACKED_ICE_BRICK_WALL, Items.PACKED_ICE,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.LARGE_PACKED_ICE_BRICKS, Items.PACKED_ICE,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.LARGE_PACKED_ICE_BRICK_STAIRS, Items.PACKED_ICE,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.LARGE_PACKED_ICE_BRICK_SLAB, Items.PACKED_ICE,2);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.LARGE_PACKED_ICE_BRICK_WALL, Items.PACKED_ICE,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.LARGE_PACKED_ICE_BRICK_STAIRS, DDBlocks.LARGE_PACKED_ICE_BRICKS,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.LARGE_PACKED_ICE_BRICK_SLAB, DDBlocks.LARGE_PACKED_ICE_BRICKS,2);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.LARGE_PACKED_ICE_BRICK_WALL, DDBlocks.LARGE_PACKED_ICE_BRICKS,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.LARGE_CRACKED_PACKED_ICE_BRICK_STAIRS, DDBlocks.LARGE_CRACKED_PACKED_ICE_BRICKS,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.LARGE_CRACKED_PACKED_ICE_BRICK_SLAB, DDBlocks.LARGE_CRACKED_PACKED_ICE_BRICKS,2);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.LARGE_CRACKED_PACKED_ICE_BRICK_WALL, DDBlocks.LARGE_CRACKED_PACKED_ICE_BRICKS,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.CRACKED_PACKED_ICE_BRICK_STAIRS, DDBlocks.CRACKED_PACKED_ICE_BRICKS,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.CRACKED_PACKED_ICE_BRICK_SLAB, DDBlocks.CRACKED_PACKED_ICE_BRICKS,2);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.CRACKED_PACKED_ICE_BRICK_WALL, DDBlocks.CRACKED_PACKED_ICE_BRICKS,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.CHISELED_PACKED_ICE_BRICKS, DDBlocks.PACKED_ICE_BRICKS,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.PACKED_ICE_BRICK_STAIRS, DDBlocks.PACKED_ICE_BRICKS,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.PACKED_ICE_BRICK_SLAB, DDBlocks.PACKED_ICE_BRICKS,2);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.PACKED_ICE_BRICK_WALL, DDBlocks.PACKED_ICE_BRICKS,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.POLISHED_PACKED_ICE_STAIRS, DDBlocks.POLISHED_PACKED_ICE,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.POLISHED_PACKED_ICE_SLAB, DDBlocks.POLISHED_PACKED_ICE,2);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.POLISHED_PACKED_ICE_WALL, DDBlocks.POLISHED_PACKED_ICE,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.CHISELED_PACKED_ICE_BRICKS, DDBlocks.POLISHED_PACKED_ICE,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.LARGE_PACKED_ICE_BRICKS, DDBlocks.POLISHED_PACKED_ICE,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.LARGE_PACKED_ICE_BRICK_STAIRS, DDBlocks.POLISHED_PACKED_ICE,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.LARGE_PACKED_ICE_BRICK_SLAB, DDBlocks.POLISHED_PACKED_ICE,2);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.LARGE_PACKED_ICE_BRICK_WALL, DDBlocks.POLISHED_PACKED_ICE,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.CHISELED_BLUE_ICE_BRICKS, Items.BLUE_ICE,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.POLISHED_BLUE_ICE, Items.BLUE_ICE,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.POLISHED_BLUE_ICE_WALL, Items.BLUE_ICE,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.POLISHED_BLUE_ICE_SLAB, Items.BLUE_ICE,2);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.POLISHED_BLUE_ICE_STAIRS, Items.BLUE_ICE,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.BLUE_ICE_BRICKS, Items.BLUE_ICE,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.BLUE_ICE_BRICK_STAIRS, Items.BLUE_ICE,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.BLUE_ICE_BRICK_SLAB, Items.BLUE_ICE,2);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.BLUE_ICE_BRICK_WALL, Items.BLUE_ICE,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.LARGE_BLUE_ICE_BRICKS, Items.BLUE_ICE,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.LARGE_BLUE_ICE_BRICK_STAIRS, Items.BLUE_ICE,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.LARGE_BLUE_ICE_BRICK_SLAB, Items.BLUE_ICE,2);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.LARGE_BLUE_ICE_BRICK_WALL, Items.BLUE_ICE,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.LARGE_BLUE_ICE_BRICK_STAIRS, DDBlocks.LARGE_BLUE_ICE_BRICKS,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.LARGE_BLUE_ICE_BRICK_SLAB, DDBlocks.LARGE_BLUE_ICE_BRICKS,2);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.LARGE_BLUE_ICE_BRICK_WALL, DDBlocks.LARGE_BLUE_ICE_BRICKS,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.LARGE_CRACKED_BLUE_ICE_BRICK_STAIRS, DDBlocks.LARGE_CRACKED_BLUE_ICE_BRICKS,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.LARGE_CRACKED_BLUE_ICE_BRICK_SLAB, DDBlocks.LARGE_CRACKED_BLUE_ICE_BRICKS,2);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.LARGE_CRACKED_BLUE_ICE_BRICK_WALL, DDBlocks.LARGE_CRACKED_BLUE_ICE_BRICKS,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.CRACKED_BLUE_ICE_BRICK_STAIRS, DDBlocks.CRACKED_BLUE_ICE_BRICKS,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.CRACKED_BLUE_ICE_BRICK_SLAB, DDBlocks.CRACKED_BLUE_ICE_BRICKS,2);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.CRACKED_BLUE_ICE_BRICK_WALL, DDBlocks.CRACKED_BLUE_ICE_BRICKS,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.CHISELED_BLUE_ICE_BRICKS, DDBlocks.BLUE_ICE_BRICKS,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.BLUE_ICE_BRICK_STAIRS, DDBlocks.BLUE_ICE_BRICKS,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.BLUE_ICE_BRICK_SLAB, DDBlocks.BLUE_ICE_BRICKS,2);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.BLUE_ICE_BRICK_WALL, DDBlocks.BLUE_ICE_BRICKS,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.POLISHED_BLUE_ICE_STAIRS, DDBlocks.POLISHED_BLUE_ICE,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.POLISHED_BLUE_ICE_SLAB, DDBlocks.POLISHED_BLUE_ICE,2);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.POLISHED_BLUE_ICE_WALL, DDBlocks.POLISHED_BLUE_ICE,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.CHISELED_BLUE_ICE_BRICKS, DDBlocks.POLISHED_BLUE_ICE,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.LARGE_BLUE_ICE_BRICKS, DDBlocks.POLISHED_BLUE_ICE,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.LARGE_BLUE_ICE_BRICK_STAIRS, DDBlocks.POLISHED_BLUE_ICE,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.LARGE_BLUE_ICE_BRICK_SLAB, DDBlocks.POLISHED_BLUE_ICE,2);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.LARGE_BLUE_ICE_BRICK_WALL, DDBlocks.POLISHED_BLUE_ICE,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.POLISHED_ANDESITE_WALL, Blocks.ANDESITE,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.POLISHED_DIORITE_WALL, Blocks.DIORITE,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.POLISHED_GRANITE_WALL, Blocks.GRANITE,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.CRACKED_DEEPSLATE_BRICK_STAIRS, Blocks.CRACKED_DEEPSLATE_BRICKS,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.CRACKED_DEEPSLATE_BRICK_SLAB, Blocks.CRACKED_DEEPSLATE_BRICKS,2);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.CRACKED_DEEPSLATE_BRICK_WALL, Blocks.CRACKED_DEEPSLATE_BRICKS,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.CRACKED_DEEPSLATE_TILE_STAIRS, Blocks.CRACKED_DEEPSLATE_TILES,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.CRACKED_DEEPSLATE_TILE_SLAB, Blocks.CRACKED_DEEPSLATE_TILES,2);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.CRACKED_DEEPSLATE_TILE_WALL, Blocks.CRACKED_DEEPSLATE_TILES,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.CRACKED_NETHER_BRICK_STAIRS, Blocks.CRACKED_NETHER_BRICKS,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.CRACKED_NETHER_BRICK_SLAB, Blocks.CRACKED_NETHER_BRICKS,2);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.CRACKED_NETHER_BRICK_WALL, Blocks.CRACKED_NETHER_BRICKS,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.CRACKED_NETHER_BRICK_FENCE, Blocks.CRACKED_NETHER_BRICKS,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.NETHER_BRICK_FENCE, Blocks.NETHER_BRICKS,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.CRACKED_POLISHED_BLACKSTONE_BRICK_STAIRS, Blocks.CRACKED_POLISHED_BLACKSTONE_BRICKS,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.CRACKED_POLISHED_BLACKSTONE_BRICK_SLAB, Blocks.CRACKED_POLISHED_BLACKSTONE_BRICKS,2);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.CRACKED_POLISHED_BLACKSTONE_BRICK_WALL, Blocks.CRACKED_POLISHED_BLACKSTONE_BRICKS,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.SMOOTH_BASALT_STAIRS, Blocks.SMOOTH_BASALT,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.SMOOTH_BASALT_SLAB, Blocks.SMOOTH_BASALT,2);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.SMOOTH_BASALT_WALL, Blocks.SMOOTH_BASALT,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.COPPER_PRESSURE_PLATE, Blocks.COPPER_BLOCK,2);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.EXPOSED_COPPER_PRESSURE_PLATE, Blocks.EXPOSED_COPPER,2);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.WEATHERED_COPPER_PRESSURE_PLATE, Blocks.WEATHERED_COPPER,2);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.OXIDIZED_COPPER_PRESSURE_PLATE, Blocks.OXIDIZED_COPPER,2);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.WAXED_COPPER_PRESSURE_PLATE, Blocks.WAXED_COPPER_BLOCK,2);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.WAXED_EXPOSED_COPPER_PRESSURE_PLATE, Blocks.WAXED_EXPOSED_COPPER,2);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.WAXED_WEATHERED_COPPER_PRESSURE_PLATE, Blocks.WAXED_WEATHERED_COPPER,2);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.WAXED_OXIDIZED_COPPER_PRESSURE_PLATE, Blocks.WAXED_OXIDIZED_COPPER,2);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.STONE_COLUMN, Blocks.STONE,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.COBBLESTONE_COLUMN, Blocks.COBBLESTONE,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.MOSSY_COBBLESTONE_COLUMN, Blocks.MOSSY_COBBLESTONE,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.SMOOTH_STONE_COLUMN, Blocks.SMOOTH_STONE,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.STONE_BRICKS_COLUMN, Blocks.STONE_BRICKS,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.STONE_BRICKS_COLUMN, Blocks.STONE,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.STONE_BRICKS_COLUMN, DDBlocks.STONE_COLUMN,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.CRACKED_STONE_BRICKS_COLUMN, Blocks.CRACKED_STONE_BRICKS,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.MOSSY_STONE_BRICKS_COLUMN, Blocks.MOSSY_STONE_BRICKS,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.GRANITE_COLUMN, Blocks.GRANITE,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.POLISHED_GRANITE_COLUMN, Blocks.GRANITE,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.GRANITE_BRICKS_COLUMN, Blocks.GRANITE,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.POLISHED_GRANITE_COLUMN, Blocks.POLISHED_GRANITE,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.GRANITE_BRICKS_COLUMN, DDBlocks.GRANITE_BRICKS,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.POLISHED_GRANITE_COLUMN, DDBlocks.GRANITE_COLUMN,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.GRANITE_BRICKS_COLUMN, DDBlocks.GRANITE_COLUMN,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.GRANITE_BRICKS_COLUMN, DDBlocks.POLISHED_GRANITE_COLUMN,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.CRACKED_GRANITE_BRICKS_COLUMN, DDBlocks.CRACKED_GRANITE_BRICKS,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.MOSSY_GRANITE_BRICKS_COLUMN, DDBlocks.MOSSY_GRANITE_BRICKS,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.DIORITE_COLUMN, Blocks.DIORITE,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.POLISHED_DIORITE_COLUMN, Blocks.DIORITE,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.DIORITE_BRICKS_COLUMN, Blocks.DIORITE,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.POLISHED_DIORITE_COLUMN, Blocks.POLISHED_DIORITE,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.DIORITE_BRICKS_COLUMN, DDBlocks.DIORITE_BRICKS,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.POLISHED_DIORITE_COLUMN, DDBlocks.DIORITE_COLUMN,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.DIORITE_BRICKS_COLUMN, DDBlocks.DIORITE_COLUMN,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.DIORITE_BRICKS_COLUMN, DDBlocks.POLISHED_DIORITE_COLUMN,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.CRACKED_DIORITE_BRICKS_COLUMN, DDBlocks.CRACKED_DIORITE_BRICKS,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.MOSSY_DIORITE_BRICKS_COLUMN, DDBlocks.MOSSY_DIORITE_BRICKS,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.ANDESITE_COLUMN, Blocks.ANDESITE,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.POLISHED_ANDESITE_COLUMN, Blocks.ANDESITE,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.ANDESITE_BRICKS_COLUMN, Blocks.ANDESITE,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.POLISHED_ANDESITE_COLUMN, Blocks.POLISHED_ANDESITE,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.ANDESITE_BRICKS_COLUMN, DDBlocks.ANDESITE_BRICKS,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.POLISHED_ANDESITE_COLUMN, DDBlocks.ANDESITE_COLUMN,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.ANDESITE_BRICKS_COLUMN, DDBlocks.ANDESITE_COLUMN,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.ANDESITE_BRICKS_COLUMN, DDBlocks.POLISHED_ANDESITE_COLUMN,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.CRACKED_ANDESITE_BRICKS_COLUMN, DDBlocks.CRACKED_ANDESITE_BRICKS,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.MOSSY_ANDESITE_BRICKS_COLUMN, DDBlocks.MOSSY_ANDESITE_BRICKS,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.CALCITE_COLUMN, Blocks.CALCITE,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.POLISHED_CALCITE_COLUMN, Blocks.CALCITE,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.CALCITE_BRICKS_COLUMN, Blocks.CALCITE,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.POLISHED_CALCITE_COLUMN, DDBlocks.POLISHED_CALCITE,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.CALCITE_BRICKS_COLUMN, DDBlocks.CALCITE_BRICKS,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.POLISHED_CALCITE_COLUMN, DDBlocks.CALCITE_COLUMN,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.CALCITE_BRICKS_COLUMN, DDBlocks.CALCITE_COLUMN,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.CALCITE_BRICKS_COLUMN, DDBlocks.POLISHED_CALCITE_COLUMN,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.CRACKED_CALCITE_BRICKS_COLUMN, DDBlocks.CRACKED_CALCITE_BRICKS,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.MOSSY_CALCITE_BRICKS_COLUMN, DDBlocks.MOSSY_CALCITE_BRICKS,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.COBBLED_DEEPSLATE_COLUMN, Blocks.COBBLED_DEEPSLATE,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.POLISHED_DEEPSLATE_COLUMN, Blocks.COBBLED_DEEPSLATE,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.DEEPSLATE_BRICKS_COLUMN, Blocks.COBBLED_DEEPSLATE,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.DEEPSLATE_TILES_COLUMN, Blocks.COBBLED_DEEPSLATE,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.COBBLED_DEEPSLATE_COLUMN, DDBlocks.COBBLED_DEEPSLATE_COLUMN,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.POLISHED_DEEPSLATE_COLUMN, DDBlocks.COBBLED_DEEPSLATE_COLUMN,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.DEEPSLATE_BRICKS_COLUMN, DDBlocks.COBBLED_DEEPSLATE_COLUMN,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.DEEPSLATE_TILES_COLUMN, DDBlocks.COBBLED_DEEPSLATE_COLUMN,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.POLISHED_DEEPSLATE_COLUMN, Blocks.POLISHED_DEEPSLATE,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.DEEPSLATE_BRICKS_COLUMN, Blocks.POLISHED_DEEPSLATE,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.DEEPSLATE_TILES_COLUMN, Blocks.POLISHED_DEEPSLATE,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.POLISHED_DEEPSLATE_COLUMN, DDBlocks.POLISHED_DEEPSLATE_COLUMN,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.DEEPSLATE_BRICKS_COLUMN, DDBlocks.POLISHED_DEEPSLATE_COLUMN,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.DEEPSLATE_TILES_COLUMN, DDBlocks.POLISHED_DEEPSLATE_COLUMN,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.DEEPSLATE_BRICKS_COLUMN, Blocks.DEEPSLATE_BRICKS,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.DEEPSLATE_TILES_COLUMN, Blocks.DEEPSLATE_BRICKS,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.DEEPSLATE_TILES_COLUMN, Blocks.DEEPSLATE_TILES,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.DEEPSLATE_BRICKS_COLUMN, DDBlocks.DEEPSLATE_BRICKS_COLUMN,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.DEEPSLATE_TILES_COLUMN, DDBlocks.DEEPSLATE_BRICKS_COLUMN,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.DEEPSLATE_TILES_COLUMN, DDBlocks.DEEPSLATE_TILES_COLUMN,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.CRACKED_DEEPSLATE_BRICKS_COLUMN, Blocks.CRACKED_DEEPSLATE_BRICKS,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.CRACKED_DEEPSLATE_TILES_COLUMN, Blocks.CRACKED_DEEPSLATE_TILES,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.BRICKS_COLUMN, Blocks.BRICKS,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.CRACKED_BRICKS_COLUMN, DDBlocks.CRACKED_BRICKS,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.MOSSY_BRICKS_COLUMN, DDBlocks.MOSSY_BRICKS,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.MUD_BRICKS_COLUMN, Blocks.MUD_BRICKS,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.SANDSTONE_COLUMN, Blocks.SANDSTONE,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.SMOOTH_SANDSTONE_COLUMN, Blocks.SMOOTH_SANDSTONE,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.POLISHED_SANDSTONE_COLUMN, Blocks.SMOOTH_SANDSTONE,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.POLISHED_SANDSTONE_COLUMN, DDBlocks.POLISHED_SANDSTONE,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.POLISHED_SANDSTONE_COLUMN, DDBlocks.SMOOTH_SANDSTONE_COLUMN,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.SANDSTONE_BRICKS_COLUMN, DDBlocks.POLISHED_SANDSTONE,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.SANDSTONE_BRICKS_COLUMN, DDBlocks.SANDSTONE_BRICKS,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.SANDSTONE_BRICKS_COLUMN, DDBlocks.POLISHED_SANDSTONE_COLUMN,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.CRACKED_SANDSTONE_BRICKS_COLUMN, DDBlocks.CRACKED_SANDSTONE_BRICKS,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.MOSSY_SANDSTONE_BRICKS_COLUMN, DDBlocks.MOSSY_SANDSTONE_BRICKS,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.RED_SANDSTONE_COLUMN, Blocks.RED_SANDSTONE,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.SMOOTH_RED_SANDSTONE_COLUMN, Blocks.SMOOTH_RED_SANDSTONE,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.POLISHED_RED_SANDSTONE_COLUMN, Blocks.SMOOTH_RED_SANDSTONE,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.POLISHED_RED_SANDSTONE_COLUMN, DDBlocks.POLISHED_RED_SANDSTONE,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.POLISHED_RED_SANDSTONE_COLUMN, DDBlocks.SMOOTH_RED_SANDSTONE_COLUMN,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.RED_SANDSTONE_BRICKS_COLUMN, DDBlocks.POLISHED_RED_SANDSTONE,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.RED_SANDSTONE_BRICKS_COLUMN, DDBlocks.RED_SANDSTONE_BRICKS,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.RED_SANDSTONE_BRICKS_COLUMN, DDBlocks.POLISHED_RED_SANDSTONE_COLUMN,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.CRACKED_RED_SANDSTONE_BRICKS_COLUMN, DDBlocks.CRACKED_RED_SANDSTONE_BRICKS,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.MOSSY_RED_SANDSTONE_BRICKS_COLUMN, DDBlocks.MOSSY_RED_SANDSTONE_BRICKS,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.PRISMARINE_COLUMN, Blocks.PRISMARINE,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.PRISMARINE_BRICKS_COLUMN, Blocks.PRISMARINE_BRICKS,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.DARK_PRISMARINE_COLUMN, Blocks.DARK_PRISMARINE,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.NETHERRACK_COLUMN, Blocks.NETHERRACK,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.NETHER_BRICKS_COLUMN, Blocks.NETHER_BRICKS,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.CRACKED_NETHER_BRICKS_COLUMN, Blocks.CRACKED_NETHER_BRICKS,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.RED_NETHER_BRICKS_COLUMN, Blocks.RED_NETHER_BRICKS,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.BASALT_COLUMN, Blocks.BASALT,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.SMOOTH_BASALT_COLUMN, Blocks.SMOOTH_BASALT,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.POLISHED_BASALT_COLUMN, Blocks.BASALT,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.POLISHED_BASALT_COLUMN, Blocks.POLISHED_BASALT,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.POLISHED_BASALT_COLUMN, DDBlocks.BASALT_COLUMN,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.BLACKSTONE_COLUMN, Blocks.BLACKSTONE,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.POLISHED_BLACKSTONE_COLUMN, Blocks.BLACKSTONE,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.POLISHED_BLACKSTONE_COLUMN, Blocks.POLISHED_BLACKSTONE,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.POLISHED_BLACKSTONE_COLUMN, DDBlocks.BLACKSTONE_COLUMN,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.POLISHED_BLACKSTONE_BRICKS_COLUMN, Blocks.BLACKSTONE,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.POLISHED_BLACKSTONE_BRICKS_COLUMN, Blocks.POLISHED_BLACKSTONE,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.POLISHED_BLACKSTONE_BRICKS_COLUMN, Blocks.POLISHED_BLACKSTONE_BRICKS,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.POLISHED_BLACKSTONE_BRICKS_COLUMN, DDBlocks.BLACKSTONE_COLUMN,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.POLISHED_BLACKSTONE_BRICKS_COLUMN, DDBlocks.POLISHED_BLACKSTONE_COLUMN,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.CRACKED_POLISHED_BLACKSTONE_BRICKS_COLUMN, Blocks.CRACKED_POLISHED_BLACKSTONE_BRICKS,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.END_STONE_BRICKS_COLUMN, Blocks.END_STONE,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.END_STONE_BRICKS_COLUMN, Blocks.END_STONE_BRICKS,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.PURPUR_BLOCK_COLUMN, Blocks.PURPUR_BLOCK,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.PURPUR_PILLAR_COLUMN, Blocks.PURPUR_BLOCK,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.PURPUR_PILLAR_COLUMN, Blocks.PURPUR_PILLAR,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.CUT_IRON_COLUMN, Blocks.IRON_BLOCK,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.CUT_IRON_COLUMN, DDBlocks.CUT_IRON_BLOCK,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.CUT_GOLD_COLUMN, Blocks.GOLD_BLOCK,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.CUT_GOLD_COLUMN, DDBlocks.CUT_GOLD_BLOCK,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.CUT_EMERALD_COLUMN, Blocks.EMERALD_BLOCK,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.CUT_EMERALD_COLUMN, DDBlocks.CUT_EMERALD_BLOCK,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.CUT_LAPIS_COLUMN, Blocks.LAPIS_BLOCK,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.CUT_LAPIS_COLUMN, DDBlocks.CUT_LAPIS_BLOCK,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.CUT_DIAMOND_COLUMN, Blocks.DIAMOND_BLOCK,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.CUT_DIAMOND_COLUMN, DDBlocks.CUT_DIAMOND_BLOCK,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.CUT_NETHERITE_COLUMN, Blocks.NETHERITE_BLOCK,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.CUT_NETHERITE_COLUMN, DDBlocks.CUT_NETHERITE_BLOCK,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.QUARTZ_BLOCK_COLUMN, Blocks.QUARTZ_BLOCK,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.QUARTZ_BRICKS_COLUMN, Blocks.QUARTZ_BLOCK,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.QUARTZ_BRICKS_COLUMN, Blocks.QUARTZ_BRICKS,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.QUARTZ_BRICKS_COLUMN, DDBlocks.QUARTZ_BLOCK_COLUMN,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.QUARTZ_PILLAR_COLUMN, Blocks.QUARTZ_BLOCK,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.QUARTZ_PILLAR_COLUMN, Blocks.QUARTZ_PILLAR,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.CHARRED_QUARTZ_BLOCK_COLUMN, DDBlocks.CHARRED_QUARTZ_BLOCK,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.CHARRED_QUARTZ_BRICKS_COLUMN, DDBlocks.CHARRED_QUARTZ_BLOCK,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.CHARRED_QUARTZ_BRICKS_COLUMN, DDBlocks.CHARRED_QUARTZ_BRICKS,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.CHARRED_QUARTZ_BRICKS_COLUMN, DDBlocks.QUARTZ_BLOCK_COLUMN,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.CHARRED_QUARTZ_PILLAR_COLUMN, DDBlocks.CHARRED_QUARTZ_BLOCK,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.CHARRED_QUARTZ_PILLAR_COLUMN, DDBlocks.CHARRED_QUARTZ_PILLAR,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.POLISHED_AMETHYST_COLUMN, Blocks.AMETHYST_BLOCK,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.POLISHED_AMETHYST_BRICKS_COLUMN, Blocks.AMETHYST_BLOCK,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.CUT_POLISHED_AMETHYST_COLUMN, Blocks.AMETHYST_BLOCK,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.POLISHED_AMETHYST_COLUMN, DDBlocks.POLISHED_AMETHYST_BLOCK,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.POLISHED_AMETHYST_BRICKS_COLUMN, DDBlocks.POLISHED_AMETHYST_BLOCK,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.POLISHED_AMETHYST_BRICKS_COLUMN, DDBlocks.POLISHED_AMETHYST_BRICKS,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.POLISHED_AMETHYST_BRICKS_COLUMN, DDBlocks.POLISHED_AMETHYST_COLUMN,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.CUT_POLISHED_AMETHYST_COLUMN, DDBlocks.POLISHED_AMETHYST_BLOCK,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.CUT_POLISHED_AMETHYST_COLUMN, DDBlocks.CUT_POLISHED_AMETHYST,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.CUT_POLISHED_AMETHYST_COLUMN, DDBlocks.POLISHED_AMETHYST_COLUMN,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.CUT_COPPER_COLUMN, Blocks.CUT_COPPER,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.CUT_COPPER_COLUMN, Blocks.COPPER_BLOCK,4);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.WAXED_CUT_COPPER_COLUMN, Blocks.WAXED_CUT_COPPER,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.WAXED_CUT_COPPER_COLUMN, Blocks.WAXED_COPPER_BLOCK,4);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.EXPOSED_CUT_COPPER_COLUMN, Blocks.EXPOSED_CUT_COPPER,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.EXPOSED_CUT_COPPER_COLUMN, Blocks.EXPOSED_COPPER,4);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.WAXED_EXPOSED_CUT_COPPER_COLUMN, Blocks.WAXED_EXPOSED_CUT_COPPER,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.WAXED_EXPOSED_CUT_COPPER_COLUMN, Blocks.WAXED_EXPOSED_COPPER,4);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.WEATHERED_CUT_COPPER_COLUMN, Blocks.WEATHERED_CUT_COPPER,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.WEATHERED_CUT_COPPER_COLUMN, Blocks.WEATHERED_COPPER,4);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.WAXED_WEATHERED_CUT_COPPER_COLUMN, Blocks.WAXED_WEATHERED_CUT_COPPER,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.WAXED_WEATHERED_CUT_COPPER_COLUMN, Blocks.WAXED_WEATHERED_COPPER,4);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.OXIDIZED_CUT_COPPER_COLUMN, Blocks.OXIDIZED_CUT_COPPER,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.OXIDIZED_CUT_COPPER_COLUMN, Blocks.OXIDIZED_COPPER,4);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.WAXED_OXIDIZED_CUT_COPPER_COLUMN, Blocks.WAXED_OXIDIZED_CUT_COPPER,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.WAXED_OXIDIZED_CUT_COPPER_COLUMN, Blocks.WAXED_OXIDIZED_COPPER,4);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.PACKED_ICE_BRICKS_COLUMN, Blocks.PACKED_ICE,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.LARGE_PACKED_ICE_BRICKS_COLUMN, Blocks.PACKED_ICE,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.POLISHED_PACKED_ICE_COLUMN, Blocks.PACKED_ICE,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.PACKED_ICE_BRICKS_COLUMN, DDBlocks.PACKED_ICE_BRICKS,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.CRACKED_PACKED_ICE_BRICKS_COLUMN, DDBlocks.CRACKED_PACKED_ICE_BRICKS,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.LARGE_PACKED_ICE_BRICKS_COLUMN, DDBlocks.LARGE_PACKED_ICE_BRICKS,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.LARGE_CRACKED_PACKED_ICE_BRICKS_COLUMN, DDBlocks.LARGE_CRACKED_PACKED_ICE_BRICKS,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.LARGE_PACKED_ICE_BRICKS_COLUMN, DDBlocks.POLISHED_PACKED_ICE,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.POLISHED_PACKED_ICE_COLUMN, DDBlocks.POLISHED_PACKED_ICE,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.LARGE_PACKED_ICE_BRICKS_COLUMN, DDBlocks.POLISHED_PACKED_ICE_COLUMN,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.BLUE_ICE_BRICKS_COLUMN, Blocks.BLUE_ICE,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.LARGE_BLUE_ICE_BRICKS_COLUMN, Blocks.BLUE_ICE,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.POLISHED_BLUE_ICE_COLUMN, Blocks.BLUE_ICE,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.BLUE_ICE_BRICKS_COLUMN, DDBlocks.BLUE_ICE_BRICKS,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.CRACKED_BLUE_ICE_BRICKS_COLUMN, DDBlocks.CRACKED_BLUE_ICE_BRICKS,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.LARGE_BLUE_ICE_BRICKS_COLUMN, DDBlocks.LARGE_BLUE_ICE_BRICKS,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.LARGE_CRACKED_BLUE_ICE_BRICKS_COLUMN, DDBlocks.LARGE_CRACKED_BLUE_ICE_BRICKS,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.LARGE_BLUE_ICE_BRICKS_COLUMN, DDBlocks.POLISHED_BLUE_ICE,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.POLISHED_BLUE_ICE_COLUMN, DDBlocks.POLISHED_BLUE_ICE,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.LARGE_BLUE_ICE_BRICKS_COLUMN, DDBlocks.POLISHED_BLUE_ICE_COLUMN,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.POLISHED_OBSIDIAN_COLUMN, Blocks.OBSIDIAN,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.POLISHED_OBSIDIAN_COLUMN, DDBlocks.POLISHED_OBSIDIAN,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.WAXED_POLISHED_OBSIDIAN_COLUMN, DDBlocks.WAXED_POLISHED_OBSIDIAN,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.BONE_BLOCK_COLUMN, Blocks.BONE_BLOCK,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.DRIPSTONE_STAIRS, Blocks.DRIPSTONE_BLOCK,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.DRIPSTONE_SLAB, Blocks.DRIPSTONE_BLOCK,2);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.DRIPSTONE_WALL, Blocks.DRIPSTONE_BLOCK,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.DRIPSTONE_BLOCK_COLUMN, Blocks.DRIPSTONE_BLOCK,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.POLISHED_DRIPSTONE, Blocks.DRIPSTONE_BLOCK,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.POLISHED_DRIPSTONE_STAIRS, Blocks.DRIPSTONE_BLOCK,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.POLISHED_DRIPSTONE_SLAB, Blocks.DRIPSTONE_BLOCK,2);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.POLISHED_DRIPSTONE_WALL, Blocks.DRIPSTONE_BLOCK,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.POLISHED_DRIPSTONE_COLUMN, Blocks.DRIPSTONE_BLOCK,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.DRIPSTONE_BRICKS, Blocks.DRIPSTONE_BLOCK,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.DRIPSTONE_BRICK_STAIRS, Blocks.DRIPSTONE_BLOCK,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.DRIPSTONE_BRICK_SLAB, Blocks.DRIPSTONE_BLOCK,2);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.DRIPSTONE_BRICK_WALL, Blocks.DRIPSTONE_BLOCK,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.DRIPSTONE_BRICKS_COLUMN, Blocks.DRIPSTONE_BLOCK,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.DRIPSTONE_BRICKS, DDBlocks.POLISHED_DRIPSTONE,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.DRIPSTONE_BRICK_STAIRS, DDBlocks.POLISHED_DRIPSTONE,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.DRIPSTONE_BRICK_SLAB, DDBlocks.POLISHED_DRIPSTONE,2);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.DRIPSTONE_BRICK_WALL, DDBlocks.POLISHED_DRIPSTONE,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.DRIPSTONE_BRICKS_COLUMN, DDBlocks.POLISHED_DRIPSTONE,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.DRIPSTONE_BRICK_STAIRS, DDBlocks.DRIPSTONE_BRICKS,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.DRIPSTONE_BRICK_SLAB, DDBlocks.DRIPSTONE_BRICKS,2);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.DRIPSTONE_BRICK_WALL, DDBlocks.DRIPSTONE_BRICKS,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.DRIPSTONE_BRICKS_COLUMN, DDBlocks.DRIPSTONE_BRICKS,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.CRACKED_DRIPSTONE_BRICK_STAIRS, DDBlocks.CRACKED_DRIPSTONE_BRICKS,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.CRACKED_DRIPSTONE_BRICK_SLAB, DDBlocks.CRACKED_DRIPSTONE_BRICKS,2);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.CRACKED_DRIPSTONE_BRICK_WALL, DDBlocks.CRACKED_DRIPSTONE_BRICKS,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.CRACKED_DRIPSTONE_BRICKS_COLUMN, DDBlocks.CRACKED_DRIPSTONE_BRICKS,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.MOSSY_DRIPSTONE_BRICK_STAIRS, DDBlocks.MOSSY_DRIPSTONE_BRICKS,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.MOSSY_DRIPSTONE_BRICK_SLAB, DDBlocks.MOSSY_DRIPSTONE_BRICKS,2);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.MOSSY_DRIPSTONE_BRICK_WALL, DDBlocks.MOSSY_DRIPSTONE_BRICKS,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.MOSSY_DRIPSTONE_BRICKS_COLUMN, DDBlocks.MOSSY_DRIPSTONE_BRICKS,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.CUT_COPPER_WALL, Blocks.CUT_COPPER,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.EXPOSED_CUT_COPPER_WALL, Blocks.EXPOSED_CUT_COPPER,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.WEATHERED_CUT_COPPER_WALL, Blocks.WEATHERED_CUT_COPPER,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.OXIDIZED_CUT_COPPER_WALL, Blocks.OXIDIZED_CUT_COPPER,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.WAXED_CUT_COPPER_WALL, Blocks.WAXED_CUT_COPPER,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.WAXED_EXPOSED_CUT_COPPER_WALL, Blocks.WAXED_EXPOSED_CUT_COPPER,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.WAXED_WEATHERED_CUT_COPPER_WALL, Blocks.WAXED_WEATHERED_CUT_COPPER,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.WAXED_OXIDIZED_CUT_COPPER_WALL, Blocks.WAXED_OXIDIZED_CUT_COPPER,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.MOSSY_COBBLED_DEEPSLATE_STAIRS, DDBlocks.MOSSY_COBBLED_DEEPSLATE,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.MOSSY_COBBLED_DEEPSLATE_SLAB, DDBlocks.MOSSY_COBBLED_DEEPSLATE,2);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.MOSSY_COBBLED_DEEPSLATE_WALL, DDBlocks.MOSSY_COBBLED_DEEPSLATE,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.MOSSY_COBBLED_DEEPSLATE_COLUMN, DDBlocks.MOSSY_COBBLED_DEEPSLATE,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.MOSSY_DEEPSLATE_BRICK_STAIRS, DDBlocks.MOSSY_DEEPSLATE_BRICKS,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.MOSSY_DEEPSLATE_BRICK_SLAB, DDBlocks.MOSSY_DEEPSLATE_BRICKS,2);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.MOSSY_DEEPSLATE_BRICK_WALL, DDBlocks.MOSSY_DEEPSLATE_BRICKS,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.MOSSY_DEEPSLATE_BRICKS_COLUMN, DDBlocks.MOSSY_DEEPSLATE_BRICKS,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.CHISELED_GRANITE, Blocks.GRANITE,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.CHISELED_DIORITE, Blocks.DIORITE,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.CHISELED_ANDESITE, Blocks.ANDESITE,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.CHISELED_CALCITE, Blocks.CALCITE,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.CHISELED_DRIPSTONE, Blocks.DRIPSTONE_BLOCK,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.CHISELED_GRANITE, Blocks.POLISHED_GRANITE,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.CHISELED_DIORITE, Blocks.POLISHED_DIORITE,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.CHISELED_ANDESITE, Blocks.POLISHED_ANDESITE,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.CHISELED_CALCITE, DDBlocks.POLISHED_CALCITE,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.CHISELED_DRIPSTONE, DDBlocks.POLISHED_DRIPSTONE,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.CHISELED_GRANITE_SLAB, Blocks.GRANITE,2);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.CHISELED_DIORITE_SLAB, Blocks.DIORITE,2);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.CHISELED_ANDESITE_SLAB, Blocks.ANDESITE,2);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.CHISELED_CALCITE_SLAB, Blocks.CALCITE,2);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.CHISELED_DRIPSTONE_SLAB, Blocks.DRIPSTONE_BLOCK,2);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.CHISELED_GRANITE_SLAB, Blocks.POLISHED_GRANITE,2);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.CHISELED_DIORITE_SLAB, Blocks.POLISHED_DIORITE,2);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.CHISELED_ANDESITE_SLAB, Blocks.POLISHED_ANDESITE,2);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.CHISELED_CALCITE_SLAB, DDBlocks.POLISHED_CALCITE,2);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.CHISELED_DRIPSTONE_SLAB, DDBlocks.POLISHED_DRIPSTONE,2);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.CHISELED_GRANITE_SLAB, DDBlocks.CHISELED_GRANITE,2);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.CHISELED_DIORITE_SLAB, DDBlocks.CHISELED_DIORITE,2);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.CHISELED_ANDESITE_SLAB, DDBlocks.CHISELED_ANDESITE,2);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.CHISELED_CALCITE_SLAB, DDBlocks.CHISELED_CALCITE,2);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.CHISELED_DRIPSTONE_SLAB, DDBlocks.CHISELED_DRIPSTONE,2);

        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.TUFF_COLUMN, Blocks.TUFF,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.POLISHED_TUFF_COLUMN, Blocks.TUFF,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.TUFF_BRICKS_COLUMN, Blocks.TUFF,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.POLISHED_TUFF_COLUMN, Blocks.POLISHED_TUFF,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.TUFF_BRICKS_COLUMN, Blocks.POLISHED_TUFF,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.TUFF_BRICKS_COLUMN, Blocks.TUFF_BRICKS,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.CRACKED_TUFF_BRICK_STAIRS, DDBlocks.CRACKED_TUFF_BRICKS,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.CRACKED_TUFF_BRICK_SLAB, DDBlocks.CRACKED_TUFF_BRICKS,2);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.CRACKED_TUFF_BRICK_WALL, DDBlocks.CRACKED_TUFF_BRICKS,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.CRACKED_TUFF_BRICKS_COLUMN, DDBlocks.CRACKED_TUFF_BRICKS,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.MOSSY_TUFF_BRICK_STAIRS, DDBlocks.MOSSY_TUFF_BRICKS,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.MOSSY_TUFF_BRICK_SLAB, DDBlocks.MOSSY_TUFF_BRICKS,2);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.MOSSY_TUFF_BRICK_WALL, DDBlocks.MOSSY_TUFF_BRICKS,1);
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.MOSSY_TUFF_BRICKS_COLUMN, DDBlocks.MOSSY_TUFF_BRICKS,1);

        uncraftMetalBlock(exporter, DDBlocks.CUT_IRON_BLOCK, Items.IRON_BLOCK, Items.IRON_INGOT, 9);
        uncraftMetalBlock(exporter, DDBlocks.CUT_IRON_STAIRS, Items.IRON_INGOT, 9);
        uncraftMetalBlock(exporter, DDBlocks.CUT_IRON_SLAB, Items.IRON_INGOT, 4);
        uncraftMetalBlock(exporter, DDBlocks.CUT_IRON_WALL, Items.IRON_INGOT, 9);
        uncraftMetalBlock(exporter, DDBlocks.CUT_GOLD_BLOCK, Items.GOLD_BLOCK, Items.GOLD_INGOT, 9);
        uncraftMetalBlock(exporter, DDBlocks.CUT_GOLD_STAIRS, Items.GOLD_INGOT, 9);
        uncraftMetalBlock(exporter, DDBlocks.CUT_GOLD_SLAB, Items.GOLD_INGOT, 4);
        uncraftMetalBlock(exporter, DDBlocks.CUT_GOLD_WALL, Items.GOLD_INGOT, 9);
        uncraftMetalBlock(exporter, DDBlocks.CUT_DIAMOND_BLOCK, Items.DIAMOND_BLOCK, Items.DIAMOND, 9);
        uncraftMetalBlock(exporter, DDBlocks.CUT_DIAMOND_STAIRS, Items.DIAMOND, 9);
        uncraftMetalBlock(exporter, DDBlocks.CUT_DIAMOND_SLAB, Items.DIAMOND, 4);
        uncraftMetalBlock(exporter, DDBlocks.CUT_DIAMOND_WALL, Items.DIAMOND, 9);
        uncraftMetalBlock(exporter, DDBlocks.CUT_EMERALD_BLOCK, Items.EMERALD_BLOCK, Items.EMERALD, 9);
        uncraftMetalBlock(exporter, DDBlocks.CUT_EMERALD_STAIRS, Items.EMERALD, 9);
        uncraftMetalBlock(exporter, DDBlocks.CUT_EMERALD_SLAB, Items.EMERALD, 4);
        uncraftMetalBlock(exporter, DDBlocks.CUT_EMERALD_WALL, Items.EMERALD, 9);
        uncraftMetalBlock(exporter, DDBlocks.CUT_NETHERITE_BLOCK, Items.NETHERITE_BLOCK, Items.NETHERITE_INGOT, 9);
        uncraftMetalBlock(exporter, DDBlocks.CUT_NETHERITE_STAIRS, Items.NETHERITE_INGOT, 9);
        uncraftMetalBlock(exporter, DDBlocks.CUT_NETHERITE_SLAB, Items.NETHERITE_INGOT, 4);
        uncraftMetalBlock(exporter, DDBlocks.CUT_NETHERITE_WALL, Items.NETHERITE_INGOT, 9);
        uncraftMetalBlock(exporter, DDBlocks.CUT_LAPIS_BLOCK, Items.LAPIS_BLOCK, Items.LAPIS_LAZULI, 9);
        uncraftMetalBlock(exporter, DDBlocks.CUT_LAPIS_STAIRS, Items.LAPIS_LAZULI, 9);
        uncraftMetalBlock(exporter, DDBlocks.CUT_LAPIS_SLAB, Items.LAPIS_LAZULI, 4);
        uncraftMetalBlock(exporter, DDBlocks.CUT_LAPIS_WALL, Items.LAPIS_LAZULI, 9);
        uncraftMetalBlock(exporter, DDBlocks.POLISHED_AMETHYST_BLOCK, Items.AMETHYST_BLOCK, Items.AMETHYST_SHARD, 4);
        uncraftMetalBlock(exporter, DDBlocks.POLISHED_AMETHYST_STAIRS, Items.AMETHYST_SHARD, 4);
        uncraftMetalBlock(exporter, DDBlocks.POLISHED_AMETHYST_SLAB, Items.AMETHYST_SHARD, 2);
        uncraftMetalBlock(exporter, DDBlocks.POLISHED_AMETHYST_WALL, Items.AMETHYST_SHARD, 4);
        uncraftMetalBlock(exporter, DDBlocks.POLISHED_AMETHYST_BRICKS, Items.AMETHYST_SHARD, 4);
        uncraftMetalBlock(exporter, DDBlocks.POLISHED_AMETHYST_BRICK_STAIRS, Items.AMETHYST_SHARD, 4);
        uncraftMetalBlock(exporter, DDBlocks.POLISHED_AMETHYST_BRICK_SLAB, Items.AMETHYST_SHARD, 2);
        uncraftMetalBlock(exporter, DDBlocks.POLISHED_AMETHYST_BRICK_WALL, Items.AMETHYST_SHARD, 4);
        uncraftMetalBlock(exporter, DDBlocks.CUT_POLISHED_AMETHYST, Items.AMETHYST_SHARD, 4);
        uncraftMetalBlock(exporter, DDBlocks.CUT_POLISHED_AMETHYST_STAIRS, Items.AMETHYST_SHARD, 4);
        uncraftMetalBlock(exporter, DDBlocks.CUT_POLISHED_AMETHYST_SLAB, Items.AMETHYST_SHARD, 2);
        uncraftMetalBlock(exporter, DDBlocks.CUT_POLISHED_AMETHYST_WALL, Items.AMETHYST_SHARD, 4);
        uncraftMetalBlock(exporter, DDBlocks.CUT_IRON_COLUMN, Items.IRON_INGOT, 9);
        uncraftMetalBlock(exporter, DDBlocks.CUT_GOLD_COLUMN, Items.GOLD_INGOT, 9);
        uncraftMetalBlock(exporter, DDBlocks.CUT_EMERALD_COLUMN, Items.EMERALD, 9);
        uncraftMetalBlock(exporter, DDBlocks.CUT_LAPIS_COLUMN, Items.LAPIS_LAZULI, 9);
        uncraftMetalBlock(exporter, DDBlocks.CUT_DIAMOND_COLUMN, Items.DIAMOND, 9);
        uncraftMetalBlock(exporter, DDBlocks.CUT_NETHERITE_COLUMN, Items.NETHERITE_INGOT, 9);
        uncraftMetalBlock(exporter, DDBlocks.POLISHED_AMETHYST_COLUMN, Items.AMETHYST_SHARD, 4);
        uncraftMetalBlock(exporter, DDBlocks.POLISHED_AMETHYST_BRICKS_COLUMN, Items.AMETHYST_SHARD, 4);
        uncraftMetalBlock(exporter, DDBlocks.CUT_POLISHED_AMETHYST_COLUMN, Items.AMETHYST_SHARD, 4);

        dequartzify(exporter, Items.QUARTZ_BLOCK, 4, false);
        dequartzify(exporter, Items.SMOOTH_QUARTZ, 4, false);
        dequartzify(exporter, Items.QUARTZ_PILLAR, 4, false);
        dequartzify(exporter, Items.CHISELED_QUARTZ_BLOCK, 4, false);
        dequartzify(exporter, Items.QUARTZ_BRICKS, 4, false);
        dequartzify(exporter, Items.QUARTZ_SLAB, 2, false);
        dequartzify(exporter, Items.QUARTZ_STAIRS, 4, false);
        dequartzify(exporter, DDBlocks.QUARTZ_WALL, 4, false);
        dequartzify(exporter, Items.SMOOTH_QUARTZ_SLAB, 2, false);
        dequartzify(exporter, Items.SMOOTH_QUARTZ_STAIRS, 4, false);
        dequartzify(exporter, DDBlocks.SMOOTH_QUARTZ_WALL, 4, false);
        dequartzify(exporter, DDBlocks.QUARTZ_BRICK_SLAB, 2, false);
        dequartzify(exporter, DDBlocks.QUARTZ_BRICK_STAIRS, 4, false);
        dequartzify(exporter, DDBlocks.QUARTZ_BRICK_WALL, 4, false);

        dequartzify(exporter, DDBlocks.CHARRED_QUARTZ_BLOCK, 4, true);
        dequartzify(exporter, DDBlocks.CHARRED_QUARTZ_SLAB, 2, true);
        dequartzify(exporter, DDBlocks.CHARRED_QUARTZ_STAIRS, 4, true);
        dequartzify(exporter, DDBlocks.CHARRED_QUARTZ_WALL, 4, true);
        dequartzify(exporter, DDBlocks.SMOOTH_CHARRED_QUARTZ_BLOCK, 4, true);
        dequartzify(exporter, DDBlocks.SMOOTH_CHARRED_QUARTZ_SLAB, 2, true);
        dequartzify(exporter, DDBlocks.SMOOTH_CHARRED_QUARTZ_STAIRS, 4, true);
        dequartzify(exporter, DDBlocks.SMOOTH_CHARRED_QUARTZ_WALL, 4, true);
        dequartzify(exporter, DDBlocks.CHARRED_QUARTZ_BRICKS, 4, true);
        dequartzify(exporter, DDBlocks.CHARRED_QUARTZ_BRICK_SLAB, 2, true);
        dequartzify(exporter, DDBlocks.CHARRED_QUARTZ_BRICK_STAIRS, 4, true);
        dequartzify(exporter, DDBlocks.CHARRED_QUARTZ_BRICK_WALL, 4, true);
        dequartzify(exporter, DDBlocks.CHISELED_CHARRED_QUARTZ_BLOCK, 4, true);
        dequartzify(exporter, DDBlocks.CHARRED_QUARTZ_PILLAR, 4, true);
        dequartzify(exporter, DDBlocks.QUARTZ_BLOCK_COLUMN, 4, false);
        dequartzify(exporter, DDBlocks.QUARTZ_BRICKS_COLUMN, 4, false);
        dequartzify(exporter, DDBlocks.QUARTZ_PILLAR_COLUMN, 4, false);
        dequartzify(exporter, DDBlocks.SMOOTH_QUARTZ_COLUMN, 4, false);
        dequartzify(exporter, DDBlocks.CHARRED_QUARTZ_BLOCK_COLUMN, 4, true);
        dequartzify(exporter, DDBlocks.CHARRED_QUARTZ_BRICKS_COLUMN, 4, true);
        dequartzify(exporter, DDBlocks.CHARRED_QUARTZ_PILLAR_COLUMN, 4, true);
        dequartzify(exporter, DDBlocks.SMOOTH_CHARRED_QUARTZ_COLUMN, 4, true);
    }

    private static void generateWoodcuttingRecipes(RecipeExporter exporter) {
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.OAK_PLANKS, Blocks.OAK_LOG, 4);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.OAK_PLANKS, Blocks.STRIPPED_OAK_LOG, 4);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.OAK_PLANKS, Blocks.OAK_WOOD, 4);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.OAK_PLANKS, Blocks.STRIPPED_OAK_WOOD, 4);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.OAK_STAIRS, Blocks.OAK_LOG, 4);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.OAK_STAIRS, Blocks.STRIPPED_OAK_LOG, 4);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.OAK_STAIRS, Blocks.OAK_WOOD, 4);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.OAK_STAIRS, Blocks.STRIPPED_OAK_WOOD, 4);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.OAK_STAIRS, Blocks.OAK_PLANKS, 1);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.OAK_SLAB, Blocks.OAK_LOG, 8);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.OAK_SLAB, Blocks.STRIPPED_OAK_LOG, 8);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.OAK_SLAB, Blocks.OAK_WOOD, 8);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.OAK_SLAB, Blocks.STRIPPED_OAK_WOOD, 8);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.OAK_SLAB, Blocks.OAK_PLANKS, 2);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.OAK_FENCE, Blocks.OAK_LOG, 16);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.OAK_FENCE, Blocks.STRIPPED_OAK_LOG, 16);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.OAK_FENCE, Blocks.OAK_WOOD, 16);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.OAK_FENCE, Blocks.STRIPPED_OAK_WOOD, 16);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.OAK_FENCE, Blocks.OAK_PLANKS, 4);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.OAK_FENCE_GATE, Blocks.OAK_LOG, 8);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.OAK_FENCE_GATE, Blocks.STRIPPED_OAK_LOG, 8);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.OAK_FENCE_GATE, Blocks.OAK_WOOD, 8);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.OAK_FENCE_GATE, Blocks.STRIPPED_OAK_WOOD, 8);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.OAK_FENCE_GATE, Blocks.OAK_PLANKS, 2);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.STRIPPED_OAK_LOG, Blocks.OAK_LOG, 1);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.OAK_WOOD, Blocks.OAK_LOG, 1);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.STRIPPED_OAK_WOOD, Blocks.OAK_LOG, 1);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.STRIPPED_OAK_WOOD, Blocks.OAK_WOOD, 1);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.STRIPPED_OAK_WOOD, Blocks.STRIPPED_OAK_LOG, 1);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.OAK_BUTTON, Blocks.OAK_LOG, 4);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.OAK_BUTTON, Blocks.STRIPPED_OAK_LOG, 4);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.OAK_BUTTON, Blocks.OAK_WOOD, 4);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.OAK_BUTTON, Blocks.STRIPPED_OAK_WOOD, 4);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.OAK_BUTTON, Blocks.OAK_PLANKS, 1);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.OAK_PRESSURE_PLATE, Blocks.OAK_LOG, 8);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.OAK_PRESSURE_PLATE, Blocks.STRIPPED_OAK_LOG, 8);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.OAK_PRESSURE_PLATE, Blocks.OAK_WOOD, 8);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.OAK_PRESSURE_PLATE, Blocks.STRIPPED_OAK_WOOD, 8);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.OAK_PRESSURE_PLATE, Blocks.OAK_PLANKS, 2);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.OAK_TRAPDOOR, Blocks.OAK_LOG, 12);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.OAK_TRAPDOOR, Blocks.STRIPPED_OAK_LOG, 12);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.OAK_TRAPDOOR, Blocks.OAK_WOOD, 12);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.OAK_TRAPDOOR, Blocks.STRIPPED_OAK_WOOD, 12);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.OAK_TRAPDOOR, Blocks.OAK_PLANKS, 3);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.OAK_DOOR, Blocks.OAK_LOG, 8);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.OAK_DOOR, Blocks.STRIPPED_OAK_LOG, 8);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.OAK_DOOR, Blocks.OAK_WOOD, 8);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.OAK_DOOR, Blocks.STRIPPED_OAK_WOOD, 8);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.OAK_DOOR, Blocks.OAK_PLANKS, 2);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Items.STICK, Blocks.OAK_LOG, 8);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Items.STICK, Blocks.STRIPPED_OAK_LOG, 8);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Items.STICK, Blocks.OAK_WOOD, 8);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Items.STICK, Blocks.STRIPPED_OAK_WOOD, 8);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Items.STICK, Blocks.OAK_PLANKS, 2);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.BIRCH_PLANKS, Blocks.BIRCH_LOG, 4);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.BIRCH_PLANKS, Blocks.STRIPPED_BIRCH_LOG, 4);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.BIRCH_PLANKS, Blocks.BIRCH_WOOD, 4);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.BIRCH_PLANKS, Blocks.STRIPPED_BIRCH_WOOD, 4);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.BIRCH_STAIRS, Blocks.BIRCH_LOG, 4);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.BIRCH_STAIRS, Blocks.STRIPPED_BIRCH_LOG, 4);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.BIRCH_STAIRS, Blocks.BIRCH_WOOD, 4);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.BIRCH_STAIRS, Blocks.STRIPPED_BIRCH_WOOD, 4);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.BIRCH_STAIRS, Blocks.BIRCH_PLANKS, 1);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.BIRCH_SLAB, Blocks.BIRCH_LOG, 8);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.BIRCH_SLAB, Blocks.STRIPPED_BIRCH_LOG, 8);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.BIRCH_SLAB, Blocks.BIRCH_WOOD, 8);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.BIRCH_SLAB, Blocks.STRIPPED_BIRCH_WOOD, 8);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.BIRCH_SLAB, Blocks.BIRCH_PLANKS, 2);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.BIRCH_FENCE, Blocks.BIRCH_LOG, 16);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.BIRCH_FENCE, Blocks.STRIPPED_BIRCH_LOG, 16);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.BIRCH_FENCE, Blocks.BIRCH_WOOD, 16);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.BIRCH_FENCE, Blocks.STRIPPED_BIRCH_WOOD, 16);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.BIRCH_FENCE, Blocks.BIRCH_PLANKS, 4);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.BIRCH_FENCE_GATE, Blocks.BIRCH_LOG, 8);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.BIRCH_FENCE_GATE, Blocks.STRIPPED_BIRCH_LOG, 8);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.BIRCH_FENCE_GATE, Blocks.BIRCH_WOOD, 8);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.BIRCH_FENCE_GATE, Blocks.STRIPPED_BIRCH_WOOD, 8);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.BIRCH_FENCE_GATE, Blocks.BIRCH_PLANKS, 2);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.STRIPPED_BIRCH_LOG, Blocks.BIRCH_LOG, 1);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.BIRCH_WOOD, Blocks.BIRCH_LOG, 1);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.STRIPPED_BIRCH_WOOD, Blocks.BIRCH_LOG, 1);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.STRIPPED_BIRCH_WOOD, Blocks.BIRCH_WOOD, 1);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.STRIPPED_BIRCH_WOOD, Blocks.STRIPPED_BIRCH_LOG, 1);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.BIRCH_BUTTON, Blocks.BIRCH_LOG, 4);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.BIRCH_BUTTON, Blocks.STRIPPED_BIRCH_LOG, 4);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.BIRCH_BUTTON, Blocks.BIRCH_WOOD, 4);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.BIRCH_BUTTON, Blocks.STRIPPED_BIRCH_WOOD, 4);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.BIRCH_BUTTON, Blocks.BIRCH_PLANKS, 1);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.BIRCH_PRESSURE_PLATE, Blocks.BIRCH_LOG, 8);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.BIRCH_PRESSURE_PLATE, Blocks.STRIPPED_BIRCH_LOG, 8);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.BIRCH_PRESSURE_PLATE, Blocks.BIRCH_WOOD, 8);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.BIRCH_PRESSURE_PLATE, Blocks.STRIPPED_BIRCH_WOOD, 8);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.BIRCH_PRESSURE_PLATE, Blocks.BIRCH_PLANKS, 2);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.BIRCH_TRAPDOOR, Blocks.BIRCH_LOG, 12);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.BIRCH_TRAPDOOR, Blocks.STRIPPED_BIRCH_LOG, 12);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.BIRCH_TRAPDOOR, Blocks.BIRCH_WOOD, 12);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.BIRCH_TRAPDOOR, Blocks.STRIPPED_BIRCH_WOOD, 12);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.BIRCH_TRAPDOOR, Blocks.BIRCH_PLANKS, 3);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.BIRCH_DOOR, Blocks.BIRCH_LOG, 8);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.BIRCH_DOOR, Blocks.STRIPPED_BIRCH_LOG, 8);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.BIRCH_DOOR, Blocks.BIRCH_WOOD, 8);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.BIRCH_DOOR, Blocks.STRIPPED_BIRCH_WOOD, 8);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.BIRCH_DOOR, Blocks.BIRCH_PLANKS, 2);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Items.STICK, Blocks.BIRCH_LOG, 8);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Items.STICK, Blocks.STRIPPED_BIRCH_LOG, 8);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Items.STICK, Blocks.BIRCH_WOOD, 8);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Items.STICK, Blocks.STRIPPED_BIRCH_WOOD, 8);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Items.STICK, Blocks.BIRCH_PLANKS, 2);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.SPRUCE_PLANKS, Blocks.SPRUCE_LOG, 4);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.SPRUCE_PLANKS, Blocks.STRIPPED_SPRUCE_LOG, 4);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.SPRUCE_PLANKS, Blocks.SPRUCE_WOOD, 4);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.SPRUCE_PLANKS, Blocks.STRIPPED_SPRUCE_WOOD, 4);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.SPRUCE_STAIRS, Blocks.SPRUCE_LOG, 4);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.SPRUCE_STAIRS, Blocks.STRIPPED_SPRUCE_LOG, 4);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.SPRUCE_STAIRS, Blocks.SPRUCE_WOOD, 4);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.SPRUCE_STAIRS, Blocks.STRIPPED_SPRUCE_WOOD, 4);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.SPRUCE_STAIRS, Blocks.SPRUCE_PLANKS, 1);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.SPRUCE_SLAB, Blocks.SPRUCE_LOG, 8);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.SPRUCE_SLAB, Blocks.STRIPPED_SPRUCE_LOG, 8);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.SPRUCE_SLAB, Blocks.SPRUCE_WOOD, 8);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.SPRUCE_SLAB, Blocks.STRIPPED_SPRUCE_WOOD, 8);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.SPRUCE_SLAB, Blocks.SPRUCE_PLANKS, 2);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.SPRUCE_FENCE, Blocks.SPRUCE_LOG, 16);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.SPRUCE_FENCE, Blocks.STRIPPED_SPRUCE_LOG, 16);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.SPRUCE_FENCE, Blocks.SPRUCE_WOOD, 16);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.SPRUCE_FENCE, Blocks.STRIPPED_SPRUCE_WOOD, 16);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.SPRUCE_FENCE, Blocks.SPRUCE_PLANKS, 4);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.SPRUCE_FENCE_GATE, Blocks.SPRUCE_LOG, 8);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.SPRUCE_FENCE_GATE, Blocks.STRIPPED_SPRUCE_LOG, 8);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.SPRUCE_FENCE_GATE, Blocks.SPRUCE_WOOD, 8);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.SPRUCE_FENCE_GATE, Blocks.STRIPPED_SPRUCE_WOOD, 8);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.SPRUCE_FENCE_GATE, Blocks.SPRUCE_PLANKS, 2);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.STRIPPED_SPRUCE_LOG, Blocks.SPRUCE_LOG, 1);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.SPRUCE_WOOD, Blocks.SPRUCE_LOG, 1);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.STRIPPED_SPRUCE_WOOD, Blocks.SPRUCE_LOG, 1);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.STRIPPED_SPRUCE_WOOD, Blocks.SPRUCE_WOOD, 1);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.STRIPPED_SPRUCE_WOOD, Blocks.STRIPPED_SPRUCE_LOG, 1);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.SPRUCE_BUTTON, Blocks.SPRUCE_LOG, 4);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.SPRUCE_BUTTON, Blocks.STRIPPED_SPRUCE_LOG, 4);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.SPRUCE_BUTTON, Blocks.SPRUCE_WOOD, 4);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.SPRUCE_BUTTON, Blocks.STRIPPED_SPRUCE_WOOD, 4);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.SPRUCE_BUTTON, Blocks.SPRUCE_PLANKS, 1);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.SPRUCE_PRESSURE_PLATE, Blocks.SPRUCE_LOG, 8);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.SPRUCE_PRESSURE_PLATE, Blocks.STRIPPED_SPRUCE_LOG, 8);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.SPRUCE_PRESSURE_PLATE, Blocks.SPRUCE_WOOD, 8);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.SPRUCE_PRESSURE_PLATE, Blocks.STRIPPED_SPRUCE_WOOD, 8);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.SPRUCE_PRESSURE_PLATE, Blocks.SPRUCE_PLANKS, 2);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.SPRUCE_TRAPDOOR, Blocks.SPRUCE_LOG, 12);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.SPRUCE_TRAPDOOR, Blocks.STRIPPED_SPRUCE_LOG, 12);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.SPRUCE_TRAPDOOR, Blocks.SPRUCE_WOOD, 12);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.SPRUCE_TRAPDOOR, Blocks.STRIPPED_SPRUCE_WOOD, 12);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.SPRUCE_TRAPDOOR, Blocks.SPRUCE_PLANKS, 3);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.SPRUCE_DOOR, Blocks.SPRUCE_LOG, 8);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.SPRUCE_DOOR, Blocks.STRIPPED_SPRUCE_LOG, 8);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.SPRUCE_DOOR, Blocks.SPRUCE_WOOD, 8);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.SPRUCE_DOOR, Blocks.STRIPPED_SPRUCE_WOOD, 8);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.SPRUCE_DOOR, Blocks.SPRUCE_PLANKS, 2);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Items.STICK, Blocks.SPRUCE_LOG, 8);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Items.STICK, Blocks.STRIPPED_SPRUCE_LOG, 8);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Items.STICK, Blocks.SPRUCE_WOOD, 8);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Items.STICK, Blocks.STRIPPED_SPRUCE_WOOD, 8);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Items.STICK, Blocks.SPRUCE_PLANKS, 2);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.ACACIA_PLANKS, Blocks.ACACIA_LOG, 4);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.ACACIA_PLANKS, Blocks.STRIPPED_ACACIA_LOG, 4);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.ACACIA_PLANKS, Blocks.ACACIA_WOOD, 4);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.ACACIA_PLANKS, Blocks.STRIPPED_ACACIA_WOOD, 4);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.ACACIA_STAIRS, Blocks.ACACIA_LOG, 4);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.ACACIA_STAIRS, Blocks.STRIPPED_ACACIA_LOG, 4);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.ACACIA_STAIRS, Blocks.ACACIA_WOOD, 4);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.ACACIA_STAIRS, Blocks.STRIPPED_ACACIA_WOOD, 4);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.ACACIA_STAIRS, Blocks.ACACIA_PLANKS, 1);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.ACACIA_SLAB, Blocks.ACACIA_LOG, 8);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.ACACIA_SLAB, Blocks.STRIPPED_ACACIA_LOG, 8);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.ACACIA_SLAB, Blocks.ACACIA_WOOD, 8);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.ACACIA_SLAB, Blocks.STRIPPED_ACACIA_WOOD, 8);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.ACACIA_SLAB, Blocks.ACACIA_PLANKS, 2);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.ACACIA_FENCE, Blocks.ACACIA_LOG, 16);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.ACACIA_FENCE, Blocks.STRIPPED_ACACIA_LOG, 16);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.ACACIA_FENCE, Blocks.ACACIA_WOOD, 16);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.ACACIA_FENCE, Blocks.STRIPPED_ACACIA_WOOD, 16);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.ACACIA_FENCE, Blocks.ACACIA_PLANKS, 4);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.ACACIA_FENCE_GATE, Blocks.ACACIA_LOG, 8);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.ACACIA_FENCE_GATE, Blocks.STRIPPED_ACACIA_LOG, 8);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.ACACIA_FENCE_GATE, Blocks.ACACIA_WOOD, 8);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.ACACIA_FENCE_GATE, Blocks.STRIPPED_ACACIA_WOOD, 8);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.ACACIA_FENCE_GATE, Blocks.ACACIA_PLANKS, 2);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.STRIPPED_ACACIA_LOG, Blocks.ACACIA_LOG, 1);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.ACACIA_WOOD, Blocks.ACACIA_LOG, 1);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.STRIPPED_ACACIA_WOOD, Blocks.ACACIA_LOG, 1);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.STRIPPED_ACACIA_WOOD, Blocks.ACACIA_WOOD, 1);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.STRIPPED_ACACIA_WOOD, Blocks.STRIPPED_ACACIA_LOG, 1);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.ACACIA_BUTTON, Blocks.ACACIA_LOG, 4);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.ACACIA_BUTTON, Blocks.STRIPPED_ACACIA_LOG, 4);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.ACACIA_BUTTON, Blocks.ACACIA_WOOD, 4);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.ACACIA_BUTTON, Blocks.STRIPPED_ACACIA_WOOD, 4);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.ACACIA_BUTTON, Blocks.ACACIA_PLANKS, 1);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.ACACIA_PRESSURE_PLATE, Blocks.ACACIA_LOG, 8);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.ACACIA_PRESSURE_PLATE, Blocks.STRIPPED_ACACIA_LOG, 8);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.ACACIA_PRESSURE_PLATE, Blocks.ACACIA_WOOD, 8);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.ACACIA_PRESSURE_PLATE, Blocks.STRIPPED_ACACIA_WOOD, 8);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.ACACIA_PRESSURE_PLATE, Blocks.ACACIA_PLANKS, 2);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.ACACIA_TRAPDOOR, Blocks.ACACIA_LOG, 12);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.ACACIA_TRAPDOOR, Blocks.STRIPPED_ACACIA_LOG, 12);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.ACACIA_TRAPDOOR, Blocks.ACACIA_WOOD, 12);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.ACACIA_TRAPDOOR, Blocks.STRIPPED_ACACIA_WOOD, 12);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.ACACIA_TRAPDOOR, Blocks.ACACIA_PLANKS, 3);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.ACACIA_DOOR, Blocks.ACACIA_LOG, 8);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.ACACIA_DOOR, Blocks.STRIPPED_ACACIA_LOG, 8);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.ACACIA_DOOR, Blocks.ACACIA_WOOD, 8);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.ACACIA_DOOR, Blocks.STRIPPED_ACACIA_WOOD, 8);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.ACACIA_DOOR, Blocks.ACACIA_PLANKS, 2);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Items.STICK, Blocks.ACACIA_LOG, 8);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Items.STICK, Blocks.STRIPPED_ACACIA_LOG, 8);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Items.STICK, Blocks.ACACIA_WOOD, 8);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Items.STICK, Blocks.STRIPPED_ACACIA_WOOD, 8);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Items.STICK, Blocks.ACACIA_PLANKS, 2);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.JUNGLE_PLANKS, Blocks.JUNGLE_LOG, 4);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.JUNGLE_PLANKS, Blocks.STRIPPED_JUNGLE_LOG, 4);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.JUNGLE_PLANKS, Blocks.JUNGLE_WOOD, 4);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.JUNGLE_PLANKS, Blocks.STRIPPED_JUNGLE_WOOD, 4);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.JUNGLE_STAIRS, Blocks.JUNGLE_LOG, 4);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.JUNGLE_STAIRS, Blocks.STRIPPED_JUNGLE_LOG, 4);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.JUNGLE_STAIRS, Blocks.JUNGLE_WOOD, 4);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.JUNGLE_STAIRS, Blocks.STRIPPED_JUNGLE_WOOD, 4);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.JUNGLE_STAIRS, Blocks.JUNGLE_PLANKS, 1);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.JUNGLE_SLAB, Blocks.JUNGLE_LOG, 8);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.JUNGLE_SLAB, Blocks.STRIPPED_JUNGLE_LOG, 8);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.JUNGLE_SLAB, Blocks.JUNGLE_WOOD, 8);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.JUNGLE_SLAB, Blocks.STRIPPED_JUNGLE_WOOD, 8);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.JUNGLE_SLAB, Blocks.JUNGLE_PLANKS, 2);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.JUNGLE_FENCE, Blocks.JUNGLE_LOG, 16);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.JUNGLE_FENCE, Blocks.STRIPPED_JUNGLE_LOG, 16);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.JUNGLE_FENCE, Blocks.JUNGLE_WOOD, 16);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.JUNGLE_FENCE, Blocks.STRIPPED_JUNGLE_WOOD, 16);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.JUNGLE_FENCE, Blocks.JUNGLE_PLANKS, 4);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.JUNGLE_FENCE_GATE, Blocks.JUNGLE_LOG, 8);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.JUNGLE_FENCE_GATE, Blocks.STRIPPED_JUNGLE_LOG, 8);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.JUNGLE_FENCE_GATE, Blocks.JUNGLE_WOOD, 8);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.JUNGLE_FENCE_GATE, Blocks.STRIPPED_JUNGLE_WOOD, 8);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.JUNGLE_FENCE_GATE, Blocks.JUNGLE_PLANKS, 2);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.STRIPPED_JUNGLE_LOG, Blocks.JUNGLE_LOG, 1);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.JUNGLE_WOOD, Blocks.JUNGLE_LOG, 1);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.STRIPPED_JUNGLE_WOOD, Blocks.JUNGLE_LOG, 1);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.STRIPPED_JUNGLE_WOOD, Blocks.JUNGLE_WOOD, 1);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.STRIPPED_JUNGLE_WOOD, Blocks.STRIPPED_JUNGLE_LOG, 1);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.JUNGLE_BUTTON, Blocks.JUNGLE_LOG, 4);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.JUNGLE_BUTTON, Blocks.STRIPPED_JUNGLE_LOG, 4);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.JUNGLE_BUTTON, Blocks.JUNGLE_WOOD, 4);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.JUNGLE_BUTTON, Blocks.STRIPPED_JUNGLE_WOOD, 4);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.JUNGLE_BUTTON, Blocks.JUNGLE_PLANKS, 1);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.JUNGLE_PRESSURE_PLATE, Blocks.JUNGLE_LOG, 8);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.JUNGLE_PRESSURE_PLATE, Blocks.STRIPPED_JUNGLE_LOG, 8);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.JUNGLE_PRESSURE_PLATE, Blocks.JUNGLE_WOOD, 8);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.JUNGLE_PRESSURE_PLATE, Blocks.STRIPPED_JUNGLE_WOOD, 8);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.JUNGLE_PRESSURE_PLATE, Blocks.JUNGLE_PLANKS, 2);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.JUNGLE_TRAPDOOR, Blocks.JUNGLE_LOG, 12);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.JUNGLE_TRAPDOOR, Blocks.STRIPPED_JUNGLE_LOG, 12);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.JUNGLE_TRAPDOOR, Blocks.JUNGLE_WOOD, 12);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.JUNGLE_TRAPDOOR, Blocks.STRIPPED_JUNGLE_WOOD, 12);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.JUNGLE_TRAPDOOR, Blocks.JUNGLE_PLANKS, 3);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.JUNGLE_DOOR, Blocks.JUNGLE_LOG, 8);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.JUNGLE_DOOR, Blocks.STRIPPED_JUNGLE_LOG, 8);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.JUNGLE_DOOR, Blocks.JUNGLE_WOOD, 8);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.JUNGLE_DOOR, Blocks.STRIPPED_JUNGLE_WOOD, 8);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.JUNGLE_DOOR, Blocks.JUNGLE_PLANKS, 2);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Items.STICK, Blocks.JUNGLE_LOG, 8);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Items.STICK, Blocks.STRIPPED_JUNGLE_LOG, 8);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Items.STICK, Blocks.JUNGLE_WOOD, 8);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Items.STICK, Blocks.STRIPPED_JUNGLE_WOOD, 8);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Items.STICK, Blocks.JUNGLE_PLANKS, 2);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.DARK_OAK_PLANKS, Blocks.DARK_OAK_LOG, 4);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.DARK_OAK_PLANKS, Blocks.STRIPPED_DARK_OAK_LOG, 4);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.DARK_OAK_PLANKS, Blocks.DARK_OAK_WOOD, 4);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.DARK_OAK_PLANKS, Blocks.STRIPPED_DARK_OAK_WOOD, 4);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.DARK_OAK_STAIRS, Blocks.DARK_OAK_LOG, 4);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.DARK_OAK_STAIRS, Blocks.STRIPPED_DARK_OAK_LOG, 4);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.DARK_OAK_STAIRS, Blocks.DARK_OAK_WOOD, 4);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.DARK_OAK_STAIRS, Blocks.STRIPPED_DARK_OAK_WOOD, 4);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.DARK_OAK_STAIRS, Blocks.DARK_OAK_PLANKS, 1);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.DARK_OAK_SLAB, Blocks.DARK_OAK_LOG, 8);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.DARK_OAK_SLAB, Blocks.STRIPPED_DARK_OAK_LOG, 8);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.DARK_OAK_SLAB, Blocks.DARK_OAK_WOOD, 8);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.DARK_OAK_SLAB, Blocks.STRIPPED_DARK_OAK_WOOD, 8);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.DARK_OAK_SLAB, Blocks.DARK_OAK_PLANKS, 2);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.DARK_OAK_FENCE, Blocks.DARK_OAK_LOG, 16);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.DARK_OAK_FENCE, Blocks.STRIPPED_DARK_OAK_LOG, 16);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.DARK_OAK_FENCE, Blocks.DARK_OAK_WOOD, 16);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.DARK_OAK_FENCE, Blocks.STRIPPED_DARK_OAK_WOOD, 16);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.DARK_OAK_FENCE, Blocks.DARK_OAK_PLANKS, 4);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.DARK_OAK_FENCE_GATE, Blocks.DARK_OAK_LOG, 8);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.DARK_OAK_FENCE_GATE, Blocks.STRIPPED_DARK_OAK_LOG, 8);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.DARK_OAK_FENCE_GATE, Blocks.DARK_OAK_WOOD, 8);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.DARK_OAK_FENCE_GATE, Blocks.STRIPPED_DARK_OAK_WOOD, 8);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.DARK_OAK_FENCE_GATE, Blocks.DARK_OAK_PLANKS, 2);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.STRIPPED_DARK_OAK_LOG, Blocks.DARK_OAK_LOG, 1);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.DARK_OAK_WOOD, Blocks.DARK_OAK_LOG, 1);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.STRIPPED_DARK_OAK_WOOD, Blocks.DARK_OAK_LOG, 1);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.STRIPPED_DARK_OAK_WOOD, Blocks.DARK_OAK_WOOD, 1);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.STRIPPED_DARK_OAK_WOOD, Blocks.STRIPPED_DARK_OAK_LOG, 1);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.DARK_OAK_BUTTON, Blocks.DARK_OAK_LOG, 4);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.DARK_OAK_BUTTON, Blocks.STRIPPED_DARK_OAK_LOG, 4);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.DARK_OAK_BUTTON, Blocks.DARK_OAK_WOOD, 4);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.DARK_OAK_BUTTON, Blocks.STRIPPED_DARK_OAK_WOOD, 4);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.DARK_OAK_BUTTON, Blocks.DARK_OAK_PLANKS, 1);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.DARK_OAK_PRESSURE_PLATE, Blocks.DARK_OAK_LOG, 8);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.DARK_OAK_PRESSURE_PLATE, Blocks.STRIPPED_DARK_OAK_LOG, 8);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.DARK_OAK_PRESSURE_PLATE, Blocks.DARK_OAK_WOOD, 8);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.DARK_OAK_PRESSURE_PLATE, Blocks.STRIPPED_DARK_OAK_WOOD, 8);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.DARK_OAK_PRESSURE_PLATE, Blocks.DARK_OAK_PLANKS, 2);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.DARK_OAK_TRAPDOOR, Blocks.DARK_OAK_LOG, 12);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.DARK_OAK_TRAPDOOR, Blocks.STRIPPED_DARK_OAK_LOG, 12);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.DARK_OAK_TRAPDOOR, Blocks.DARK_OAK_WOOD, 12);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.DARK_OAK_TRAPDOOR, Blocks.STRIPPED_DARK_OAK_WOOD, 12);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.DARK_OAK_TRAPDOOR, Blocks.DARK_OAK_PLANKS, 3);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.DARK_OAK_DOOR, Blocks.DARK_OAK_LOG, 8);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.DARK_OAK_DOOR, Blocks.STRIPPED_DARK_OAK_LOG, 8);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.DARK_OAK_DOOR, Blocks.DARK_OAK_WOOD, 8);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.DARK_OAK_DOOR, Blocks.STRIPPED_DARK_OAK_WOOD, 8);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.DARK_OAK_DOOR, Blocks.DARK_OAK_PLANKS, 2);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Items.STICK, Blocks.DARK_OAK_LOG, 8);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Items.STICK, Blocks.STRIPPED_DARK_OAK_LOG, 8);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Items.STICK, Blocks.DARK_OAK_WOOD, 8);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Items.STICK, Blocks.STRIPPED_DARK_OAK_WOOD, 8);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Items.STICK, Blocks.DARK_OAK_PLANKS, 2);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.MANGROVE_PLANKS, Blocks.MANGROVE_LOG, 4);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.MANGROVE_PLANKS, Blocks.STRIPPED_MANGROVE_LOG, 4);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.MANGROVE_PLANKS, Blocks.MANGROVE_WOOD, 4);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.MANGROVE_PLANKS, Blocks.STRIPPED_MANGROVE_WOOD, 4);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.MANGROVE_STAIRS, Blocks.MANGROVE_LOG, 4);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.MANGROVE_STAIRS, Blocks.STRIPPED_MANGROVE_LOG, 4);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.MANGROVE_STAIRS, Blocks.MANGROVE_WOOD, 4);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.MANGROVE_STAIRS, Blocks.STRIPPED_MANGROVE_WOOD, 4);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.MANGROVE_STAIRS, Blocks.MANGROVE_PLANKS, 1);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.MANGROVE_SLAB, Blocks.MANGROVE_LOG, 8);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.MANGROVE_SLAB, Blocks.STRIPPED_MANGROVE_LOG, 8);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.MANGROVE_SLAB, Blocks.MANGROVE_WOOD, 8);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.MANGROVE_SLAB, Blocks.STRIPPED_MANGROVE_WOOD, 8);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.MANGROVE_SLAB, Blocks.MANGROVE_PLANKS, 2);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.MANGROVE_FENCE, Blocks.MANGROVE_LOG, 16);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.MANGROVE_FENCE, Blocks.STRIPPED_MANGROVE_LOG, 16);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.MANGROVE_FENCE, Blocks.MANGROVE_WOOD, 16);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.MANGROVE_FENCE, Blocks.STRIPPED_MANGROVE_WOOD, 16);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.MANGROVE_FENCE, Blocks.MANGROVE_PLANKS, 4);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.MANGROVE_FENCE_GATE, Blocks.MANGROVE_LOG, 8);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.MANGROVE_FENCE_GATE, Blocks.STRIPPED_MANGROVE_LOG, 8);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.MANGROVE_FENCE_GATE, Blocks.MANGROVE_WOOD, 8);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.MANGROVE_FENCE_GATE, Blocks.STRIPPED_MANGROVE_WOOD, 8);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.MANGROVE_FENCE_GATE, Blocks.MANGROVE_PLANKS, 2);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.STRIPPED_MANGROVE_LOG, Blocks.MANGROVE_LOG, 1);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.MANGROVE_WOOD, Blocks.MANGROVE_LOG, 1);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.STRIPPED_MANGROVE_WOOD, Blocks.MANGROVE_LOG, 1);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.STRIPPED_MANGROVE_WOOD, Blocks.MANGROVE_WOOD, 1);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.STRIPPED_MANGROVE_WOOD, Blocks.STRIPPED_MANGROVE_LOG, 1);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.MANGROVE_BUTTON, Blocks.MANGROVE_LOG, 4);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.MANGROVE_BUTTON, Blocks.STRIPPED_MANGROVE_LOG, 4);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.MANGROVE_BUTTON, Blocks.MANGROVE_WOOD, 4);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.MANGROVE_BUTTON, Blocks.STRIPPED_MANGROVE_WOOD, 4);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.MANGROVE_BUTTON, Blocks.MANGROVE_PLANKS, 1);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.MANGROVE_PRESSURE_PLATE, Blocks.MANGROVE_LOG, 8);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.MANGROVE_PRESSURE_PLATE, Blocks.STRIPPED_MANGROVE_LOG, 8);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.MANGROVE_PRESSURE_PLATE, Blocks.MANGROVE_WOOD, 8);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.MANGROVE_PRESSURE_PLATE, Blocks.STRIPPED_MANGROVE_WOOD, 8);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.MANGROVE_PRESSURE_PLATE, Blocks.MANGROVE_PLANKS, 2);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.MANGROVE_TRAPDOOR, Blocks.MANGROVE_LOG, 12);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.MANGROVE_TRAPDOOR, Blocks.STRIPPED_MANGROVE_LOG, 12);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.MANGROVE_TRAPDOOR, Blocks.MANGROVE_WOOD, 12);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.MANGROVE_TRAPDOOR, Blocks.STRIPPED_MANGROVE_WOOD, 12);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.MANGROVE_TRAPDOOR, Blocks.MANGROVE_PLANKS, 3);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.MANGROVE_DOOR, Blocks.MANGROVE_LOG, 8);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.MANGROVE_DOOR, Blocks.STRIPPED_MANGROVE_LOG, 8);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.MANGROVE_DOOR, Blocks.MANGROVE_WOOD, 8);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.MANGROVE_DOOR, Blocks.STRIPPED_MANGROVE_WOOD, 8);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.MANGROVE_DOOR, Blocks.MANGROVE_PLANKS, 2);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Items.STICK, Blocks.MANGROVE_LOG, 8);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Items.STICK, Blocks.STRIPPED_MANGROVE_LOG, 8);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Items.STICK, Blocks.MANGROVE_WOOD, 8);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Items.STICK, Blocks.STRIPPED_MANGROVE_WOOD, 8);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Items.STICK, Blocks.MANGROVE_PLANKS, 2);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.CHERRY_PLANKS, Blocks.CHERRY_LOG, 4);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.CHERRY_PLANKS, Blocks.STRIPPED_CHERRY_LOG, 4);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.CHERRY_PLANKS, Blocks.CHERRY_WOOD, 4);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.CHERRY_PLANKS, Blocks.STRIPPED_CHERRY_WOOD, 4);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.CHERRY_STAIRS, Blocks.CHERRY_LOG, 4);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.CHERRY_STAIRS, Blocks.STRIPPED_CHERRY_LOG, 4);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.CHERRY_STAIRS, Blocks.CHERRY_WOOD, 4);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.CHERRY_STAIRS, Blocks.STRIPPED_CHERRY_WOOD, 4);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.CHERRY_STAIRS, Blocks.CHERRY_PLANKS, 1);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.CHERRY_SLAB, Blocks.CHERRY_LOG, 8);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.CHERRY_SLAB, Blocks.STRIPPED_CHERRY_LOG, 8);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.CHERRY_SLAB, Blocks.CHERRY_WOOD, 8);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.CHERRY_SLAB, Blocks.STRIPPED_CHERRY_WOOD, 8);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.CHERRY_SLAB, Blocks.CHERRY_PLANKS, 2);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.CHERRY_FENCE, Blocks.CHERRY_LOG, 16);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.CHERRY_FENCE, Blocks.STRIPPED_CHERRY_LOG, 16);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.CHERRY_FENCE, Blocks.CHERRY_WOOD, 16);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.CHERRY_FENCE, Blocks.STRIPPED_CHERRY_WOOD, 16);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.CHERRY_FENCE, Blocks.CHERRY_PLANKS, 4);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.CHERRY_FENCE_GATE, Blocks.CHERRY_LOG, 8);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.CHERRY_FENCE_GATE, Blocks.STRIPPED_CHERRY_LOG, 8);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.CHERRY_FENCE_GATE, Blocks.CHERRY_WOOD, 8);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.CHERRY_FENCE_GATE, Blocks.STRIPPED_CHERRY_WOOD, 8);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.CHERRY_FENCE_GATE, Blocks.CHERRY_PLANKS, 2);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.STRIPPED_CHERRY_LOG, Blocks.CHERRY_LOG, 1);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.CHERRY_WOOD, Blocks.CHERRY_LOG, 1);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.STRIPPED_CHERRY_WOOD, Blocks.CHERRY_LOG, 1);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.STRIPPED_CHERRY_WOOD, Blocks.CHERRY_WOOD, 1);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.STRIPPED_CHERRY_WOOD, Blocks.STRIPPED_CHERRY_LOG, 1);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.CHERRY_BUTTON, Blocks.CHERRY_LOG, 4);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.CHERRY_BUTTON, Blocks.STRIPPED_CHERRY_LOG, 4);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.CHERRY_BUTTON, Blocks.CHERRY_WOOD, 4);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.CHERRY_BUTTON, Blocks.STRIPPED_CHERRY_WOOD, 4);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.CHERRY_BUTTON, Blocks.CHERRY_PLANKS, 1);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.CHERRY_PRESSURE_PLATE, Blocks.CHERRY_LOG, 8);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.CHERRY_PRESSURE_PLATE, Blocks.STRIPPED_CHERRY_LOG, 8);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.CHERRY_PRESSURE_PLATE, Blocks.CHERRY_WOOD, 8);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.CHERRY_PRESSURE_PLATE, Blocks.STRIPPED_CHERRY_WOOD, 8);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.CHERRY_PRESSURE_PLATE, Blocks.CHERRY_PLANKS, 2);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.CHERRY_TRAPDOOR, Blocks.CHERRY_LOG, 12);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.CHERRY_TRAPDOOR, Blocks.STRIPPED_CHERRY_LOG, 12);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.CHERRY_TRAPDOOR, Blocks.CHERRY_WOOD, 12);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.CHERRY_TRAPDOOR, Blocks.STRIPPED_CHERRY_WOOD, 12);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.CHERRY_TRAPDOOR, Blocks.CHERRY_PLANKS, 3);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.CHERRY_DOOR, Blocks.CHERRY_LOG, 8);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.CHERRY_DOOR, Blocks.STRIPPED_CHERRY_LOG, 8);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.CHERRY_DOOR, Blocks.CHERRY_WOOD, 8);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.CHERRY_DOOR, Blocks.STRIPPED_CHERRY_WOOD, 8);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.CHERRY_DOOR, Blocks.CHERRY_PLANKS, 2);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Items.STICK, Blocks.CHERRY_LOG, 8);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Items.STICK, Blocks.STRIPPED_CHERRY_LOG, 8);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Items.STICK, Blocks.CHERRY_WOOD, 8);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Items.STICK, Blocks.STRIPPED_CHERRY_WOOD, 8);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Items.STICK, Blocks.CHERRY_PLANKS, 2);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.BAMBOO_PLANKS, Blocks.BAMBOO_BLOCK, 2);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.BAMBOO_PLANKS, Blocks.STRIPPED_BAMBOO_BLOCK, 2);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.BAMBOO_MOSAIC, Blocks.BAMBOO_BLOCK, 2);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.BAMBOO_MOSAIC, Blocks.STRIPPED_BAMBOO_BLOCK, 2);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.BAMBOO_MOSAIC, Blocks.BAMBOO_PLANKS, 1);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.BAMBOO_STAIRS, Blocks.BAMBOO_BLOCK, 2);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.BAMBOO_STAIRS, Blocks.STRIPPED_BAMBOO_BLOCK, 2);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.BAMBOO_STAIRS, Blocks.BAMBOO_PLANKS, 1);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.BAMBOO_MOSAIC_STAIRS, Blocks.BAMBOO_BLOCK, 2);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.BAMBOO_MOSAIC_STAIRS, Blocks.STRIPPED_BAMBOO_BLOCK, 2);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.BAMBOO_MOSAIC_STAIRS, Blocks.BAMBOO_PLANKS, 1);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.BAMBOO_MOSAIC_STAIRS, Blocks.BAMBOO_MOSAIC, 1);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.BAMBOO_SLAB, Blocks.BAMBOO_BLOCK, 4);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.BAMBOO_SLAB, Blocks.STRIPPED_BAMBOO_BLOCK, 4);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.BAMBOO_SLAB, Blocks.BAMBOO_PLANKS, 2);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.BAMBOO_MOSAIC_SLAB, Blocks.BAMBOO_BLOCK, 4);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.BAMBOO_MOSAIC_SLAB, Blocks.STRIPPED_BAMBOO_BLOCK, 4);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.BAMBOO_MOSAIC_SLAB, Blocks.BAMBOO_PLANKS, 2);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.BAMBOO_MOSAIC_SLAB, Blocks.BAMBOO_MOSAIC, 2);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.BAMBOO_FENCE, Blocks.BAMBOO_BLOCK, 16);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.BAMBOO_FENCE, Blocks.STRIPPED_BAMBOO_BLOCK, 16);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.BAMBOO_FENCE, Blocks.BAMBOO_PLANKS, 4);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.BAMBOO_FENCE, Blocks.BAMBOO_MOSAIC, 4);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.BAMBOO_FENCE_GATE, Blocks.BAMBOO_BLOCK, 8);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.BAMBOO_FENCE_GATE, Blocks.STRIPPED_BAMBOO_BLOCK, 8);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.BAMBOO_FENCE_GATE, Blocks.BAMBOO_PLANKS, 2);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.BAMBOO_FENCE_GATE, Blocks.BAMBOO_MOSAIC, 2);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.STRIPPED_BAMBOO_BLOCK, Blocks.BAMBOO_BLOCK, 1);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.BAMBOO_BUTTON, Blocks.BAMBOO_BLOCK, 4);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.BAMBOO_BUTTON, Blocks.STRIPPED_BAMBOO_BLOCK, 4);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.BAMBOO_BUTTON, Blocks.BAMBOO_PLANKS, 1);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.BAMBOO_BUTTON, Blocks.BAMBOO_MOSAIC, 1);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.BAMBOO_PRESSURE_PLATE, Blocks.BAMBOO_BLOCK, 8);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.BAMBOO_PRESSURE_PLATE, Blocks.STRIPPED_BAMBOO_BLOCK, 8);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.BAMBOO_PRESSURE_PLATE, Blocks.BAMBOO_PLANKS, 2);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.BAMBOO_PRESSURE_PLATE, Blocks.BAMBOO_MOSAIC, 2);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.BAMBOO_TRAPDOOR, Blocks.BAMBOO_BLOCK, 12);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.BAMBOO_TRAPDOOR, Blocks.STRIPPED_BAMBOO_BLOCK, 12);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.BAMBOO_TRAPDOOR, Blocks.BAMBOO_PLANKS, 3);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.BAMBOO_TRAPDOOR, Blocks.BAMBOO_MOSAIC, 3);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.BAMBOO_DOOR, Blocks.BAMBOO_BLOCK, 8);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.BAMBOO_DOOR, Blocks.STRIPPED_BAMBOO_BLOCK, 8);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.BAMBOO_DOOR, Blocks.BAMBOO_PLANKS, 2);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.BAMBOO_DOOR, Blocks.BAMBOO_MOSAIC, 2);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Items.STICK, Blocks.BAMBOO_BLOCK, 8);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Items.STICK, Blocks.STRIPPED_BAMBOO_BLOCK, 8);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Items.STICK, Blocks.BAMBOO_PLANKS, 2);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Items.STICK, Blocks.BAMBOO_MOSAIC, 2);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.AZALEA_PLANKS, DDBlocks.AZALEA_LOG, 4);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.AZALEA_PLANKS, DDBlocks.STRIPPED_AZALEA_LOG, 4);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.AZALEA_PLANKS, DDBlocks.AZALEA_WOOD, 4);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.AZALEA_PLANKS, DDBlocks.STRIPPED_AZALEA_WOOD, 4);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.AZALEA_STAIRS, DDBlocks.AZALEA_LOG, 4);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.AZALEA_STAIRS, DDBlocks.STRIPPED_AZALEA_LOG, 4);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.AZALEA_STAIRS, DDBlocks.AZALEA_WOOD, 4);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.AZALEA_STAIRS, DDBlocks.STRIPPED_AZALEA_WOOD, 4);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.AZALEA_STAIRS, DDBlocks.AZALEA_PLANKS, 1);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.AZALEA_SLAB, DDBlocks.AZALEA_LOG, 8);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.AZALEA_SLAB, DDBlocks.STRIPPED_AZALEA_LOG, 8);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.AZALEA_SLAB, DDBlocks.AZALEA_WOOD, 8);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.AZALEA_SLAB, DDBlocks.STRIPPED_AZALEA_WOOD, 8);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.AZALEA_SLAB, DDBlocks.AZALEA_PLANKS, 2);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.AZALEA_FENCE, DDBlocks.AZALEA_LOG, 16);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.AZALEA_FENCE, DDBlocks.STRIPPED_AZALEA_LOG, 16);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.AZALEA_FENCE, DDBlocks.AZALEA_WOOD, 16);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.AZALEA_FENCE, DDBlocks.STRIPPED_AZALEA_WOOD, 16);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.AZALEA_FENCE, DDBlocks.AZALEA_PLANKS, 4);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.AZALEA_FENCE_GATE, DDBlocks.AZALEA_LOG, 8);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.AZALEA_FENCE_GATE, DDBlocks.STRIPPED_AZALEA_LOG, 8);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.AZALEA_FENCE_GATE, DDBlocks.AZALEA_WOOD, 8);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.AZALEA_FENCE_GATE, DDBlocks.STRIPPED_AZALEA_WOOD, 8);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.AZALEA_FENCE_GATE, DDBlocks.AZALEA_PLANKS, 2);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.STRIPPED_AZALEA_LOG, DDBlocks.AZALEA_LOG, 1);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.AZALEA_WOOD, DDBlocks.AZALEA_LOG, 1);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.STRIPPED_AZALEA_WOOD, DDBlocks.AZALEA_LOG, 1);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.STRIPPED_AZALEA_WOOD, DDBlocks.AZALEA_WOOD, 1);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.STRIPPED_AZALEA_WOOD, DDBlocks.STRIPPED_AZALEA_LOG, 1);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.AZALEA_BUTTON, DDBlocks.AZALEA_LOG, 4);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.AZALEA_BUTTON, DDBlocks.STRIPPED_AZALEA_LOG, 4);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.AZALEA_BUTTON, DDBlocks.AZALEA_WOOD, 4);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.AZALEA_BUTTON, DDBlocks.STRIPPED_AZALEA_WOOD, 4);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.AZALEA_BUTTON, DDBlocks.AZALEA_PLANKS, 1);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.AZALEA_PRESSURE_PLATE, DDBlocks.AZALEA_LOG, 8);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.AZALEA_PRESSURE_PLATE, DDBlocks.STRIPPED_AZALEA_LOG, 8);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.AZALEA_PRESSURE_PLATE, DDBlocks.AZALEA_WOOD, 8);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.AZALEA_PRESSURE_PLATE, DDBlocks.STRIPPED_AZALEA_WOOD, 8);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.AZALEA_PRESSURE_PLATE, DDBlocks.AZALEA_PLANKS, 2);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.AZALEA_TRAPDOOR, DDBlocks.AZALEA_LOG, 12);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.AZALEA_TRAPDOOR, DDBlocks.STRIPPED_AZALEA_LOG, 12);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.AZALEA_TRAPDOOR, DDBlocks.AZALEA_WOOD, 12);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.AZALEA_TRAPDOOR, DDBlocks.STRIPPED_AZALEA_WOOD, 12);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.AZALEA_TRAPDOOR, DDBlocks.AZALEA_PLANKS, 3);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.AZALEA_DOOR, DDBlocks.AZALEA_LOG, 8);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.AZALEA_DOOR, DDBlocks.STRIPPED_AZALEA_LOG, 8);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.AZALEA_DOOR, DDBlocks.AZALEA_WOOD, 8);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.AZALEA_DOOR, DDBlocks.STRIPPED_AZALEA_WOOD, 8);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.AZALEA_DOOR, DDBlocks.AZALEA_PLANKS, 2);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Items.STICK, DDBlocks.AZALEA_LOG, 8);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Items.STICK, DDBlocks.STRIPPED_AZALEA_LOG, 8);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Items.STICK, DDBlocks.AZALEA_WOOD, 8);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Items.STICK, DDBlocks.STRIPPED_AZALEA_WOOD, 8);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Items.STICK, DDBlocks.AZALEA_PLANKS, 2);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.SWAMP_PLANKS, DDBlocks.SWAMP_LOG, 4);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.SWAMP_PLANKS, DDBlocks.STRIPPED_SWAMP_LOG, 4);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.SWAMP_PLANKS, DDBlocks.SWAMP_WOOD, 4);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.SWAMP_PLANKS, DDBlocks.STRIPPED_SWAMP_WOOD, 4);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.SWAMP_STAIRS, DDBlocks.SWAMP_LOG, 4);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.SWAMP_STAIRS, DDBlocks.STRIPPED_SWAMP_LOG, 4);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.SWAMP_STAIRS, DDBlocks.SWAMP_WOOD, 4);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.SWAMP_STAIRS, DDBlocks.STRIPPED_SWAMP_WOOD, 4);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.SWAMP_STAIRS, DDBlocks.SWAMP_PLANKS, 1);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.SWAMP_SLAB, DDBlocks.SWAMP_LOG, 8);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.SWAMP_SLAB, DDBlocks.STRIPPED_SWAMP_LOG, 8);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.SWAMP_SLAB, DDBlocks.SWAMP_WOOD, 8);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.SWAMP_SLAB, DDBlocks.STRIPPED_SWAMP_WOOD, 8);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.SWAMP_SLAB, DDBlocks.SWAMP_PLANKS, 2);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.SWAMP_FENCE, DDBlocks.SWAMP_LOG, 16);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.SWAMP_FENCE, DDBlocks.STRIPPED_SWAMP_LOG, 16);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.SWAMP_FENCE, DDBlocks.SWAMP_WOOD, 16);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.SWAMP_FENCE, DDBlocks.STRIPPED_SWAMP_WOOD, 16);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.SWAMP_FENCE, DDBlocks.SWAMP_PLANKS, 4);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.SWAMP_FENCE_GATE, DDBlocks.SWAMP_LOG, 8);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.SWAMP_FENCE_GATE, DDBlocks.STRIPPED_SWAMP_LOG, 8);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.SWAMP_FENCE_GATE, DDBlocks.SWAMP_WOOD, 8);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.SWAMP_FENCE_GATE, DDBlocks.STRIPPED_SWAMP_WOOD, 8);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.SWAMP_FENCE_GATE, DDBlocks.SWAMP_PLANKS, 2);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.STRIPPED_SWAMP_LOG, DDBlocks.SWAMP_LOG, 1);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.SWAMP_WOOD, DDBlocks.SWAMP_LOG, 1);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.STRIPPED_SWAMP_WOOD, DDBlocks.SWAMP_LOG, 1);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.STRIPPED_SWAMP_WOOD, DDBlocks.SWAMP_WOOD, 1);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.STRIPPED_SWAMP_WOOD, DDBlocks.STRIPPED_SWAMP_LOG, 1);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.SWAMP_BUTTON, DDBlocks.SWAMP_LOG, 4);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.SWAMP_BUTTON, DDBlocks.STRIPPED_SWAMP_LOG, 4);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.SWAMP_BUTTON, DDBlocks.SWAMP_WOOD, 4);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.SWAMP_BUTTON, DDBlocks.STRIPPED_SWAMP_WOOD, 4);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.SWAMP_BUTTON, DDBlocks.SWAMP_PLANKS, 1);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.SWAMP_PRESSURE_PLATE, DDBlocks.SWAMP_LOG, 8);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.SWAMP_PRESSURE_PLATE, DDBlocks.STRIPPED_SWAMP_LOG, 8);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.SWAMP_PRESSURE_PLATE, DDBlocks.SWAMP_WOOD, 8);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.SWAMP_PRESSURE_PLATE, DDBlocks.STRIPPED_SWAMP_WOOD, 8);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.SWAMP_PRESSURE_PLATE, DDBlocks.SWAMP_PLANKS, 2);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.SWAMP_TRAPDOOR, DDBlocks.SWAMP_LOG, 12);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.SWAMP_TRAPDOOR, DDBlocks.STRIPPED_SWAMP_LOG, 12);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.SWAMP_TRAPDOOR, DDBlocks.SWAMP_WOOD, 12);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.SWAMP_TRAPDOOR, DDBlocks.STRIPPED_SWAMP_WOOD, 12);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.SWAMP_TRAPDOOR, DDBlocks.SWAMP_PLANKS, 3);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.SWAMP_DOOR, DDBlocks.SWAMP_LOG, 8);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.SWAMP_DOOR, DDBlocks.STRIPPED_SWAMP_LOG, 8);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.SWAMP_DOOR, DDBlocks.SWAMP_WOOD, 8);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.SWAMP_DOOR, DDBlocks.STRIPPED_SWAMP_WOOD, 8);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.SWAMP_DOOR, DDBlocks.SWAMP_PLANKS, 2);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Items.STICK, DDBlocks.SWAMP_LOG, 8);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Items.STICK, DDBlocks.STRIPPED_SWAMP_LOG, 8);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Items.STICK, DDBlocks.SWAMP_WOOD, 8);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Items.STICK, DDBlocks.STRIPPED_SWAMP_WOOD, 8);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Items.STICK, DDBlocks.SWAMP_PLANKS, 2);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.OAK_PLANKS, DDBlocks.OAK_LOG_COLUMN, 4);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.OAK_PLANKS, DDBlocks.OAK_WOOD_COLUMN, 4);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.OAK_PLANKS, DDBlocks.STRIPPED_OAK_LOG_COLUMN, 4);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.OAK_PLANKS, DDBlocks.STRIPPED_OAK_WOOD_COLUMN, 4);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Items.STICK, DDBlocks.OAK_PLANKS_COLUMN, 2);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.OAK_LOG_COLUMN, Blocks.OAK_LOG, 1);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.OAK_WOOD_COLUMN, Blocks.OAK_LOG, 1);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.STRIPPED_OAK_LOG_COLUMN, Blocks.OAK_LOG, 1);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.STRIPPED_OAK_WOOD_COLUMN, Blocks.OAK_LOG, 1);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.STRIPPED_OAK_LOG_COLUMN, Blocks.STRIPPED_OAK_LOG, 1);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.STRIPPED_OAK_WOOD_COLUMN, Blocks.STRIPPED_OAK_WOOD, 1);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.OAK_PLANKS_COLUMN, Blocks.OAK_LOG, 4);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.OAK_WOOD_COLUMN, Blocks.OAK_WOOD, 1);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.STRIPPED_OAK_WOOD_COLUMN, Blocks.OAK_WOOD, 1);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.OAK_PLANKS_COLUMN, Blocks.OAK_WOOD, 4);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.STRIPPED_OAK_WOOD_COLUMN, Blocks.STRIPPED_OAK_LOG, 1);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.OAK_PLANKS_COLUMN, Blocks.STRIPPED_OAK_LOG, 4);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.OAK_PLANKS_COLUMN, Blocks.STRIPPED_OAK_WOOD, 4);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.OAK_PLANKS_COLUMN, Blocks.OAK_PLANKS, 1);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.STRIPPED_OAK_LOG_COLUMN, DDBlocks.OAK_LOG_COLUMN, 1);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.OAK_WOOD_COLUMN, DDBlocks.OAK_LOG_COLUMN, 1);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.STRIPPED_OAK_WOOD_COLUMN, DDBlocks.OAK_LOG_COLUMN, 1);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.OAK_PLANKS_COLUMN, DDBlocks.OAK_LOG_COLUMN, 4);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.STRIPPED_OAK_WOOD_COLUMN, DDBlocks.OAK_WOOD_COLUMN, 1);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.STRIPPED_OAK_WOOD_COLUMN, DDBlocks.STRIPPED_OAK_LOG_COLUMN, 1);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.OAK_PLANKS_COLUMN, DDBlocks.OAK_WOOD_COLUMN, 4);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.OAK_PLANKS_COLUMN, DDBlocks.STRIPPED_OAK_LOG_COLUMN, 4);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.OAK_PLANKS_COLUMN, DDBlocks.STRIPPED_OAK_WOOD_COLUMN, 4);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.SPRUCE_PLANKS, DDBlocks.SPRUCE_LOG_COLUMN, 4);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.SPRUCE_PLANKS, DDBlocks.SPRUCE_WOOD_COLUMN, 4);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.SPRUCE_PLANKS, DDBlocks.STRIPPED_SPRUCE_LOG_COLUMN, 4);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.SPRUCE_PLANKS, DDBlocks.STRIPPED_SPRUCE_WOOD_COLUMN, 4);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Items.STICK, DDBlocks.SPRUCE_PLANKS_COLUMN, 2);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.SPRUCE_LOG_COLUMN, Blocks.SPRUCE_LOG, 1);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.SPRUCE_WOOD_COLUMN, Blocks.SPRUCE_LOG, 1);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.STRIPPED_SPRUCE_LOG_COLUMN, Blocks.SPRUCE_LOG, 1);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.STRIPPED_SPRUCE_WOOD_COLUMN, Blocks.SPRUCE_LOG, 1);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.STRIPPED_SPRUCE_LOG_COLUMN, Blocks.STRIPPED_SPRUCE_LOG, 1);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.STRIPPED_SPRUCE_WOOD_COLUMN, Blocks.STRIPPED_SPRUCE_WOOD, 1);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.SPRUCE_PLANKS_COLUMN, Blocks.SPRUCE_LOG, 4);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.SPRUCE_WOOD_COLUMN, Blocks.SPRUCE_WOOD, 1);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.STRIPPED_SPRUCE_WOOD_COLUMN, Blocks.SPRUCE_WOOD, 1);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.SPRUCE_PLANKS_COLUMN, Blocks.SPRUCE_WOOD, 4);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.STRIPPED_SPRUCE_WOOD_COLUMN, Blocks.STRIPPED_SPRUCE_LOG, 1);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.SPRUCE_PLANKS_COLUMN, Blocks.STRIPPED_SPRUCE_LOG, 4);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.SPRUCE_PLANKS_COLUMN, Blocks.STRIPPED_SPRUCE_WOOD, 4);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.SPRUCE_PLANKS_COLUMN, Blocks.SPRUCE_PLANKS, 1);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.STRIPPED_SPRUCE_LOG_COLUMN, DDBlocks.SPRUCE_LOG_COLUMN, 1);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.SPRUCE_WOOD_COLUMN, DDBlocks.SPRUCE_LOG_COLUMN, 1);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.STRIPPED_SPRUCE_WOOD_COLUMN, DDBlocks.SPRUCE_LOG_COLUMN, 1);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.SPRUCE_PLANKS_COLUMN, DDBlocks.SPRUCE_LOG_COLUMN, 4);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.STRIPPED_SPRUCE_WOOD_COLUMN, DDBlocks.SPRUCE_WOOD_COLUMN, 1);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.STRIPPED_SPRUCE_WOOD_COLUMN, DDBlocks.STRIPPED_SPRUCE_LOG_COLUMN, 1);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.SPRUCE_PLANKS_COLUMN, DDBlocks.SPRUCE_WOOD_COLUMN, 4);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.SPRUCE_PLANKS_COLUMN, DDBlocks.STRIPPED_SPRUCE_LOG_COLUMN, 4);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.SPRUCE_PLANKS_COLUMN, DDBlocks.STRIPPED_SPRUCE_WOOD_COLUMN, 4);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.BIRCH_PLANKS, DDBlocks.BIRCH_LOG_COLUMN, 4);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.BIRCH_PLANKS, DDBlocks.BIRCH_WOOD_COLUMN, 4);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.BIRCH_PLANKS, DDBlocks.STRIPPED_BIRCH_LOG_COLUMN, 4);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.BIRCH_PLANKS, DDBlocks.STRIPPED_BIRCH_WOOD_COLUMN, 4);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Items.STICK, DDBlocks.BIRCH_PLANKS_COLUMN, 2);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.BIRCH_LOG_COLUMN, Blocks.BIRCH_LOG, 1);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.BIRCH_WOOD_COLUMN, Blocks.BIRCH_LOG, 1);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.STRIPPED_BIRCH_LOG_COLUMN, Blocks.BIRCH_LOG, 1);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.STRIPPED_BIRCH_WOOD_COLUMN, Blocks.BIRCH_LOG, 1);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.STRIPPED_BIRCH_LOG_COLUMN, Blocks.STRIPPED_BIRCH_LOG, 1);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.STRIPPED_BIRCH_WOOD_COLUMN, Blocks.STRIPPED_BIRCH_WOOD, 1);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.BIRCH_PLANKS_COLUMN, Blocks.BIRCH_LOG, 4);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.BIRCH_WOOD_COLUMN, Blocks.BIRCH_WOOD, 1);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.STRIPPED_BIRCH_WOOD_COLUMN, Blocks.BIRCH_WOOD, 1);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.BIRCH_PLANKS_COLUMN, Blocks.BIRCH_WOOD, 4);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.STRIPPED_BIRCH_WOOD_COLUMN, Blocks.STRIPPED_BIRCH_LOG, 1);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.BIRCH_PLANKS_COLUMN, Blocks.STRIPPED_BIRCH_LOG, 4);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.BIRCH_PLANKS_COLUMN, Blocks.STRIPPED_BIRCH_WOOD, 4);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.BIRCH_PLANKS_COLUMN, Blocks.BIRCH_PLANKS, 1);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.STRIPPED_BIRCH_LOG_COLUMN, DDBlocks.BIRCH_LOG_COLUMN, 1);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.BIRCH_WOOD_COLUMN, DDBlocks.BIRCH_LOG_COLUMN, 1);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.STRIPPED_BIRCH_WOOD_COLUMN, DDBlocks.BIRCH_LOG_COLUMN, 1);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.BIRCH_PLANKS_COLUMN, DDBlocks.BIRCH_LOG_COLUMN, 4);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.STRIPPED_BIRCH_WOOD_COLUMN, DDBlocks.BIRCH_WOOD_COLUMN, 1);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.STRIPPED_BIRCH_WOOD_COLUMN, DDBlocks.STRIPPED_BIRCH_LOG_COLUMN, 1);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.BIRCH_PLANKS_COLUMN, DDBlocks.BIRCH_WOOD_COLUMN, 4);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.BIRCH_PLANKS_COLUMN, DDBlocks.STRIPPED_BIRCH_LOG_COLUMN, 4);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.BIRCH_PLANKS_COLUMN, DDBlocks.STRIPPED_BIRCH_WOOD_COLUMN, 4);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.JUNGLE_PLANKS, DDBlocks.JUNGLE_LOG_COLUMN, 4);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.JUNGLE_PLANKS, DDBlocks.JUNGLE_WOOD_COLUMN, 4);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.JUNGLE_PLANKS, DDBlocks.STRIPPED_JUNGLE_LOG_COLUMN, 4);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.JUNGLE_PLANKS, DDBlocks.STRIPPED_JUNGLE_WOOD_COLUMN, 4);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Items.STICK, DDBlocks.JUNGLE_PLANKS_COLUMN, 2);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.JUNGLE_LOG_COLUMN, Blocks.JUNGLE_LOG, 1);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.JUNGLE_WOOD_COLUMN, Blocks.JUNGLE_LOG, 1);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.STRIPPED_JUNGLE_LOG_COLUMN, Blocks.JUNGLE_LOG, 1);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.STRIPPED_JUNGLE_WOOD_COLUMN, Blocks.JUNGLE_LOG, 1);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.STRIPPED_JUNGLE_LOG_COLUMN, Blocks.STRIPPED_JUNGLE_LOG, 1);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.STRIPPED_JUNGLE_WOOD_COLUMN, Blocks.STRIPPED_JUNGLE_WOOD, 1);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.JUNGLE_PLANKS_COLUMN, Blocks.JUNGLE_LOG, 4);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.JUNGLE_WOOD_COLUMN, Blocks.JUNGLE_WOOD, 1);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.STRIPPED_JUNGLE_WOOD_COLUMN, Blocks.JUNGLE_WOOD, 1);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.JUNGLE_PLANKS_COLUMN, Blocks.JUNGLE_WOOD, 4);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.STRIPPED_JUNGLE_WOOD_COLUMN, Blocks.STRIPPED_JUNGLE_LOG, 1);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.JUNGLE_PLANKS_COLUMN, Blocks.STRIPPED_JUNGLE_LOG, 4);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.JUNGLE_PLANKS_COLUMN, Blocks.STRIPPED_JUNGLE_WOOD, 4);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.JUNGLE_PLANKS_COLUMN, Blocks.JUNGLE_PLANKS, 1);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.STRIPPED_JUNGLE_LOG_COLUMN, DDBlocks.JUNGLE_LOG_COLUMN, 1);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.JUNGLE_WOOD_COLUMN, DDBlocks.JUNGLE_LOG_COLUMN, 1);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.STRIPPED_JUNGLE_WOOD_COLUMN, DDBlocks.JUNGLE_LOG_COLUMN, 1);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.JUNGLE_PLANKS_COLUMN, DDBlocks.JUNGLE_LOG_COLUMN, 4);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.STRIPPED_JUNGLE_WOOD_COLUMN, DDBlocks.JUNGLE_WOOD_COLUMN, 1);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.STRIPPED_JUNGLE_WOOD_COLUMN, DDBlocks.STRIPPED_JUNGLE_LOG_COLUMN, 1);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.JUNGLE_PLANKS_COLUMN, DDBlocks.JUNGLE_WOOD_COLUMN, 4);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.JUNGLE_PLANKS_COLUMN, DDBlocks.STRIPPED_JUNGLE_LOG_COLUMN, 4);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.JUNGLE_PLANKS_COLUMN, DDBlocks.STRIPPED_JUNGLE_WOOD_COLUMN, 4);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.ACACIA_PLANKS, DDBlocks.ACACIA_LOG_COLUMN, 4);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.ACACIA_PLANKS, DDBlocks.ACACIA_WOOD_COLUMN, 4);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.ACACIA_PLANKS, DDBlocks.STRIPPED_ACACIA_LOG_COLUMN, 4);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.ACACIA_PLANKS, DDBlocks.STRIPPED_ACACIA_WOOD_COLUMN, 4);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Items.STICK, DDBlocks.ACACIA_PLANKS_COLUMN, 2);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.ACACIA_LOG_COLUMN, Blocks.ACACIA_LOG, 1);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.ACACIA_WOOD_COLUMN, Blocks.ACACIA_LOG, 1);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.STRIPPED_ACACIA_LOG_COLUMN, Blocks.ACACIA_LOG, 1);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.STRIPPED_ACACIA_WOOD_COLUMN, Blocks.ACACIA_LOG, 1);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.STRIPPED_ACACIA_LOG_COLUMN, Blocks.STRIPPED_ACACIA_LOG, 1);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.STRIPPED_ACACIA_WOOD_COLUMN, Blocks.STRIPPED_ACACIA_WOOD, 1);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.ACACIA_PLANKS_COLUMN, Blocks.ACACIA_LOG, 4);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.ACACIA_WOOD_COLUMN, Blocks.ACACIA_WOOD, 1);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.STRIPPED_ACACIA_WOOD_COLUMN, Blocks.ACACIA_WOOD, 1);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.ACACIA_PLANKS_COLUMN, Blocks.ACACIA_WOOD, 4);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.STRIPPED_ACACIA_WOOD_COLUMN, Blocks.STRIPPED_ACACIA_LOG, 1);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.ACACIA_PLANKS_COLUMN, Blocks.STRIPPED_ACACIA_LOG, 4);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.ACACIA_PLANKS_COLUMN, Blocks.STRIPPED_ACACIA_WOOD, 4);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.ACACIA_PLANKS_COLUMN, Blocks.ACACIA_PLANKS, 1);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.STRIPPED_ACACIA_LOG_COLUMN, DDBlocks.ACACIA_LOG_COLUMN, 1);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.ACACIA_WOOD_COLUMN, DDBlocks.ACACIA_LOG_COLUMN, 1);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.STRIPPED_ACACIA_WOOD_COLUMN, DDBlocks.ACACIA_LOG_COLUMN, 1);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.ACACIA_PLANKS_COLUMN, DDBlocks.ACACIA_LOG_COLUMN, 4);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.STRIPPED_ACACIA_WOOD_COLUMN, DDBlocks.ACACIA_WOOD_COLUMN, 1);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.STRIPPED_ACACIA_WOOD_COLUMN, DDBlocks.STRIPPED_ACACIA_LOG_COLUMN, 1);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.ACACIA_PLANKS_COLUMN, DDBlocks.ACACIA_WOOD_COLUMN, 4);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.ACACIA_PLANKS_COLUMN, DDBlocks.STRIPPED_ACACIA_LOG_COLUMN, 4);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.ACACIA_PLANKS_COLUMN, DDBlocks.STRIPPED_ACACIA_WOOD_COLUMN, 4);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.DARK_OAK_PLANKS, DDBlocks.DARK_OAK_LOG_COLUMN, 4);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.DARK_OAK_PLANKS, DDBlocks.DARK_OAK_WOOD_COLUMN, 4);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.DARK_OAK_PLANKS, DDBlocks.STRIPPED_DARK_OAK_LOG_COLUMN, 4);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.DARK_OAK_PLANKS, DDBlocks.STRIPPED_DARK_OAK_WOOD_COLUMN, 4);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Items.STICK, DDBlocks.DARK_OAK_PLANKS_COLUMN, 2);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.DARK_OAK_LOG_COLUMN, Blocks.DARK_OAK_LOG, 1);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.DARK_OAK_WOOD_COLUMN, Blocks.DARK_OAK_LOG, 1);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.STRIPPED_DARK_OAK_LOG_COLUMN, Blocks.DARK_OAK_LOG, 1);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.STRIPPED_DARK_OAK_WOOD_COLUMN, Blocks.DARK_OAK_LOG, 1);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.STRIPPED_DARK_OAK_LOG_COLUMN, Blocks.STRIPPED_DARK_OAK_LOG, 1);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.STRIPPED_DARK_OAK_WOOD_COLUMN, Blocks.STRIPPED_DARK_OAK_WOOD, 1);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.DARK_OAK_PLANKS_COLUMN, Blocks.DARK_OAK_LOG, 4);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.DARK_OAK_WOOD_COLUMN, Blocks.DARK_OAK_WOOD, 1);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.STRIPPED_DARK_OAK_WOOD_COLUMN, Blocks.DARK_OAK_WOOD, 1);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.DARK_OAK_PLANKS_COLUMN, Blocks.DARK_OAK_WOOD, 4);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.STRIPPED_DARK_OAK_WOOD_COLUMN, Blocks.STRIPPED_DARK_OAK_LOG, 1);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.DARK_OAK_PLANKS_COLUMN, Blocks.STRIPPED_DARK_OAK_LOG, 4);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.DARK_OAK_PLANKS_COLUMN, Blocks.STRIPPED_DARK_OAK_WOOD, 4);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.DARK_OAK_PLANKS_COLUMN, Blocks.DARK_OAK_PLANKS, 1);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.STRIPPED_DARK_OAK_LOG_COLUMN, DDBlocks.DARK_OAK_LOG_COLUMN, 1);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.DARK_OAK_WOOD_COLUMN, DDBlocks.DARK_OAK_LOG_COLUMN, 1);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.STRIPPED_DARK_OAK_WOOD_COLUMN, DDBlocks.DARK_OAK_LOG_COLUMN, 1);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.DARK_OAK_PLANKS_COLUMN, DDBlocks.DARK_OAK_LOG_COLUMN, 4);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.STRIPPED_DARK_OAK_WOOD_COLUMN, DDBlocks.DARK_OAK_WOOD_COLUMN, 1);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.STRIPPED_DARK_OAK_WOOD_COLUMN, DDBlocks.STRIPPED_DARK_OAK_LOG_COLUMN, 1);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.DARK_OAK_PLANKS_COLUMN, DDBlocks.DARK_OAK_WOOD_COLUMN, 4);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.DARK_OAK_PLANKS_COLUMN, DDBlocks.STRIPPED_DARK_OAK_LOG_COLUMN, 4);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.DARK_OAK_PLANKS_COLUMN, DDBlocks.STRIPPED_DARK_OAK_WOOD_COLUMN, 4);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.MANGROVE_PLANKS, DDBlocks.MANGROVE_LOG_COLUMN, 4);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.MANGROVE_PLANKS, DDBlocks.MANGROVE_WOOD_COLUMN, 4);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.MANGROVE_PLANKS, DDBlocks.STRIPPED_MANGROVE_LOG_COLUMN, 4);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.MANGROVE_PLANKS, DDBlocks.STRIPPED_MANGROVE_WOOD_COLUMN, 4);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Items.STICK, DDBlocks.MANGROVE_PLANKS_COLUMN, 2);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.MANGROVE_LOG_COLUMN, Blocks.MANGROVE_LOG, 1);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.MANGROVE_WOOD_COLUMN, Blocks.MANGROVE_LOG, 1);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.STRIPPED_MANGROVE_LOG_COLUMN, Blocks.MANGROVE_LOG, 1);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.STRIPPED_MANGROVE_WOOD_COLUMN, Blocks.MANGROVE_LOG, 1);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.STRIPPED_MANGROVE_LOG_COLUMN, Blocks.STRIPPED_MANGROVE_LOG, 1);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.STRIPPED_MANGROVE_WOOD_COLUMN, Blocks.STRIPPED_MANGROVE_WOOD, 1);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.MANGROVE_PLANKS_COLUMN, Blocks.MANGROVE_LOG, 4);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.MANGROVE_WOOD_COLUMN, Blocks.MANGROVE_WOOD, 1);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.STRIPPED_MANGROVE_WOOD_COLUMN, Blocks.MANGROVE_WOOD, 1);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.MANGROVE_PLANKS_COLUMN, Blocks.MANGROVE_WOOD, 4);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.STRIPPED_MANGROVE_WOOD_COLUMN, Blocks.STRIPPED_MANGROVE_LOG, 1);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.MANGROVE_PLANKS_COLUMN, Blocks.STRIPPED_MANGROVE_LOG, 4);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.MANGROVE_PLANKS_COLUMN, Blocks.STRIPPED_MANGROVE_WOOD, 4);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.MANGROVE_PLANKS_COLUMN, Blocks.MANGROVE_PLANKS, 1);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.STRIPPED_MANGROVE_LOG_COLUMN, DDBlocks.MANGROVE_LOG_COLUMN, 1);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.MANGROVE_WOOD_COLUMN, DDBlocks.MANGROVE_LOG_COLUMN, 1);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.STRIPPED_MANGROVE_WOOD_COLUMN, DDBlocks.MANGROVE_LOG_COLUMN, 1);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.MANGROVE_PLANKS_COLUMN, DDBlocks.MANGROVE_LOG_COLUMN, 4);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.STRIPPED_MANGROVE_WOOD_COLUMN, DDBlocks.MANGROVE_WOOD_COLUMN, 1);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.STRIPPED_MANGROVE_WOOD_COLUMN, DDBlocks.STRIPPED_MANGROVE_LOG_COLUMN, 1);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.MANGROVE_PLANKS_COLUMN, DDBlocks.MANGROVE_WOOD_COLUMN, 4);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.MANGROVE_PLANKS_COLUMN, DDBlocks.STRIPPED_MANGROVE_LOG_COLUMN, 4);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.MANGROVE_PLANKS_COLUMN, DDBlocks.STRIPPED_MANGROVE_WOOD_COLUMN, 4);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.CHERRY_PLANKS, DDBlocks.CHERRY_LOG_COLUMN, 4);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.CHERRY_PLANKS, DDBlocks.CHERRY_WOOD_COLUMN, 4);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.CHERRY_PLANKS, DDBlocks.STRIPPED_CHERRY_LOG_COLUMN, 4);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.CHERRY_PLANKS, DDBlocks.STRIPPED_CHERRY_WOOD_COLUMN, 4);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Items.STICK, DDBlocks.CHERRY_PLANKS_COLUMN, 2);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.CHERRY_LOG_COLUMN, Blocks.CHERRY_LOG, 1);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.CHERRY_WOOD_COLUMN, Blocks.CHERRY_LOG, 1);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.STRIPPED_CHERRY_LOG_COLUMN, Blocks.CHERRY_LOG, 1);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.STRIPPED_CHERRY_WOOD_COLUMN, Blocks.CHERRY_LOG, 1);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.STRIPPED_CHERRY_LOG_COLUMN, Blocks.STRIPPED_CHERRY_LOG, 1);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.STRIPPED_CHERRY_WOOD_COLUMN, Blocks.STRIPPED_CHERRY_WOOD, 1);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.CHERRY_PLANKS_COLUMN, Blocks.CHERRY_LOG, 4);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.CHERRY_WOOD_COLUMN, Blocks.CHERRY_WOOD, 1);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.STRIPPED_CHERRY_WOOD_COLUMN, Blocks.CHERRY_WOOD, 1);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.CHERRY_PLANKS_COLUMN, Blocks.CHERRY_WOOD, 4);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.STRIPPED_CHERRY_WOOD_COLUMN, Blocks.STRIPPED_CHERRY_LOG, 1);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.CHERRY_PLANKS_COLUMN, Blocks.STRIPPED_CHERRY_LOG, 4);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.CHERRY_PLANKS_COLUMN, Blocks.STRIPPED_CHERRY_WOOD, 4);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.CHERRY_PLANKS_COLUMN, Blocks.CHERRY_PLANKS, 1);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.STRIPPED_CHERRY_LOG_COLUMN, DDBlocks.CHERRY_LOG_COLUMN, 1);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.CHERRY_WOOD_COLUMN, DDBlocks.CHERRY_LOG_COLUMN, 1);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.STRIPPED_CHERRY_WOOD_COLUMN, DDBlocks.CHERRY_LOG_COLUMN, 1);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.CHERRY_PLANKS_COLUMN, DDBlocks.CHERRY_LOG_COLUMN, 4);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.STRIPPED_CHERRY_WOOD_COLUMN, DDBlocks.CHERRY_WOOD_COLUMN, 1);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.STRIPPED_CHERRY_WOOD_COLUMN, DDBlocks.STRIPPED_CHERRY_LOG_COLUMN, 1);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.CHERRY_PLANKS_COLUMN, DDBlocks.CHERRY_WOOD_COLUMN, 4);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.CHERRY_PLANKS_COLUMN, DDBlocks.STRIPPED_CHERRY_LOG_COLUMN, 4);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.CHERRY_PLANKS_COLUMN, DDBlocks.STRIPPED_CHERRY_WOOD_COLUMN, 4);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Items.STICK, DDBlocks.BAMBOO_PLANKS_COLUMN, 2);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.BAMBOO_PLANKS, DDBlocks.BAMBOO_BLOCK_COLUMN, 2);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.BAMBOO_PLANKS, DDBlocks.STRIPPED_BAMBOO_BLOCK_COLUMN, 2);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.BAMBOO_MOSAIC, DDBlocks.BAMBOO_BLOCK_COLUMN, 2);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.BAMBOO_MOSAIC, DDBlocks.STRIPPED_BAMBOO_BLOCK_COLUMN, 2);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.BAMBOO_BLOCK_COLUMN, Blocks.BAMBOO_BLOCK, 1);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.STRIPPED_BAMBOO_BLOCK_COLUMN, Blocks.BAMBOO_BLOCK, 1);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.BAMBOO_PLANKS_COLUMN, Blocks.BAMBOO_BLOCK, 2);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.BAMBOO_MOSAIC_COLUMN, Blocks.BAMBOO_BLOCK, 2);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.STRIPPED_BAMBOO_BLOCK_COLUMN, Blocks.STRIPPED_BAMBOO_BLOCK, 1);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.BAMBOO_PLANKS_COLUMN, Blocks.STRIPPED_BAMBOO_BLOCK, 2);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.BAMBOO_MOSAIC_COLUMN, Blocks.STRIPPED_BAMBOO_BLOCK, 2);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.BAMBOO_PLANKS_COLUMN, Blocks.BAMBOO_PLANKS, 1);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.BAMBOO_MOSAIC_COLUMN, Blocks.BAMBOO_PLANKS, 1);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.BAMBOO_MOSAIC_COLUMN, Blocks.BAMBOO_MOSAIC, 1);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.BAMBOO_MOSAIC_COLUMN, DDBlocks.BAMBOO_PLANKS_COLUMN, 1);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.BAMBOO_MOSAIC_COLUMN, DDBlocks.BAMBOO_BLOCK_COLUMN, 2);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.BAMBOO_MOSAIC_COLUMN, DDBlocks.STRIPPED_BAMBOO_BLOCK_COLUMN, 2);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.BAMBOO_PLANKS_COLUMN, DDBlocks.BAMBOO_BLOCK_COLUMN, 2);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.BAMBOO_PLANKS_COLUMN, DDBlocks.STRIPPED_BAMBOO_BLOCK_COLUMN, 2);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.STRIPPED_BAMBOO_BLOCK_COLUMN, DDBlocks.BAMBOO_BLOCK_COLUMN, 1);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.AZALEA_PLANKS, DDBlocks.AZALEA_LOG_COLUMN, 4);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.AZALEA_PLANKS, DDBlocks.AZALEA_WOOD_COLUMN, 4);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.AZALEA_PLANKS, DDBlocks.STRIPPED_AZALEA_LOG_COLUMN, 4);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.AZALEA_PLANKS, DDBlocks.STRIPPED_AZALEA_WOOD_COLUMN, 4);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Items.STICK, DDBlocks.AZALEA_PLANKS_COLUMN, 2);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.AZALEA_LOG_COLUMN, DDBlocks.AZALEA_LOG, 1);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.AZALEA_WOOD_COLUMN, DDBlocks.AZALEA_LOG, 1);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.STRIPPED_AZALEA_LOG_COLUMN, DDBlocks.AZALEA_LOG, 1);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.STRIPPED_AZALEA_WOOD_COLUMN, DDBlocks.AZALEA_LOG, 1);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.STRIPPED_AZALEA_LOG_COLUMN, DDBlocks.STRIPPED_AZALEA_LOG, 1);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.STRIPPED_AZALEA_WOOD_COLUMN, DDBlocks.STRIPPED_AZALEA_WOOD, 1);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.AZALEA_PLANKS_COLUMN, DDBlocks.AZALEA_LOG, 4);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.AZALEA_WOOD_COLUMN, DDBlocks.AZALEA_WOOD, 1);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.STRIPPED_AZALEA_WOOD_COLUMN, DDBlocks.AZALEA_WOOD, 1);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.AZALEA_PLANKS_COLUMN, DDBlocks.AZALEA_WOOD, 4);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.STRIPPED_AZALEA_WOOD_COLUMN, DDBlocks.STRIPPED_AZALEA_LOG, 1);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.AZALEA_PLANKS_COLUMN, DDBlocks.STRIPPED_AZALEA_LOG, 4);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.AZALEA_PLANKS_COLUMN, DDBlocks.STRIPPED_AZALEA_WOOD, 4);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.AZALEA_PLANKS_COLUMN, DDBlocks.AZALEA_PLANKS, 1);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.STRIPPED_AZALEA_LOG_COLUMN, DDBlocks.AZALEA_LOG_COLUMN, 1);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.AZALEA_WOOD_COLUMN, DDBlocks.AZALEA_LOG_COLUMN, 1);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.STRIPPED_AZALEA_WOOD_COLUMN, DDBlocks.AZALEA_LOG_COLUMN, 1);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.AZALEA_PLANKS_COLUMN, DDBlocks.AZALEA_LOG_COLUMN, 4);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.STRIPPED_AZALEA_WOOD_COLUMN, DDBlocks.AZALEA_WOOD_COLUMN, 1);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.STRIPPED_AZALEA_WOOD_COLUMN, DDBlocks.STRIPPED_AZALEA_LOG_COLUMN, 1);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.AZALEA_PLANKS_COLUMN, DDBlocks.AZALEA_WOOD_COLUMN, 4);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.AZALEA_PLANKS_COLUMN, DDBlocks.STRIPPED_AZALEA_LOG_COLUMN, 4);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.AZALEA_PLANKS_COLUMN, DDBlocks.STRIPPED_AZALEA_WOOD_COLUMN, 4);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.SWAMP_PLANKS, DDBlocks.SWAMP_LOG_COLUMN, 4);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.SWAMP_PLANKS, DDBlocks.SWAMP_WOOD_COLUMN, 4);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.SWAMP_PLANKS, DDBlocks.STRIPPED_SWAMP_LOG_COLUMN, 4);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.SWAMP_PLANKS, DDBlocks.STRIPPED_SWAMP_WOOD_COLUMN, 4);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Items.STICK, DDBlocks.SWAMP_PLANKS_COLUMN, 2);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.SWAMP_LOG_COLUMN, DDBlocks.SWAMP_LOG, 1);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.SWAMP_WOOD_COLUMN, DDBlocks.SWAMP_LOG, 1);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.STRIPPED_SWAMP_LOG_COLUMN, DDBlocks.SWAMP_LOG, 1);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.STRIPPED_SWAMP_WOOD_COLUMN, DDBlocks.SWAMP_LOG, 1);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.STRIPPED_SWAMP_LOG_COLUMN, DDBlocks.STRIPPED_SWAMP_LOG, 1);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.STRIPPED_SWAMP_WOOD_COLUMN, DDBlocks.STRIPPED_SWAMP_WOOD, 1);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.SWAMP_PLANKS_COLUMN, DDBlocks.SWAMP_LOG, 4);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.SWAMP_WOOD_COLUMN, DDBlocks.SWAMP_WOOD, 1);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.STRIPPED_SWAMP_WOOD_COLUMN, DDBlocks.SWAMP_WOOD, 1);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.SWAMP_PLANKS_COLUMN, DDBlocks.SWAMP_WOOD, 4);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.STRIPPED_SWAMP_WOOD_COLUMN, DDBlocks.STRIPPED_SWAMP_LOG, 1);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.SWAMP_PLANKS_COLUMN, DDBlocks.STRIPPED_SWAMP_LOG, 4);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.SWAMP_PLANKS_COLUMN, DDBlocks.STRIPPED_SWAMP_WOOD, 4);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.SWAMP_PLANKS_COLUMN, DDBlocks.SWAMP_PLANKS, 1);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.STRIPPED_SWAMP_LOG_COLUMN, DDBlocks.SWAMP_LOG_COLUMN, 1);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.SWAMP_WOOD_COLUMN, DDBlocks.SWAMP_LOG_COLUMN, 1);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.STRIPPED_SWAMP_WOOD_COLUMN, DDBlocks.SWAMP_LOG_COLUMN, 1);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.SWAMP_PLANKS_COLUMN, DDBlocks.SWAMP_LOG_COLUMN, 4);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.STRIPPED_SWAMP_WOOD_COLUMN, DDBlocks.SWAMP_WOOD_COLUMN, 1);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.STRIPPED_SWAMP_WOOD_COLUMN, DDBlocks.STRIPPED_SWAMP_LOG_COLUMN, 1);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.SWAMP_PLANKS_COLUMN, DDBlocks.SWAMP_WOOD_COLUMN, 4);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.SWAMP_PLANKS_COLUMN, DDBlocks.STRIPPED_SWAMP_LOG_COLUMN, 4);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.SWAMP_PLANKS_COLUMN, DDBlocks.STRIPPED_SWAMP_WOOD_COLUMN, 4);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.CRIMSON_PLANKS, DDBlocks.CRIMSON_STEM_COLUMN, 4);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.CRIMSON_PLANKS, DDBlocks.CRIMSON_HYPHAE_COLUMN, 4);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.CRIMSON_PLANKS, DDBlocks.STRIPPED_CRIMSON_STEM_COLUMN, 4);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.CRIMSON_PLANKS, DDBlocks.STRIPPED_CRIMSON_HYPHAE_COLUMN, 4);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Items.STICK, DDBlocks.CRIMSON_PLANKS_COLUMN, 2);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.CRIMSON_STEM_COLUMN, Blocks.CRIMSON_STEM, 1);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.CRIMSON_HYPHAE_COLUMN, Blocks.CRIMSON_STEM, 1);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.STRIPPED_CRIMSON_STEM_COLUMN, Blocks.CRIMSON_STEM, 1);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.STRIPPED_CRIMSON_HYPHAE_COLUMN, Blocks.CRIMSON_STEM, 1);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.STRIPPED_CRIMSON_STEM_COLUMN, Blocks.STRIPPED_CRIMSON_STEM, 1);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.STRIPPED_CRIMSON_HYPHAE_COLUMN, Blocks.STRIPPED_CRIMSON_HYPHAE, 1);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.CRIMSON_PLANKS_COLUMN, Blocks.CRIMSON_STEM, 4);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.CRIMSON_HYPHAE_COLUMN, Blocks.CRIMSON_HYPHAE, 1);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.STRIPPED_CRIMSON_HYPHAE_COLUMN, Blocks.CRIMSON_HYPHAE, 1);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.CRIMSON_PLANKS_COLUMN, Blocks.CRIMSON_HYPHAE, 4);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.STRIPPED_CRIMSON_HYPHAE_COLUMN, Blocks.STRIPPED_CRIMSON_STEM, 1);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.CRIMSON_PLANKS_COLUMN, Blocks.STRIPPED_CRIMSON_STEM, 4);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.CRIMSON_PLANKS_COLUMN, Blocks.STRIPPED_CRIMSON_HYPHAE, 4);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.CRIMSON_PLANKS_COLUMN, Blocks.CRIMSON_PLANKS, 1);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.STRIPPED_CRIMSON_STEM_COLUMN, DDBlocks.CRIMSON_STEM_COLUMN, 1);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.CRIMSON_HYPHAE_COLUMN, DDBlocks.CRIMSON_STEM_COLUMN, 1);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.STRIPPED_CRIMSON_HYPHAE_COLUMN, DDBlocks.CRIMSON_STEM_COLUMN, 1);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.CRIMSON_PLANKS_COLUMN, DDBlocks.CRIMSON_STEM_COLUMN, 4);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.STRIPPED_CRIMSON_HYPHAE_COLUMN, DDBlocks.CRIMSON_HYPHAE_COLUMN, 1);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.STRIPPED_CRIMSON_HYPHAE_COLUMN, DDBlocks.STRIPPED_CRIMSON_STEM_COLUMN, 1);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.CRIMSON_PLANKS_COLUMN, DDBlocks.CRIMSON_HYPHAE_COLUMN, 4);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.CRIMSON_PLANKS_COLUMN, DDBlocks.STRIPPED_CRIMSON_STEM_COLUMN, 4);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.CRIMSON_PLANKS_COLUMN, DDBlocks.STRIPPED_CRIMSON_HYPHAE_COLUMN, 4);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.WARPED_PLANKS, DDBlocks.WARPED_STEM_COLUMN, 4);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.WARPED_PLANKS, DDBlocks.WARPED_HYPHAE_COLUMN, 4);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.WARPED_PLANKS, DDBlocks.STRIPPED_WARPED_STEM_COLUMN, 4);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Blocks.WARPED_PLANKS, DDBlocks.STRIPPED_WARPED_HYPHAE_COLUMN, 4);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, Items.STICK, DDBlocks.WARPED_PLANKS_COLUMN, 2);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.WARPED_STEM_COLUMN, Blocks.WARPED_STEM, 1);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.WARPED_HYPHAE_COLUMN, Blocks.WARPED_STEM, 1);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.STRIPPED_WARPED_STEM_COLUMN, Blocks.WARPED_STEM, 1);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.STRIPPED_WARPED_HYPHAE_COLUMN, Blocks.WARPED_STEM, 1);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.STRIPPED_WARPED_STEM_COLUMN, Blocks.STRIPPED_WARPED_STEM, 1);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.STRIPPED_WARPED_HYPHAE_COLUMN, Blocks.STRIPPED_WARPED_HYPHAE, 1);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.WARPED_PLANKS_COLUMN, Blocks.WARPED_STEM, 4);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.WARPED_HYPHAE_COLUMN, Blocks.WARPED_HYPHAE, 1);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.STRIPPED_WARPED_HYPHAE_COLUMN, Blocks.WARPED_HYPHAE, 1);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.WARPED_PLANKS_COLUMN, Blocks.WARPED_HYPHAE, 4);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.STRIPPED_WARPED_HYPHAE_COLUMN, Blocks.STRIPPED_WARPED_STEM, 1);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.WARPED_PLANKS_COLUMN, Blocks.STRIPPED_WARPED_STEM, 4);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.WARPED_PLANKS_COLUMN, Blocks.STRIPPED_WARPED_HYPHAE, 4);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.WARPED_PLANKS_COLUMN, Blocks.WARPED_PLANKS, 1);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.STRIPPED_WARPED_STEM_COLUMN, DDBlocks.WARPED_STEM_COLUMN, 1);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.WARPED_HYPHAE_COLUMN, DDBlocks.WARPED_STEM_COLUMN, 1);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.STRIPPED_WARPED_HYPHAE_COLUMN, DDBlocks.WARPED_STEM_COLUMN, 1);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.WARPED_PLANKS_COLUMN, DDBlocks.WARPED_STEM_COLUMN, 4);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.STRIPPED_WARPED_HYPHAE_COLUMN, DDBlocks.WARPED_HYPHAE_COLUMN, 1);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.STRIPPED_WARPED_HYPHAE_COLUMN, DDBlocks.STRIPPED_WARPED_STEM_COLUMN, 1);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.WARPED_PLANKS_COLUMN, DDBlocks.WARPED_HYPHAE_COLUMN, 4);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.WARPED_PLANKS_COLUMN, DDBlocks.STRIPPED_WARPED_STEM_COLUMN, 4);
        offerWoodcuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, DDBlocks.WARPED_PLANKS_COLUMN, DDBlocks.STRIPPED_WARPED_HYPHAE_COLUMN, 4);
    }

    private static void generateSmithingRecipes(RecipeExporter exporter) {
        RecipeProvider.offerNetheriteUpgradeRecipe(exporter, DDBlocks.DIAMOND_BARREL.asItem(), RecipeCategory.BUILDING_BLOCKS, DDBlocks.NETHERITE_BARREL.asItem());
        RecipeProvider.offerNetheriteUpgradeRecipe(exporter, DDItems.DIAMOND_BARREL_UPGRADE, RecipeCategory.BUILDING_BLOCKS, DDItems.NETHERITE_BARREL_UPGRADE);
    }

    private static void generateVanillaItemRecipesFromModItems(RecipeExporter exporter) {
        ShapelessRecipeJsonBuilder.create(RecipeCategory.REDSTONE, Items.QUARTZ, 1)
            .input(DDItems.QUARTZ_CHUNK)
            .input(DDItems.QUARTZ_CHUNK)
            .criterion(hasItem(DDItems.QUARTZ_CHUNK), conditionsFromItem(DDItems.QUARTZ_CHUNK))
            .offerTo(exporter, new Identifier(getRecipeName(Items.QUARTZ)) + "_from_chunks");

        ShapedRecipeJsonBuilder.create(RecipeCategory.MISC, Items.JACK_O_LANTERN)
            .pattern("P")
            .pattern("T")
            .input('P', Items.CARVED_PUMPKIN)
            .input('T', DDItemTags.DEFAULT_TORCHES)
            .criterion(FabricRecipeProvider.hasItem(Items.CARVED_PUMPKIN), FabricRecipeProvider.conditionsFromItem(Items.CARVED_PUMPKIN))
            .criterion(FabricRecipeProvider.hasItem(Items.TORCH), FabricRecipeProvider.conditionsFromItem(Items.TORCH))
            .criterion(FabricRecipeProvider.hasItem(DDItems.BONE_TORCH_ITEM), FabricRecipeProvider.conditionsFromItem(DDItems.BONE_TORCH_ITEM))
            .offerTo(exporter, new Identifier(getRecipeName(Blocks.JACK_O_LANTERN)));

        ShapedRecipeJsonBuilder.create(RecipeCategory.BUILDING_BLOCKS, Items.LANTERN, 1)
            .pattern("NNN")
            .pattern("NBN")
            .pattern("NNN")
            .input('N', Items.IRON_NUGGET)
            .input('B', DDItemTags.DEFAULT_TORCHES)
            .criterion(FabricRecipeProvider.hasItem(Items.IRON_NUGGET), FabricRecipeProvider.conditionsFromItem(Items.IRON_NUGGET))
            .criterion(FabricRecipeProvider.hasItem(Items.TORCH), FabricRecipeProvider.conditionsFromItem(Items.TORCH))
            .criterion(FabricRecipeProvider.hasItem(DDItems.BONE_TORCH_ITEM), FabricRecipeProvider.conditionsFromItem(DDItems.BONE_TORCH_ITEM))
            .offerTo(exporter, new Identifier(getRecipeName(Blocks.LANTERN)) + "_with_bone_torch");

        ShapedRecipeJsonBuilder.create(RecipeCategory.BUILDING_BLOCKS, Items.SOUL_LANTERN, 1)
            .pattern("NNN")
            .pattern("NBN")
            .pattern("NNN")
            .input('N', Items.IRON_NUGGET)
            .input('B', DDItemTags.SOUL_TORCHES)
            .criterion(FabricRecipeProvider.hasItem(Items.IRON_NUGGET), FabricRecipeProvider.conditionsFromItem(Items.IRON_NUGGET))
            .criterion(FabricRecipeProvider.hasItem(Items.SOUL_TORCH), FabricRecipeProvider.conditionsFromItem(Items.SOUL_TORCH))
            .criterion(FabricRecipeProvider.hasItem(DDItems.SOUL_BONE_TORCH_ITEM), FabricRecipeProvider.conditionsFromItem(DDItems.SOUL_BONE_TORCH_ITEM))
            .offerTo(exporter, new Identifier(getRecipeName(Blocks.SOUL_LANTERN)) + "_with_bone_torch");

        ShapedRecipeJsonBuilder.create(RecipeCategory.BUILDING_BLOCKS, Items.REPEATER, 1)
            .pattern("BRB")
            .pattern("SSS")
            .input('R', Items.REDSTONE)
            .input('S', Items.STONE)
            .input('B', DDItemTags.REDSTONE_TORCHES)
            .criterion(FabricRecipeProvider.hasItem(Items.REDSTONE), FabricRecipeProvider.conditionsFromItem(Items.REDSTONE))
            .criterion(FabricRecipeProvider.hasItem(Items.STONE), FabricRecipeProvider.conditionsFromItem(Items.STONE))
            .criterion(FabricRecipeProvider.hasItem(Items.REDSTONE_TORCH), FabricRecipeProvider.conditionsFromItem(Items.REDSTONE_TORCH))
            .criterion(FabricRecipeProvider.hasItem(DDItems.REDSTONE_BONE_TORCH_ITEM), FabricRecipeProvider.conditionsFromItem(DDItems.REDSTONE_BONE_TORCH_ITEM))
            .offerTo(exporter, new Identifier(getRecipeName(Blocks.REPEATER)));

        ShapedRecipeJsonBuilder.create(RecipeCategory.BUILDING_BLOCKS, Items.COMPARATOR, 1)
            .pattern(" B ")
            .pattern("BQB")
            .pattern("SSS")
            .input('Q', Items.QUARTZ)
            .input('S', Items.STONE)
            .input('B', DDItemTags.REDSTONE_TORCHES)
            .criterion(FabricRecipeProvider.hasItem(Items.QUARTZ), FabricRecipeProvider.conditionsFromItem(Items.QUARTZ))
            .criterion(FabricRecipeProvider.hasItem(Items.STONE), FabricRecipeProvider.conditionsFromItem(Items.STONE))
            .criterion(FabricRecipeProvider.hasItem(Items.REDSTONE_TORCH), FabricRecipeProvider.conditionsFromItem(Items.REDSTONE_TORCH))
            .criterion(FabricRecipeProvider.hasItem(DDItems.REDSTONE_BONE_TORCH_ITEM), FabricRecipeProvider.conditionsFromItem(DDItems.REDSTONE_BONE_TORCH_ITEM))
            .offerTo(exporter, new Identifier(getRecipeName(Blocks.COMPARATOR)));

        ShapedRecipeJsonBuilder.create(RecipeCategory.BUILDING_BLOCKS, Items.ACTIVATOR_RAIL, 1)
            .pattern("ISI")
            .pattern("IBI")
            .pattern("ISI")
            .input('I', Items.IRON_INGOT)
            .input('S', Items.STICK)
            .input('B', DDItemTags.REDSTONE_TORCHES)
            .criterion(FabricRecipeProvider.hasItem(Items.IRON_INGOT), FabricRecipeProvider.conditionsFromItem(Items.IRON_INGOT))
            .criterion(FabricRecipeProvider.hasItem(Items.STICK), FabricRecipeProvider.conditionsFromItem(Items.STICK))
            .criterion(FabricRecipeProvider.hasItem(Items.REDSTONE_TORCH), FabricRecipeProvider.conditionsFromItem(Items.REDSTONE_TORCH))
            .criterion(FabricRecipeProvider.hasItem(DDItems.REDSTONE_BONE_TORCH_ITEM), FabricRecipeProvider.conditionsFromItem(DDItems.REDSTONE_BONE_TORCH_ITEM))
            .offerTo(exporter, new Identifier(getRecipeName(Blocks.ACTIVATOR_RAIL)) + "_with_bone_torch");
    }

    private static void generateTorchRecipes(RecipeExporter exporter) {
        ShapedRecipeJsonBuilder.create(RecipeCategory.BUILDING_BLOCKS, DDBlocks.BONE_TORCH, 4)
            .pattern("C")
            .pattern("B")
            .input('C', ItemTags.COALS)
            .input('B', Items.BONE)
            .criterion(FabricRecipeProvider.hasItem(Items.COAL), FabricRecipeProvider.conditionsFromItem(Items.COAL))
            .criterion(FabricRecipeProvider.hasItem(Items.BONE), FabricRecipeProvider.conditionsFromItem(Items.BONE))
            .offerTo(exporter);

        ShapedRecipeJsonBuilder.create(RecipeCategory.BUILDING_BLOCKS, DDBlocks.SOUL_BONE_TORCH, 4)
            .pattern("C")
            .pattern("B")
            .pattern("S")
            .input('C', ItemTags.COALS)
            .input('B', Items.BONE)
            .input('S', ItemTags.SOUL_FIRE_BASE_BLOCKS)
            .criterion(FabricRecipeProvider.hasItem(Items.COAL), FabricRecipeProvider.conditionsFromItem(Items.COAL))
            .criterion(FabricRecipeProvider.hasItem(Items.BONE), FabricRecipeProvider.conditionsFromItem(Items.BONE))
            .criterion(FabricRecipeProvider.hasItem(Items.SOUL_SOIL), FabricRecipeProvider.conditionsFromItem(Items.SOUL_SOIL))
            .criterion(FabricRecipeProvider.hasItem(Items.SOUL_SAND), FabricRecipeProvider.conditionsFromItem(Items.SOUL_SAND))
            .offerTo(exporter);

        ShapedRecipeJsonBuilder.create(RecipeCategory.BUILDING_BLOCKS, DDBlocks.REDSTONE_BONE_TORCH, 1)
            .pattern("R")
            .pattern("B")
            .input('R', Items.REDSTONE)
            .input('B', Items.BONE)
            .criterion(FabricRecipeProvider.hasItem(Items.REDSTONE), FabricRecipeProvider.conditionsFromItem(Items.REDSTONE))
            .criterion(FabricRecipeProvider.hasItem(Items.BONE), FabricRecipeProvider.conditionsFromItem(Items.BONE))
            .offerTo(exporter);

        ShapedRecipeJsonBuilder.create(RecipeCategory.BUILDING_BLOCKS, DDItems.COPPER_TORCH_ITEM, 4)
            .pattern("C")
            .pattern("S")
            .pattern("P")
            .input('C', ItemTags.COALS)
            .input('S', Items.STICK)
            .input('P', Items.COPPER_INGOT)
            .criterion(FabricRecipeProvider.hasItem(Items.COAL), FabricRecipeProvider.conditionsFromItem(Items.COAL))
            .criterion(FabricRecipeProvider.hasItem(Items.CHARCOAL), FabricRecipeProvider.conditionsFromItem(Items.CHARCOAL))
            .criterion(FabricRecipeProvider.hasItem(Items.STICK), FabricRecipeProvider.conditionsFromItem(Items.STICK))
            .criterion(FabricRecipeProvider.hasItem(Items.COPPER_INGOT), FabricRecipeProvider.conditionsFromItem(Items.COPPER_INGOT))
            .offerTo(exporter, new Identifier(getRecipeName(DDItems.COPPER_TORCH_ITEM)));

    ShapedRecipeJsonBuilder.create(RecipeCategory.BUILDING_BLOCKS, DDItems.COPPER_BONE_TORCH_ITEM, 4)
            .pattern("C")
            .pattern("B")
            .pattern("P")
            .input('C', ItemTags.COALS)
            .input('B', Items.BONE)
            .input('P', Items.COPPER_INGOT)
            .criterion(FabricRecipeProvider.hasItem(Items.COAL), FabricRecipeProvider.conditionsFromItem(Items.COAL))
            .criterion(FabricRecipeProvider.hasItem(Items.CHARCOAL), FabricRecipeProvider.conditionsFromItem(Items.CHARCOAL))
            .criterion(FabricRecipeProvider.hasItem(Items.BONE), FabricRecipeProvider.conditionsFromItem(Items.BONE))
            .criterion(FabricRecipeProvider.hasItem(Items.COPPER_INGOT), FabricRecipeProvider.conditionsFromItem(Items.COPPER_INGOT))
            .offerTo(exporter, new Identifier(getRecipeName(DDItems.COPPER_BONE_TORCH_ITEM)));

        ShapedRecipeJsonBuilder.create(RecipeCategory.BUILDING_BLOCKS, DDItems.AMETHYST_TORCH_ITEM, 4)
            .pattern("C")
            .pattern("S")
            .pattern("P")
            .input('C', ItemTags.COALS)
            .input('S', Items.STICK)
            .input('P', Items.COPPER_INGOT)
            .criterion("coals", FabricRecipeProvider.conditionsFromTag(ItemTags.COALS))
            .criterion(FabricRecipeProvider.hasItem(Items.STICK), FabricRecipeProvider.conditionsFromItem(Items.STICK))
            .criterion(FabricRecipeProvider.hasItem(Items.AMETHYST_SHARD), FabricRecipeProvider.conditionsFromItem(Items.AMETHYST_SHARD))
            .offerTo(exporter, new Identifier(getRecipeName(DDItems.AMETHYST_TORCH_ITEM)));

        ShapedRecipeJsonBuilder.create(RecipeCategory.BUILDING_BLOCKS, DDItems.AMETHYST_BONE_TORCH_ITEM, 4)
            .pattern("C")
            .pattern("B")
            .pattern("P")
            .input('C', ItemTags.COALS)
            .input('B', Items.BONE)
            .input('P', Items.COPPER_INGOT)
            .criterion("coals", FabricRecipeProvider.conditionsFromTag(ItemTags.COALS))
            .criterion(FabricRecipeProvider.hasItem(Items.BONE), FabricRecipeProvider.conditionsFromItem(Items.BONE))
            .criterion(FabricRecipeProvider.hasItem(Items.AMETHYST_SHARD), FabricRecipeProvider.conditionsFromItem(Items.AMETHYST_SHARD))
            .offerTo(exporter, new Identifier(getRecipeName(DDItems.AMETHYST_BONE_TORCH_ITEM)));
    }

    private static void generateLanternRecipes(RecipeExporter exporter) {
        ShapedRecipeJsonBuilder.create(RecipeCategory.BUILDING_BLOCKS, DDBlocks.COPPER_LANTERN, 1)
            .pattern("NNN")
            .pattern("NBN")
            .pattern("NNN")
            .input('N', Items.IRON_NUGGET)
            .input('B', DDItemTags.COPPER_TORCHES)
            .criterion(FabricRecipeProvider.hasItem(Items.IRON_NUGGET), FabricRecipeProvider.conditionsFromItem(Items.IRON_NUGGET))
            .criterion("copper_torches", FabricRecipeProvider.conditionsFromTag(DDItemTags.COPPER_TORCHES))
            .offerTo(exporter, new Identifier(getRecipeName(DDBlocks.COPPER_LANTERN)));

        ShapedRecipeJsonBuilder.create(RecipeCategory.BUILDING_BLOCKS, DDBlocks.AMETHYST_LANTERN, 1)
            .pattern("NNN")
            .pattern("NBN")
            .pattern("NNN")
            .input('N', Items.IRON_NUGGET)
            .input('B', DDItemTags.AMETHYST_TORCHES)
            .criterion(FabricRecipeProvider.hasItem(Items.IRON_NUGGET), FabricRecipeProvider.conditionsFromItem(Items.IRON_NUGGET))
            .criterion("amethyst_torches", FabricRecipeProvider.conditionsFromTag(DDItemTags.AMETHYST_TORCHES))
            .offerTo(exporter, new Identifier(getRecipeName(DDBlocks.AMETHYST_LANTERN)));
    }

    private static void generateCampfireRecipes(RecipeExporter exporter) {
        ShapedRecipeJsonBuilder.create(RecipeCategory.BUILDING_BLOCKS, DDBlocks.COPPER_CAMPFIRE, 1)
            .pattern(" S ")
            .pattern("SCS")
            .pattern("LLL")
            .input('C', Items.COPPER_INGOT)
            .input('S', Items.STICK)
            .input('L', ItemTags.LOGS)
            .criterion(FabricRecipeProvider.hasItem(Items.COPPER_INGOT), FabricRecipeProvider.conditionsFromItem(Items.COPPER_INGOT))
            .criterion(FabricRecipeProvider.hasItem(Items.STICK), FabricRecipeProvider.conditionsFromItem(Items.STICK))
            .criterion("logs", FabricRecipeProvider.conditionsFromTag(ItemTags.LOGS))
            .offerTo(exporter, new Identifier(getRecipeName(DDBlocks.COPPER_CAMPFIRE)));

        ShapedRecipeJsonBuilder.create(RecipeCategory.BUILDING_BLOCKS, DDBlocks.AMETHYST_CAMPFIRE, 1)
            .pattern(" S ")
            .pattern("SAS")
            .pattern("LLL")
            .input('A', Items.AMETHYST_SHARD)
            .input('S', Items.STICK)
            .input('L', ItemTags.LOGS)
            .criterion(FabricRecipeProvider.hasItem(Items.AMETHYST_SHARD), FabricRecipeProvider.conditionsFromItem(Items.AMETHYST_SHARD))
            .criterion(FabricRecipeProvider.hasItem(Items.STICK), FabricRecipeProvider.conditionsFromItem(Items.STICK))
            .criterion("logs", FabricRecipeProvider.conditionsFromTag(ItemTags.LOGS))
            .offerTo(exporter, new Identifier(getRecipeName(DDBlocks.AMETHYST_CAMPFIRE)));
    }

    private static void offerMossyBlockRecipe(RecipeExporter exporter, RecipeCategory category, ItemConvertible input, ItemConvertible output) {
        ShapelessRecipeJsonBuilder.create(category, output, 1)
            .input(DDItemTags.MOSS_FOR_RECIPE)
            .input(input)
            .criterion("moss_item", conditionsFromTag(DDItemTags.MOSS_FOR_RECIPE))
            .criterion(hasItem(input), conditionsFromItem(input))
            .offerTo(exporter);
    }

    private static void offerWaxBlockRecipe(RecipeExporter exporter, RecipeCategory category, ItemConvertible input, ItemConvertible output) {
        ShapelessRecipeJsonBuilder.create(category, output, 1)
            .input(Items.HONEYCOMB)
            .input(input)
            .criterion("honeycomb", conditionsFromItem(Items.HONEYCOMB))
            .criterion(hasItem(input), conditionsFromItem(input))
            .offerTo(exporter, new Identifier(getRecipeName(output) + "2"));
    }

    private static void offerDispenserShapedRecipe(RecipeExporter exporter, ItemConvertible centerItem, ItemConvertible output) {
        ShapedRecipeJsonBuilder.create(RecipeCategory.REDSTONE, output, 1)
            .pattern("CCC")
            .pattern("C#C")
            .pattern("CRC")
            .input('C', Items.COBBLESTONE)
            .input('R', Items.REDSTONE)
            .input('#', centerItem)
            .criterion(FabricRecipeProvider.hasItem(Items.COBBLESTONE), FabricRecipeProvider.conditionsFromItem(Items.COBBLESTONE))
            .criterion(FabricRecipeProvider.hasItem(Items.REDSTONE), FabricRecipeProvider.conditionsFromItem(Items.REDSTONE))
            .criterion(FabricRecipeProvider.hasItem(centerItem), FabricRecipeProvider.conditionsFromItem(centerItem))
            .offerTo(exporter, new Identifier(getRecipeName(output)));
    }

    private static void offerPillarRecipe(RecipeExporter exporter, ItemConvertible input, ItemConvertible output) {
        ShapedRecipeJsonBuilder.create(RecipeCategory.REDSTONE, output, 2)
            .pattern("#")
            .pattern("#")
            .input('#', input)
            .criterion(FabricRecipeProvider.hasItem(input), FabricRecipeProvider.conditionsFromItem(input))
            .offerTo(exporter, new Identifier(getRecipeName(output)));
    }

    private static void offer3ItemShapelessCompactingRecipe(RecipeExporter exporter, RecipeCategory category, ItemConvertible input, ItemConvertible output, int count) {
        ShapelessRecipeJsonBuilder.create(category, output, count)
            .input(input).input(input).input(input)
            .criterion(hasItem(input), conditionsFromItem(input))
            .offerTo(exporter, new Identifier(getRecipeName(output) + "_three_item_shapeless_compacting"));
    }

    private static void offer2x2BrickRecipe(RecipeExporter exporter, RecipeCategory category, ItemConvertible input, ItemConvertible output) {
        ShapedRecipeJsonBuilder.create(category, output, 4)
            .pattern("##")
            .pattern("##")
            .input('#', input)
            .criterion(FabricRecipeProvider.hasItem(input), FabricRecipeProvider.conditionsFromItem(input))
            .offerTo(exporter, new Identifier(getRecipeName(output)));
    }

    private static void offer3x3CompactingNoNameConflict(RecipeExporter exporter, RecipeCategory category, ItemConvertible output, ItemConvertible input) {
        ShapedRecipeJsonBuilder.create(category, output, 1)
            .pattern("@@@")
            .pattern("@@@")
            .pattern("@@@")
            .input('@', input)
            .criterion(FabricRecipeProvider.hasItem(input), FabricRecipeProvider.conditionsFromItem(input))
            .offerTo(exporter, new Identifier(getRecipeName(output)) + "_3x3_compacting");
    }

    private static void offerColumnRecipe(RecipeExporter exporter, RecipeCategory category, ItemConvertible output, ItemConvertible input) {
        ShapedRecipeJsonBuilder.create(category, output, 3)
            .pattern("@")
            .pattern("@")
            .pattern("@")
            .input('@', input)
            .criterion(FabricRecipeProvider.hasItem(input), FabricRecipeProvider.conditionsFromItem(input))
            .offerTo(exporter, new Identifier(getRecipeName(output)));
    }

    private static void dequartzify(RecipeExporter exporter, ItemConvertible input, int count, boolean charred) {
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.BUILDING_BLOCKS, charred ? DDItems.CHARRED_QUARTZ : Items.QUARTZ, input, count);
    }

    private static void uncraftMetalBlock(RecipeExporter exporter, ItemConvertible input, Item output, int count) {
        uncraftMetalBlock(exporter, input, null, output, count);
    }

    private static void uncraftMetalBlock(RecipeExporter exporter, ItemConvertible input1, ItemConvertible input2, Item output, int count) {
        RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.MISC, output, input1, count);
        if (input2 != null) RecipeProvider.offerStonecuttingRecipe(exporter, RecipeCategory.MISC, output, input2, count);
    }

    public static SingleItemRecipeJsonBuilder createWoodcutting(Ingredient input, RecipeCategory category, ItemConvertible output) {
        return new SingleItemRecipeJsonBuilder(category, WoodcuttingRecipe::new, input, output, 1);
    }

    public static SingleItemRecipeJsonBuilder createWoodcutting(Ingredient input, RecipeCategory category, ItemConvertible output, int count) {
        return new SingleItemRecipeJsonBuilder(category, WoodcuttingRecipe::new, input, output, count);
    }

    public static void offerWoodcuttingRecipe(RecipeExporter exporter, RecipeCategory category, ItemConvertible output, ItemConvertible input) {
        offerWoodcuttingRecipe(exporter, category, output, input, 1);
    }

    public static void offerWoodcuttingRecipe(RecipeExporter exporter, RecipeCategory category, ItemConvertible output, ItemConvertible input, int count) {
        createWoodcutting(Ingredient.ofItems(input), category, output, count).criterion(RecipeProvider.hasItem(input), RecipeProvider.conditionsFromItem(input)).offerTo(exporter, RecipeProvider.convertBetween(output, input) + "_woodcutting");
    }
}
