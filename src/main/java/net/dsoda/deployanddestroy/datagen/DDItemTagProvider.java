package net.dsoda.deployanddestroy.datagen;

import net.dsoda.deployanddestroy.blocks.DDBlocks;
import net.dsoda.deployanddestroy.items.DDItems;
import net.dsoda.deployanddestroy.util.tag.DDItemTags;
import net.fabricmc.fabric.api.datagen.v1.FabricDataOutput;
import net.fabricmc.fabric.api.datagen.v1.provider.FabricTagProvider;
import net.minecraft.item.Items;
import net.minecraft.registry.RegistryWrapper;
import net.minecraft.registry.tag.ItemTags;

import java.util.concurrent.CompletableFuture;

public class DDItemTagProvider extends FabricTagProvider.ItemTagProvider {
    public DDItemTagProvider(FabricDataOutput output, CompletableFuture<RegistryWrapper.WrapperLookup> registriesFuture) {
        super(output, registriesFuture);
    }

    @Override
    protected void configure(RegistryWrapper.WrapperLookup arg) {
        getOrCreateTagBuilder(DDItemTags.FARMLAND_PLANTABLE)
            .add(Items.WHEAT_SEEDS)
            .add(Items.BEETROOT_SEEDS)
            .add(Items.MELON_SEEDS)
            .add(Items.PUMPKIN_SEEDS)
            .add(Items.TORCHFLOWER_SEEDS)
            .add(Items.PITCHER_POD)
            .add(Items.POTATO)
            .add(Items.CARROT);

        getOrCreateTagBuilder(ItemTags.SIGNS)
            .add(DDItems.AZALEA_SIGN)
            .add(DDItems.SWAMP_SIGN);

        getOrCreateTagBuilder(ItemTags.HANGING_SIGNS)
            .add(DDItems.AZALEA_HANGING_SIGN)
            .add(DDItems.SWAMP_HANGING_SIGN);

        getOrCreateTagBuilder(ItemTags.SAPLINGS)
            .add(DDBlocks.SWAMP_SAPLING.asItem());

        getOrCreateTagBuilder(ItemTags.LEAVES)
            .add(DDBlocks.SWAMP_LEAVES.asItem());

        getOrCreateTagBuilder(ItemTags.PLANKS)
            .add(DDBlocks.AZALEA_PLANKS.asItem())
            .add(DDBlocks.SWAMP_PLANKS.asItem());

        getOrCreateTagBuilder(ItemTags.LOGS_THAT_BURN)
            .add(DDBlocks.AZALEA_LOG.asItem())
            .add(DDBlocks.AZALEA_WOOD.asItem())
            .add(DDBlocks.STRIPPED_AZALEA_LOG.asItem())
            .add(DDBlocks.STRIPPED_AZALEA_WOOD.asItem())
            .add(DDBlocks.SWAMP_LOG.asItem())
            .add(DDBlocks.SWAMP_WOOD.asItem())
            .add(DDBlocks.STRIPPED_SWAMP_LOG.asItem())
            .add(DDBlocks.STRIPPED_SWAMP_WOOD.asItem());

        getOrCreateTagBuilder(ItemTags.LOGS)
            .add(DDBlocks.AZALEA_LOG.asItem())
            .add(DDBlocks.AZALEA_WOOD.asItem())
            .add(DDBlocks.STRIPPED_AZALEA_LOG.asItem())
            .add(DDBlocks.STRIPPED_AZALEA_WOOD.asItem())
            .add(DDBlocks.SWAMP_LOG.asItem())
            .add(DDBlocks.SWAMP_WOOD.asItem())
            .add(DDBlocks.STRIPPED_SWAMP_LOG.asItem())
            .add(DDBlocks.STRIPPED_SWAMP_WOOD.asItem());

        getOrCreateTagBuilder(DDItemTags.AZALEA_LOGS)
            .add(DDBlocks.AZALEA_LOG.asItem())
            .add(DDBlocks.STRIPPED_AZALEA_LOG.asItem())
            .add(DDBlocks.AZALEA_WOOD.asItem())
            .add(DDBlocks.STRIPPED_AZALEA_WOOD.asItem());

        getOrCreateTagBuilder(DDItemTags.SWAMP_LOGS)
            .add(DDBlocks.SWAMP_LOG.asItem())
            .add(DDBlocks.SWAMP_WOOD.asItem())
            .add(DDBlocks.STRIPPED_SWAMP_LOG.asItem())
            .add(DDBlocks.STRIPPED_SWAMP_WOOD.asItem());

        getOrCreateTagBuilder(ItemTags.DOORS)
            .add(DDBlocks.AZALEA_DOOR.asItem())
            .add(DDBlocks.SWAMP_DOOR.asItem());

        getOrCreateTagBuilder(ItemTags.WOODEN_DOORS)
            .add(DDBlocks.AZALEA_DOOR.asItem())
            .add(DDBlocks.SWAMP_DOOR.asItem());

        getOrCreateTagBuilder(ItemTags.TRAPDOORS)
            .add(DDBlocks.AZALEA_TRAPDOOR.asItem())
            .add(DDBlocks.SWAMP_TRAPDOOR.asItem());

        getOrCreateTagBuilder(ItemTags.WOODEN_TRAPDOORS)
            .add(DDBlocks.AZALEA_TRAPDOOR.asItem())
            .add(DDBlocks.SWAMP_TRAPDOOR.asItem());

        getOrCreateTagBuilder(ItemTags.BUTTONS)
            .add(DDBlocks.AZALEA_BUTTON.asItem())
            .add(DDBlocks.SWAMP_BUTTON.asItem());

        getOrCreateTagBuilder(ItemTags.WOODEN_BUTTONS)
            .add(DDBlocks.AZALEA_BUTTON.asItem())
            .add(DDBlocks.SWAMP_BUTTON.asItem());

        getOrCreateTagBuilder(ItemTags.FENCES)
            .add(DDBlocks.AZALEA_FENCE.asItem())
            .add(DDBlocks.SWAMP_FENCE.asItem())
            .add(DDBlocks.CRACKED_NETHER_BRICK_FENCE.asItem());

        getOrCreateTagBuilder(ItemTags.WOODEN_FENCES)
            .add(DDBlocks.AZALEA_FENCE.asItem())
            .add(DDBlocks.SWAMP_FENCE.asItem());

        getOrCreateTagBuilder(ItemTags.WOODEN_STAIRS)
            .add(DDBlocks.AZALEA_STAIRS.asItem())
            .add(DDBlocks.SWAMP_STAIRS.asItem());

        getOrCreateTagBuilder(ItemTags.WOODEN_SLABS)
            .add(DDBlocks.AZALEA_SLAB.asItem())
            .add(DDBlocks.SWAMP_SLAB.asItem());

        getOrCreateTagBuilder(ItemTags.LEAVES)
            .add(DDBlocks.SWAMP_LEAVES.asItem());

        getOrCreateTagBuilder(ItemTags.REDSTONE_ORES)
            .add(DDBlocks.END_REDSTONE_ORE.asItem());

        getOrCreateTagBuilder(DDItemTags.DEFAULT_TORCHES)
            .add(Items.TORCH)
            .add(DDItems.BONE_TORCH_ITEM);

        getOrCreateTagBuilder(DDItemTags.SOUL_TORCHES)
            .add(Items.SOUL_TORCH)
            .add(DDItems.SOUL_BONE_TORCH_ITEM);

        getOrCreateTagBuilder(DDItemTags.REDSTONE_TORCHES)
            .add(Items.REDSTONE_TORCH)
            .add(DDItems.REDSTONE_BONE_TORCH_ITEM);

        getOrCreateTagBuilder(DDItemTags.COPPER_TORCHES)
            .add(DDItems.COPPER_TORCH_ITEM)
            .add(DDItems.COPPER_BONE_TORCH_ITEM);

        getOrCreateTagBuilder(DDItemTags.AMETHYST_TORCHES)
            .add(DDItems.AMETHYST_TORCH_ITEM)
            .add(DDItems.AMETHYST_BONE_TORCH_ITEM);

        getOrCreateTagBuilder(DDItemTags.MOSS_FOR_RECIPE)
            .add(Items.MOSS_BLOCK)
            .add(Items.VINE);

        getOrCreateTagBuilder(DDItemTags.BURNABLE_LOG_COLUMNS)
            .add(DDBlocks.OAK_LOG_COLUMN.asItem())
            .add(DDBlocks.OAK_WOOD_COLUMN.asItem())
            .add(DDBlocks.STRIPPED_OAK_LOG_COLUMN.asItem())
            .add(DDBlocks.STRIPPED_OAK_WOOD_COLUMN.asItem())
            .add(DDBlocks.BIRCH_LOG_COLUMN.asItem())
            .add(DDBlocks.BIRCH_WOOD_COLUMN.asItem())
            .add(DDBlocks.STRIPPED_BIRCH_LOG_COLUMN.asItem())
            .add(DDBlocks.STRIPPED_BIRCH_WOOD_COLUMN.asItem())
            .add(DDBlocks.SPRUCE_LOG_COLUMN.asItem())
            .add(DDBlocks.SPRUCE_WOOD_COLUMN.asItem())
            .add(DDBlocks.STRIPPED_SPRUCE_LOG_COLUMN.asItem())
            .add(DDBlocks.STRIPPED_SPRUCE_WOOD_COLUMN.asItem())
            .add(DDBlocks.JUNGLE_LOG_COLUMN.asItem())
            .add(DDBlocks.JUNGLE_WOOD_COLUMN.asItem())
            .add(DDBlocks.STRIPPED_JUNGLE_LOG_COLUMN.asItem())
            .add(DDBlocks.STRIPPED_JUNGLE_WOOD_COLUMN.asItem())
            .add(DDBlocks.ACACIA_LOG_COLUMN.asItem())
            .add(DDBlocks.ACACIA_WOOD_COLUMN.asItem())
            .add(DDBlocks.STRIPPED_ACACIA_LOG_COLUMN.asItem())
            .add(DDBlocks.STRIPPED_ACACIA_WOOD_COLUMN.asItem())
            .add(DDBlocks.DARK_OAK_LOG_COLUMN.asItem())
            .add(DDBlocks.DARK_OAK_WOOD_COLUMN.asItem())
            .add(DDBlocks.STRIPPED_DARK_OAK_LOG_COLUMN.asItem())
            .add(DDBlocks.STRIPPED_DARK_OAK_WOOD_COLUMN.asItem())
            .add(DDBlocks.MANGROVE_LOG_COLUMN.asItem())
            .add(DDBlocks.MANGROVE_WOOD_COLUMN.asItem())
            .add(DDBlocks.STRIPPED_MANGROVE_LOG_COLUMN.asItem())
            .add(DDBlocks.STRIPPED_MANGROVE_WOOD_COLUMN.asItem())
            .add(DDBlocks.CHERRY_LOG_COLUMN.asItem())
            .add(DDBlocks.CHERRY_WOOD_COLUMN.asItem())
            .add(DDBlocks.STRIPPED_CHERRY_LOG_COLUMN.asItem())
            .add(DDBlocks.STRIPPED_CHERRY_WOOD_COLUMN.asItem())
            .add(DDBlocks.BAMBOO_BLOCK_COLUMN.asItem())
            .add(DDBlocks.STRIPPED_BAMBOO_BLOCK_COLUMN.asItem())
            .add(DDBlocks.AZALEA_LOG_COLUMN.asItem())
            .add(DDBlocks.AZALEA_WOOD_COLUMN.asItem())
            .add(DDBlocks.STRIPPED_AZALEA_LOG_COLUMN.asItem())
            .add(DDBlocks.STRIPPED_AZALEA_WOOD_COLUMN.asItem())
            .add(DDBlocks.SWAMP_LOG_COLUMN.asItem())
            .add(DDBlocks.SWAMP_WOOD_COLUMN.asItem())
            .add(DDBlocks.STRIPPED_SWAMP_LOG_COLUMN.asItem())
            .add(DDBlocks.STRIPPED_SWAMP_WOOD_COLUMN.asItem());

        getOrCreateTagBuilder(DDItemTags.BURNABLE_PLANK_COLUMNS)
            .add(DDBlocks.OAK_PLANKS_COLUMN.asItem())
            .add(DDBlocks.SPRUCE_PLANKS_COLUMN.asItem())
            .add(DDBlocks.BIRCH_PLANKS_COLUMN.asItem())
            .add(DDBlocks.JUNGLE_PLANKS_COLUMN.asItem())
            .add(DDBlocks.ACACIA_PLANKS_COLUMN.asItem())
            .add(DDBlocks.DARK_OAK_PLANKS_COLUMN.asItem())
            .add(DDBlocks.MANGROVE_PLANKS_COLUMN.asItem())
            .add(DDBlocks.CHERRY_PLANKS_COLUMN.asItem())
            .add(DDBlocks.BAMBOO_PLANKS_COLUMN.asItem())
            .add(DDBlocks.BAMBOO_MOSAIC_COLUMN.asItem())
            .add(DDBlocks.AZALEA_PLANKS_COLUMN.asItem())
            .add(DDBlocks.SWAMP_PLANKS_COLUMN.asItem());

        getOrCreateTagBuilder(DDItemTags.BARREL_UPGRADE_ITEMS)
            .add(DDItems.COPPER_BARREL_UPGRADE)
            .add(DDItems.IRON_BARREL_UPGRADE)
            .add(DDItems.GOLD_BARREL_UPGRADE)
            .add(DDItems.DIAMOND_BARREL_UPGRADE)
            .add(DDItems.NETHERITE_BARREL_UPGRADE);

        getOrCreateTagBuilder(ItemTags.STAIRS)
            .add(DDBlocks.AZALEA_STAIRS.asItem())
            .add(DDBlocks.SWAMP_STAIRS.asItem())
            .add(DDBlocks.SMOOTH_STONE_STAIRS.asItem())
            .add(DDBlocks.CRACKED_BRICK_STAIRS.asItem())
            .add(DDBlocks.GRANITE_BRICK_STAIRS.asItem())
            .add(DDBlocks.CRACKED_GRANITE_BRICK_STAIRS.asItem())
            .add(DDBlocks.MOSSY_GRANITE_BRICK_STAIRS.asItem())
            .add(DDBlocks.DIORITE_BRICK_STAIRS.asItem())
            .add(DDBlocks.CRACKED_DIORITE_BRICK_STAIRS.asItem())
            .add(DDBlocks.MOSSY_DIORITE_BRICK_STAIRS.asItem())
            .add(DDBlocks.ANDESITE_BRICK_STAIRS.asItem())
            .add(DDBlocks.CRACKED_ANDESITE_BRICK_STAIRS.asItem())
            .add(DDBlocks.MOSSY_ANDESITE_BRICK_STAIRS.asItem())
            .add(DDBlocks.CALCITE_STAIRS.asItem())
            .add(DDBlocks.POLISHED_CALCITE_STAIRS.asItem())
            .add(DDBlocks.CALCITE_BRICK_STAIRS.asItem())
            .add(DDBlocks.CRACKED_CALCITE_BRICK_STAIRS.asItem())
            .add(DDBlocks.MOSSY_CALCITE_BRICK_STAIRS.asItem())
            .add(DDBlocks.DRIPSTONE_STAIRS.asItem())
            .add(DDBlocks.POLISHED_DRIPSTONE_STAIRS.asItem())
            .add(DDBlocks.DRIPSTONE_BRICK_STAIRS.asItem())
            .add(DDBlocks.CRACKED_DRIPSTONE_BRICK_STAIRS.asItem())
            .add(DDBlocks.MOSSY_DRIPSTONE_BRICK_STAIRS.asItem())
            .add(DDBlocks.MOSSY_COBBLED_DEEPSLATE_STAIRS.asItem())
            .add(DDBlocks.CRACKED_DEEPSLATE_BRICK_STAIRS.asItem())
            .add(DDBlocks.MOSSY_DEEPSLATE_BRICK_STAIRS.asItem())
            .add(DDBlocks.CRACKED_DEEPSLATE_TILE_STAIRS.asItem())
            .add(DDBlocks.CRACKED_BRICK_STAIRS.asItem())
            .add(DDBlocks.MOSSY_BRICK_STAIRS.asItem())
            .add(DDBlocks.SANDSTONE_BRICK_STAIRS.asItem())
            .add(DDBlocks.CRACKED_SANDSTONE_BRICK_STAIRS.asItem())
            .add(DDBlocks.MOSSY_SANDSTONE_BRICK_STAIRS.asItem())
            .add(DDBlocks.POLISHED_SANDSTONE_STAIRS.asItem())
            .add(DDBlocks.RED_SANDSTONE_BRICK_STAIRS.asItem())
            .add(DDBlocks.CRACKED_RED_SANDSTONE_BRICK_STAIRS.asItem())
            .add(DDBlocks.MOSSY_RED_SANDSTONE_BRICK_STAIRS.asItem())
            .add(DDBlocks.POLISHED_RED_SANDSTONE_STAIRS.asItem())
            .add(DDBlocks.SANDSTONE_BRICK_STAIRS.asItem())
            .add(DDBlocks.CRACKED_SANDSTONE_BRICK_STAIRS.asItem())
            .add(DDBlocks.MOSSY_SANDSTONE_BRICK_STAIRS.asItem())
            .add(DDBlocks.NETHERRACK_STAIRS.asItem())
            .add(DDBlocks.CRACKED_NETHER_BRICK_STAIRS.asItem())
            .add(DDBlocks.SMOOTH_BASALT_STAIRS.asItem())
            .add(DDBlocks.CRACKED_POLISHED_BLACKSTONE_BRICK_STAIRS.asItem())
            .add(DDBlocks.CUT_IRON_STAIRS.asItem())
            .add(DDBlocks.CUT_GOLD_STAIRS.asItem())
            .add(DDBlocks.CUT_EMERALD_STAIRS.asItem())
            .add(DDBlocks.CUT_LAPIS_STAIRS.asItem())
            .add(DDBlocks.CUT_DIAMOND_STAIRS.asItem())
            .add(DDBlocks.CUT_NETHERITE_STAIRS.asItem())
            .add(DDBlocks.QUARTZ_BRICK_STAIRS.asItem())
            .add(DDBlocks.CHARRED_QUARTZ_STAIRS.asItem())
            .add(DDBlocks.SMOOTH_CHARRED_QUARTZ_STAIRS.asItem())
            .add(DDBlocks.CHARRED_QUARTZ_BRICK_STAIRS.asItem())
            .add(DDBlocks.POLISHED_AMETHYST_STAIRS.asItem())
            .add(DDBlocks.POLISHED_AMETHYST_BRICK_STAIRS.asItem())
            .add(DDBlocks.CUT_POLISHED_AMETHYST_STAIRS.asItem())
            .add(DDBlocks.PACKED_ICE_BRICK_STAIRS.asItem())
            .add(DDBlocks.CRACKED_PACKED_ICE_BRICK_STAIRS.asItem())
            .add(DDBlocks.LARGE_PACKED_ICE_BRICK_STAIRS.asItem())
            .add(DDBlocks.LARGE_CRACKED_PACKED_ICE_BRICK_STAIRS.asItem())
            .add(DDBlocks.POLISHED_PACKED_ICE_STAIRS.asItem())
            .add(DDBlocks.BLUE_ICE_BRICK_STAIRS.asItem())
            .add(DDBlocks.CRACKED_BLUE_ICE_BRICK_STAIRS.asItem())
            .add(DDBlocks.LARGE_BLUE_ICE_BRICK_STAIRS.asItem())
            .add(DDBlocks.LARGE_CRACKED_BLUE_ICE_BRICK_STAIRS.asItem())
            .add(DDBlocks.POLISHED_BLUE_ICE_STAIRS.asItem());

        getOrCreateTagBuilder(ItemTags.SLABS)
            .add(DDBlocks.AZALEA_SLAB.asItem())
            .add(DDBlocks.SWAMP_SLAB.asItem())
            .add(DDBlocks.CRACKED_BRICK_SLAB.asItem())
            .add(DDBlocks.GRANITE_BRICK_SLAB.asItem())
            .add(DDBlocks.CRACKED_GRANITE_BRICK_SLAB.asItem())
            .add(DDBlocks.MOSSY_GRANITE_BRICK_SLAB.asItem())
            .add(DDBlocks.DIORITE_BRICK_SLAB.asItem())
            .add(DDBlocks.CRACKED_DIORITE_BRICK_SLAB.asItem())
            .add(DDBlocks.MOSSY_DIORITE_BRICK_SLAB.asItem())
            .add(DDBlocks.ANDESITE_BRICK_SLAB.asItem())
            .add(DDBlocks.CRACKED_ANDESITE_BRICK_SLAB.asItem())
            .add(DDBlocks.MOSSY_ANDESITE_BRICK_SLAB.asItem())
            .add(DDBlocks.CALCITE_SLAB.asItem())
            .add(DDBlocks.POLISHED_CALCITE_SLAB.asItem())
            .add(DDBlocks.CALCITE_BRICK_SLAB.asItem())
            .add(DDBlocks.CRACKED_CALCITE_BRICK_SLAB.asItem())
            .add(DDBlocks.MOSSY_CALCITE_BRICK_SLAB.asItem())
            .add(DDBlocks.DRIPSTONE_SLAB.asItem())
            .add(DDBlocks.POLISHED_DRIPSTONE_SLAB.asItem())
            .add(DDBlocks.DRIPSTONE_BRICK_SLAB.asItem())
            .add(DDBlocks.CRACKED_DRIPSTONE_BRICK_SLAB.asItem())
            .add(DDBlocks.MOSSY_DRIPSTONE_BRICK_SLAB.asItem())
            .add(DDBlocks.MOSSY_COBBLED_DEEPSLATE_SLAB.asItem())
            .add(DDBlocks.CRACKED_DEEPSLATE_BRICK_SLAB.asItem())
            .add(DDBlocks.MOSSY_DEEPSLATE_BRICK_SLAB.asItem())
            .add(DDBlocks.CRACKED_DEEPSLATE_TILE_SLAB.asItem())
            .add(DDBlocks.CRACKED_BRICK_SLAB.asItem())
            .add(DDBlocks.MOSSY_BRICK_SLAB.asItem())
            .add(DDBlocks.SANDSTONE_BRICK_SLAB.asItem())
            .add(DDBlocks.CRACKED_SANDSTONE_BRICK_SLAB.asItem())
            .add(DDBlocks.MOSSY_SANDSTONE_BRICK_SLAB.asItem())
            .add(DDBlocks.POLISHED_SANDSTONE_SLAB.asItem())
            .add(DDBlocks.RED_SANDSTONE_BRICK_SLAB.asItem())
            .add(DDBlocks.CRACKED_RED_SANDSTONE_BRICK_SLAB.asItem())
            .add(DDBlocks.MOSSY_RED_SANDSTONE_BRICK_SLAB.asItem())
            .add(DDBlocks.POLISHED_RED_SANDSTONE_SLAB.asItem())
            .add(DDBlocks.SANDSTONE_BRICK_SLAB.asItem())
            .add(DDBlocks.CRACKED_SANDSTONE_BRICK_SLAB.asItem())
            .add(DDBlocks.MOSSY_SANDSTONE_BRICK_SLAB.asItem())
            .add(DDBlocks.NETHERRACK_SLAB.asItem())
            .add(DDBlocks.CRACKED_NETHER_BRICK_SLAB.asItem())
            .add(DDBlocks.SMOOTH_BASALT_SLAB.asItem())
            .add(DDBlocks.CRACKED_POLISHED_BLACKSTONE_BRICK_SLAB.asItem())
            .add(DDBlocks.CUT_IRON_SLAB.asItem())
            .add(DDBlocks.CUT_GOLD_SLAB.asItem())
            .add(DDBlocks.CUT_EMERALD_SLAB.asItem())
            .add(DDBlocks.CUT_LAPIS_SLAB.asItem())
            .add(DDBlocks.CUT_DIAMOND_SLAB.asItem())
            .add(DDBlocks.CUT_NETHERITE_SLAB.asItem())
            .add(DDBlocks.QUARTZ_BRICK_SLAB.asItem())
            .add(DDBlocks.CHARRED_QUARTZ_SLAB.asItem())
            .add(DDBlocks.SMOOTH_CHARRED_QUARTZ_SLAB.asItem())
            .add(DDBlocks.CHARRED_QUARTZ_BRICK_SLAB.asItem())
            .add(DDBlocks.POLISHED_AMETHYST_SLAB.asItem())
            .add(DDBlocks.POLISHED_AMETHYST_BRICK_SLAB.asItem())
            .add(DDBlocks.CUT_POLISHED_AMETHYST_SLAB.asItem())
            .add(DDBlocks.PACKED_ICE_BRICK_SLAB.asItem())
            .add(DDBlocks.CRACKED_PACKED_ICE_BRICK_SLAB.asItem())
            .add(DDBlocks.LARGE_PACKED_ICE_BRICK_SLAB.asItem())
            .add(DDBlocks.LARGE_CRACKED_PACKED_ICE_BRICK_SLAB.asItem())
            .add(DDBlocks.POLISHED_PACKED_ICE_SLAB.asItem())
            .add(DDBlocks.BLUE_ICE_BRICK_SLAB.asItem())
            .add(DDBlocks.CRACKED_BLUE_ICE_BRICK_SLAB.asItem())
            .add(DDBlocks.LARGE_BLUE_ICE_BRICK_SLAB.asItem())
            .add(DDBlocks.LARGE_CRACKED_BLUE_ICE_BRICK_SLAB.asItem())
            .add(DDBlocks.POLISHED_BLUE_ICE_SLAB.asItem())
            .add(DDBlocks.CHISELED_GRANITE_SLAB.asItem())
            .add(DDBlocks.CHISELED_DIORITE_SLAB.asItem())
            .add(DDBlocks.CHISELED_ANDESITE_SLAB.asItem())
            .add(DDBlocks.CHISELED_CALCITE_SLAB.asItem())
            .add(DDBlocks.CHISELED_DRIPSTONE_SLAB.asItem());

        getOrCreateTagBuilder(ItemTags.WALLS)
            .add(DDBlocks.SMOOTH_STONE_WALL.asItem())
            .add(DDBlocks.CRACKED_BRICK_WALL.asItem())
            .add(DDBlocks.GRANITE_BRICK_WALL.asItem())
            .add(DDBlocks.CRACKED_GRANITE_BRICK_WALL.asItem())
            .add(DDBlocks.MOSSY_GRANITE_BRICK_WALL.asItem())
            .add(DDBlocks.DIORITE_BRICK_WALL.asItem())
            .add(DDBlocks.CRACKED_DIORITE_BRICK_WALL.asItem())
            .add(DDBlocks.MOSSY_DIORITE_BRICK_WALL.asItem())
            .add(DDBlocks.ANDESITE_BRICK_WALL.asItem())
            .add(DDBlocks.CRACKED_ANDESITE_BRICK_WALL.asItem())
            .add(DDBlocks.MOSSY_ANDESITE_BRICK_WALL.asItem())
            .add(DDBlocks.CALCITE_WALL.asItem())
            .add(DDBlocks.POLISHED_CALCITE_WALL.asItem())
            .add(DDBlocks.CALCITE_BRICK_WALL.asItem())
            .add(DDBlocks.CRACKED_CALCITE_BRICK_WALL.asItem())
            .add(DDBlocks.MOSSY_CALCITE_BRICK_WALL.asItem())
            .add(DDBlocks.DRIPSTONE_WALL.asItem())
            .add(DDBlocks.POLISHED_DRIPSTONE_WALL.asItem())
            .add(DDBlocks.DRIPSTONE_BRICK_WALL.asItem())
            .add(DDBlocks.CRACKED_DRIPSTONE_BRICK_WALL.asItem())
            .add(DDBlocks.MOSSY_DRIPSTONE_BRICK_WALL.asItem())
            .add(DDBlocks.MOSSY_COBBLED_DEEPSLATE_WALL.asItem())
            .add(DDBlocks.CRACKED_DEEPSLATE_BRICK_WALL.asItem())
            .add(DDBlocks.MOSSY_DEEPSLATE_BRICK_WALL.asItem())
            .add(DDBlocks.CRACKED_DEEPSLATE_TILE_WALL.asItem())
            .add(DDBlocks.CRACKED_BRICK_WALL.asItem())
            .add(DDBlocks.MOSSY_BRICK_WALL.asItem())
            .add(DDBlocks.SANDSTONE_BRICK_WALL.asItem())
            .add(DDBlocks.CRACKED_SANDSTONE_BRICK_WALL.asItem())
            .add(DDBlocks.MOSSY_SANDSTONE_BRICK_WALL.asItem())
            .add(DDBlocks.POLISHED_SANDSTONE_WALL.asItem())
            .add(DDBlocks.RED_SANDSTONE_BRICK_WALL.asItem())
            .add(DDBlocks.CRACKED_RED_SANDSTONE_BRICK_WALL.asItem())
            .add(DDBlocks.MOSSY_RED_SANDSTONE_BRICK_WALL.asItem())
            .add(DDBlocks.POLISHED_RED_SANDSTONE_WALL.asItem())
            .add(DDBlocks.SANDSTONE_BRICK_WALL.asItem())
            .add(DDBlocks.CRACKED_SANDSTONE_BRICK_WALL.asItem())
            .add(DDBlocks.MOSSY_SANDSTONE_BRICK_WALL.asItem())
            .add(DDBlocks.NETHERRACK_WALL.asItem())
            .add(DDBlocks.CRACKED_NETHER_BRICK_WALL.asItem())
            .add(DDBlocks.SMOOTH_BASALT_WALL.asItem())
            .add(DDBlocks.CRACKED_POLISHED_BLACKSTONE_BRICK_WALL.asItem())
            .add(DDBlocks.CUT_IRON_WALL.asItem())
            .add(DDBlocks.CUT_GOLD_WALL.asItem())
            .add(DDBlocks.CUT_EMERALD_WALL.asItem())
            .add(DDBlocks.CUT_LAPIS_WALL.asItem())
            .add(DDBlocks.CUT_DIAMOND_WALL.asItem())
            .add(DDBlocks.CUT_NETHERITE_WALL.asItem())
            .add(DDBlocks.QUARTZ_BRICK_WALL.asItem())
            .add(DDBlocks.CHARRED_QUARTZ_WALL.asItem())
            .add(DDBlocks.SMOOTH_CHARRED_QUARTZ_WALL.asItem())
            .add(DDBlocks.CHARRED_QUARTZ_BRICK_WALL.asItem())
            .add(DDBlocks.POLISHED_AMETHYST_WALL.asItem())
            .add(DDBlocks.POLISHED_AMETHYST_BRICK_WALL.asItem())
            .add(DDBlocks.CUT_POLISHED_AMETHYST_WALL.asItem())
            .add(DDBlocks.PACKED_ICE_BRICK_WALL.asItem())
            .add(DDBlocks.CRACKED_PACKED_ICE_BRICK_WALL.asItem())
            .add(DDBlocks.LARGE_PACKED_ICE_BRICK_WALL.asItem())
            .add(DDBlocks.LARGE_CRACKED_PACKED_ICE_BRICK_WALL.asItem())
            .add(DDBlocks.POLISHED_PACKED_ICE_WALL.asItem())
            .add(DDBlocks.BLUE_ICE_BRICK_WALL.asItem())
            .add(DDBlocks.CRACKED_BLUE_ICE_BRICK_WALL.asItem())
            .add(DDBlocks.LARGE_BLUE_ICE_BRICK_WALL.asItem())
            .add(DDBlocks.LARGE_CRACKED_BLUE_ICE_BRICK_WALL.asItem())
            .add(DDBlocks.POLISHED_BLUE_ICE_WALL.asItem())
            .add(DDBlocks.POLISHED_GRANITE_WALL.asItem())
            .add(DDBlocks.POLISHED_DIORITE_WALL.asItem())
            .add(DDBlocks.POLISHED_ANDESITE_WALL.asItem())
            .add(DDBlocks.SMOOTH_SANDSTONE_WALL.asItem())
            .add(DDBlocks.SMOOTH_RED_SANDSTONE_WALL.asItem())
            .add(DDBlocks.PRISMARINE_BRICK_WALL.asItem())
            .add(DDBlocks.DARK_PRISMARINE_WALL.asItem())
            .add(DDBlocks.PURPUR_WALL.asItem())
            .add(DDBlocks.QUARTZ_WALL.asItem())
            .add(DDBlocks.CUT_COPPER_WALL.asItem())
            .add(DDBlocks.WAXED_CUT_COPPER_WALL.asItem())
            .add(DDBlocks.EXPOSED_CUT_COPPER_WALL.asItem())
            .add(DDBlocks.WAXED_EXPOSED_CUT_COPPER_WALL.asItem())
            .add(DDBlocks.WEATHERED_CUT_COPPER_WALL.asItem())
            .add(DDBlocks.WAXED_WEATHERED_CUT_COPPER_WALL.asItem())
            .add(DDBlocks.OXIDIZED_CUT_COPPER_WALL.asItem())
            .add(DDBlocks.WAXED_OXIDIZED_CUT_COPPER_WALL.asItem())
            .add(DDBlocks.STONE_WALL.asItem());
    }
}
