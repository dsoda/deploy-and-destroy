package net.dsoda.deployanddestroy.datagen;

import net.dsoda.deployanddestroy.blocks.DDBlockFamilies;
import net.dsoda.deployanddestroy.blocks.DDBlocks;
import net.dsoda.deployanddestroy.items.DDItems;
import net.fabricmc.fabric.api.datagen.v1.FabricDataOutput;
import net.fabricmc.fabric.api.datagen.v1.provider.FabricModelProvider;
import net.minecraft.block.Block;
import net.minecraft.block.Blocks;
import net.minecraft.data.client.*;
import net.minecraft.util.Identifier;

import java.util.List;

public class DDModelProvider extends FabricModelProvider {

    public DDModelProvider(FabricDataOutput dataOutput) {
        super(dataOutput);
    }

    @Override
    public void generateBlockStateModels(BlockStateModelGenerator blockStateModelGenerator) {
        blockStateModelGenerator.registerLog(DDBlocks.AZALEA_LOG).log(DDBlocks.AZALEA_LOG).wood(DDBlocks.AZALEA_WOOD);
        blockStateModelGenerator.registerLog(DDBlocks.STRIPPED_AZALEA_LOG).log(DDBlocks.STRIPPED_AZALEA_LOG).wood(DDBlocks.STRIPPED_AZALEA_WOOD);
        blockStateModelGenerator.registerLog(DDBlocks.SWAMP_LOG).log(DDBlocks.SWAMP_LOG).wood(DDBlocks.SWAMP_WOOD);
        blockStateModelGenerator.registerLog(DDBlocks.STRIPPED_SWAMP_LOG).log(DDBlocks.STRIPPED_SWAMP_LOG).wood(DDBlocks.STRIPPED_SWAMP_WOOD);
        blockStateModelGenerator.registerSimpleCubeAll(DDBlocks.CHARCOAL_BLOCK);
        blockStateModelGenerator.registerSimpleCubeAll(DDBlocks.BLAZING_COAL_BLOCK);
        blockStateModelGenerator.registerSimpleCubeAll(DDBlocks.END_REDSTONE_ORE);
        blockStateModelGenerator.registerAxisRotated(DDBlocks.CHARRED_QUARTZ_PILLAR, TexturedModel.END_FOR_TOP_CUBE_COLUMN, TexturedModel.END_FOR_TOP_CUBE_COLUMN_HORIZONTAL);
        blockStateModelGenerator.registerTorch(DDBlocks.BONE_TORCH, DDBlocks.BONE_WALL_TORCH);
        blockStateModelGenerator.registerTorch(DDBlocks.SOUL_BONE_TORCH, DDBlocks.SOUL_BONE_WALL_TORCH);
        blockStateModelGenerator.registerTorch(DDBlocks.COPPER_TORCH, DDBlocks.COPPER_WALL_TORCH);
        blockStateModelGenerator.registerTorch(DDBlocks.COPPER_BONE_TORCH, DDBlocks.COPPER_BONE_WALL_TORCH);
        blockStateModelGenerator.registerTorch(DDBlocks.AMETHYST_TORCH, DDBlocks.AMETHYST_WALL_TORCH);
        blockStateModelGenerator.registerTorch(DDBlocks.AMETHYST_BONE_TORCH, DDBlocks.AMETHYST_BONE_WALL_TORCH);
        blockStateModelGenerator.registerLantern(DDBlocks.COPPER_LANTERN);
        blockStateModelGenerator.registerLantern(DDBlocks.AMETHYST_LANTERN);
        blockStateModelGenerator.registerCampfire(DDBlocks.COPPER_CAMPFIRE);
        blockStateModelGenerator.registerCampfire(DDBlocks.AMETHYST_CAMPFIRE);
        blockStateModelGenerator.registerCubeAllModelTexturePool(DDBlocks.AZALEA_PLANKS).family(DDBlockFamilies.AZALEA_PLANKS);
        blockStateModelGenerator.registerCubeAllModelTexturePool(DDBlocks.SWAMP_PLANKS).family(DDBlockFamilies.SWAMP_PLANKS);
        blockStateModelGenerator.registerHangingSign(DDBlocks.STRIPPED_AZALEA_LOG, DDBlocks.AZALEA_HANGING_SIGN, DDBlocks.AZALEA_WALL_HANGING_SIGN);
        blockStateModelGenerator.registerHangingSign(DDBlocks.STRIPPED_SWAMP_LOG, DDBlocks.SWAMP_HANGING_SIGN, DDBlocks.SWAMP_WALL_HANGING_SIGN);
        blockStateModelGenerator.registerCubeAllModelTexturePool(DDBlocks.SANDSTONE_BRICKS).family(DDBlockFamilies.SANDSTONE_BRICKS);
        blockStateModelGenerator.registerCubeAllModelTexturePool(DDBlocks.CRACKED_SANDSTONE_BRICKS).family(DDBlockFamilies.CRACKED_SANDSTONE_BRICKS);
        blockStateModelGenerator.registerCubeAllModelTexturePool(DDBlocks.MOSSY_SANDSTONE_BRICKS).family(DDBlockFamilies.MOSSY_SANDSTONE_BRICKS);
        blockStateModelGenerator.registerCubeAllModelTexturePool(DDBlocks.POLISHED_SANDSTONE).family(DDBlockFamilies.POLISHED_SANDSTONE);
        blockStateModelGenerator.registerCubeAllModelTexturePool(DDBlocks.RED_SANDSTONE_BRICKS).family(DDBlockFamilies.RED_SANDSTONE_BRICKS);
        blockStateModelGenerator.registerCubeAllModelTexturePool(DDBlocks.CRACKED_RED_SANDSTONE_BRICKS).family(DDBlockFamilies.CRACKED_RED_SANDSTONE_BRICKS);
        blockStateModelGenerator.registerCubeAllModelTexturePool(DDBlocks.MOSSY_RED_SANDSTONE_BRICKS).family(DDBlockFamilies.MOSSY_RED_SANDSTONE_BRICKS);
        blockStateModelGenerator.registerCubeAllModelTexturePool(DDBlocks.POLISHED_RED_SANDSTONE).family(DDBlockFamilies.POLISHED_RED_SANDSTONE);
        blockStateModelGenerator.registerCubeAllModelTexturePool(Blocks.CRACKED_STONE_BRICKS).family(DDBlockFamilies.CRACKED_STONE_BRICKS);
        blockStateModelGenerator.registerCubeAllModelTexturePool(Blocks.NETHERRACK).family(DDBlockFamilies.NETHERRACK);
        blockStateModelGenerator.registerCubeAllModelTexturePool(Blocks.SMOOTH_STONE).stairs(DDBlocks.SMOOTH_STONE_STAIRS).wall(DDBlocks.SMOOTH_STONE_WALL);
        blockStateModelGenerator.registerCubeAllModelTexturePool(Blocks.STONE).wall(DDBlocks.STONE_WALL);
        blockStateModelGenerator.registerCubeAllModelTexturePool(Blocks.POLISHED_ANDESITE).wall(DDBlocks.POLISHED_ANDESITE_WALL);
        blockStateModelGenerator.registerCubeAllModelTexturePool(Blocks.POLISHED_DIORITE).wall(DDBlocks.POLISHED_DIORITE_WALL);
        blockStateModelGenerator.registerCubeAllModelTexturePool(Blocks.POLISHED_GRANITE).wall(DDBlocks.POLISHED_GRANITE_WALL);
        blockStateModelGenerator.registerCubeAllModelTexturePool(Blocks.SMOOTH_SANDSTONE).wall(DDBlocks.SMOOTH_SANDSTONE_WALL);
        blockStateModelGenerator.registerCubeAllModelTexturePool(Blocks.SMOOTH_RED_SANDSTONE).wall(DDBlocks.SMOOTH_RED_SANDSTONE_WALL);
        blockStateModelGenerator.registerCubeAllModelTexturePool(Blocks.PRISMARINE_BRICKS).wall(DDBlocks.PRISMARINE_BRICK_WALL);
        blockStateModelGenerator.registerCubeAllModelTexturePool(Blocks.DARK_PRISMARINE).wall(DDBlocks.DARK_PRISMARINE_WALL);
        blockStateModelGenerator.registerCubeAllModelTexturePool(Blocks.PURPUR_BLOCK).wall(DDBlocks.PURPUR_WALL);
        blockStateModelGenerator.registerCubeAllModelTexturePool(DDBlocks.CRACKED_BRICKS).family(DDBlockFamilies.CRACKED_BRICKS);
        blockStateModelGenerator.registerCubeAllModelTexturePool(DDBlocks.MOSSY_BRICKS).family(DDBlockFamilies.MOSSY_BRICKS);
        blockStateModelGenerator.registerCubeAllModelTexturePool(Blocks.QUARTZ_BRICKS).family(DDBlockFamilies.QUARTZ_BRICKS);
        blockStateModelGenerator.registerCubeAllModelTexturePool(Blocks.SMOOTH_QUARTZ).wall(DDBlocks.SMOOTH_QUARTZ_WALL);
        blockStateModelGenerator.registerCubeAllModelTexturePool(DDBlocks.SMOOTH_CHARRED_QUARTZ_BLOCK).family(DDBlockFamilies.SMOOTH_CHARRED_QUARTZ);
        blockStateModelGenerator.registerCubeAllModelTexturePool(DDBlocks.CHARRED_QUARTZ_BRICKS).family(DDBlockFamilies.CHARRED_QUARTZ_BRICKS);
        blockStateModelGenerator.registerCubeAllModelTexturePool(DDBlocks.ANDESITE_BRICKS).family(DDBlockFamilies.ANDESITE_BRICKS);
        blockStateModelGenerator.registerCubeAllModelTexturePool(DDBlocks.DIORITE_BRICKS).family(DDBlockFamilies.DIORITE_BRICKS);
        blockStateModelGenerator.registerCubeAllModelTexturePool(DDBlocks.GRANITE_BRICKS).family(DDBlockFamilies.GRANITE_BRICKS);
        blockStateModelGenerator.registerCubeAllModelTexturePool(DDBlocks.CRACKED_ANDESITE_BRICKS).family(DDBlockFamilies.CRACKED_ANDESITE_BRICKS);
        blockStateModelGenerator.registerCubeAllModelTexturePool(DDBlocks.CRACKED_DIORITE_BRICKS).family(DDBlockFamilies.CRACKED_DIORITE_BRICKS);
        blockStateModelGenerator.registerCubeAllModelTexturePool(DDBlocks.CRACKED_GRANITE_BRICKS).family(DDBlockFamilies.CRACKED_GRANITE_BRICKS);
        blockStateModelGenerator.registerCubeAllModelTexturePool(DDBlocks.MOSSY_ANDESITE_BRICKS).family(DDBlockFamilies.MOSSY_ANDESITE_BRICKS);
        blockStateModelGenerator.registerCubeAllModelTexturePool(DDBlocks.MOSSY_DIORITE_BRICKS).family(DDBlockFamilies.MOSSY_DIORITE_BRICKS);
        blockStateModelGenerator.registerCubeAllModelTexturePool(DDBlocks.MOSSY_GRANITE_BRICKS).family(DDBlockFamilies.MOSSY_GRANITE_BRICKS);
        blockStateModelGenerator.registerCubeAllModelTexturePool(Blocks.CALCITE).family(DDBlockFamilies.CALCITE);
        blockStateModelGenerator.registerCubeAllModelTexturePool(DDBlocks.POLISHED_CALCITE).family(DDBlockFamilies.POLISHED_CALCITE);
        blockStateModelGenerator.registerCubeAllModelTexturePool(DDBlocks.CALCITE_BRICKS).family(DDBlockFamilies.CALCITE_BRICKS);
        blockStateModelGenerator.registerCubeAllModelTexturePool(DDBlocks.CRACKED_CALCITE_BRICKS).family(DDBlockFamilies.CRACKED_CALCITE_BRICKS);
        blockStateModelGenerator.registerCubeAllModelTexturePool(DDBlocks.MOSSY_CALCITE_BRICKS).family(DDBlockFamilies.MOSSY_CALCITE_BRICKS);
        blockStateModelGenerator.registerCubeAllModelTexturePool(DDBlocks.POLISHED_OBSIDIAN);
        blockStateModelGenerator.registerCubeAllModelTexturePool(DDBlocks.CUT_IRON_BLOCK).family(DDBlockFamilies.CUT_IRON);
        blockStateModelGenerator.registerCubeAllModelTexturePool(DDBlocks.CUT_GOLD_BLOCK).family(DDBlockFamilies.CUT_GOLD);
        blockStateModelGenerator.registerCubeAllModelTexturePool(DDBlocks.CUT_DIAMOND_BLOCK).family(DDBlockFamilies.CUT_DIAMOND);
        blockStateModelGenerator.registerCubeAllModelTexturePool(DDBlocks.CUT_EMERALD_BLOCK).family(DDBlockFamilies.CUT_EMERALD);
        blockStateModelGenerator.registerCubeAllModelTexturePool(DDBlocks.CUT_NETHERITE_BLOCK).family(DDBlockFamilies.CUT_NETHERITE);
        blockStateModelGenerator.registerCubeAllModelTexturePool(DDBlocks.CUT_LAPIS_BLOCK).family(DDBlockFamilies.CUT_LAPIS);
        blockStateModelGenerator.registerCubeAllModelTexturePool(DDBlocks.POLISHED_AMETHYST_BLOCK).family(DDBlockFamilies.POLISHED_AMETHYST);
        blockStateModelGenerator.registerCubeAllModelTexturePool(DDBlocks.POLISHED_AMETHYST_BRICKS).family(DDBlockFamilies.POLISHED_AMETHYST_BRICKS);
        blockStateModelGenerator.registerCubeAllModelTexturePool(DDBlocks.CUT_POLISHED_AMETHYST).family(DDBlockFamilies.CUT_POLISHED_AMETHYST);
        blockStateModelGenerator.registerCubeAllModelTexturePool(DDBlocks.PACKED_ICE_BRICKS).family(DDBlockFamilies.PACKED_ICE_BRICKS);
        blockStateModelGenerator.registerCubeAllModelTexturePool(DDBlocks.CRACKED_PACKED_ICE_BRICKS).family(DDBlockFamilies.CRACKED_PACKED_ICE_BRICKS);
        blockStateModelGenerator.registerCubeAllModelTexturePool(DDBlocks.LARGE_PACKED_ICE_BRICKS).family(DDBlockFamilies.LARGE_PACKED_ICE_BRICKS);
        blockStateModelGenerator.registerCubeAllModelTexturePool(DDBlocks.LARGE_CRACKED_PACKED_ICE_BRICKS).family(DDBlockFamilies.LARGE_CRACKED_PACKED_ICE_BRICKS);
        blockStateModelGenerator.registerCubeAllModelTexturePool(DDBlocks.POLISHED_PACKED_ICE).family(DDBlockFamilies.POLISHED_PACKED_ICE);
        blockStateModelGenerator.registerCubeAllModelTexturePool(DDBlocks.BLUE_ICE_BRICKS).family(DDBlockFamilies.BLUE_ICE_BRICKS);
        blockStateModelGenerator.registerCubeAllModelTexturePool(DDBlocks.CRACKED_BLUE_ICE_BRICKS).family(DDBlockFamilies.CRACKED_BLUE_ICE_BRICKS);
        blockStateModelGenerator.registerCubeAllModelTexturePool(DDBlocks.LARGE_BLUE_ICE_BRICKS).family(DDBlockFamilies.LARGE_BLUE_ICE_BRICKS);
        blockStateModelGenerator.registerCubeAllModelTexturePool(DDBlocks.LARGE_CRACKED_BLUE_ICE_BRICKS).family(DDBlockFamilies.LARGE_CRACKED_BLUE_ICE_BRICKS);
        blockStateModelGenerator.registerCubeAllModelTexturePool(DDBlocks.POLISHED_BLUE_ICE).family(DDBlockFamilies.POLISHED_BLUE_ICE);
        blockStateModelGenerator.registerCubeAllModelTexturePool(Blocks.CRACKED_DEEPSLATE_BRICKS).family(DDBlockFamilies.CRACKED_DEEPSLATE_BRICKS);
        blockStateModelGenerator.registerCubeAllModelTexturePool(Blocks.CRACKED_DEEPSLATE_TILES).family(DDBlockFamilies.CRACKED_DEEPSLATE_TILES);
        blockStateModelGenerator.registerCubeAllModelTexturePool(Blocks.CRACKED_NETHER_BRICKS).family(DDBlockFamilies.CRACKED_NETHER_BRICKS).fence(DDBlocks.CRACKED_NETHER_BRICK_FENCE);
        blockStateModelGenerator.registerCubeAllModelTexturePool(Blocks.CRACKED_POLISHED_BLACKSTONE_BRICKS).family(DDBlockFamilies.CRACKED_POLISHED_BLACKSTONE_BRICKS);
        blockStateModelGenerator.registerCubeAllModelTexturePool(Blocks.SMOOTH_BASALT).family(DDBlockFamilies.SMOOTH_BASALT);
        blockStateModelGenerator.registerCubeAllModelTexturePool(Blocks.COPPER_BLOCK).pressurePlate(DDBlocks.COPPER_PRESSURE_PLATE).pressurePlate(DDBlocks.WAXED_COPPER_PRESSURE_PLATE);
        blockStateModelGenerator.registerCubeAllModelTexturePool(Blocks.EXPOSED_COPPER).pressurePlate(DDBlocks.EXPOSED_COPPER_PRESSURE_PLATE).pressurePlate(DDBlocks.WAXED_EXPOSED_COPPER_PRESSURE_PLATE);
        blockStateModelGenerator.registerCubeAllModelTexturePool(Blocks.WEATHERED_COPPER).pressurePlate(DDBlocks.WEATHERED_COPPER_PRESSURE_PLATE).pressurePlate(DDBlocks.WAXED_WEATHERED_COPPER_PRESSURE_PLATE);
        blockStateModelGenerator.registerCubeAllModelTexturePool(Blocks.OXIDIZED_COPPER).pressurePlate(DDBlocks.OXIDIZED_COPPER_PRESSURE_PLATE).pressurePlate(DDBlocks.WAXED_OXIDIZED_COPPER_PRESSURE_PLATE);
        blockStateModelGenerator.registerCubeAllModelTexturePool(Blocks.DRIPSTONE_BLOCK).family(DDBlockFamilies.DRIPSTONE);
        blockStateModelGenerator.registerCubeAllModelTexturePool(DDBlocks.POLISHED_DRIPSTONE).family(DDBlockFamilies.POLISHED_DRIPSTONE);
        blockStateModelGenerator.registerCubeAllModelTexturePool(DDBlocks.DRIPSTONE_BRICKS).family(DDBlockFamilies.DRIPSTONE_BRICKS);
        blockStateModelGenerator.registerCubeAllModelTexturePool(DDBlocks.CRACKED_DRIPSTONE_BRICKS).family(DDBlockFamilies.CRACKED_DRIPSTONE_BRICKS);
        blockStateModelGenerator.registerCubeAllModelTexturePool(DDBlocks.MOSSY_DRIPSTONE_BRICKS).family(DDBlockFamilies.MOSSY_DRIPSTONE_BRICKS);
        blockStateModelGenerator.registerCubeAllModelTexturePool(Blocks.CUT_COPPER).wall(DDBlocks.CUT_COPPER_WALL).wall(DDBlocks.WAXED_CUT_COPPER_WALL);
        blockStateModelGenerator.registerCubeAllModelTexturePool(Blocks.EXPOSED_CUT_COPPER).wall(DDBlocks.EXPOSED_CUT_COPPER_WALL).wall(DDBlocks.WAXED_EXPOSED_CUT_COPPER_WALL);
        blockStateModelGenerator.registerCubeAllModelTexturePool(Blocks.WEATHERED_CUT_COPPER).wall(DDBlocks.WEATHERED_CUT_COPPER_WALL).wall(DDBlocks.WAXED_WEATHERED_CUT_COPPER_WALL);
        blockStateModelGenerator.registerCubeAllModelTexturePool(Blocks.OXIDIZED_CUT_COPPER).wall(DDBlocks.OXIDIZED_CUT_COPPER_WALL).wall(DDBlocks.WAXED_OXIDIZED_CUT_COPPER_WALL);
        blockStateModelGenerator.registerCubeAllModelTexturePool(DDBlocks.MOSSY_COBBLED_DEEPSLATE).family(DDBlockFamilies.MOSSY_COBBLED_DEEPSLATE);
        blockStateModelGenerator.registerCubeAllModelTexturePool(DDBlocks.MOSSY_DEEPSLATE_BRICKS).family(DDBlockFamilies.MOSSY_DEEPSLATE_BRICKS);
        blockStateModelGenerator.registerCubeAllModelTexturePool(DDBlocks.CHISELED_GRANITE);
        blockStateModelGenerator.registerCubeAllModelTexturePool(DDBlocks.CHISELED_DIORITE);
        blockStateModelGenerator.registerCubeAllModelTexturePool(DDBlocks.CHISELED_ANDESITE);
        blockStateModelGenerator.registerCubeAllModelTexturePool(DDBlocks.CHISELED_CALCITE);
        blockStateModelGenerator.registerCubeAllModelTexturePool(DDBlocks.CHISELED_DRIPSTONE);
        blockStateModelGenerator.registerCubeAllModelTexturePool(DDBlocks.CRACKED_TUFF_BRICKS).family(DDBlockFamilies.CRACKED_TUFF_BRICKS);
        blockStateModelGenerator.registerCubeAllModelTexturePool(DDBlocks.MOSSY_TUFF_BRICKS).family(DDBlockFamilies.MOSSY_TUFF_BRICKS);

        registerFire(blockStateModelGenerator, DDBlocks.COPPER_FIRE);
        registerFire(blockStateModelGenerator, DDBlocks.AMETHYST_FIRE);
    }

    @Override
    public void generateItemModels(ItemModelGenerator itemModelGenerator) {
        itemModelGenerator.register(DDItems.AZALEA_BOAT, Models.GENERATED);
        itemModelGenerator.register(DDItems.AZALEA_CHEST_BOAT, Models.GENERATED);
        itemModelGenerator.register(DDItems.SWAMP_BOAT, Models.GENERATED);
        itemModelGenerator.register(DDItems.SWAMP_CHEST_BOAT, Models.GENERATED);
        itemModelGenerator.register(DDItems.BLAZING_COAL, Models.GENERATED);
        itemModelGenerator.register(DDItems.NETHERITE_NUGGET, Models.GENERATED);
        itemModelGenerator.register(DDItems.QUARTZ_CHUNK, Models.GENERATED);
        itemModelGenerator.register(DDItems.CHARRED_QUARTZ, Models.GENERATED);
        itemModelGenerator.register(DDItems.SAND_PILE, Models.GENERATED);
        itemModelGenerator.register(DDItems.ICE_SHARD, Models.GENERATED);
        itemModelGenerator.register(DDItems.CHOCOLATE_MILK, Models.GENERATED);
        itemModelGenerator.register(DDItems.WRENCH, Models.HANDHELD);
        itemModelGenerator.register(DDItems.STRIDER_BUCKET, Models.GENERATED);
        itemModelGenerator.register(DDItems.COPPER_BARREL_UPGRADE, Models.GENERATED);
        itemModelGenerator.register(DDItems.IRON_BARREL_UPGRADE, Models.GENERATED);
        itemModelGenerator.register(DDItems.GOLD_BARREL_UPGRADE, Models.GENERATED);
        itemModelGenerator.register(DDItems.DIAMOND_BARREL_UPGRADE, Models.GENERATED);
        itemModelGenerator.register(DDItems.NETHERITE_BARREL_UPGRADE, Models.GENERATED);
    }

    private void registerFire(BlockStateModelGenerator blockStateModelGenerator, Block block) {
        List<Identifier> list = blockStateModelGenerator.getFireFloorModels(block);
        List<Identifier> list2 = blockStateModelGenerator.getFireSideModels(block);
        blockStateModelGenerator.blockStateCollector.accept(
            MultipartBlockStateSupplier.create(block).with(
                BlockStateModelGenerator.buildBlockStateVariants(
                    list, blockStateVariant -> blockStateVariant)
            ).with(BlockStateModelGenerator.buildBlockStateVariants(
                list2, blockStateVariant -> blockStateVariant)
            ).with(BlockStateModelGenerator.buildBlockStateVariants(
                list2, blockStateVariant -> blockStateVariant.put(
                    VariantSettings.Y, VariantSettings.Rotation.R90))
            ).with(BlockStateModelGenerator.buildBlockStateVariants(
                list2, blockStateVariant -> blockStateVariant.put(
                    VariantSettings.Y, VariantSettings.Rotation.R180))
            ).with(BlockStateModelGenerator.buildBlockStateVariants(
                list2, blockStateVariant -> blockStateVariant.put(
                    VariantSettings.Y, VariantSettings.Rotation.R270)))
        );
    }
}
