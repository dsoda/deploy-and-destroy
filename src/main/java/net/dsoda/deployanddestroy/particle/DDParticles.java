package net.dsoda.deployanddestroy.particle;

import net.fabricmc.fabric.api.particle.v1.FabricParticleTypes;
import net.minecraft.particle.DefaultParticleType;
import net.minecraft.registry.Registries;
import net.minecraft.registry.Registry;
import net.minecraft.util.Identifier;

import static net.dsoda.deployanddestroy.DeployAndDestroy.MOD_ID;

public class DDParticles {

    public static DefaultParticleType COPPER_FLAME = FabricParticleTypes.simple();
    public static DefaultParticleType AMETHYST_FLAME = FabricParticleTypes.simple();

    public static void registerParticles() {
        Registry.register(Registries.PARTICLE_TYPE, new Identifier(MOD_ID, "copper_flame"), COPPER_FLAME);
        Registry.register(Registries.PARTICLE_TYPE, new Identifier(MOD_ID, "amethyst_flame"), AMETHYST_FLAME);
    }
}
