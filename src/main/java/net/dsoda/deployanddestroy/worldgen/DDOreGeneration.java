package net.dsoda.deployanddestroy.worldgen;

import net.dsoda.deployanddestroy.features.DDPlacedFeatures;
import net.fabricmc.fabric.api.biome.v1.BiomeModifications;
import net.fabricmc.fabric.api.biome.v1.BiomeSelectors;
import net.minecraft.world.gen.GenerationStep;

public class DDOreGeneration {

    public static void generateOres() {
        BiomeModifications.addFeature(BiomeSelectors.foundInTheEnd(), GenerationStep.Feature.UNDERGROUND_ORES, DDPlacedFeatures.END_REDSTONE_ORE_PLACED);
    }
}
