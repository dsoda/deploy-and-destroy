package net.dsoda.deployanddestroy.worldgen;

import net.minecraft.block.SaplingGenerator;
import net.minecraft.world.gen.feature.TreeConfiguredFeatures;

import java.util.Optional;

public class DDSaplingGenerators {

    public static final SaplingGenerator SWAMP = new SaplingGenerator("swamp_oak", 0f, Optional.empty(), Optional.empty(), Optional.of(TreeConfiguredFeatures.SWAMP_OAK), Optional.empty(), Optional.empty(), Optional.empty());
}
