package net.dsoda.deployanddestroy.worldgen;

import net.dsoda.deployanddestroy.util.api.StructurePoolAPI;
import net.fabricmc.fabric.api.event.lifecycle.v1.ServerLifecycleEvents;
import net.minecraft.util.Identifier;

import java.util.ArrayList;
import java.util.List;

import static net.dsoda.deployanddestroy.DeployAndDestroy.MOD_ID;

public class DDVillagerHouses {

    private static final List<Structure> structures = new ArrayList<>();

    public static void generateVillagerHouses() {

        registerNew("plains", "plains_carpenter_1", 2, "mossify_10_percent", 2);
        registerNew("plains/zombie", "plains_carpenter_1", 2, "zombie_plains", 2);
        registerNew("plains", "plains_carpenter_2", 2, "mossify_10_percent", 2);
        registerNew("plains/zombie", "plains_carpenter_2", 2, "zombie_plains", 2);
        registerNew("snowy", "snowy_carpenter_1", 2, 2);
        registerNew("snowy/zombie", "snowy_carpenter_1", 2, "zombie_snowy", 2);
        registerNew("snowy", "snowy_carpenter_2", 2, 2);
        registerNew("snowy/zombie", "snowy_carpenter_2", 2, "zombie_snowy", 2);
        registerNew("savanna", "savanna_carpenter_1", 2, 1);
        registerNew("savanna/zombie", "savanna_carpenter_1", 1, "zombie_savanna", 1);
        registerNew("savanna", "savanna_carpenter_2", 2, 1);
        registerNew("savanna/zombie", "savanna_carpenter_2", 1, "zombie_savanna", 1);
        registerNew("savanna", "savanna_carpenter_3", 2, 2);
        registerNew("savanna/zombie", "savanna_carpenter_3", 2, "zombie_savanna", 2);
        registerNew("desert", "desert_carpenter_1", 2, 2);
        registerNew("desert/zombie", "desert_carpenter_1", 2, "zombie_desert", 2);
        registerNew("desert", "desert_carpenter_2", 2, 2);
        registerNew("desert/zombie", "desert_carpenter_2", 2, "zombie_desert", 2);
        registerNew("taiga", "taiga_carpenter_1", 2, "mossify_10_percent", 2);
        registerNew("taiga/zombie", "taiga_carpenter_1", 2, "zombie_taiga", 2);
        registerNew("taiga", "taiga_carpenter_2", 2, "mossify_10_percent", 2);
        registerNew("taiga/zombie", "taiga_carpenter_2", 2, "zombie_taiga", 2);

        injectHouses();
    }

    private static void registerNew(String type, String name, int weight, int limit) {
        registerNew(type, name, weight, "empty", limit);
    }

    private static void registerNew(String type, String name, int weight, String processorList) {
        registerNew(type, name, weight, processorList, -1);
    }

    private static void registerNew(String type, String name, int weight) {
        registerNew(type, name, weight, "empty", -1);
    }

    private static void registerNew(String type, String name, int weight, String processorList, int limit) {
        structures.add(new Structure(type, name, weight, processorList, limit));
    }

    private static void injectHouses() {
        ServerLifecycleEvents.SERVER_STARTING.register(
            server -> {
                for (Structure structure : structures) {
                    StructurePoolAPI.injectIntoStructurePool(
                        server, structure.targetPool, structure.structureId, structure.weight, structure.processorList);
                    if (structure.limit > 0)
                        StructurePoolAPI.limitSpawn(structure.targetPool, structure.structureId, structure.limit);
                }
            }
        );
    }

    private static class Structure {

        public final int limit;
        public final int weight;
        public final Identifier targetPool;
        public final Identifier structureId;
        public final String processorList;

        protected Structure(String type, String name, int weight, String pList, int limit) {
            this.limit = limit;
            this.weight = weight;
            this.targetPool = new Identifier(String.format("minecraft:village/%s/houses", type));
            this.structureId = new Identifier(MOD_ID, String.format("village/%s/houses/%s", type, name));
            this.processorList = pList;
        }
    }
}
