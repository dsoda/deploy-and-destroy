package net.dsoda.deployanddestroy.features;

import net.dsoda.deployanddestroy.DeployAndDestroy;
import net.dsoda.deployanddestroy.blocks.DDBlocks;
import net.minecraft.block.Blocks;
import net.minecraft.registry.Registerable;
import net.minecraft.registry.RegistryKey;
import net.minecraft.registry.RegistryKeys;
import net.minecraft.structure.rule.BlockMatchRuleTest;
import net.minecraft.structure.rule.RuleTest;
import net.minecraft.util.Identifier;
import net.minecraft.world.gen.feature.ConfiguredFeature;
import net.minecraft.world.gen.feature.Feature;
import net.minecraft.world.gen.feature.FeatureConfig;
import net.minecraft.world.gen.feature.OreFeatureConfig;

import java.util.List;

public class DDConfiguredFeatures {
    public static final RegistryKey<ConfiguredFeature<?, ?>> END_REDSTONE_ORE_CONFIGURED = registerKey("end_redstone_ore_configured");

    public static void bootstrap(Registerable<ConfiguredFeature<?, ?>> context) {
        RuleTest endReplaceables = new BlockMatchRuleTest(Blocks.END_STONE);

        List<OreFeatureConfig.Target> endRedstoneOres = List.of(OreFeatureConfig.createTarget(endReplaceables, DDBlocks.END_REDSTONE_ORE.getDefaultState()));

        register(context, END_REDSTONE_ORE_CONFIGURED, Feature.ORE, new OreFeatureConfig(endRedstoneOres, 8));
    }

    public static RegistryKey<ConfiguredFeature<?, ?>> registerKey(String name) {
        return RegistryKey.of(RegistryKeys.CONFIGURED_FEATURE, new Identifier(DeployAndDestroy.MOD_ID, name));
    }

    private static <FC extends FeatureConfig, F extends Feature<FC>> void register(Registerable<ConfiguredFeature<?, ?>> context, RegistryKey<ConfiguredFeature<?, ?>> key, F feature, FC configuration) {
        context.register(key, new ConfiguredFeature<>(feature, configuration));
    }
}
