package net.dsoda.deployanddestroy.items;

import com.terraformersmc.terraform.boat.api.item.TerraformBoatItemHelper;
import net.dsoda.deployanddestroy.DeployAndDestroy;
import net.dsoda.deployanddestroy.blocks.DDBlocks;
import net.dsoda.deployanddestroy.blocks.block.MaterialBarrelBlock;
import net.dsoda.deployanddestroy.entity.DDBoats;
import net.dsoda.deployanddestroy.items.item.ChocolateMilkItem;
import net.dsoda.deployanddestroy.items.item.DyeBucketItem;
import net.dsoda.deployanddestroy.items.item.MaterialBarrelUpgradeItem;
import net.dsoda.deployanddestroy.items.item.WrenchItem;
import net.dsoda.deployanddestroy.util.DDFoods;
import net.fabricmc.fabric.api.item.v1.FabricItemSettings;
import net.minecraft.block.Block;
import net.minecraft.entity.EntityType;
import net.minecraft.fluid.Fluids;
import net.minecraft.item.*;
import net.minecraft.registry.Registries;
import net.minecraft.registry.Registry;
import net.minecraft.sound.SoundEvents;
import net.minecraft.util.Identifier;
import net.minecraft.util.math.Direction;

import static net.dsoda.deployanddestroy.DeployAndDestroy.MOD_ID;

public class DDItems {
    public static final Item AZALEA_SIGN = registerItem("azalea_sign", new SignItem(new FabricItemSettings().maxCount(16), DDBlocks.AZALEA_SIGN, DDBlocks.AZALEA_WALL_SIGN));
    public static final Item AZALEA_HANGING_SIGN = registerItem("azalea_hanging_sign", new HangingSignItem(DDBlocks.AZALEA_HANGING_SIGN, DDBlocks.AZALEA_WALL_HANGING_SIGN, new FabricItemSettings().maxCount(16)));
    public static final Item AZALEA_BOAT = TerraformBoatItemHelper.registerBoatItem(DDBoats.AZALEA_BOAT_ID, DDBoats.AZALEA_BOAT_KEY, false);
    public static final Item AZALEA_CHEST_BOAT = TerraformBoatItemHelper.registerBoatItem(DDBoats.AZALEA_CHEST_BOAT_ID, DDBoats.AZALEA_BOAT_KEY, true);
    public static final Item SWAMP_SIGN = registerItem("swamp_sign", new SignItem(new FabricItemSettings().maxCount(16), DDBlocks.SWAMP_SIGN, DDBlocks.SWAMP_WALL_SIGN));
    public static final Item SWAMP_HANGING_SIGN = registerItem("swamp_hanging_sign", new HangingSignItem(DDBlocks.SWAMP_HANGING_SIGN, DDBlocks.SWAMP_WALL_HANGING_SIGN, new FabricItemSettings().maxCount(16)));
    public static final Item SWAMP_BOAT = TerraformBoatItemHelper.registerBoatItem(DDBoats.SWAMP_BOAT_ID, DDBoats.SWAMP_BOAT_KEY, false);
    public static final Item SWAMP_CHEST_BOAT = TerraformBoatItemHelper.registerBoatItem(DDBoats.SWAMP_CHEST_BOAT_ID, DDBoats.SWAMP_BOAT_KEY, true);

    public static final Item BLAZING_COAL = registerItem("blazing_coal", new Item(new FabricItemSettings()));
    public static final Item NETHERITE_NUGGET = registerItem("netherite_nugget", new Item(new FabricItemSettings().fireproof()));
    public static final Item SAND_PILE = registerItem("sand_pile", new Item(new FabricItemSettings()));
    public static final Item ICE_SHARD = registerItem("ice_shard", new Item(new FabricItemSettings()));
    public static final Item QUARTZ_CHUNK = registerItem("quartz_chunk", new Item(new FabricItemSettings()));
    public static final Item CHARRED_QUARTZ = registerItem("charred_quartz", new Item(new FabricItemSettings()));
    public static final Item CHOCOLATE_MILK = registerItem("chocolate_milk", new ChocolateMilkItem(new Item.Settings().food(DDFoods.CHOCOLATE_MILK).maxCount(16)));
    public static final Item DYE_BUCKET = registerItem("dye_bucket", new DyeBucketItem(new Item.Settings().maxDamage(96)));
    public static final Item WRENCH = registerItem("wrench", new WrenchItem(new Item.Settings().maxCount(1)));
    public static final Item STRIDER_BUCKET = registerItem("strider_bucket", new EntityBucketItem(EntityType.STRIDER, Fluids.LAVA, SoundEvents.ITEM_BUCKET_EMPTY_LAVA, new FabricItemSettings().maxCount(1)));

    public static final Item BONE_TORCH_ITEM = registerTorchBlockItem("bone_torch", DDBlocks.BONE_TORCH, DDBlocks.BONE_WALL_TORCH);
    public static final Item SOUL_BONE_TORCH_ITEM = registerTorchBlockItem("soul_bone_torch", DDBlocks.SOUL_BONE_TORCH, DDBlocks.SOUL_BONE_WALL_TORCH);
    public static final Item REDSTONE_BONE_TORCH_ITEM = registerTorchBlockItem("redstone_bone_torch", DDBlocks.REDSTONE_BONE_TORCH, DDBlocks.REDSTONE_BONE_WALL_TORCH);
    public static final Item COPPER_TORCH_ITEM = registerTorchBlockItem("copper_torch", DDBlocks.COPPER_TORCH, DDBlocks.COPPER_WALL_TORCH);
    public static final Item COPPER_BONE_TORCH_ITEM = registerTorchBlockItem("copper_bone_torch", DDBlocks.COPPER_BONE_TORCH, DDBlocks.COPPER_BONE_WALL_TORCH);
    public static final Item AMETHYST_TORCH_ITEM = registerTorchBlockItem("amethyst_torch", DDBlocks.AMETHYST_TORCH, DDBlocks.AMETHYST_WALL_TORCH);
    public static final Item AMETHYST_BONE_TORCH_ITEM = registerTorchBlockItem("amethyst_bone_torch", DDBlocks.AMETHYST_BONE_TORCH, DDBlocks.AMETHYST_BONE_WALL_TORCH);

    public static final Item COPPER_BARREL_UPGRADE = registerItem("copper_barrel_upgrade", new MaterialBarrelUpgradeItem(new FabricItemSettings(), MaterialBarrelBlock.BarrelMaterial.COPPER));
    public static final Item IRON_BARREL_UPGRADE = registerItem("iron_barrel_upgrade", new MaterialBarrelUpgradeItem(new FabricItemSettings(), MaterialBarrelBlock.BarrelMaterial.IRON));
    public static final Item GOLD_BARREL_UPGRADE = registerItem("gold_barrel_upgrade", new MaterialBarrelUpgradeItem(new FabricItemSettings(), MaterialBarrelBlock.BarrelMaterial.GOLD));
    public static final Item DIAMOND_BARREL_UPGRADE = registerItem("diamond_barrel_upgrade", new MaterialBarrelUpgradeItem(new FabricItemSettings(), MaterialBarrelBlock.BarrelMaterial.DIAMOND));
    public static final Item NETHERITE_BARREL_UPGRADE = registerItem("netherite_barrel_upgrade", new MaterialBarrelUpgradeItem(new FabricItemSettings(), MaterialBarrelBlock.BarrelMaterial.NETHERITE));

    private static Item registerItem(String name, Item item) {
        return Registry.register(Registries.ITEM, new Identifier(MOD_ID, name), item);
    }

    private static Item registerTorchBlockItem(String name, Block torchGround, Block torchWall) {
        return Registry.register(Registries.ITEM, new Identifier(MOD_ID, name), new VerticallyAttachableBlockItem(torchGround, torchWall, new Item.Settings(), Direction.DOWN));
    }

    public static void registerItems() {
        DeployAndDestroy.LOGGER.info("Registering mod items.");
    }
}
