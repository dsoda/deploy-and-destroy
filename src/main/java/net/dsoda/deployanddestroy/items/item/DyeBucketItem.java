package net.dsoda.deployanddestroy.items.item;

import com.google.common.collect.ImmutableMap;
import net.dsoda.deployanddestroy.items.DDItems;
import net.dsoda.deployanddestroy.stats.DDStats;
import net.dsoda.deployanddestroy.util.DDColorHelper;
import net.dsoda.deployanddestroy.util.DDTooltipHelper;
import net.dsoda.deployanddestroy.util.behavior.DDDyeBehaviorHelper;
import net.minecraft.block.*;
import net.minecraft.block.entity.SignBlockEntity;
import net.minecraft.client.item.TooltipContext;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.passive.SheepEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.*;
import net.minecraft.nbt.NbtCompound;
import net.minecraft.sound.SoundCategory;
import net.minecraft.sound.SoundEvents;
import net.minecraft.text.Text;
import net.minecraft.util.*;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.MathHelper;
import net.minecraft.world.World;

import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;

public class DyeBucketItem extends Item implements SignChangingItem {

    public static final String COLOR_KEY = "deployanddestroy.dye_bucket_color";

    private static final Map<Integer, DyeColor> NBT_INT_TO_DYE_COLOR = new ImmutableMap.Builder<Integer, DyeColor>()
        .put(0, DyeColor.WHITE).put(1, DyeColor.LIGHT_GRAY).put(2, DyeColor.GRAY).put(3, DyeColor.BLACK)
        .put(4, DyeColor.BROWN).put(5, DyeColor.RED).put(6, DyeColor.ORANGE).put(7, DyeColor.YELLOW)
        .put(8, DyeColor.LIME).put(9, DyeColor.GREEN).put(10, DyeColor.CYAN).put(11, DyeColor.LIGHT_BLUE)
        .put(12, DyeColor.BLUE).put(13, DyeColor.PURPLE).put(14, DyeColor.MAGENTA).put(15, DyeColor.PINK).build();

    private static final Map<DyeColor, Integer> DYE_COLOR_TO_NBT_INT = new ImmutableMap.Builder<DyeColor, Integer>()
        .put(DyeColor.WHITE, 0).put(DyeColor.LIGHT_GRAY, 1).put(DyeColor.GRAY, 2).put(DyeColor.BLACK, 3)
        .put(DyeColor.BROWN, 4).put(DyeColor.RED, 5).put(DyeColor.ORANGE, 6).put(DyeColor.YELLOW, 7)
        .put(DyeColor.LIME, 8).put(DyeColor.GREEN, 9).put(DyeColor.CYAN, 10).put(DyeColor.LIGHT_BLUE, 11)
        .put(DyeColor.BLUE, 12).put(DyeColor.PURPLE, 13).put(DyeColor.MAGENTA, 14).put(DyeColor.PINK, 15).build();

    public DyeBucketItem(Settings settings) {
        super(settings);
    }

    @Override
    public ActionResult useOnBlock(ItemUsageContext context) {
        World world = context.getWorld();
        BlockPos pos = context.getBlockPos();
        ItemStack stack = context.getStack();
        PlayerEntity player = context.getPlayer();
        if (Objects.requireNonNull(context.getPlayer()).isSneaking()) {
            Optional<DyeColor> newColor = this.getNewColor(world, player, pos);
            return newColor.isPresent() && newColor.get() != getColor(stack) ? this.setNewColor(player, stack, newColor.get()) : ActionResult.FAIL;
        } else {
            return this.changeBlockColor(world, player, stack, context.getHand(), pos, getColor(stack));
        }
    }

    @Override
    public ActionResult useOnEntity(ItemStack stack, PlayerEntity player, LivingEntity entity, Hand hand) {
        if (player.isSneaking()) {
            Optional<DyeColor> newColor = getNewColorFromEntity(entity);
            return newColor.isPresent() ? this.setNewColor(player, player.getStackInHand(hand), newColor.get()) : ActionResult.FAIL;
        } else {
            return this.setEntityColor(player.getStackInHand(hand), player, entity, hand);
        }
    }

    @Override
    public boolean useOnSign(World world, SignBlockEntity signBlockEntity, boolean front, PlayerEntity player) {
        if (signBlockEntity.changeText(text -> text.withColor(getColor(player.getStackInHand(player.getActiveHand()))), front)) {
            world.playSound(null, signBlockEntity.getPos(), SoundEvents.ENTITY_ITEM_PICKUP, SoundCategory.BLOCKS, 1.0f, (float)(world.random.nextBetween(8, 12)) / 10);
            return true;
        }
        return false;
    }

    @Override
    public void appendTooltip(ItemStack stack, World world, List<Text> tooltip, TooltipContext context) {
        if (stack.hasNbt()) {
            tooltip.add(DDTooltipHelper.getTooltipText(getColor(stack)));
        }
    }

    @Override
    public ItemStack getDefaultStack() {
        NbtCompound nbt = new NbtCompound();
        nbt.putInt(COLOR_KEY, 0);
        ItemStack stack = new ItemStack(this);
        stack.setNbt(nbt);
        return stack;
    }

    @Override
    public int getItemBarColor(ItemStack stack) {
        DyeColor color = getColor(stack);
        if (color.equals(DyeColor.BLACK)) return 0x383751;
        float[] c = color.getColorComponents();
        return MathHelper.packRgb(c[0], c[1], c[2]);
    }

    private Optional<DyeColor> getNewColor(World world, PlayerEntity player, BlockPos pos) {
        Block block = world.getBlockState(pos).getBlock();
        Optional<DyeColor> wool = DDColorHelper.getColorFromWool(block);
        Optional<DyeColor> carpet = DDColorHelper.getColorFromCarpet(block);
        Optional<DyeColor> terracotta = DDColorHelper.getColorFromTerracotta(block);
        Optional<DyeColor> glazedTerracotta = DDColorHelper.getColorFromGlazedTerracotta(block);
        Optional<DyeColor> concrete = DDColorHelper.getColorFromConcrete(block);
        Optional<DyeColor> concretePowder = DDColorHelper.getColorFromConcretePowder(block);
        Optional<DyeColor> stainedGlass = DDColorHelper.getColorFromStainedGlass(block);
        Optional<DyeColor> stainedGlassPane = DDColorHelper.getColorFromStainedGlassPane(block);
        Optional<DyeColor> shulkerBox = DDColorHelper.getColorFromShulkerBox(block);
        Optional<DyeColor> bed = DDColorHelper.getColorFromBed(block);
        Optional<DyeColor> candle = DDColorHelper.getColorFromCandle(block);
        Optional<DyeColor> candleCake = DDColorHelper.getColorFromCandleCake(block);
        Optional<DyeColor> banner = DDColorHelper.getColorFromBanner(block);
        Optional<DyeColor> lamp = DDColorHelper.getColorFromLamp(block);
        Optional<DyeColor> sign = DDColorHelper.getColorFromSign(world, player, pos);
        if (wool.isPresent()) return wool;
        if (carpet.isPresent()) return carpet;
        if (terracotta.isPresent()) return terracotta;
        if (glazedTerracotta.isPresent()) return glazedTerracotta;
        if (concrete.isPresent()) return concrete;
        if (concretePowder.isPresent()) return concretePowder;
        if (stainedGlass.isPresent()) return stainedGlass;
        if (stainedGlassPane.isPresent()) return stainedGlassPane;
        if (shulkerBox.isPresent()) return shulkerBox;
        if (bed.isPresent()) return bed;
        if (candle.isPresent()) return candle;
        if (candleCake.isPresent()) return candleCake;
        if (banner.isPresent()) return banner;
        if (lamp.isPresent()) return lamp;
        return sign;
    }

    private ActionResult changeBlockColor(World world, PlayerEntity player, ItemStack stack, Hand hand, BlockPos pos, DyeColor color) {
        BlockState state = world.getBlockState(pos);
        Optional<BlockState> wool = DDDyeBehaviorHelper.getWoolForDyeColor(color, state);
        Optional<BlockState> carpet = DDDyeBehaviorHelper.getCarpetForDyeColor(color, state);
        Optional<BlockState> terracotta = DDDyeBehaviorHelper.getTerracottaForDyeColor(color, state);
        Optional<BlockState> glazedTerracotta = DDDyeBehaviorHelper.getGlazedTerracottaForDyeColor(color, state);
        Optional<BlockState> concrete = DDDyeBehaviorHelper.getConcreteForDyeColor(color, state);
        Optional<BlockState> concretePowder = DDDyeBehaviorHelper.getConcretePowderForDyeColor(color, state);
        Optional<BlockState> stainedGlass = DDDyeBehaviorHelper.getStainedGlassForDyeColor(color, state);
        Optional<BlockState> stainedGlassPane = DDDyeBehaviorHelper.getStainedGlassPaneForDyeColor(color, state);
        Optional<BlockState> candle = DDDyeBehaviorHelper.getCandleForDyeColor(color, state);
        Optional<BlockState> candleCake = DDDyeBehaviorHelper.getCandleCakeForDyeColor(color, state);
        Optional<BlockState> lamp = DDDyeBehaviorHelper.getLampForDyeColor(color, state);
        Optional<BlockState> setBlock = Optional.empty();
        if (wool.isPresent()) setBlock = wool;
        if (carpet.isPresent()) setBlock = carpet;
        if (terracotta.isPresent()) setBlock = terracotta;
        if (glazedTerracotta.isPresent()) setBlock = glazedTerracotta;
        if (concrete.isPresent()) setBlock = concrete;
        if (concretePowder.isPresent()) setBlock = concretePowder;
        if (stainedGlass.isPresent()) setBlock = stainedGlass;
        if (stainedGlassPane.isPresent()) setBlock = stainedGlassPane;
        if (candle.isPresent()) setBlock = candle;
        if (candleCake.isPresent()) setBlock = candleCake;
        if (lamp.isPresent()) setBlock = lamp;
        if (setBlock.isPresent()) {
            BlockState newState = setBlock.get();
            if (newState.getBlock() instanceof ShulkerBoxBlock) DDDyeBehaviorHelper.setShulker(world, pos, newState);
            else if (newState.getBlock() instanceof BedBlock) DDDyeBehaviorHelper.setBed(world, pos, newState);
            else {
                world.setBlockState(pos, newState.getBlock().getStateWithProperties(state), Block.NOTIFY_ALL | Block.REDRAW_ON_MAIN_THREAD);
            }
            world.playSound(null, pos, SoundEvents.ENTITY_ITEM_PICKUP, SoundCategory.BLOCKS, 1.0f, (float)(world.random.nextBetween(8, 12)) / 10);
            if (!player.getAbilities().creativeMode && stack.damage(1, world.random, null))
                player.setStackInHand(hand, Items.BUCKET.getDefaultStack());
            player.incrementStat(DDStats.DYE_BUCKET_CONVERSIONS);
            return ActionResult.SUCCESS;
        }
        return ActionResult.FAIL;
    }

    private Optional<DyeColor> getNewColorFromEntity(LivingEntity e) {
        if (e instanceof SheepEntity) return Optional.ofNullable(((SheepEntity) e).getColor());
        return Optional.empty();
    }

    private ActionResult setEntityColor(ItemStack stack, PlayerEntity player, LivingEntity entity, Hand hand) {
        if (entity instanceof SheepEntity) {
            World world = player.getWorld();
            ((SheepEntity) entity).setColor(getColor(stack));
            if (!player.getAbilities().creativeMode && stack.damage(1, world.random, null))
                player.setStackInHand(hand, Items.BUCKET.getDefaultStack());
            world.playSound(null, entity.getBlockPos(), SoundEvents.ENTITY_ITEM_PICKUP, SoundCategory.BLOCKS, 1.0f, (float)(world.random.nextBetween(8, 12)) / 10);
            player.incrementStat(DDStats.DYE_BUCKET_CONVERSIONS);
            return ActionResult.SUCCESS;
        }
        return ActionResult.FAIL;
    }

    private ActionResult setNewColor(PlayerEntity player, ItemStack stack, DyeColor color) {
        setColor(player, stack, color);
        return ActionResult.SUCCESS;
    }

    public static DyeColor getColor(ItemStack stack) {
        if (stack.isOf(DDItems.DYE_BUCKET)) return NBT_INT_TO_DYE_COLOR.get(stack.getNbt().getInt(COLOR_KEY));
        return DyeColor.WHITE;
    }

    public static void setColor(PlayerEntity player, ItemStack stack, DyeColor color) {
        if (stack.isOf(DDItems.DYE_BUCKET)) {
            NbtCompound nbt = stack.getNbt().copy();
            nbt.putInt(COLOR_KEY, DYE_COLOR_TO_NBT_INT.get(color));
            stack.setNbt(nbt);
            if (player != null)
                player.sendMessage(DDTooltipHelper.getTooltipText(getColor(stack)), true);
        }
    }
}
