package net.dsoda.deployanddestroy.items.item.behavior;

import net.minecraft.advancement.criterion.Criteria;
import net.minecraft.block.BlockState;
import net.minecraft.block.Blocks;
import net.minecraft.block.PistonBlock;
import net.minecraft.block.enums.PistonType;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.server.network.ServerPlayerEntity;
import net.minecraft.sound.SoundCategory;
import net.minecraft.sound.SoundEvents;
import net.minecraft.stat.Stats;
import net.minecraft.state.property.Properties;
import net.minecraft.util.ActionResult;
import net.minecraft.util.hit.BlockHitResult;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraft.world.event.GameEvent;

public class SlimeBallBehavior {

    public static ActionResult useOnBlock(PlayerEntity player, World world, ItemStack itemStack, BlockHitResult hitResult) {
        BlockPos usedOn = hitResult.getBlockPos();
        BlockState state = world.getBlockState(usedOn);
        boolean notExtended = state.isOf(Blocks.PISTON)
                && !state.get(Properties.EXTENDED);
        boolean extended = state.isOf(Blocks.PISTON_HEAD)
                && state.get(Properties.PISTON_TYPE).asString().equals("normal");
        if(notExtended || extended) {
            if (notExtended) replacePistonWithStickyAtPos(usedOn, world);
            else replaceExtendedPistonWithStickyAtPos(usedOn, world);
            if (!player.getAbilities().creativeMode) itemStack.decrement(1);
            world.playSound(null, usedOn, SoundEvents.BLOCK_SLIME_BLOCK_BREAK, SoundCategory.BLOCKS, 1f, 1f);
            world.addBlockBreakParticles(usedOn, Blocks.SLIME_BLOCK.getDefaultState());
            if (player instanceof ServerPlayerEntity)
                Criteria.ITEM_USED_ON_BLOCK.trigger((ServerPlayerEntity)player, usedOn, itemStack);
            world.emitGameEvent(GameEvent.BLOCK_CHANGE, usedOn, GameEvent.Emitter.of(player, state));
            player.incrementStat(Stats.USED.getOrCreateStat(itemStack.getItem()));
            return ActionResult.success(world.isClient);
        }
        return ActionResult.PASS;
    }

    private static void replacePistonWithStickyAtPos(BlockPos pos, World world) {
        BlockState state = world.getBlockState(pos);
        world.setBlockState(pos, Blocks.STICKY_PISTON.getStateWithProperties(state));
    }

    private static void replaceExtendedPistonWithStickyAtPos(BlockPos pos, World world) {
        BlockState state = world.getBlockState(pos);
        BlockPos connected = pos.offset(state.get(PistonBlock.FACING), -1);
        world.setBlockState(pos, Blocks.PISTON_HEAD.getStateWithProperties(state).with(Properties.PISTON_TYPE, PistonType.STICKY));
        replacePistonWithStickyAtPos(connected, world);
    }
}
