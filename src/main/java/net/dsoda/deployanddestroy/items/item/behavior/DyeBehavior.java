package net.dsoda.deployanddestroy.items.item.behavior;

import net.dsoda.deployanddestroy.util.behavior.DDDyeBehaviorHelper;
import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.DyeItem;
import net.minecraft.item.ItemStack;
import net.minecraft.sound.SoundCategory;
import net.minecraft.sound.SoundEvents;
import net.minecraft.stat.Stats;
import net.minecraft.util.ActionResult;
import net.minecraft.util.DyeColor;
import net.minecraft.util.hit.BlockHitResult;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraft.world.event.GameEvent;

import java.util.Optional;

public class DyeBehavior {

    public static ActionResult useOnBlock(PlayerEntity player, World world, ItemStack itemStack, BlockHitResult hitResult) {
        DyeItem item = (DyeItem) itemStack.getItem();
        DyeColor color = item.getColor();
        BlockPos pos = hitResult.getBlockPos();
        BlockState state = world.getBlockState(pos);
        Optional<BlockState> optional   = DDDyeBehaviorHelper.getWoolForDyeColor(color, state);
        Optional<BlockState> optional2  = DDDyeBehaviorHelper.getStainedGlassForDyeColor(color, state);
        Optional<BlockState> optional3  = DDDyeBehaviorHelper.getStainedGlassPaneForDyeColor(color, state);
        Optional<BlockState> optional4  = DDDyeBehaviorHelper.getConcreteForDyeColor(color, state);
        Optional<BlockState> optional5  = DDDyeBehaviorHelper.getConcretePowderForDyeColor(color, state);
        Optional<BlockState> optional6  = DDDyeBehaviorHelper.getTerracottaForDyeColor(color, state);
        Optional<BlockState> optional7  = DDDyeBehaviorHelper.getCandleForDyeColor(color, state);
        Optional<BlockState> optional8  = DDDyeBehaviorHelper.getCandleCakeForDyeColor(color, state);
        Optional<BlockState> optional9  = DDDyeBehaviorHelper.getCarpetForDyeColor(color, state);
        Optional<BlockState> optional10 = DDDyeBehaviorHelper.getLampForDyeColor(color, state);
        Optional<BlockState> optional11 = DDDyeBehaviorHelper.getGlazedTerracottaForDyeColor(color, state);
        Optional<BlockState> optionalThinkOfBetterName = DDDyeBehaviorHelper.getShulkerBoxForDyeColor(color, state);
        Optional<BlockState> optionalThinkOfBetterName2 = DDDyeBehaviorHelper.getBedForDyeColor(color, state);
        Optional<BlockState> optionalSuccess = Optional.empty();
        if (optional.isPresent()) optionalSuccess = optional;
        if (optional2.isPresent()) optionalSuccess = optional2;
        if (optional3.isPresent()) optionalSuccess = optional3;
        if (optional4.isPresent()) optionalSuccess = optional4;
        if (optional5.isPresent()) optionalSuccess = optional5;
        if (optional6.isPresent()) optionalSuccess = optional6;
        if (optional7.isPresent()) optionalSuccess = optional7;
        if (optional8.isPresent()) optionalSuccess = optional8;
        if (optional9.isPresent()) optionalSuccess = optional9;
        if (optional10.isPresent()) optionalSuccess = optional10;
        if (optional11.isPresent()) optionalSuccess = optional11;
        if (optionalThinkOfBetterName.isPresent()) {
            DDDyeBehaviorHelper.setShulker(world, pos, optionalThinkOfBetterName.get());
            world.playSound(player, pos, SoundEvents.BLOCK_SLIME_BLOCK_PLACE, SoundCategory.BLOCKS, 1f, 1f);
            player.incrementStat(Stats.USED.getOrCreateStat(itemStack.getItem()));
            if (!player.getAbilities().creativeMode) itemStack.decrement(1);
            return ActionResult.success(world.isClient);
        }
        if (optionalThinkOfBetterName2.isPresent()) {
            DDDyeBehaviorHelper.setBed(world, pos, optionalThinkOfBetterName2.get());
            world.playSound(player, pos, SoundEvents.BLOCK_SLIME_BLOCK_PLACE, SoundCategory.BLOCKS, 1f, 1f);
            player.incrementStat(Stats.USED.getOrCreateStat(itemStack.getItem()));
            if (!player.getAbilities().creativeMode) itemStack.decrement(1);
            return ActionResult.success(world.isClient);
        }
        if (optionalSuccess.isPresent()) {
            world.playSound(player, pos, SoundEvents.BLOCK_SLIME_BLOCK_PLACE, SoundCategory.BLOCKS, 1f, 1f);
            world.setBlockState(pos, optionalSuccess.get(), Block.NOTIFY_ALL | Block.REDRAW_ON_MAIN_THREAD);
            world.emitGameEvent(null, GameEvent.BLOCK_CHANGE, pos);
            player.incrementStat(Stats.USED.getOrCreateStat(itemStack.getItem()));
            if (!player.getAbilities().creativeMode) itemStack.decrement(1);
            return ActionResult.success(world.isClient);
        }
        return ActionResult.PASS;
    }
}
