package net.dsoda.deployanddestroy.items.item;

import net.dsoda.deployanddestroy.blocks.DDBlocks;
import net.dsoda.deployanddestroy.blocks.block.MaterialBarrelBlock;
import net.dsoda.deployanddestroy.stats.DDStats;
import net.minecraft.advancement.criterion.Criteria;
import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.block.entity.BlockEntity;
import net.minecraft.block.entity.LootableContainerBlockEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.item.ItemUsageContext;
import net.minecraft.nbt.NbtCompound;
import net.minecraft.server.network.ServerPlayerEntity;
import net.minecraft.sound.SoundCategory;
import net.minecraft.sound.SoundEvents;
import net.minecraft.text.Text;
import net.minecraft.util.ActionResult;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

import java.util.Optional;

public class MaterialBarrelUpgradeItem extends Item {

    private final MaterialBarrelBlock.BarrelMaterial material;

    public MaterialBarrelUpgradeItem(Settings settings, MaterialBarrelBlock.BarrelMaterial material) {
        super(settings);
        this.material = material;
    }

    @Override
    public ActionResult useOnBlock(ItemUsageContext ctx) {
        World world = ctx.getWorld();
        BlockPos pos = ctx.getBlockPos();
        BlockState state = world.getBlockState(pos);
        PlayerEntity player = ctx.getPlayer();
        Optional<Block> optional = MaterialBarrelBlock.getUpgradedBarrel(state.getBlock(), this.material);
        if (optional.isPresent()) {
            this.replaceBarrel(world, pos, optional.get().getStateWithProperties(state));
            world.playSound(null, pos, SoundEvents.BLOCK_ANVIL_USE, SoundCategory.BLOCKS, 1.0f, (float)(world.random.nextBetween(8, 12)) / 10);
            player.incrementStat(DDStats.BARRELS_UPGRADED);
            if (player instanceof ServerPlayerEntity)
                Criteria.INVENTORY_CHANGED.trigger((ServerPlayerEntity)player, player.getInventory(), new ItemStack(optional.get()));
            return ActionResult.success(world.isClient);
        }
        return ActionResult.PASS;
    }

    private void replaceBarrel(World world, BlockPos pos, BlockState newState) {
        BlockEntity oldBarrelEntity = world.getBlockEntity(pos);
        LootableContainerBlockEntity barrel = (LootableContainerBlockEntity) oldBarrelEntity;
        Text barrelName = barrel.getCustomName();
        NbtCompound nbt = barrel.createNbt();
        world.removeBlockEntity(pos);
        world.removeBlock(pos, false);
        world.setBlockState(pos, newState, Block.NOTIFY_ALL | Block.REDRAW_ON_MAIN_THREAD);
        world.updateListeners(pos, newState, newState, Block.NOTIFY_ALL | Block.REDRAW_ON_MAIN_THREAD);
        world.getBlockEntity(pos).readNbt(nbt);
        if (barrelName != null) ((LootableContainerBlockEntity) world.getBlockEntity(pos)).setCustomName(barrelName);
    }
}
