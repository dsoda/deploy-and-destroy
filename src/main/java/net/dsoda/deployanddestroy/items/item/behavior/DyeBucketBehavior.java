package net.dsoda.deployanddestroy.items.item.behavior;

import net.dsoda.deployanddestroy.items.item.DyeBucketItem;
import net.dsoda.deployanddestroy.stats.DDStats;
import net.dsoda.deployanddestroy.util.behavior.DDDyeBehaviorHelper;
import net.minecraft.advancement.criterion.Criteria;
import net.minecraft.block.BlockState;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Items;
import net.minecraft.server.network.ServerPlayerEntity;
import net.minecraft.sound.SoundCategory;
import net.minecraft.sound.SoundEvents;
import net.minecraft.stat.Stats;
import net.minecraft.util.ActionResult;
import net.minecraft.util.DyeColor;
import net.minecraft.util.Hand;
import net.minecraft.util.hit.BlockHitResult;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

import java.util.Optional;

public class DyeBucketBehavior {

    public static ActionResult useOnBlock(PlayerEntity player, World world, ItemStack itemStack, Hand hand, BlockHitResult hitResult) {
        BlockPos pos = hitResult.getBlockPos();
        BlockState state = world.getBlockState(pos);
        DyeColor c = DyeBucketItem.getColor(itemStack);
        Optional<BlockState> shulkerBox = DDDyeBehaviorHelper.getShulkerBoxForDyeColor(c, state);
        Optional<BlockState> bed = DDDyeBehaviorHelper.getBedForDyeColor(c, state);
        Optional<BlockState> setBlock = Optional.empty();

        if (shulkerBox.isPresent()) {
            DDDyeBehaviorHelper.setShulker(world, pos, shulkerBox.get());
            setBlock = shulkerBox;
        }
        if (bed.isPresent()) {
            DDDyeBehaviorHelper.setBed(world, pos, bed.get());
            setBlock = bed;
        }

        if (setBlock.isPresent()) {
            if (player instanceof ServerPlayerEntity)
                Criteria.ITEM_USED_ON_BLOCK.trigger((ServerPlayerEntity)player, pos, itemStack);
            world.playSound(null, pos, SoundEvents.ENTITY_ITEM_PICKUP, SoundCategory.BLOCKS, 1.0f, (float)(world.random.nextBetween(8, 12)) / 10);
            if (!player.getAbilities().creativeMode && itemStack.damage(1, world.random, null))
                player.setStackInHand(hand, Items.BUCKET.getDefaultStack());
            player.incrementStat(Stats.USED.getOrCreateStat(itemStack.getItem()));
            player.incrementStat(DDStats.DYE_BUCKET_CONVERSIONS);
            return ActionResult.success(world.isClient);
        }

        return ActionResult.FAIL;
    }
}
