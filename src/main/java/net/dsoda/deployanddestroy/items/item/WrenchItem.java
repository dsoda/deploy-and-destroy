package net.dsoda.deployanddestroy.items.item;

import net.dsoda.deployanddestroy.stats.DDStats;
import net.minecraft.block.*;
import net.minecraft.block.enums.BlockHalf;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.Item;
import net.minecraft.item.ItemUsageContext;
import net.minecraft.sound.SoundCategory;
import net.minecraft.sound.SoundEvents;
import net.minecraft.state.property.Properties;
import net.minecraft.util.ActionResult;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Direction;
import net.minecraft.world.World;

public class WrenchItem extends Item {

    public WrenchItem(Settings settings) {
        super(settings);
    }

    @Override
    public ActionResult useOnBlock(ItemUsageContext ctx) {
        World world = ctx.getWorld();
        if (tryRotateBlock(world, ctx.getPlayer(), ctx.getBlockPos(), ctx.getPlayer().isSneaking() ? ctx.getSide().getOpposite() : ctx.getSide())) {
            return ActionResult.success(world.isClient);
        }
        return ActionResult.PASS;
    }

    public static boolean tryRotateBlock(World world, PlayerEntity player, BlockPos pos, Direction side) {
        boolean b = side.equals(Direction.UP) || side.equals(Direction.DOWN);
        BlockState state = world.getBlockState(pos);
        Block block = state.getBlock();
        BlockState newState;

        if (notRotatable(state)) return false;

        // improve later or something idk
        if (state.contains(Properties.FACING)) {
            if (state.get(Properties.FACING).equals(side)) return false;
            newState = state.with(Properties.FACING, side);
        } else if (block instanceof StairsBlock) {
            if (state.get(Properties.HORIZONTAL_FACING).equals(side) || state.get(Properties.BLOCK_HALF).equals(blockHalfFromDirection(side))) return false;
            newState = b ? state.with(Properties.BLOCK_HALF, blockHalfFromDirection(side)) : state.with(Properties.HORIZONTAL_FACING, side);
        } else if (state.contains(Properties.HORIZONTAL_FACING)) {
            if (state.get(Properties.HORIZONTAL_FACING).equals(side) || b) return false;
            newState = state.with(Properties.HORIZONTAL_FACING, side);
        } else if (state.contains(Properties.AXIS)) {
            if (matches(side, state.get(Properties.AXIS))) return false;
            newState = newAxisState(side, state);
        } else if (block instanceof HopperBlock) {
            if (state.get(Properties.HOPPER_FACING).equals(side) || (state.get(Properties.HOPPER_FACING).equals(Direction.DOWN) && side.equals(Direction.UP))) return false;
            newState = b ? state.with(Properties.HOPPER_FACING, Direction.DOWN) : state.with(Properties.HOPPER_FACING, side);
        } else {
            return false;
        }

        world.setBlockState(pos, newState);
        world.playSound(null, pos, SoundEvents.ENTITY_ITEM_PICKUP, SoundCategory.BLOCKS, 1.0f, (float)(world.random.nextBetween(8, 12)) / 10);
        if (player != null)
            player.incrementStat(DDStats.BLOCKS_ROTATED);
        return true;
    }

    private static BlockHalf blockHalfFromDirection(Direction side) {
        return switch(side) {
            case UP -> BlockHalf.TOP;
            case DOWN -> BlockHalf.BOTTOM;
            default -> null;
        };
    }

    private static boolean matches(Direction side, Direction.Axis facing) {
        if ((side.equals(Direction.UP) || side.equals(Direction.DOWN)) && facing.equals(Direction.Axis.Y)) return true;
        else if ((side.equals(Direction.EAST) || side.equals(Direction.WEST)) && facing.equals(Direction.Axis.X)) return true;
        else return (side.equals(Direction.NORTH) || side.equals(Direction.SOUTH)) && facing.equals(Direction.Axis.Z);
    }

    private static BlockState newAxisState(Direction side, BlockState state) {
        if (side.equals(Direction.UP) || side.equals(Direction.DOWN)) {
            return state.with(Properties.AXIS, Direction.Axis.Y);
        } else if (side.equals(Direction.EAST) || side.equals(Direction.WEST)) {
            return state.with(Properties.AXIS, Direction.Axis.X);
        } else {
            return state.with(Properties.AXIS, Direction.Axis.Z);
        }
    }

    private static boolean notRotatable(BlockState state) {
        Block block = state.getBlock();
        return block instanceof TorchBlock ||
               block instanceof LeverBlock ||
               block instanceof ButtonBlock ||
               block instanceof ChestBlock ||
               block instanceof BedBlock ||
              (block instanceof PistonBlock && state.get(Properties.EXTENDED)) ||
               block instanceof PistonHeadBlock ||
               block instanceof EndPortalFrameBlock;
    }
}
