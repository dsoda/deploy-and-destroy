package net.dsoda.deployanddestroy.items.item.behavior;

import com.google.common.collect.ImmutableMap;
import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.block.Blocks;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.registry.tag.BlockTags;
import net.minecraft.sound.SoundCategory;
import net.minecraft.sound.SoundEvents;
import net.minecraft.stat.Stats;
import net.minecraft.util.ActionResult;
import net.minecraft.util.hit.BlockHitResult;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraft.world.event.GameEvent;

import java.util.Map;

public class IronIngotBehavior {

    private static final Map<Block, Block> REPAIR_ANVIL = new ImmutableMap.Builder<Block, Block>()
        .put(Blocks.CHIPPED_ANVIL, Blocks.ANVIL)
        .put(Blocks.DAMAGED_ANVIL, Blocks.CHIPPED_ANVIL)
        .build();

    public static ActionResult useOnBlock(PlayerEntity player, World world, ItemStack itemStack, BlockHitResult hitResult) {
        BlockPos usedOn = hitResult.getBlockPos();
        BlockState state = world.getBlockState(usedOn);
        if (state.isIn(BlockTags.ANVIL) && state.getBlock() != Blocks.ANVIL) {
            world.setBlockState(usedOn, REPAIR_ANVIL.get(state.getBlock()).getStateWithProperties(state), Block.NOTIFY_ALL | Block.REDRAW_ON_MAIN_THREAD);
            world.playSound(null, usedOn, SoundEvents.BLOCK_ANVIL_USE, SoundCategory.BLOCKS, 1f, 1f);
            world.emitGameEvent(GameEvent.BLOCK_CHANGE, usedOn, GameEvent.Emitter.of(player, state));
            player.incrementStat(Stats.USED.getOrCreateStat(itemStack.getItem()));
            if (!player.getAbilities().creativeMode) itemStack.decrement(1);
            return ActionResult.success(world.isClient);
        }
        return ActionResult.PASS;
    }
}
