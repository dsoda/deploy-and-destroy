package net.dsoda.deployanddestroy.stats;

import net.dsoda.deployanddestroy.DeployAndDestroy;
import net.minecraft.registry.Registries;
import net.minecraft.registry.Registry;
import net.minecraft.stat.StatFormatter;
import net.minecraft.stat.Stats;
import net.minecraft.util.Identifier;

import static net.dsoda.deployanddestroy.DeployAndDestroy.MOD_ID;

public class DDStats {

    public static final Identifier INTERACT_WITH_DESTROYER = register("interact_with_destroyer", StatFormatter.DEFAULT);
    public static final Identifier INTERACT_WITH_DEPLOYER = register("interact_with_deployer", StatFormatter.DEFAULT);
    public static final Identifier INTERACT_WITH_FAST_HOPPER = register("interact_with_fast_hopper", StatFormatter.DEFAULT);
    public static final Identifier INTERACT_WITH_WOODCUTTER = register("interact_with_woodcutter", StatFormatter.DEFAULT);
    public static final Identifier BLOCKS_ROTATED = register("blocks_rotated", StatFormatter.DEFAULT);
    public static final Identifier DYE_BUCKET_CONVERSIONS = register("dye_bucket_conversions", StatFormatter.DEFAULT);
    public static final Identifier OPEN_COPPER_BARREL = register("interact_with_copper_barrel", StatFormatter.DEFAULT);
    public static final Identifier OPEN_IRON_BARREL = register("interact_with_iron_barrel", StatFormatter.DEFAULT);
    public static final Identifier OPEN_GOLD_BARREL = register("interact_with_gold_barrel", StatFormatter.DEFAULT);
    public static final Identifier OPEN_DIAMOND_BARREL = register("interact_with_diamond_barrel", StatFormatter.DEFAULT);
    public static final Identifier OPEN_NETHERITE_BARREL = register("interact_with_netherite_barrel", StatFormatter.DEFAULT);
    public static final Identifier BARRELS_UPGRADED = register("barrels_upgraded", StatFormatter.DEFAULT);
    public static final Identifier FEATHERS_PLUCKED = register("feathers_plucked", StatFormatter.DEFAULT);

    private static Identifier register(String id, StatFormatter formatter) {
        Identifier identifier = new Identifier(MOD_ID, id);
        Registry.register(Registries.CUSTOM_STAT, id, identifier);
        Stats.CUSTOM.getOrCreateStat(identifier, formatter);
        return identifier;
    }

    public static void registerStats() {
        DeployAndDestroy.LOGGER.info("Registering Item Groups For " + MOD_ID);
    }
}
