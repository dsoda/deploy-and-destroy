package net.dsoda.deployanddestroy.mixin.entity;

import net.minecraft.entity.EntityType;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.vehicle.AbstractMinecartEntity;
import net.minecraft.entity.vehicle.MinecartEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Items;
import net.minecraft.sound.SoundCategory;
import net.minecraft.sound.SoundEvents;
import net.minecraft.util.ActionResult;
import net.minecraft.util.Hand;
import net.minecraft.world.World;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Unique;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;

@Mixin(MinecartEntity.class)
public abstract class MinecartEntityMixin extends AbstractMinecartEntity {

    protected MinecartEntityMixin(EntityType<?> entityType, World world) {
        super(entityType, world);
    }

    @Inject(method = "interact", at = @At(value = "HEAD"), cancellable = true)
    private void changeMinecartType(PlayerEntity player, Hand hand, CallbackInfoReturnable<ActionResult> cir) {
        World world = this.getWorld();
        ItemStack stack = player.getStackInHand(hand);
        if (stack.isOf(Items.HOPPER))
            deployanddestroy$swapMinecartEntity(world, this, EntityType.HOPPER_MINECART);
        else if (stack.isOf(Items.CHEST))
            deployanddestroy$swapMinecartEntity(world, this, EntityType.CHEST_MINECART);
        else if (stack.isOf(Items.FURNACE))
            deployanddestroy$swapMinecartEntity(world, this, EntityType.FURNACE_MINECART);
        else if (stack.isOf(Items.TNT))
            deployanddestroy$swapMinecartEntity(world, this, EntityType.TNT_MINECART);
        else return;

        if (!player.getAbilities().creativeMode) stack.decrement(1);
        cir.setReturnValue(ActionResult.success(world.isClient));
    }

    @Unique
    private static void deployanddestroy$swapMinecartEntity(World world, MinecartEntityMixin minecart, EntityType<?> e) {
        AbstractMinecartEntity newMinecart;
        world.playSoundFromEntity(null, minecart, SoundEvents.ENTITY_ITEM_PICKUP, SoundCategory.BLOCKS, 1.0f, (float)(world.random.nextBetween(8, 12)) / 10);
        if (!world.isClient() && (newMinecart = (AbstractMinecartEntity) e.create(world)) != null) {
            minecart.discard();
            newMinecart.refreshPositionAndAngles(minecart.getX(), minecart.getY(), minecart.getZ(), minecart.getYaw(), minecart.getPitch());
            if (minecart.hasCustomName()) {
                newMinecart.setCustomName(minecart.getCustomName());
                newMinecart.setCustomNameVisible(minecart.isCustomNameVisible());
            }
            world.spawnEntity(newMinecart);
        }
    }
}
