package net.dsoda.deployanddestroy.mixin.entity.accessor;

import net.minecraft.block.SuspiciousStewIngredient;
import net.minecraft.entity.passive.MooshroomEntity;
import net.minecraft.item.ItemStack;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.gen.Accessor;
import org.spongepowered.asm.mixin.gen.Invoker;

import java.util.List;
import java.util.Optional;

@Mixin(MooshroomEntity.class)
public interface MooshroomStatusEffectAccessor {

    @Accessor("stewEffects")
    List<SuspiciousStewIngredient.StewEffect> getStewEffects();

    @Accessor("stewEffects")
    void setStewEffects(List<SuspiciousStewIngredient.StewEffect> effect);

    @Invoker("getStewEffectFrom")
    Optional<List<SuspiciousStewIngredient.StewEffect>> invokeGetStewEffectFrom(ItemStack flower);
}
