package net.dsoda.deployanddestroy.mixin.entity;

import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.vehicle.BoatEntity;
import net.minecraft.entity.vehicle.ChestBoatEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Items;
import net.minecraft.sound.SoundCategory;
import net.minecraft.sound.SoundEvents;
import net.minecraft.util.ActionResult;
import net.minecraft.util.Hand;
import net.minecraft.world.World;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;
import org.spongepowered.asm.mixin.Unique;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;

@Mixin(BoatEntity.class)
public abstract class BoatEntityMixin extends Entity {

    @Shadow
    public abstract BoatEntity.Type getVariant();

    public BoatEntityMixin(EntityType<?> type, World world) {
        super(type, world);
    }

    @Inject(method = "interact", at = @At(value = "HEAD"), cancellable = true)
    private void convertBoatWithChest(PlayerEntity player, Hand hand, CallbackInfoReturnable<ActionResult> cir) {
        World world = this.getWorld();
        ItemStack stack = player.getStackInHand(hand);
        if (stack.isOf(Items.CHEST))
            deployanddestroy$swapBoatEntity(world, this, EntityType.CHEST_BOAT);
        else return;

        cir.setReturnValue(ActionResult.success(world.isClient));
    }

    @Unique
    private static void deployanddestroy$swapBoatEntity(World world, BoatEntityMixin boat, EntityType<?> e) {
        ChestBoatEntity newBoat;
        world.playSoundFromEntity(null, boat, SoundEvents.ENTITY_ITEM_PICKUP, SoundCategory.BLOCKS, 1.0f, (float)(world.random.nextBetween(8, 12)) / 10);
        if (!world.isClient() && (newBoat = (ChestBoatEntity) e.create(world)) != null) {
            newBoat.setVariant(boat.getVariant());
            boat.discard();
            newBoat.refreshPositionAndAngles(boat.getX(), boat.getY(), boat.getZ(), boat.getYaw(), boat.getPitch());
            if (boat.hasCustomName()) {
                newBoat.setCustomName(boat.getCustomName());
                newBoat.setCustomNameVisible(boat.isCustomNameVisible());
            }
            world.spawnEntity(newBoat);
        }
    }
}
