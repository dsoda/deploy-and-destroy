package net.dsoda.deployanddestroy.mixin.entity;

import net.dsoda.deployanddestroy.items.item.DyeBucketItem;
import net.dsoda.deployanddestroy.stats.DDStats;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.passive.TameableEntity;
import net.minecraft.entity.passive.WolfEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Items;
import net.minecraft.sound.SoundCategory;
import net.minecraft.sound.SoundEvents;
import net.minecraft.util.ActionResult;
import net.minecraft.util.DyeColor;
import net.minecraft.util.Hand;
import net.minecraft.world.World;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;

@Mixin(WolfEntity.class)
public abstract class WolfEntityMixin extends TameableEntity {

    protected WolfEntityMixin(EntityType<? extends TameableEntity> entityType, World world) {
        super(entityType, world);
    }

    @Inject(method = "interactMob", at = @At(value = "INVOKE", target = "Lnet/minecraft/entity/passive/WolfEntity;isTamed()Z", ordinal = 2, shift = At.Shift.AFTER), cancellable = true)
    private void paintbrushWolfInteractions(PlayerEntity player, Hand hand, CallbackInfoReturnable<ActionResult> cir) {
        boolean success = true;
        ItemStack stack = player.getStackInHand(hand);
        if (stack.getItem() instanceof DyeBucketItem) {
            DyeColor color = DyeBucketItem.getColor(stack);
            DyeColor collarColor = this.getCollarColor();
            if (color == collarColor) success = false;
            if (success) {
                if (player.isSneaking()) {
                    DyeBucketItem.setColor(player, stack, collarColor);
                } else if (this.isOwner(player)) {
                    World world = player.getWorld();
                    this.setCollarColor(color);
                    world.playSound(null, this.getBlockPos(), SoundEvents.ENTITY_ITEM_PICKUP, SoundCategory.BLOCKS, 1.0f, (float)(world.random.nextBetween(8, 12)) / 10);
                    if (!player.getAbilities().creativeMode && stack.damage(1, world.random, null))
                        player.setStackInHand(hand, Items.BUCKET.getDefaultStack());
                    player.incrementStat(DDStats.DYE_BUCKET_CONVERSIONS);
                } else {
                    success = false;
                }
            }
            cir.setReturnValue(success ? ActionResult.SUCCESS : super.interactMob(player, hand));
        }
    }

    @Shadow
    public abstract DyeColor getCollarColor();

    @Shadow
    public abstract void setCollarColor(DyeColor color);
}
