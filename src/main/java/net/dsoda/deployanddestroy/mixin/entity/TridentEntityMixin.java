package net.dsoda.deployanddestroy.mixin.entity;

import net.minecraft.entity.EntityType;
import net.minecraft.entity.projectile.PersistentProjectileEntity;
import net.minecraft.entity.projectile.TridentEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

@Mixin(TridentEntity.class)
public abstract class TridentEntityMixin extends PersistentProjectileEntity {

    @Shadow
    private boolean dealtDamage;

    protected TridentEntityMixin(EntityType<? extends PersistentProjectileEntity> type, World world, ItemStack stack) {
        super(type, world, stack);
    }


    @Override
    public ItemStack asItemStack() {
        return null;
    }

    @Inject(method = "tick", at = @At(value = "HEAD"))
    private void tickInject(CallbackInfo ci) {
        boolean isOverworld = this.getWorld().getRegistryKey().equals(World.OVERWORLD);
        if (this.getY() < (isOverworld ? -74f : -10f)) this.dealtDamage = true;
    }
}
