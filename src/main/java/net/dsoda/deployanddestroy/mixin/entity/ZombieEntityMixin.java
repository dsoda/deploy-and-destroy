package net.dsoda.deployanddestroy.mixin.entity;

import net.dsoda.deployanddestroy.stats.DDStats;
import net.minecraft.entity.EntityData;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.SpawnReason;
import net.minecraft.entity.data.DataTracker;
import net.minecraft.entity.data.TrackedData;
import net.minecraft.entity.data.TrackedDataHandlerRegistry;
import net.minecraft.entity.mob.HostileEntity;
import net.minecraft.entity.mob.ZombieEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Items;
import net.minecraft.nbt.NbtCompound;
import net.minecraft.util.ActionResult;
import net.minecraft.util.Hand;
import net.minecraft.world.LocalDifficulty;
import net.minecraft.world.ServerWorldAccess;
import net.minecraft.world.World;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Unique;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;

@Mixin(ZombieEntity.class)
public abstract class ZombieEntityMixin extends HostileEntity {

    @Unique
    private static final TrackedData<Boolean> deployanddestroy$PLUCKED = DataTracker.registerData(ZombieEntityMixin.class, TrackedDataHandlerRegistry.BOOLEAN);

    @Unique
    private static final String deployanddestroy$PLUCKED_KEY = "Plucked";

    protected ZombieEntityMixin(EntityType<? extends HostileEntity> entityType, World world) {
        super(entityType, world);
    }

    @Inject(method="initialize", at=@At(value="HEAD"), cancellable = true)
    protected void initializePlucked(ServerWorldAccess world, LocalDifficulty difficulty, SpawnReason spawnReason, EntityData entityData, NbtCompound entityNbt, CallbackInfoReturnable<EntityData> cir) {
        if (this.deployanddestroy$plucked()) {
            cir.setReturnValue(entityData);
        }
    }

    @Inject(method="initDataTracker", at=@At(value="TAIL"))
    protected void initPluckedTracking(CallbackInfo ci) {
        this.dataTracker.startTracking(deployanddestroy$PLUCKED, false);
    }

    @Override
    @Unique
    public ActionResult interactMob(PlayerEntity player, Hand hand) {
        if (!this.deployanddestroy$plucked() && !this.isBaby() && player.getStackInHand(hand).isOf(Items.AIR)) {
            this.deployanddestroy$setPlucked(true);
            int amount = this.getWorld().random.nextBetween(1, 3);
            this.dropStack(new ItemStack(Items.FEATHER, amount));
            this.damage(this.getDamageSources().playerAttack(player), 1f);
            for (int i = 0; i < amount; i++)
                player.incrementStat(DDStats.FEATHERS_PLUCKED);
            return ActionResult.success(this.getWorld().isClient);
        }
        return super.interactMob(player, hand);
    }

    @Unique
    public boolean deployanddestroy$plucked() {
        return this.dataTracker.get(deployanddestroy$PLUCKED);
    }

    @Unique
    public void deployanddestroy$setPlucked(boolean plucked) {
        this.dataTracker.set(deployanddestroy$PLUCKED, plucked);
    }

    @Inject(at = @At("HEAD"), method = "writeCustomDataToNbt")
    public void writeCustomDataToNbt(NbtCompound nbt, CallbackInfo ci){
        super.writeCustomDataToNbt(nbt);
        nbt.putBoolean(deployanddestroy$PLUCKED_KEY, this.deployanddestroy$plucked());
    }

    @Inject(at = @At("HEAD"), method = "readCustomDataFromNbt")
    public void readCustomDataFromNbt(NbtCompound nbt, CallbackInfo ci) {
        super.readCustomDataFromNbt(nbt);
        this.deployanddestroy$setPlucked(nbt.getBoolean(deployanddestroy$PLUCKED_KEY));
    }
}
