package net.dsoda.deployanddestroy.mixin.entity;

import net.dsoda.deployanddestroy.stats.DDStats;
import net.minecraft.entity.EntityData;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.SpawnReason;
import net.minecraft.entity.data.DataTracker;
import net.minecraft.entity.data.TrackedData;
import net.minecraft.entity.data.TrackedDataHandlerRegistry;
import net.minecraft.entity.passive.AnimalEntity;
import net.minecraft.entity.passive.ChickenEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Items;
import net.minecraft.nbt.NbtCompound;
import net.minecraft.util.ActionResult;
import net.minecraft.util.Hand;
import net.minecraft.world.LocalDifficulty;
import net.minecraft.world.ServerWorldAccess;
import net.minecraft.world.World;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Unique;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

@Mixin(ChickenEntity.class)
public abstract class ChickenEntityMixin extends AnimalEntity {

    @Unique
    private static final TrackedData<Boolean> deployanddestroy$PLUCKED = DataTracker.registerData(ChickenEntityMixin.class, TrackedDataHandlerRegistry.BOOLEAN);

    @Unique
    private static final String deployanddestroy$PLUCKED_KEY = "Plucked";

    protected ChickenEntityMixin(EntityType<? extends AnimalEntity> entityType, World world) {
        super(entityType, world);
    }

    @Override
    @Unique
    public EntityData initialize(ServerWorldAccess world, LocalDifficulty difficulty, SpawnReason spawnReason, EntityData entityData, NbtCompound entityNbt) {
        if (this.deployanddestroy$plucked()) {
            return entityData;
        }
        return super.initialize(world, difficulty, spawnReason, entityData, entityNbt);
    }

    @Override
    @Unique
    protected void initDataTracker() {
        super.initDataTracker();
        this.dataTracker.startTracking(deployanddestroy$PLUCKED, false);
    }

    @Override
    @Unique
    public ActionResult interactMob(PlayerEntity player, Hand hand) {
        if (!this.deployanddestroy$plucked() && !this.isBaby() && player.getStackInHand(hand).isOf(Items.AIR)) {
            this.deployanddestroy$setPlucked(true);
            int amount = this.getWorld().random.nextBetween(1, 2);
            this.dropStack(new ItemStack(Items.FEATHER, amount));
            this.damage(this.getDamageSources().playerAttack(player), 1f);
            for (int i = 0; i < amount; i++)
                player.incrementStat(DDStats.FEATHERS_PLUCKED);
            return ActionResult.success(this.getWorld().isClient);
        }
        return super.interactMob(player, hand);
    }

    @Unique
    public boolean deployanddestroy$plucked() {
        return this.dataTracker.get(deployanddestroy$PLUCKED);
    }

    @Unique
    public void deployanddestroy$setPlucked(boolean plucked) {
        this.dataTracker.set(deployanddestroy$PLUCKED, plucked);
    }

    @Inject(at = @At("HEAD"), method = "writeCustomDataToNbt")
    public void writeCustomDataToNbt(NbtCompound nbt, CallbackInfo ci){
        super.writeCustomDataToNbt(nbt);
        nbt.putBoolean(deployanddestroy$PLUCKED_KEY, this.deployanddestroy$plucked());
    }

    @Inject(at = @At("HEAD"), method = "readCustomDataFromNbt")
    public void readCustomDataFromNbt(NbtCompound nbt, CallbackInfo ci) {
        super.readCustomDataFromNbt(nbt);
        this.deployanddestroy$setPlucked(nbt.getBoolean(deployanddestroy$PLUCKED_KEY));
    }
}
