package net.dsoda.deployanddestroy.mixin.entity;

import net.dsoda.deployanddestroy.items.DDItems;
import net.dsoda.deployanddestroy.util.tag.DDItemTags;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.Item;
import net.minecraft.registry.tag.TagKey;
import net.minecraft.world.World;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Unique;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;

@Mixin(PlayerEntity.class)
public abstract class PlayerEntityMixin extends LivingEntity {

    protected PlayerEntityMixin(EntityType<? extends LivingEntity> entityType, World world) {
        super(entityType, world);
    }

    @Inject(method = "shouldCancelInteraction", at = @At(value = "TAIL"), cancellable = true)
    private void cancelInteraction(CallbackInfoReturnable<Boolean> cir) {
        cir.setReturnValue(
            this.isHolding(DDItems.WRENCH) || this.deployanddestroy$isHolding(DDItemTags.BARREL_UPGRADE_ITEMS) || cir.getReturnValue()
        );
    }

    @Unique
    private boolean deployanddestroy$isHolding(TagKey<Item> tag) {
        return this.getMainHandStack().isIn(tag) || this.getOffHandStack().isIn(tag);
    }
}
