package net.dsoda.deployanddestroy.mixin.entity;

import net.dsoda.deployanddestroy.items.item.DyeBucketItem;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.passive.AbstractDonkeyEntity;
import net.minecraft.entity.passive.LlamaEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ActionResult;
import net.minecraft.util.DyeColor;
import net.minecraft.util.Hand;
import net.minecraft.world.World;
import org.jetbrains.annotations.Nullable;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;

@Mixin(LlamaEntity.class)
public abstract class LlamaEntityMixin extends AbstractDonkeyEntity {

    @Shadow
    protected abstract void setCarpetColor(@Nullable DyeColor color);

    public LlamaEntityMixin(EntityType<? extends AbstractDonkeyEntity> entityType, World world) {
        super(entityType, world);
    }

    @Override
    public ActionResult interactMob(PlayerEntity player, Hand hand) {
        ItemStack stack = player.getStackInHand(hand);
        if (stack.getItem() instanceof DyeBucketItem) {
            if (DyeBucketItem.getColor(stack) == this.getCarpetColor()) return super.interactMob(player, hand);
            if (player.isSneaking()) DyeBucketItem.setColor(player, stack, this.getCarpetColor());
            return ActionResult.SUCCESS;
        } else {
            return super.interactMob(player, hand);
        }
    }

    @Shadow
    public abstract DyeColor getCarpetColor();
}
