package net.dsoda.deployanddestroy.mixin.misc;

import net.dsoda.deployanddestroy.util.recipe.RecipeRemainderOverride;
import net.minecraft.inventory.Inventory;
import net.minecraft.item.ItemStack;
import net.minecraft.recipe.Recipe;
import net.minecraft.util.collection.DefaultedList;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;

@Mixin(Recipe.class)
public interface RecipeMixin<C extends Inventory> {

    @Inject(method = "getRemainder", at = @At(value = "RETURN", shift = At.Shift.BEFORE), cancellable = true)
    private void overrideRemainder(C inventory, CallbackInfoReturnable<DefaultedList<ItemStack>> cir) {
        if(RecipeRemainderOverride.SHOULD_OVERRIDE) {
            DefaultedList<ItemStack> defaultedList = DefaultedList.ofSize(inventory.size(), ItemStack.EMPTY);
            for (int i = 0; i < defaultedList.size(); ++i)
                defaultedList.set(i, new ItemStack(RecipeRemainderOverride.OVERRIDE.getRemainderOverride(inventory.getStack(i).getItem())));
            cir.setReturnValue(defaultedList);
        }
    }
}
