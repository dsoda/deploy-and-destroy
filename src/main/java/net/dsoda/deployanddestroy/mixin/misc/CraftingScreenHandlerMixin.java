package net.dsoda.deployanddestroy.mixin.misc;

import net.dsoda.deployanddestroy.util.recipe.DDRecipeRemainderOverrides;
import net.dsoda.deployanddestroy.util.recipe.RecipeRemainderOverride;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.inventory.CraftingResultInventory;
import net.minecraft.inventory.RecipeInputInventory;
import net.minecraft.screen.CraftingScreenHandler;
import net.minecraft.screen.ScreenHandler;
import net.minecraft.world.World;
import org.spongepowered.asm.mixin.*;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

@Mixin(CraftingScreenHandler.class)
public abstract class CraftingScreenHandlerMixin {

    @Inject(method = "updateResult", at = @At(value = "TAIL"))
    private static void removeBucketRemainder(ScreenHandler handler, World world, PlayerEntity player, RecipeInputInventory craftingInventory, CraftingResultInventory resultInventory, CallbackInfo ci) {
        for(RecipeRemainderOverride override : DDRecipeRemainderOverrides.OVERRIDES) {
            RecipeRemainderOverride.SHOULD_OVERRIDE = resultInventory.getStack(0).isOf(override.getOutputItem());
            RecipeRemainderOverride.OVERRIDE = RecipeRemainderOverride.SHOULD_OVERRIDE ? override : null;
        }
    }
}
