package net.dsoda.deployanddestroy.mixin.misc;

import net.dsoda.deployanddestroy.blocks.block.OxidizableColumnBlock;
import net.dsoda.deployanddestroy.blocks.block.OxidizablePressurePlateBlock;
import net.dsoda.deployanddestroy.blocks.block.OxidizableWallBlock;
import net.dsoda.deployanddestroy.util.DDOxidationHelper;
import net.minecraft.block.Block;
import net.minecraft.block.Oxidizable;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;

import java.util.Optional;

@Mixin(Oxidizable.class)
public interface OxidizableMixin {

    @Inject(method="getDecreasedOxidationBlock", at=@At(value="RETURN"), cancellable = true)
    private static void decreasedPressurePlate(Block block, CallbackInfoReturnable<Optional<Block>> cir) {
        if (block instanceof OxidizablePressurePlateBlock || block instanceof OxidizableColumnBlock || block instanceof OxidizableWallBlock) {
            cir.setReturnValue(Optional.ofNullable(DDOxidationHelper.OXIDIZABLE_DECREASE.get(block)));
        }
    }

    @Inject(method="getUnaffectedOxidationBlock", at=@At(value="RETURN"), cancellable = true)
    private static void unaffectedPressurePlate(Block block, CallbackInfoReturnable<Block> cir) {
        if (block instanceof OxidizablePressurePlateBlock || block instanceof OxidizableColumnBlock || block instanceof OxidizableWallBlock) {
            Block block2 = block;
            Optional<Block> block3 = Optional.ofNullable(DDOxidationHelper.OXIDIZABLE_DECREASE.get(block2));
            while (block3.isPresent()) {
                block2 = block3.get();
                block3 = Optional.ofNullable(DDOxidationHelper.OXIDIZABLE_DECREASE.get(block2));
            }
            cir.setReturnValue(block2);
        }
    }

    @Inject(method="getIncreasedOxidationBlock", at=@At(value="RETURN"), cancellable = true)
    private static void increasedPressurePlate(Block block, CallbackInfoReturnable<Optional<Block>> cir) {
        if (block instanceof OxidizablePressurePlateBlock || block instanceof OxidizableColumnBlock || block instanceof OxidizableWallBlock) {
            cir.setReturnValue(Optional.ofNullable(DDOxidationHelper.OXIDIZABLE_INCREASE.get(block)));
        }
    }
}
