package net.dsoda.deployanddestroy.mixin.misc;

import com.llamalad7.mixinextras.injector.wrapoperation.Operation;
import com.llamalad7.mixinextras.injector.wrapoperation.WrapOperation;
import com.mojang.serialization.Codec;
import net.dsoda.deployanddestroy.DeployAndDestroy;
import net.dsoda.deployanddestroy.blocks.DDBlocks;
import net.minecraft.block.BlockState;
import net.minecraft.block.Blocks;
import net.minecraft.entity.EntityType;
import net.minecraft.util.Util;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.random.Random;
import net.minecraft.world.StructureWorldAccess;
import net.minecraft.world.gen.feature.DefaultFeatureConfig;
import net.minecraft.world.gen.feature.DungeonFeature;
import net.minecraft.world.gen.feature.Feature;
import org.apache.commons.lang3.ArrayUtils;
import org.spongepowered.asm.mixin.Final;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;
import org.spongepowered.asm.mixin.Unique;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.Redirect;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;

import java.util.ArrayList;
import java.util.function.Predicate;

@Mixin(DungeonFeature.class)
public abstract class DungeonFeatureMixin extends Feature<DefaultFeatureConfig> {

    @Unique
    private static EntityType<?>[] deployanddestroy$NEW_ENTITIES = new EntityType[]
        {
            EntityType.SKELETON,
            EntityType.ZOMBIE, EntityType.ZOMBIE,
            EntityType.SPIDER,
            EntityType.HUSK,
            EntityType.STRAY
        };

    public DungeonFeatureMixin(Codec<DefaultFeatureConfig> configCodec) {
        super(configCodec);
    }

    @Redirect(method = "generate", at = @At(value = "INVOKE", target = "Lnet/minecraft/world/gen/feature/DungeonFeature;setBlockStateIf(Lnet/minecraft/world/StructureWorldAccess;Lnet/minecraft/util/math/BlockPos;Lnet/minecraft/block/BlockState;Ljava/util/function/Predicate;)V", ordinal = 0))
    private void generateMossyCobbledDeepslate(DungeonFeature instance, StructureWorldAccess structureWorldAccess, BlockPos blockPos, BlockState blockState, Predicate<BlockState> predicate) {
        if (blockPos.getY() < 0)
            this.setBlockStateIf(structureWorldAccess, blockPos, DDBlocks.MOSSY_COBBLED_DEEPSLATE.getDefaultState(), predicate);
        else
            this.setBlockStateIf(structureWorldAccess, blockPos, Blocks.MOSSY_COBBLESTONE.getDefaultState(), predicate);
    }

    @Redirect(method = "generate", at = @At(value = "INVOKE", target = "Lnet/minecraft/world/gen/feature/DungeonFeature;setBlockStateIf(Lnet/minecraft/world/StructureWorldAccess;Lnet/minecraft/util/math/BlockPos;Lnet/minecraft/block/BlockState;Ljava/util/function/Predicate;)V", ordinal = 1))
    private void generateCobbledDeepslate(DungeonFeature instance, StructureWorldAccess structureWorldAccess, BlockPos blockPos, BlockState blockState, Predicate<BlockState> predicate) {
        if (blockPos.getY() < 0)
            this.setBlockStateIf(structureWorldAccess, blockPos, Blocks.COBBLED_DEEPSLATE.getDefaultState(), predicate);
        else
            this.setBlockStateIf(structureWorldAccess, blockPos, Blocks.COBBLESTONE.getDefaultState(), predicate);
    }

    @WrapOperation(method = "getMobSpawnerEntity", at = @At(value = "INVOKE", target = "Lnet/minecraft/util/Util;getRandom([Ljava/lang/Object;Lnet/minecraft/util/math/random/Random;)Ljava/lang/Object;"))
    private <T> T addEntitiesToSpawnerPool(T[] array, Random random, Operation<T> original) {
        DeployAndDestroy.LOGGER.info("Inserting new dungeon spawner entities");
        return original.call(ArrayUtils.insert(array.length, array, deployanddestroy$NEW_ENTITIES), random);
    }
}
