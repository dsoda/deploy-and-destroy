package net.dsoda.deployanddestroy.mixin.item;

import net.dsoda.deployanddestroy.util.CrackedBlockCraftingHelper;
import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.*;
import net.minecraft.registry.tag.TagKey;
import net.minecraft.sound.SoundCategory;
import net.minecraft.sound.SoundEvents;
import net.minecraft.util.ActionResult;
import net.minecraft.util.Hand;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import org.spongepowered.asm.mixin.Mixin;

import java.util.Optional;

@Mixin(PickaxeItem.class)
public abstract class PickaxeItemMixin extends MiningToolItem {

    public PickaxeItemMixin(float attackDamage, float attackSpeed, ToolMaterial material, TagKey<Block> effectiveBlocks, Settings settings) {
        super(attackDamage, attackSpeed, material, effectiveBlocks, settings);
    }

    @Override
    public ActionResult useOnBlock(ItemUsageContext context) {
        World world = context.getWorld();
        PlayerEntity player = context.getPlayer();
        Hand hand = context.getHand();
        ItemStack stack = context.getStack();
        BlockPos pos = context.getBlockPos();
        Optional<Block> optional = CrackedBlockCraftingHelper.getCrackedBlock(world, pos);
        if (optional.isPresent()) {
            BlockState state1 = world.getBlockState(pos);
            BlockState state = optional.get().getStateWithProperties(state1);
            if (!player.getAbilities().creativeMode) stack.damage(1, player, p -> p.sendToolBreakStatus(hand));
            world.setBlockState(pos, state, Block.NOTIFY_ALL | Block.REDRAW_ON_MAIN_THREAD);
            world.playSound(null, pos, SoundEvents.BLOCK_ANVIL_LAND, SoundCategory.BLOCKS, 1, 1);
            return ActionResult.success(world.isClient);
        }
        return ActionResult.PASS;
    }

}
