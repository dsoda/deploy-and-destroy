package net.dsoda.deployanddestroy.mixin.item;

import net.dsoda.deployanddestroy.util.behavior.DDUnmossHelper;
import net.minecraft.advancement.criterion.Criteria;
import net.minecraft.block.BlockState;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.item.ItemUsageContext;
import net.minecraft.item.ShearsItem;
import net.minecraft.server.network.ServerPlayerEntity;
import net.minecraft.util.ActionResult;
import net.minecraft.util.Hand;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Direction;
import net.minecraft.world.World;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;

import java.util.Optional;

@Mixin(ShearsItem.class)
public abstract class ShearsItemMixin extends Item {

    public ShearsItemMixin(Settings settings) {
        super(settings);
    }

    @Inject(method = "useOnBlock", at = @At(value = "TAIL"), cancellable = true)
    private void useOnBlockInject(ItemUsageContext context, CallbackInfoReturnable<ActionResult> cir) {
        World world = context.getWorld();
        PlayerEntity player = context.getPlayer();
        Hand hand = context.getHand();
        ItemStack itemStack = context.getStack();
        BlockPos usedOn = context.getBlockPos();
        BlockState state = world.getBlockState(usedOn);
        Direction direction = context.getSide().getAxis() == Direction.Axis.Y ? player.getHorizontalFacing().getOpposite() : context.getSide();
        Optional<BlockState> optional = DDUnmossHelper.getMossyBlock(world, usedOn, state);
        if (optional.isPresent()) {
            DDUnmossHelper.removeMoss(world, direction, usedOn, optional.get());
            if (player instanceof ServerPlayerEntity) Criteria.ITEM_USED_ON_BLOCK.trigger((ServerPlayerEntity)player, usedOn, itemStack);
            if (!player.getAbilities().creativeMode) itemStack.damage(1, player, p -> p.sendToolBreakStatus(hand));
            cir.setReturnValue(ActionResult.success(world.isClient));
        }
    }
}
