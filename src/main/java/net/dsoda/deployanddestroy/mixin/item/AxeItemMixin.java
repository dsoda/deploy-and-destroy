package net.dsoda.deployanddestroy.mixin.item;

import net.dsoda.deployanddestroy.util.ColumnStripperHelper;
import net.dsoda.deployanddestroy.util.DDOxidationHelper;
import net.minecraft.advancement.criterion.Criteria;
import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.block.Blocks;
import net.minecraft.block.PistonBlock;
import net.minecraft.block.enums.PistonType;
import net.minecraft.client.util.ParticleUtil;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.AxeItem;
import net.minecraft.item.ItemStack;
import net.minecraft.item.ItemUsageContext;
import net.minecraft.particle.ParticleTypes;
import net.minecraft.server.network.ServerPlayerEntity;
import net.minecraft.sound.SoundCategory;
import net.minecraft.sound.SoundEvent;
import net.minecraft.sound.SoundEvents;
import net.minecraft.state.property.Properties;
import net.minecraft.util.ActionResult;
import net.minecraft.util.Hand;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.intprovider.UniformIntProvider;
import net.minecraft.world.World;
import net.minecraft.world.event.GameEvent;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Unique;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;

import java.util.Optional;

@Mixin(AxeItem.class)
public abstract class AxeItemMixin {

    @Inject(method = "useOnBlock", at = @At(value = "TAIL"), cancellable = true)
    private void useOnBlockInject(ItemUsageContext context, CallbackInfoReturnable<ActionResult> cir) {
        boolean success = false;
        BlockPos usedOn = context.getBlockPos();
        World world = context.getWorld();
        BlockState state = world.getBlockState(usedOn);
        SoundEvent sound = SoundEvents.ITEM_AXE_SCRAPE;
        ItemStack itemStack = context.getStack();
        PlayerEntity player = context.getPlayer();
        Hand hand = context.getHand();
        Block block = state.getBlock();
        Optional<Block> unwaxed = Optional.ofNullable(DDOxidationHelper.WAXED_TO_UNWAXED.get(block));
        Optional<Block> unstripped = ColumnStripperHelper.getStrippedColumn(block);
        boolean notExtended = state.isOf(Blocks.STICKY_PISTON) && !state.get(Properties.EXTENDED);
        boolean extended = state.isOf(Blocks.PISTON_HEAD) && state.get(Properties.PISTON_TYPE).equals(PistonType.STICKY);
        if(notExtended || extended) {
            if (notExtended) deployanddestroy$replaceStickyPistonWithNormalAtPos(usedOn, world);
            else deployanddestroy$replaceExtendedStickyPistonWithNormalAtPos(usedOn, world);
            world.addBlockBreakParticles(usedOn, Blocks.PISTON.getDefaultState());
            sound = SoundEvents.ITEM_AXE_STRIP;
            success = true;
        }
        else if (unstripped.isPresent()) {
            BlockState newState = unstripped.get().getStateWithProperties(state);
            world.setBlockState(usedOn, newState, Block.NOTIFY_ALL | Block.REDRAW_ON_MAIN_THREAD);
            sound = SoundEvents.ITEM_AXE_STRIP;
            success = true;
        }
        else if (unwaxed.isPresent()) {
            BlockState newState = unwaxed.get().getStateWithProperties(state);
            world.setBlockState(usedOn, newState, Block.NOTIFY_ALL | Block.REDRAW_ON_MAIN_THREAD);
            ParticleUtil.spawnParticle(world, usedOn, ParticleTypes.WAX_OFF, UniformIntProvider.create(3, 5));
            success = true;
        }
        if (success) {
            if (player instanceof ServerPlayerEntity)
                Criteria.ITEM_USED_ON_BLOCK.trigger((ServerPlayerEntity) player, usedOn, itemStack);
            if (!player.getAbilities().creativeMode) itemStack.damage(1, player, p -> p.sendToolBreakStatus(hand));
            world.playSoundAtBlockCenter(usedOn, sound, SoundCategory.BLOCKS, 1.0f, 1.0f, false);
            world.emitGameEvent(GameEvent.BLOCK_CHANGE, usedOn, GameEvent.Emitter.of(player, state));
            cir.setReturnValue(ActionResult.success(world.isClient));
        }
    }

    @Unique
    private static void deployanddestroy$replaceStickyPistonWithNormalAtPos(BlockPos pos, World world) {
        BlockState state = world.getBlockState(pos);
        world.setBlockState(pos, Blocks.PISTON.getStateWithProperties(state));
    }

    @Unique
    private static void deployanddestroy$replaceExtendedStickyPistonWithNormalAtPos(BlockPos pos, World world) {
        BlockState state = world.getBlockState(pos);
        BlockPos connected = pos.offset(state.get(PistonBlock.FACING), -1);
        world.setBlockState(pos, Blocks.PISTON_HEAD.getStateWithProperties(state).with(Properties.PISTON_TYPE, PistonType.DEFAULT));
        deployanddestroy$replaceStickyPistonWithNormalAtPos(connected, world);
    }
}
