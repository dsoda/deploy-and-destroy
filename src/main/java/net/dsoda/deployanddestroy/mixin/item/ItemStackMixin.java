package net.dsoda.deployanddestroy.mixin.item;

import net.dsoda.deployanddestroy.util.DDMaxStackSizeOverrides;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;

@Mixin(ItemStack.class)
public abstract class ItemStackMixin {

    @Shadow public abstract Item getItem();

    @Inject(method = "getMaxCount", at = @At(value = "HEAD"), cancellable = true)
    private void overrideMaxStackSize(CallbackInfoReturnable<Integer> cir) {
        Item item = this.getItem();
        if(DDMaxStackSizeOverrides.OVERRIDES.containsKey(item)) cir.setReturnValue(DDMaxStackSizeOverrides.OVERRIDES.get(item));
    }
}
