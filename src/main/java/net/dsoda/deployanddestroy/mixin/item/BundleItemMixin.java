package net.dsoda.deployanddestroy.mixin.item;

import net.minecraft.item.BundleItem;
import net.minecraft.item.ItemStack;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;

@Mixin(BundleItem.class)
public class BundleItemMixin {

    @Inject(method = "getItemOccupancy", at = @At(value = "RETURN", ordinal = 2), cancellable = true)
    private static void itemOccupancyInject(ItemStack stack, CallbackInfoReturnable<Integer> cir) {
        cir.setReturnValue(1);
    }
}
