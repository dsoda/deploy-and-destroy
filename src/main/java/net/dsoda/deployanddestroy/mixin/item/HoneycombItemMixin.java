package net.dsoda.deployanddestroy.mixin.item;

import net.dsoda.deployanddestroy.util.DDOxidationHelper;
import net.minecraft.advancement.criterion.Criteria;
import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.HoneycombItem;
import net.minecraft.item.ItemStack;
import net.minecraft.item.ItemUsageContext;
import net.minecraft.server.network.ServerPlayerEntity;
import net.minecraft.util.ActionResult;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraft.world.WorldEvents;
import net.minecraft.world.event.GameEvent;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;

import java.util.Optional;

@Mixin(HoneycombItem.class)
public abstract class HoneycombItemMixin {

    @Inject(method = "useOnBlock", at = @At(value = "TAIL"), cancellable = true)
    private void useOnBlockInject(ItemUsageContext context, CallbackInfoReturnable<ActionResult> cir) {
        World world = context.getWorld();
        BlockPos pos = context.getBlockPos();
        BlockState state = world.getBlockState(pos);
        ItemStack stack = context.getStack();
        PlayerEntity player = context.getPlayer();
        Optional<Block> waxed = Optional.ofNullable(DDOxidationHelper.UNWAXED_TO_WAXED.get(state.getBlock()));
        if(waxed.isPresent()) {
            if (player instanceof ServerPlayerEntity)
                Criteria.ITEM_USED_ON_BLOCK.trigger((ServerPlayerEntity)player, pos, stack);
            BlockState newState = waxed.get().getStateWithProperties(state);
            if (!player.getAbilities().creativeMode) stack.decrement(1);
            world.setBlockState(pos, newState, Block.NOTIFY_ALL | Block.REDRAW_ON_MAIN_THREAD);
            world.emitGameEvent(GameEvent.BLOCK_CHANGE, pos, GameEvent.Emitter.of(player, newState));
            world.syncWorldEvent(player, WorldEvents.BLOCK_WAXED, pos, 0);
            cir.setReturnValue(ActionResult.success(world.isClient));
        }
    }
}
