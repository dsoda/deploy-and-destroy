package net.dsoda.deployanddestroy.mixin.block;

import net.dsoda.deployanddestroy.blocks.block.ColumnBlock;
import net.minecraft.block.BlockState;
import net.minecraft.block.FenceBlock;
import net.minecraft.util.math.Direction;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;

@Mixin(FenceBlock.class)
public abstract class FenceBlockMixin {

    @Inject(method = "canConnect", at = @At(value = "RETURN"), cancellable = true)
    private void connectToColumn(BlockState state, boolean neighborIsFullSquare, Direction dir, CallbackInfoReturnable<Boolean> cir) {
        cir.setReturnValue(cir.getReturnValue() || (state.getBlock() instanceof ColumnBlock));
    }
}
