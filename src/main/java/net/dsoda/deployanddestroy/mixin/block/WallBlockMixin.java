package net.dsoda.deployanddestroy.mixin.block;

import net.dsoda.deployanddestroy.blocks.block.ColumnBlock;
import net.minecraft.block.BlockState;
import net.minecraft.block.WallBlock;
import net.minecraft.util.math.Direction;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;

@Mixin(WallBlock.class)
public abstract class WallBlockMixin {

    @Inject(method = "shouldConnectTo", at = @At(value = "RETURN"), cancellable = true)
    private void connectToColumn(BlockState state, boolean faceFullSquare, Direction side, CallbackInfoReturnable<Boolean> cir) {
        cir.setReturnValue(cir.getReturnValue() || (state.getBlock() instanceof ColumnBlock));
    }
}
