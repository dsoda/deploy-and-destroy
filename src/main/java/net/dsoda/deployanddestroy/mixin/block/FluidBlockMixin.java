package net.dsoda.deployanddestroy.mixin.block;

import net.minecraft.block.*;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import org.spongepowered.asm.mixin.*;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;

@Mixin(FluidBlock.class)
public class FluidBlockMixin  {

    @Inject(method = "receiveNeighborFluids", at = @At(value = "INVOKE", target = "Lnet/minecraft/block/FluidBlock;playExtinguishSound(Lnet/minecraft/world/WorldAccess;Lnet/minecraft/util/math/BlockPos;)V", shift = At.Shift.BEFORE))
    private void generateDeepslate(World world, BlockPos pos, BlockState state, CallbackInfoReturnable<Boolean> cir) {
        if (pos.getY() < 0 && world.getBlockState(pos) != Blocks.OBSIDIAN.getDefaultState()) {
            world.setBlockState(pos, Blocks.COBBLED_DEEPSLATE.getDefaultState(), Block.NOTIFY_ALL);
        }
    }
}
