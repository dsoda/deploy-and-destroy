package net.dsoda.deployanddestroy.mixin.block;

import net.dsoda.deployanddestroy.blocks.dispenser.DDDispenserBehavior;
import net.minecraft.block.BlockState;
import net.minecraft.block.DispenserBlock;
import net.minecraft.block.dispenser.DispenserBehavior;
import net.minecraft.block.entity.DispenserBlockEntity;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.server.world.ServerWorld;
import net.minecraft.util.math.BlockPointer;
import net.minecraft.util.math.BlockPos;
import org.spongepowered.asm.mixin.Final;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;
import org.spongepowered.asm.mixin.injection.callback.LocalCapture;

import java.util.Map;

@Mixin(DispenserBlock.class)
public abstract class DispenserBlockMixin {

    @Shadow
    @Final
    private static Map<Item, DispenserBehavior> BEHAVIORS;

    @Inject(method = "dispense", at = @At(value = "INVOKE", target = "Lnet/minecraft/block/DispenserBlock;getBehaviorForItem(Lnet/minecraft/item/ItemStack;)Lnet/minecraft/block/dispenser/DispenserBehavior;"), locals = LocalCapture.CAPTURE_FAILHARD, cancellable = true)
    private void getBehaviorForItemInject(ServerWorld world, BlockState state, BlockPos pos, CallbackInfo cir, DispenserBlockEntity dispenserEntity, BlockPointer pointer, int slot, ItemStack stack) {
        DispenserBehavior newBehavior = DDDispenserBehavior.getAdditionalBehaviors(world, pos, pointer, dispenserEntity, stack, BEHAVIORS);
        if (newBehavior != null) {
            ItemStack resultStack = newBehavior.dispense(pointer, stack);
            dispenserEntity.setStack(slot, resultStack);
            cir.cancel();
        }
    }
}
