package net.dsoda.deployanddestroy.mixin.block;

import net.dsoda.deployanddestroy.blocks.DDBlocks;
import net.dsoda.deployanddestroy.blocks.block.AmethystFireBlock;
import net.dsoda.deployanddestroy.blocks.block.CopperFireBlock;
import net.minecraft.block.AbstractFireBlock;
import net.minecraft.block.BlockState;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.BlockView;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;

@Mixin(AbstractFireBlock.class)
public abstract class AbstractFireBlockMixin {

    @Inject(method = "getState", at = @At(value = "RETURN", ordinal = 1), cancellable = true)
    private static void getState(BlockView world, BlockPos pos, CallbackInfoReturnable<BlockState> cir) {
        BlockPos blockPos = pos.down();
        BlockState state = world.getBlockState(blockPos);
        if (CopperFireBlock.isCopperBase(state)) {
            cir.setReturnValue(DDBlocks.COPPER_FIRE.getDefaultState());
        }
        else if (AmethystFireBlock.isAmethystBase(state)) {
            cir.setReturnValue(DDBlocks.AMETHYST_FIRE.getDefaultState());
        }
    }
}
