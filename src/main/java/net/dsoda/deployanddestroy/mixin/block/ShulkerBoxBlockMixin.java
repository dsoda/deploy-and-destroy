package net.dsoda.deployanddestroy.mixin.block;

import net.dsoda.deployanddestroy.items.DDItems;
import net.dsoda.deployanddestroy.items.item.DyeBucketItem;
import net.minecraft.block.BlockState;
import net.minecraft.block.ShulkerBoxBlock;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ActionResult;
import net.minecraft.util.DyeColor;
import net.minecraft.util.Hand;
import net.minecraft.util.hit.BlockHitResult;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import org.jetbrains.annotations.Nullable;
import org.spongepowered.asm.mixin.Final;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;

@Mixin(ShulkerBoxBlock.class)
public abstract class ShulkerBoxBlockMixin {

    @Shadow @Final private @Nullable DyeColor color;

    @Inject(method = "onUse", at = @At(value = "INVOKE", target = "Lnet/minecraft/world/World;getBlockEntity(Lnet/minecraft/util/math/BlockPos;)Lnet/minecraft/block/entity/BlockEntity;", shift = At.Shift.BEFORE), cancellable = true)
    private void setDyeBucketColor(BlockState state, World world, BlockPos pos, PlayerEntity player, Hand hand, BlockHitResult hit, CallbackInfoReturnable<ActionResult> cir) {
        boolean success = true;
        ItemStack stack = player.getStackInHand(hand);
        if (stack.isOf(DDItems.DYE_BUCKET)) {
            if (this.color == DyeBucketItem.getColor(stack)) success = false;
            DyeBucketItem.setColor(player, stack, this.color);
            cir.setReturnValue(success ? ActionResult.SUCCESS : ActionResult.FAIL);
        }
    }
}
