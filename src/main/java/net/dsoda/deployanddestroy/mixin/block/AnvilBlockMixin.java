package net.dsoda.deployanddestroy.mixin.block;

import net.dsoda.deployanddestroy.util.CrackedBlockCraftingHelper;
import net.minecraft.block.AnvilBlock;
import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.entity.FallingBlockEntity;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Direction;
import net.minecraft.world.World;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

import java.util.Optional;

@Mixin(AnvilBlock.class)
public class AnvilBlockMixin {

    @Inject(method = "onLanding", at = @At(value = "INVOKE", target = "Lnet/minecraft/world/World;syncWorldEvent(ILnet/minecraft/util/math/BlockPos;I)V", shift = At.Shift.BEFORE))
    private void doBlockConversionOnLanding(World world, BlockPos pos, BlockState fallingBlockState, BlockState currentStateInPos, FallingBlockEntity fallingBlockEntity, CallbackInfo ci) {
        BlockPos belowPos = pos.offset(Direction.DOWN);
        Optional<Block> optional = CrackedBlockCraftingHelper.getCrackedBlock(world, belowPos);
        BlockState state1 = world.getBlockState(belowPos);
        if (optional.isPresent()) {
            BlockState state = optional.get().getStateWithProperties(state1);
            world.setBlockState(belowPos, state, Block.NOTIFY_ALL | Block.REDRAW_ON_MAIN_THREAD);
        }
    }
}
