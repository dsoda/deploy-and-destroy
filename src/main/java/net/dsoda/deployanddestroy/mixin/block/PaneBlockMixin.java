package net.dsoda.deployanddestroy.mixin.block;

import net.dsoda.deployanddestroy.blocks.block.ColumnBlock;
import net.minecraft.block.BlockState;
import net.minecraft.block.PaneBlock;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;

@Mixin(PaneBlock.class)
public abstract class PaneBlockMixin {

    @Inject(method="connectsTo", at = @At(value = "RETURN"), cancellable = true)
    private void connectToColumn(BlockState state, boolean sideSolidFullSquare, CallbackInfoReturnable<Boolean> cir) {
        cir.setReturnValue(cir.getReturnValue() || (state.getBlock() instanceof ColumnBlock));
    }
}
