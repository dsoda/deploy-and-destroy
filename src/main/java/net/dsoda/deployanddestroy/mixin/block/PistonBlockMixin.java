package net.dsoda.deployanddestroy.mixin.block;

import net.dsoda.deployanddestroy.util.tag.DDBlockTags;
import net.minecraft.block.BlockState;
import net.minecraft.block.PistonBlock;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Direction;
import net.minecraft.world.World;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;

@Mixin(PistonBlock.class)
public class PistonBlockMixin {

    @Inject(method = "isMovable", at = @At(value = "HEAD"), cancellable = true)
    private static void definePistonNonMovables(BlockState state, World world, BlockPos pos, Direction direction, boolean canBreak, Direction pistonDir, CallbackInfoReturnable<Boolean> cir) {
        if (state.isIn(DDBlockTags.PISTON_NON_MOVABLE)) {
            cir.setReturnValue(false);
        }
    }
}
