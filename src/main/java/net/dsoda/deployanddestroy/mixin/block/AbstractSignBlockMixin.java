package net.dsoda.deployanddestroy.mixin.block;

import com.llamalad7.mixinextras.injector.wrapoperation.Operation;
import com.llamalad7.mixinextras.injector.wrapoperation.WrapOperation;
import com.llamalad7.mixinextras.sugar.Local;
import com.llamalad7.mixinextras.sugar.ref.LocalRef;
import net.dsoda.deployanddestroy.items.DDItems;
import net.dsoda.deployanddestroy.stats.DDStats;
import net.minecraft.block.AbstractSignBlock;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Items;
import net.minecraft.util.Hand;
import net.minecraft.world.World;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;

@Mixin(AbstractSignBlock.class)
public abstract class AbstractSignBlockMixin {

    @WrapOperation(method = "onUse", at = @At(value = "INVOKE", target = "Lnet/minecraft/item/ItemStack;decrement(I)V"))
    private void handleDyeBucketUse(
        ItemStack instance, int amount, Operation<Integer> original,
        @Local(argsOnly = true) LocalRef<PlayerEntity> player,
        @Local(argsOnly = true) LocalRef<World> world,
        @Local(argsOnly = true) LocalRef<Hand> hand
    ) {
        if (instance.isOf(DDItems.DYE_BUCKET)) {
            if (instance.damage(1, world.get().random, null))
                player.get().setStackInHand(hand.get(), Items.BUCKET.getDefaultStack());
            player.get().incrementStat(DDStats.DYE_BUCKET_CONVERSIONS);
            original.call(instance, 0);
        }
        else original.call(instance, amount);
    }
}
