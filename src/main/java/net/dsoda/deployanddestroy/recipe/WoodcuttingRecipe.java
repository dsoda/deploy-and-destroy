package net.dsoda.deployanddestroy.recipe;

import com.mojang.serialization.Codec;
import com.mojang.serialization.codecs.RecordCodecBuilder;
import net.dsoda.deployanddestroy.blocks.DDBlocks;
import net.minecraft.inventory.Inventory;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.network.PacketByteBuf;
import net.minecraft.recipe.*;
import net.minecraft.registry.DynamicRegistryManager;
import net.minecraft.registry.Registries;
import net.minecraft.util.dynamic.Codecs;
import net.minecraft.world.World;

public class WoodcuttingRecipe extends CuttingRecipe implements Recipe<Inventory> {

    private final Ingredient ingredient;
    private final ItemStack result;
    private final String group;
    private final RecipeType<?> type;
    private final RecipeSerializer<?> serializer;

    public WoodcuttingRecipe(String group, Ingredient ingredient, ItemStack result) {
        super(DDRecipes.WOODCUTTING_TYPE, DDRecipes.WOODCUTTING_SERIALIZER, group, ingredient, result);
        this.ingredient = ingredient;
        this.result = result;
        this.serializer = DDRecipes.WOODCUTTING_SERIALIZER;
        this.type = DDRecipes.WOODCUTTING_TYPE;
        this.group = group;
    }

    public WoodcuttingRecipe(String group, Ingredient ingredient, Item result, int count) {
        this(group, ingredient, new ItemStack(result, count));
    }

    @Override
    public boolean matches(Inventory inventory, World world) {
        return this.ingredient.test(inventory.getStack(0));
    }

    @Override
    public ItemStack craft(Inventory inventory, DynamicRegistryManager registryManager) {
        return this.result.copy();
    }

    @Override
    public boolean fits(int width, int height) {
        return true;
    }

    @Override
    public ItemStack getResult(DynamicRegistryManager registryManager) {
        return this.result;
    }

    @Override
    public ItemStack createIcon() {
        return new ItemStack(DDBlocks.WOODCUTTER);
    }

    @Override
    public RecipeSerializer<?> getSerializer() {
        return this.serializer;
    }

    @Override
    public RecipeType<?> getType() {
        return this.type;
    }

    public static class Type implements RecipeType<WoodcuttingRecipe> {
        public static final Type INSTANCE = new Type();
    }

    public static class Serializer implements RecipeSerializer<WoodcuttingRecipe> {

        public static RecipeSerializer<WoodcuttingRecipe> WOODCUTTING_SERIALIZER = new Serializer(WoodcuttingRecipe::new);
        final RecipeFactory recipeFactory;
        private final Codec<WoodcuttingRecipe> codec;

        public Serializer(RecipeFactory recipeFactory) {
            this.recipeFactory = recipeFactory;
            this.codec = RecordCodecBuilder.create(instance -> instance
                .group(Codecs.createStrictOptionalFieldCodec(Codec.STRING, "group", "")
                    .forGetter(recipe -> recipe.group), (Ingredient.DISALLOW_EMPTY_CODEC.fieldOf("ingredient"))
                    .forGetter(recipe -> recipe.ingredient), (Registries.ITEM.getCodec().fieldOf("result"))
                    .forGetter(recipe -> recipe.result.getItem()), (Codec.INT.fieldOf("count"))
                    .forGetter(recipe -> recipe.result.getCount()))
                .apply(instance, recipeFactory::create));
        }

        @Override
        public Codec<WoodcuttingRecipe> codec() {
            return this.codec;
        }
        @Override
        public WoodcuttingRecipe read(PacketByteBuf packetByteBuf) {
            String string = packetByteBuf.readString();
            Ingredient ingredient = Ingredient.fromPacket(packetByteBuf);
            ItemStack itemStack = packetByteBuf.readItemStack();
            return this.recipeFactory.create(string, ingredient, itemStack.getItem(), itemStack.getCount());
        }

        @Override
        public void write(PacketByteBuf packetByteBuf, WoodcuttingRecipe woodcuttingRecipe) {
            packetByteBuf.writeString(woodcuttingRecipe.group);
            woodcuttingRecipe.ingredient.write(packetByteBuf);
            packetByteBuf.writeItemStack(woodcuttingRecipe.result);
        }

        public interface RecipeFactory {
            WoodcuttingRecipe create(String var1, Ingredient var2, Item var3, int var4);
        }
    }
}
