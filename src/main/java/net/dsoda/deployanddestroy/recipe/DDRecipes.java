package net.dsoda.deployanddestroy.recipe;

import net.dsoda.deployanddestroy.DeployAndDestroy;
import net.minecraft.recipe.RecipeSerializer;
import net.minecraft.recipe.RecipeType;
import net.minecraft.registry.Registries;
import net.minecraft.registry.Registry;
import net.minecraft.util.Identifier;

import static net.dsoda.deployanddestroy.DeployAndDestroy.MOD_ID;

public class DDRecipes {

    public static final RecipeSerializer<?> WOODCUTTING_SERIALIZER = Registry.register(Registries.RECIPE_SERIALIZER,
            new Identifier(DeployAndDestroy.MOD_ID, "woodcutting"), WoodcuttingRecipe.Serializer.WOODCUTTING_SERIALIZER);
    public static final RecipeType<WoodcuttingRecipe> WOODCUTTING_TYPE = Registry.register(Registries.RECIPE_TYPE,
            new Identifier(DeployAndDestroy.MOD_ID, "woodcutting"), WoodcuttingRecipe.Type.INSTANCE);

    public static void registerRecipes() {
        DeployAndDestroy.LOGGER.info("Registering Recipes For " + MOD_ID);
    }
}
