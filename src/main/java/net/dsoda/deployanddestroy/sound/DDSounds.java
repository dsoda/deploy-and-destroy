package net.dsoda.deployanddestroy.sound;

import net.dsoda.deployanddestroy.DeployAndDestroy;
import net.minecraft.registry.Registries;
import net.minecraft.registry.Registry;
import net.minecraft.sound.SoundEvent;
import net.minecraft.util.Identifier;

public class DDSounds {

    public static final SoundEvent VILLAGER_USES_WOODCUTTER = register("villager_uses_woodcutter");

    public static void registerSounds() {
        DeployAndDestroy.LOGGER.info("Registering Sounds For " + DeployAndDestroy.MOD_ID);
    }

    private static SoundEvent register(String name) {
        Identifier id = new Identifier(DeployAndDestroy.MOD_ID, name);
        return Registry.register(Registries.SOUND_EVENT, id, SoundEvent.of(id));
    }
}
